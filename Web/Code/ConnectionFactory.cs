﻿using DataContext.Context;
using InfluxData.Net.InfluxDb;
using KafkaBatLibrary;
using UsersDataModel;
using Microsoft.Extensions.Configuration;
using System;
using InfluxData.Net.Common.Enums;
using Microsoft.AspNetCore.Hosting;
using CognitoUsers;
using Microsoft.AspNetCore.Http;
using DataModel;
using System.Security.Claims;
using Veit.Cognito.Service;

namespace Web
{
   public interface IConnectionFactory
   {
      IKafkaSender KafkaSender { get; }

      IInfluxDbClient InfluxClient { get; }

      UserContext UserContext { get; }
      BatContext BatContext { get; }

      IHostingEnvironment HostingEnvironment { get; }
      IConfiguration Configuration { get; }

      CognitoSettings CognitoSettings { get; }

      CognitoUserManager CognitoUserManager { get; }
   }


   public class ConnectionFactory : IConnectionFactory
   {

      private readonly string influxUrl;

      private readonly BatModelContainer batModel;
      private readonly ClaimsPrincipal claimsPrincipal;
      private readonly UserModelContainer userModel;

      private BatContext batContext;
      private UserContext userContext;

      private CognitoUserManager cognitoManager;

      private IInfluxDbClient influxClient;      

      public ConnectionFactory(
         UserModelContainer userModel,
         BatModelContainer batModel,
         IKafkaSender kafkaSender,
         ClaimsPrincipal claimsPrincipal,
         IConfiguration configuration,
         IHostingEnvironment hostingEnvironment)
      {
         this.userModel = userModel;
         this.batModel = batModel;
         this.claimsPrincipal = claimsPrincipal;

         KafkaSender = kafkaSender;

         Configuration = configuration;
         HostingEnvironment = hostingEnvironment;

         CognitoSettings = configuration.GetSection("Cognito").Get<CognitoSettings>();
         CognitoOptions = configuration.GetSection("Cognito").Get<CognitoOptions>();
         influxUrl = configuration["Influx"];
      }

      public UserContext UserContext =>
         userContext ?? (userContext = new UserContext(userModel, claimsPrincipal));

      public BatContext BatContext =>
         batContext ?? (batContext = new BatContext(batModel, UserContext, InfluxClient, KafkaSender));

      public IKafkaSender KafkaSender { get; private set; }

      public IConfiguration Configuration { get; private set; }

      public IHostingEnvironment HostingEnvironment { get; private set; }

      public CognitoOptions CognitoOptions { get; private set; }

      public CognitoSettings CognitoSettings { get; private set; }

      public CognitoUserManager CognitoUserManager
         => cognitoManager ?? (cognitoManager = new CognitoUserManager(userModel, CognitoOptions));

      public IInfluxDbClient InfluxClient
         => influxClient ?? (influxClient = new InfluxDbClient(influxUrl, "name", "pass", InfluxDbVersion.v_1_0_0));
   }
}
