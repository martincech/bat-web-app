﻿using DataModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Data.SqlClient;
using System.Linq;
using UsersDataModel;

namespace Web
{
   public interface IDatabaseValidation
   {
      void ValidateUserDatabase();

      void ValidateCompanyDatabase(string batModelConnectionString);
   }

   public class DatabaseValidation : IDatabaseValidation
   {
      private readonly UserModelContainer userModel;
      private readonly ILogger<DatabaseValidation> logger;

      public DatabaseValidation(UserModelContainer userModel, ILogger<DatabaseValidation> logger)
      {
         this.userModel = userModel;
         this.logger = logger;
      }

      public void ValidateUserDatabase()
      {
         MigrateDatabase(userModel, "20180814084017_InitialCreate", "2.1.1-rtm-30846");
      }

      public void ValidateCompanyDatabase(string batModelConnectionString)
      {
         var databases = userModel.Companies.Select(s => s.Database).Where(db => db != null);
         foreach (var db in databases)
         {
            MigrateCompanyDatabases(batModelConnectionString, db);
         }
      }

      private void MigrateCompanyDatabases(string connectionString, string database)
      {
         using (BatModelContainer batModelContainer = new BatModelContainer(string.Format(connectionString, database)))
         {
            MigrateDatabase(batModelContainer, "20180731134616_InitialCreate", "2.1.1-rtm-30846");
         }
      }

      private void MigrateDatabase(DbContext context, string migrationId, string productVersion)
      {
         try
         {
            context.Database.Migrate();
         }
         catch (Exception e)
         {
            logger.LogDebug(e, "Failed to migrate {database}");
         }

         try
         {
            CheckMigrationTable(context, migrationId, productVersion);
         }
         catch (Exception e)
         {
            logger.LogDebug(e, "Failed to update __EFMigrationsHistory for {database}");
         }
      }

      private static void CheckMigrationTable(DbContext db, string migrationId, string productVersion)
      {
         using (var command = db.Database.GetDbConnection().CreateCommand())
         {
            command.CommandText = "SELECT * FROM [__EFMigrationsHistory]";
            db.Database.OpenConnection();
            using (var result = command.ExecuteReader())
            {
               if (!result.HasRows)
               {
                  db.Database.ExecuteSqlCommand(
                     new RawSqlString($"INSERT INTO [__EFMigrationsHistory] (MigrationId,ProductVersion) VALUES (@MigrationId,@ProductVersion)"),
                     new SqlParameter("@MigrationId", migrationId),
                     new SqlParameter("@ProductVersion", productVersion));
               }
            }
         }
      }
   }
}
