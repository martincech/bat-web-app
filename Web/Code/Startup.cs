﻿using Amazon;
using App.Metrics;
using DataModel;
using KafkaBatLibrary;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using NLog;
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using UsersDataModel;
using UsersDataModel.Mapping;
using Veit.Cognito.Service;

namespace Web
{
   public class Startup
   {
      private CognitoSettings cognitoSettings;

      private string BatModelConnectionString;
      private string UserModelConnectionString;

      private static readonly Logger LOG = LogManager.GetCurrentClassLogger();

      public Startup(IConfiguration configuration, IHostingEnvironment environment)
      {
         Configuration = configuration;
         Environment = environment;

         cognitoSettings = Configuration.GetSection("Cognito").Get<CognitoSettings>();

         BatModelConnectionString = Configuration.GetConnectionString("BatModelContainer");
         UserModelConnectionString = Configuration.GetConnectionString("UserModelContainer");
         LOG.Info($"WEB - ENVIROMENT : {Environment.EnvironmentName}");
      }

      public IConfiguration Configuration { get; }
      public IHostingEnvironment Environment { get; }

      // This method gets called by the runtime. Use this method to add services to the container.
      public void ConfigureServices(IServiceCollection services)
      {
         services.Configure<CookiePolicyOptions>(options =>
         {
            // This lambda determines whether user consent for non-essential cookies is needed for a given request.
            options.CheckConsentNeeded = context => true;
            options.MinimumSameSitePolicy = SameSiteMode.None;
         });

         services.AddMvc()
            .AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver())
            .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
            .AddMetrics();

         services.AddAuthentication(options =>
         {
            options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = "Cognito";
            options.DefaultSignOutScheme = CookieAuthenticationDefaults.AuthenticationScheme;
         })
         .AddCookie(
             op =>
             {
                op.Events.OnValidatePrincipal = async context =>
                   {
                      if (context == null) throw new System.ArgumentNullException(nameof(context));

                      var userId = context.Principal.Claims.FirstOrDefault(claim => claim.Type == ClaimTypes.NameIdentifier)?.Value;
                      var database = context.Principal.Claims.FirstOrDefault(claim => claim.Type == "Database")?.Value;
                      var role = context.Principal.Claims.FirstOrDefault(claim => claim.Type == ClaimTypes.Role)?.Value;
                      if (userId == null || database == null || role == null)
                      {
                         context.RejectPrincipal();
                         return;
                      }

                      var dbContext = context.HttpContext.RequestServices.GetRequiredService<UserModelContainer>();
                      var user = await dbContext.Users.FindAsync(Guid.Parse(userId));
                      if (user == null || user?.Company?.Database != database || !user.Roles.Any(a => a.Name == role))
                      {
                         context.RejectPrincipal();
                      }
                   };
             })
         .AddAWSCognito(
             "Cognito",
             "Cognito",
             options =>
             {
                options.ClientId = cognitoSettings.ClientId;
                options.ClientSecret = cognitoSettings.ClientSecret;

                options.CallbackPath = cognitoSettings.CallbackPath; // Your UserPool App Callback Url
                options.UserPoolAppDomainPrefix = cognitoSettings.Domain;
                options.AmazonRegionEndpoint = RegionEndpoint.GetBySystemName(cognitoSettings.Region); // AWS Region of your Cognito User Pool  


                // Add Your Scopes of Interest

                foreach (var scope in cognitoSettings.Scopes.Split(" ").Select(s => s.Trim()))
                {
                   options.Scope.Add(scope);
                }

                options.Events.OnCreatingTicket = context =>
                   {
                      var userId = context.Identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
                      var db = context.HttpContext.RequestServices.GetService<UserModelContainer>();
                      var user = db.Users.FirstOrDefault(f => f.Id.ToString() == userId) ?? throw new Exception("user do not exist");
                      var role = user.Roles.FirstOrDefault() ?? throw new Exception("user has not role");

                      //Save user settings into cookie
                      var settings = user.MapSettings();
                      if (settings.ContainsKey("lang"))
                      {
                         settings.Add(CookieRequestCultureProvider.DefaultCookieName, string.Format("c={0}|uic={0}", settings["lang"]));
                      }

                      foreach (var setting in settings)
                      {
                         if (context.HttpContext.Request.Cookies.ContainsKey(setting.Key))
                         {
                            context.HttpContext.Response.Cookies.Delete(setting.Key);
                         }
                         context.HttpContext.Response.Cookies.Append(setting.Key, setting.Value);
                      }

                      //Add role and database claim to user identity
                      context.Identity.AddClaim(new Claim(ClaimTypes.Role, role.Name));
                      context.Identity.AddClaim(new Claim("Database", user.Company.Database));
                      return Task.CompletedTask;
                   };
             });

         services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
         services.AddTransient<ClaimsPrincipal>(provider => provider.GetService<IHttpContextAccessor>().HttpContext.User);

         services.AddSingleton<IKafkaSender>(new KafkaSender(Configuration["Kafka"], Path.Combine(Environment.ContentRootPath, "Cache", "API.kafkaCache")));

         services.AddDbContext<UserModelContainer>(o => o.UseLazyLoadingProxies().UseSqlServer(UserModelConnectionString));
         services.AddDbContext<BatModelContainer>((provider, options) =>
         {
            var principal = provider.GetService<ClaimsPrincipal>();
            var database = principal.HasClaim(c => c.Type == "Database") ? principal.FindFirstValue("Database") : null;
            options.UseSqlServer(string.Format(BatModelConnectionString, database));
         });

         services.AddScoped<IConnectionFactory, ConnectionFactory>();
         services.AddTransient<IDatabaseValidation, DatabaseValidation>();
      }

      // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
      public void Configure(IApplicationBuilder app, IHostingEnvironment env, IDatabaseValidation dbValidator)
      {

         var supportedCultures = new[] { new CultureInfo("en-US"), new CultureInfo("de-DE") };
         app.UseRequestLocalization(new RequestLocalizationOptions
         {
            DefaultRequestCulture = new RequestCulture("en-US"),
            SupportedCultures = supportedCultures,
            SupportedUICultures = supportedCultures,
         });

         dbValidator.ValidateUserDatabase();
         dbValidator.ValidateCompanyDatabase(BatModelConnectionString);

         if (env.IsDevelopment())
         {
            app.UseDeveloperExceptionPage();
         }
         else
         {
            app.UseExceptionHandler("/Home/Error");
            app.UseHsts();
         }

         //app.UseHttpsRedirection();
         app.UseStaticFiles();
         app.UseAuthentication();
         app.UseCookiePolicy();

         app.UseMvc(routes =>
         {
            routes.MapRoute(
                   name: "default",
                   template: "{controller=Router}/{action=Index}/{id?}");
         });
      }
   }
}
