﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DataContext.DTOs;
using DataContext.DTOs.Enums;
using Microsoft.AspNetCore.Authorization;

namespace Web.Controllers
{
   [Authorize]
   public class BirdsController : BaseController
   {
      public BirdsController(IConnectionFactory connectionFactory) : base(connectionFactory)
      {
      }

      // GET: Chickens
      public ActionResult Index()
      {
         ViewData["Controller"] = "birdsCtrl";
         return View();
      }

      [HttpGet]
      public async Task<JsonResult> Data()
      {
         var curves = await DataContext.GetCurves();
         var birds = (await DataContext.GetBirds()).Reverse().ToList();

         var data = new
         {
            Birds = birds,
            Images = Images(birds.Select(s => s.Id.ToString()).ToList(), "Bird"),
            Curves = curves.Where(w => w.CurvePoints.Any()).Select(s => new { s.Name, s.Type, s.Id })
         };

         return Json(data);
      }

      [HttpPost]
      public async Task<JsonResult> Create([FromBody]Bird bird)
      {
         return Json(await DataContext.CreateBird(bird));
      }

      [HttpPost]
      public async Task<JsonResult> Update([FromBody]Bird config)
      {
         return Json(await DataContext.UpdateBird(config));
      }

      [HttpPost]
      public async Task Delete([FromBody]ICollection<int> birdIds)
      {
         await DataContext.DeleteBirds(birdIds);
      }
   }
}