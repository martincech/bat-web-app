﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using UsersDataModel.Mapping;

namespace Web.Controllers
{
   [Authorize]
   public class SettingsController : BaseController
   {
      public SettingsController(IConnectionFactory connectionFactory) : base(connectionFactory)
      {
      }

      // GET: Profile
      public ActionResult Index()
      {
         ViewData["Controller"] = "settingsCtrl";
         return View();
      }

      public JsonResult Resources(string regex)
      {
         var resources = new Dictionary<string, string>();
         if (string.IsNullOrEmpty(regex)) return Json(resources);
         try
         {
            var r = new Regex(regex);
            LoadResources(ref resources, r, CultureInfo.CurrentUICulture);
            LoadResources(ref resources, r, CultureInfo.InvariantCulture);
         }
         catch (Exception) { }
         return Json(resources);
      }

      private void LoadResources(ref Dictionary<string, string> resources, Regex regex, CultureInfo cultureInfo)
      {
         try
         {
            var resourceSet = Properties.Resources.ResourceManager.GetResourceSet(cultureInfo, true, true);
            foreach (DictionaryEntry entry in resourceSet)
            {
               var key = entry.Key.ToString();
               if (resources.ContainsKey(key)) continue;
               var value = entry.Value as string;
               if (!regex.IsMatch(key) && value != null) continue;
               resources.Add(key, value);
            }
         }
         catch (Exception) { }
      }

      public JsonResult GetEventLength()
      {
         var company = UserContext.GetCurrentUserCompany();
         return Json(new { EventLength = company.EventLength });
      }

      [HttpPost]
      [Authorize(Roles = "Veit, Owner, Admin")]
      public JsonResult SetEventLength([FromBody]int? eventLength)
      {
         UserContext.SetEventLength(eventLength);
         return Json("success");
      }

      [HttpGet]
      public async Task TrimNames()
      {
         var farms = await DataContext.GetFarms();
         var houses = await DataContext.GetHouses();
         foreach (var house in houses)
         {
            if (house.FarmId == null) continue;
            var farm = farms.FirstOrDefault(f => f.Id == house.FarmId);
            if (farm == null) continue;
            if (house.Name.StartsWith(farm.Name + " ")) await DataContext.ChangeName(house.Id, (house.Name.Replace(farm.Name + " ", "")));
         }
      }

      [HttpPost]
      public void Update()
      {
         UserContext.UpdateUserSettings(Request.Cookies.MapSetttings());
      }
   }
}