﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Web.Controllers.Utils;
using Web.Properties;
using DataContext.DTOs;
using DataContext.DTOs.Enums;
using KafkaBatLibrary;
using Microsoft.AspNetCore.Authorization;

namespace Web.Controllers
{
   [Authorize]
   public class HousesController : BaseController
   {
      public HousesController(IConnectionFactory connectionFactory) : base(connectionFactory)
      {
      }

      // GET: Halls
      public ActionResult Index()
      {
         ViewData["Controller"] = "housesCtrl";
         return View("~/Views/Sensors/Index.cshtml");
      }

      public ActionResult Setup()
      {
         ViewData["Controller"] = "housesSetupCtrl";
         return View("~/Views/Setup/Houses/Setup.cshtml");
      }

      [HttpGet]
      public async Task<JsonResult> Data()
      {
         var houses = (await DataContext.GetHouses()).ToList();
         var events = (await DataContext.GetDevicesEvent()).ToList();
         var results = ConvertOut.Convert(await DataContext.GetHousesLast(events, houses), DataContext.Events.ToList(), DataContext.Houses.ToList()).ToList();
         var weightStats = results.Where(g => g.Sensors.Any(st => st.IsWeightSensorString())).ToList();

         var data = new
         {
            Houses = houses,
            Events = events,
            WeightStats = weightStats.GetUniqueLastValues(),
            Stats = results.Where(g => g.Sensors.Any(st => !st.IsWeightSensorString())),
            Farms = await DataContext.GetFarms()
         };
         return Json(data);
      }


      public ActionResult Detail()
      {
         ViewData["Controller"] = "housesDetailCtrl";
         return View("~/Views/Sensors/Detail/Index.cshtml");
      }

      [HttpGet]
      public async Task<JsonResult> DetailData(int? houseId)
      {
         if (houseId == null) return null;
         var house = (await DataContext.GetHouses()).FirstOrDefault(f => f.Id == houseId);
         if (house == null) return null;
         var farm = house.FarmId != null
            ? (await DataContext.GetFarms()).FirstOrDefault(f => f.Id == house.FarmId)
            : null;
         var devices = (await DataContext.GetDevices()).Where(w => w.HouseId == null || w.HouseId == house.Id).ToList();
         var ev = (await DataContext.GetDevicesEvent()).FirstOrDefault(f => f.Devices.Intersect(house.Devices).Any());
         if (ev == null) return Json(new { House = house, Devices = devices, Farm = farm });
         var timeSizes = (await DataContext.GetTimeSizes()).ToList();
         var results = await GetLastAndConvert(StatisticGroup.HOUSE.ToLowerName(), house.Id.ToString());


         var dr = await GetLastAndConvert(StatisticGroup.DEVICE.ToLowerName(), devices.Where(w => w.HouseId == house.Id).Select(s => s.Uid));

         var weightStats = dr.Where(g => g.Sensors.Any(st => st.IsWeightSensorString())).ToList();
         var stats = dr.Where(g => g.Sensors.Any(st => !st.IsWeightSensorString()));

         var data = new
         {
            TimeSizes = timeSizes,
            Curves = ConvertOut.Convert(await DataContext.GetCurvesByEvent(ev.Id)),
            House = house,
            Devices = devices,
            Event = ev,
            Last = results,
            WeightStats = weightStats,
            Stats = stats,
            Farm = farm
         };
         return Json(data);
      }

      [HttpGet]
      public async Task<JsonResult> SetupData()
      {
         var houses = await DataContext.GetHouses();
         var events = await DataContext.GetAllEvents();
         var data = new
         {
            Events = events.Where(w => w.To > DateTime.Now),
            Images = Images(houses.Select(s => s.Id.ToString()).ToList(), "House")
         };
         return Json(data);
      }

      [HttpPost]
      public async Task<JsonResult> Create([FromBody]string name)
      {
         return Json(await DataContext.CreateHouse(new House { Name = name }));
      }

      [HttpPost]
      public async Task<JsonResult> Update([FromBody]House house)
      {
         return Json(await DataContext.UpdateHouse(house));
      }

      [HttpPost]
      public async Task<JsonResult> UpdateDevice(int houseId, int deviceId, bool add)
      {
         return Json(await DataContext.UpdateDevicesInHouse(houseId, deviceId, add));
      }

      [HttpPost]
      public async Task Delete([FromBody]List<int> houseIds)
      {
         await DataContext.DeleteHouses(houseIds);
      }

      [HttpPost]
      public async Task<JsonResult> ChangeName(int houseId, [FromBody]string name)
      {
         await DataContext.ChangeName(houseId, name);
         return Json("success");
      }

      [HttpPost]
      public async Task<JsonResult> CreateEvent(int houseId)
      {
         return Json(await DataContext.StartHouseEvent(houseId, Resources.event_new));
      }
   }
}