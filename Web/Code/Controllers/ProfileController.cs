﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Web.Properties;
using UsersDataModel.DTO;
using Microsoft.AspNetCore.Authorization;
using DataContext.DTOs;

namespace Web.Controllers
{
   [Authorize]
   public class ProfileController : BaseController
   {
      public ProfileController(IConnectionFactory connectionFactory) : base(connectionFactory)
      {
      }

      // GET: Profile
      public ActionResult Index()
      {
         ViewData["Controller"] = "profileCtrl";
         return View();
      }

      [HttpGet]
      public JsonResult Data()
      {
         return Json(new
         {
            Company = UserContext.GetCurrentUserCompany()
         });
      }

      [HttpPost]
      public JsonResult Update([FromBody]CompanyDto company)
      {
         return Json(UserContext.UpdateCompany(company));
      }

      [HttpPost]
      public async Task<JsonResult> AddTerminal([FromBody]Terminal terminal)
      {
         try
         {
            var result = await DataContext.CreateTerminalAndRegisterInUserCompany(terminal.Uid, terminal.Name);
            if (result.Item1 == null)
            {
               Response.StatusCode = (int)HttpStatusCode.BadRequest;
               return Json(Resources.profile_terminal_already_registered);
            }
            return Json(result.Item1);
         }
         catch (Exception e)
         {
            LOG.Error(e, "TerminalUID:{0} TerminalName:{1}", terminal.Uid, terminal.Name);
            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return Json(Resources.profile_terminal_already_registered);
         }
      }

      [HttpPost]
      public async Task<JsonResult> DeleteTerminal(string uid)
      {
         try
         {
            await DataContext.DeleteTerminal(uid);
            return Json(Resources.m_success);
         }
         catch (Exception)
         {
            return Json(Resources.profile_cannot_delete_terminal);
         }
      }

      [HttpPost]
      public async Task<JsonResult> DeleteDevice(string uid)
      {
         if (string.IsNullOrEmpty(uid)) return null;
         await DataContext.DeleteDeviceByUid(uid);
         return Json(Resources.m_success);
      }
   }
}