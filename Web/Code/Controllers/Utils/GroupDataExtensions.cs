﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataContext.DTOs;
using DataContext.DTOs.Enums;

namespace Web.Controllers.Utils
{
   public static class GroupDataExtensions
   {
      /// <summary>
      /// Merge values within group for same type of weight sensor.
      /// </summary>
      /// <param name="values">Data to merge</param>
      /// <returns>Merged data wihout duplicated sensor types</returns>
      public static ICollection<GroupData> MergeSensorValues(this ICollection<GroupData> values)
      {
         if (values == null || !values.Any()) return values;
         foreach (var data in values)
         {
            try
            {
               if (data == null || data.Sensors == null || data.Sensors.Count < 2) continue;
               var nonunique = data.Sensors.GroupBy(g => g).Where(w => w.Count() > 1).Select(s => s.Key).ToList();
               if (!nonunique.Any()) continue;
               var timeIndex = GetTimeIndex(data);
               foreach (var non in nonunique)
               {
                  var lastIndex = data.Sensors.LastIndexOf(non);
                  var firstIndex = data.Sensors.IndexOf(non);
                  if (data.Values.Count <= lastIndex || lastIndex == firstIndex) continue;
                  data.Values[firstIndex].AddRange(data.Values[lastIndex]);
                  data.Values.RemoveAt(lastIndex);
                  data.Values[firstIndex] = data.Values[firstIndex].OrderBy(o => o[timeIndex]).ToList();
                  data.Sensors.RemoveAt(lastIndex);
               }
            }
            catch (Exception) { }
         }
         return values;
      }

      /// <summary>
      /// Find last statistic data and remove duplicates.  
      /// </summary>
      /// <param name="lastValues">List of last statistic values</param>
      /// <returns>List of last statistic values without duplicates</returns>
      public static ICollection<GroupData> GetUniqueLastValues(this ICollection<GroupData> lastValues)
      {
         if (lastValues == null || !lastValues.Any()) return lastValues;
         var uniqueGroupData = new Dictionary<string, GroupData>();
         foreach (var last in lastValues)
         {
            foreach (var sensor in last.Sensors)
            {
               try
               {
                  var sensorIndex = last.Sensors.IndexOf(sensor);
                  if (!HasValidData(last, sensorIndex)) continue;
                  var key = last.Group + "|" + last.GroupId + "|" + sensor + "|" + last.TimeSize;
                  RemoveNonLastValues(last);
                  if (uniqueGroupData.ContainsKey(key))
                  {
                     SetLastValues(uniqueGroupData[key], last, sensorIndex);
                  }
                  else
                  {
                     uniqueGroupData.Add(key, last);
                  }
               }
               catch (Exception) { }
            }
         }
         return uniqueGroupData.Select(s => s.Value).ToList();
      }

      private static void SetLastValues(GroupData original, GroupData update, int sernsorIndex)
      {
         var originalDate = GetDate(original, sernsorIndex);
         var updateDate = GetDate(update, sernsorIndex);
         if (originalDate == null || updateDate == null) return;
         if (updateDate > originalDate) original.Values[sernsorIndex][0] = update.Values[sernsorIndex][0];
      }

      private static void RemoveNonLastValues(GroupData data)
      {
         var timeIndex = GetTimeIndex(data);
         for (int i = 0; i < data.Sensors.Count; i++)
         {
            if (data.Values[i].Count <= 1) continue;
            data.Values[i] = data.Values[i].OrderByDescending(o => o[timeIndex]).Take(1).ToList();
         }
      }

      private static int GetTimeIndex(GroupData data)
      {
         var timeIndex = data.Types.IndexOf(AggregationType.TIMESTAMP.ToLowerName());
         if (timeIndex == -1) timeIndex = data.Types.IndexOf("timestamp");
         return timeIndex == -1 ? 1 : timeIndex;
      }

      private static DateTime? GetDate(GroupData data, int sernsorIndex)
      {
         var timeIndex = data.Types.IndexOf(AggregationType.TIMESTAMP.ToLowerName());
         if (timeIndex == -1) return null;
         return data.Values[sernsorIndex][0][timeIndex] as DateTime?;
      }

      private static bool HasValidData(GroupData data, int sensorIndex)
      {
         if (data == null || data.Types == null || data.Sensors == null || !data.Sensors.Any()
            || data.Values == null || !data.Values.Any()
            || data.Values[sensorIndex] == null || !data.Values[sensorIndex].Any()
            || data.Values[sensorIndex][0] == null || !data.Values[sensorIndex][0].Any()) return false;
         return true;
      }
   }
}