﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using System.Linq;


namespace Web.Controllers.Utils
{
   public static class ActiveLinkUtil
   {
      public static string IsActive(this ViewContext viewContext, string controllers = "", string actions = "", string cssClass = "active")
      {
         var routeValues = viewContext.RouteData.Values;
         var currentAction = routeValues["action"].ToString().ToLower();
         var currentController = routeValues["controller"].ToString().ToLower();

         if (String.IsNullOrEmpty(actions))
            actions = currentAction;

         if (String.IsNullOrEmpty(controllers))
            controllers = currentController;

         var acceptedActions = actions.ToLower().Trim().Split(',').Distinct().ToArray();
         var acceptedControllers = controllers.ToLower().Trim().Split(',').Distinct().ToArray();

         return acceptedActions.Contains(currentAction) && acceptedControllers.Contains(currentController) ?
             cssClass : String.Empty;
      }
   }
}