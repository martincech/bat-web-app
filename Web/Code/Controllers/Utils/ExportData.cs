using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Web;
using Web.Properties;
using DataContext.DTOs;
using DataContext.DTOs.Enums;
using Microsoft.AspNetCore.Http;

namespace Web.Controllers.Utils
{
   public class ExportData
   {
      public const string NEW_LINE = "\n";
      public const string DELIMENTER_DEF = "sep=;\n";

      private static readonly string[][] SENSORS =
      {
         new [] {SensorType.WEIGHT.ToLowerName() + "_male", Resources.weight + " " + Resources.male},
         new [] {SensorType.WEIGHT.ToLowerName() + "_female",Resources.weight + " " + Resources.female},
         new [] {SensorType.WEIGHT.ToLowerName() + "_undefined",Resources.weight + " " + Resources.config_undefined},
         new [] {SensorType.TEMPERATURE.ToLowerName(), Resources.temperature},
         new [] {SensorType.HUMIDITY.ToLowerName(),Resources.humidity},
         new [] {SensorType.CO2.ToLowerName(),Resources.co2}
      };

      private static readonly Dictionary<string, string> UNITS = new Dictionary<string, string>
      {
         {"g"," [g]"},
         {"kg"," [kg]"},
         {"lb"," [lb]"},
         {"c",@" [�C]"},
         {"f",@" [�F]"},
         {"k"," [K]"},
         {"%"," [%]"},
         {"-"," [-]"},
         {"ppm"," [ppm]"},
      }; 

      public static string GrupsDataToCsv(List<GroupData> groupsData, List<Event> events, HttpRequest request)
      {

         var lang = "en-us";
         if (request.Cookies.ContainsKey("lang")) lang = request.Cookies["lang"];
         var decimals = 0;
         if (request.Cookies.ContainsKey("decimals")) int.TryParse(request.Cookies["decimals"], out decimals);
         var wUnit = "g";
         if (request.Cookies.ContainsKey("weightUnit")) wUnit = request.Cookies["weightUnit"];
         var tUnit = "c";
         if (request.Cookies.ContainsKey("temperatureUnit")) tUnit = request.Cookies["temperatureUnit"];
         var numberFormat = lang.StartsWith("de") ? "," : ".";
         if (request.Cookies.ContainsKey("numberFormat")) numberFormat = Uri.UnescapeDataString(request.Cookies["numberFormat"]);
         var dateFormat = lang.StartsWith("de") ? "dd.MM.yyyy HH:mm:ss" : "MM/dd/yyyy hh:mm:ss a";
         if (request.Cookies.ContainsKey("dateFormat")) dateFormat = Uri.UnescapeDataString(request.Cookies["dateFormat"]);
         dateFormat = dateFormat.Replace("a", "tt");

         NumberFormatInfo nfi = new NumberFormatInfo {NumberDecimalSeparator = numberFormat};

         StringBuilder sb = new StringBuilder();
         Dictionary<int, List<EventColumn>> rows = new Dictionary<int, List<EventColumn>>();

         foreach (var groupData in groupsData)
         {
            var timeStamp = groupData.Types.IndexOf(AggregationType.TIMESTAMP.ToLowerName());
            if (timeStamp == -1) timeStamp = groupData.Types.IndexOf("time");
            var day = groupData.Types.IndexOf(AggregationType.DAY.ToLowerName());
            var average = groupData.Types.IndexOf(AggregationType.AVERAGE.ToLowerName());
            var min = groupData.Types.IndexOf(AggregationType.MIN.ToLowerName());
            var max = groupData.Types.IndexOf(AggregationType.MAX.ToLowerName());
            var count = groupData.Types.IndexOf(AggregationType.COUNT.ToLowerName());
            var sigma = groupData.Types.IndexOf(AggregationType.SIGMA.ToLowerName());
            var cv = groupData.Types.IndexOf(AggregationType.CV.ToLowerName());
            var uniformity = groupData.Types.IndexOf(AggregationType.UNIFORMITY.ToLowerName());
            var gain = groupData.Types.IndexOf(AggregationType.GAIN.ToLowerName());
            var ev = events.FirstOrDefault(f => f.Id.ToString() == groupData.GroupId);
            List<string> sens;
            var ticks = GetEventTicks(groupData, timeStamp, out sens).OrderBy(o=>o).ToList();

            for (int i = 0; i < ticks.Count; i++)
            {
               List<EventColumn> row;
               if (rows.ContainsKey(i))
               {
                  row = rows[i];
               }
               else
               {
                  row = new List<EventColumn>();
                  rows.Add(i, row);
               }
               foreach (var sensor in SENSORS)
               {
                  var eventColumn = row.FirstOrDefault(f => f.Column == groupsData.IndexOf(groupData));
                  if (eventColumn == null)
                  {
                     eventColumn = new EventColumn { Column = groupsData.IndexOf(groupData), Values = new List<object>(), EventId = -1 };
                     if (ev != null) eventColumn.EventId = ev.Id;
                     row.Add(eventColumn);
                  }

                  var index = groupData.Sensors.IndexOf(sensor[0]);
                  if (index == -1) continue;
                  var listValues = groupData.Values[index];
                  var tick = ticks.ElementAt(i);
                  var rowValues = listValues.FirstOrDefault(f => ((DateTime)f[timeStamp]).Ticks == tick) ?? new List<object>();
                  
                  if (rowValues.Count == 0) rowValues = Enumerable.Range(0, groupData.Types.Count).Select(s => (object)"").ToList();

                  if (eventColumn.Values.Count == 0)
                  {
                     eventColumn.Values.Add(new DateTime(tick));
                     eventColumn.Values.Add(rowValues[day]);
                     if (i == 0)
                     {
                        eventColumn.Headers = new List<string> { Resources.date , Resources.day };
                     }
                  }

               
                
                  if (sensor[0].StartsWith(SensorType.WEIGHT.ToLowerName()))
                  {
                     eventColumn.Values.Add(Round(rowValues[average], decimals));
                     eventColumn.Values.Add(Round(rowValues[gain], decimals));
                     eventColumn.Values.Add(rowValues[count]);
                     eventColumn.Values.Add(Round(rowValues[cv], 1));
                     eventColumn.Values.Add(Round(rowValues[sigma], 1));
                     eventColumn.Values.Add(Round(rowValues[uniformity], 1));
                     if (i == 0)
                     {
                        eventColumn.Headers.Add(Resources.average + UNITS[wUnit]);
                        eventColumn.Headers.Add(Resources.table_gain + UNITS[wUnit]);
                        eventColumn.Headers.Add(Resources.count + UNITS["-"]);
                        eventColumn.Headers.Add(Resources.cv + UNITS["%"]);
                        eventColumn.Headers.Add(Resources.sigma + UNITS["-"]);
                        eventColumn.Headers.Add(Resources.uniformity + UNITS["%"]);
                     }
                  }
                  else
                  {
                     eventColumn.Values.Add(Round(rowValues[average], 1));
                     if (i == 0)
                     {
                        var unit = "-";
                        if (sensor[0] == SensorType.TEMPERATURE.ToLowerName()) unit = tUnit;
                        if (sensor[0] == SensorType.HUMIDITY.ToLowerName()) unit = "%";
                        if (sensor[0] == SensorType.CO2.ToLowerName()) unit = "ppm";
                        eventColumn.Headers.Add(sensor[1] + UNITS[unit]);
                     }
                  }
               }
            }
         }

         if (rows.Count == 0) return "";
         var info = "";
         foreach (var col in rows.First().Value)
         {
            var columnInfo = string.Join(";", Enumerable.Range(0, col.Headers.Count).Select(s => ""));
            var ev = events.FirstOrDefault(f => f.Id == col.EventId);
            if (ev != null)
            {
               columnInfo = ev.Name + columnInfo;
               columnInfo = columnInfo.Insert(columnInfo.IndexOf(";;", StringComparison.InvariantCulture) + 2, ev.From.ToString(dateFormat));
               columnInfo = columnInfo.Insert(columnInfo.IndexOf(";;;", StringComparison.InvariantCulture) + 2, ev.To.ToString(dateFormat));
            }
            info += columnInfo + ";;;";
         }
         info += NEW_LINE;
         sb.Append(info);
         var header = string.Join(";;;", rows[0].Select(s => string.Join(";", s.Headers))) + ";;;" + NEW_LINE;
         sb.Append(header);
     
         foreach (var row in rows)
         {
            var rowCsv = "";
            for (var i = 0; i <= row.Value.Max(s => s.Column); i++)
            {
               var column = row.Value.FirstOrDefault(f => f.Column == i);
               if (column == null)
               {
                  var firsRow = rows.First().Value.FirstOrDefault(f => f.Column == i);
                  if (firsRow == null) continue;
                  column = new EventColumn() { Values = Enumerable.Range(0, firsRow.Headers.Count).Select(s => (object)"").ToList() };
               }
               rowCsv += (column.Values[0] is DateTime) ? ((DateTime)column.Values[0]).ToString(dateFormat) : column.Values[0].ToString();
               rowCsv += ";" + String.Join(";", column.Values.Skip(1).Select(s => Parse(s, nfi))) + ";;;";
            }
            sb.Append(rowCsv + NEW_LINE);
         }
         return DELIMENTER_DEF + sb;
      }

      private static string Parse(object value, NumberFormatInfo nfi)
      {
         try
         {
            if (value == null) return "";
            if (value is double) return ((double)value).ToString(nfi);
            return value.ToString();
         }
         catch (Exception)
         {
            if (value == null) return "";
            return value.ToString();
         }
      }

      private static object Round(object value, int decimals)
      {
         try
         {
            if(value is double) return Math.Round((double)value, decimals);
            return value;
         }
         catch (Exception)
         {
            return null;
         }
      }

      private static HashSet<long> GetEventTicks(GroupData groupData, int timeIndex, out List<string> sensors)
      {
         HashSet<long> ticks = new HashSet<long>();
         sensors = new List<string>();
         for (int i = 0; i < groupData.Sensors.Count; i++)
         {
            var values = groupData.Values[i];
            if (values.Count == 0) continue;
            sensors.Add(groupData.Sensors[i]);
            foreach (var value in values)
            {
               ticks.Add(((DateTime)value[timeIndex]).Ticks);
            }
         }
         return ticks;
      }


      public class EventColumn
      {
         public EventColumn()
         {
            Values = new List<object>();
         }

         public int Column { get; set; }
         public List<string> Headers { get; set; }
         public int EventId { get; set; }
         public List<object> Values { get; set; }
      }

   }
}