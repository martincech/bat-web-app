﻿using System;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace Web.Controllers.Utils
{
   public static class EmailUtil
   {
      private const string FROM = "bat@veit.cz";
      private const string SMTP = "smtp.office365.com";
      private const string PASSWORD = "Modricka_52";

      public static bool SendEmail(string to, string subject, string body, string replyTo = null)
      {
         try
         {
            using (var client = new SmtpClient
            {
               Port = 587,
               Host = SMTP,
               EnableSsl = true,
               Timeout = 10000,
               DeliveryMethod = SmtpDeliveryMethod.Network,
               UseDefaultCredentials = false,
               Credentials = new NetworkCredential(FROM, PASSWORD)
            })
            {
               var mm = new MailMessage(FROM, to, subject, body)
               {
                  BodyEncoding = Encoding.UTF8,
                  IsBodyHtml = true,
                  DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
               };
               if (replyTo != null) mm.ReplyToList.Add(replyTo);
               client.Send(mm);
            }
         }
         catch (Exception)
         {
            return false;
         }

         return true;
      }
   }
}