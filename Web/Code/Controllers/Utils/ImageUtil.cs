﻿using System;
using System.IO;
using System.Linq;

namespace Web.Controllers.Utils
{
   public class ImageUtil
   {
      public static bool Base64ToImage(string file, string base64String)
      {
         try
         {
            var imagebase = base64String.Split(',').LastOrDefault();
            var bytes = Convert.FromBase64String(imagebase);
            using (var imageFile = new FileStream(file, FileMode.Create))
            {
               imageFile.Write(bytes, 0, bytes.Length);
               imageFile.Flush();
            }
         }
         catch (Exception)
         {
            return false;
         }
         return true;
      }
   }
}