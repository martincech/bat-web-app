﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Web.Controllers.Utils;
using DataContext.DTOs;
using DataContext.DTOs.Enums;
using Microsoft.AspNetCore.Authorization;
using System.Text;
using System.IO;

namespace Web.Controllers
{
   [Authorize]
   public class CalendarController : BaseController
   {
      public CalendarController(IConnectionFactory connectionFactory) : base(connectionFactory)
      {
      }

      // GET: Calendar
      public ActionResult Index()
      {
         ViewData["Controller"] = "calendarCtrl";
         return View();
      }

      public async Task<JsonResult> Data()
      {
         var birds = await DataContext.GetBirds();
         var houses = await DataContext.GetHouses();
         var farms = await DataContext.GetFarms();

         var data = new
         {
            Events = ConvertOut.Convert(await DataContext.GetAllEvents()),
            Curves = ConvertOut.Convert(await DataContext.GetCurvesName()),
            Birds = birds.Where(IsValidBird),
            Terminals = await DataContext.GetTerminals(),
            Devices = await DataContext.GetDevices(),
            Houses = houses.Where(w => w.Devices.Any()),
            Farms = farms.Where(w => w.Houses.Any()),
            CurrentUser?.Company?.EventLength
         };

         return Json(data);
      }

      private static bool IsValidBird(Bird bird)
      {
         if (bird == null) return false;
         if (bird.SexMode == SexMode.FEMALE && bird.FemaleCurveId == null) return false;
         if (bird.SexMode == SexMode.MALE && bird.MaleCurveId == null) return false;
         if (bird.SexMode == SexMode.UNDEFINED && bird.UndefinedCurveId == null) return false;
         if (bird.SexMode == SexMode.MIXED && (bird.FemaleCurveId == null || bird.MaleCurveId == null)) return false;
         return true;
      }

      [HttpPost]
      public async Task<JsonResult> Create([FromBody]Event ev)
      {
         if (ev == null) return null;
         return Json(ConvertOut.Convert(await DataContext.CreateEvent(ConvertIn.Convert(ev))));
      }

      [HttpPost]
      public async Task<JsonResult> Update([FromBody]Event ev, bool? basicOnly = false)
      {
         return Json(ConvertOut.Convert(await DataContext.UpdateEvent(ev.Id, ConvertIn.Convert(ev), basicOnly ?? false)));
      }

      [HttpPost]
      public async Task Delete([FromBody]ICollection<int> eventIds)
      {
         if (eventIds == null) return;
         await DataContext.DeleteEvents(eventIds);
      }


      [HttpGet]
      public async Task<IActionResult> Export(ICollection<int> eventIds, int timeSize)
      {
         if (eventIds == null || eventIds.Count == 0 || timeSize == 0) return null;
         var gf = new GroupFilter
         {
            Group = StatisticGroup.EVENT.ToLowerName(),
            GroupIds = eventIds.Select(s => s.ToString()).ToList(),
            TimeSizes = new List<int> { timeSize },
            Sensors = SensorTypeExtension.SensorLowerNames().ToList(),
            Types = AggregationTypeExtensions.AggregationLowerNames().ToList()
         };

         var result = await QueryAndConvertData(gf);
         var csv = ExportData.GrupsDataToCsv(result.ToList(), (await DataContext.GetAllEvents()).ToList(), Request);
         return File(new MemoryStream(Encoding.UTF8.GetBytes(csv)), "text/csv", "data.csv");
      }
   }
}