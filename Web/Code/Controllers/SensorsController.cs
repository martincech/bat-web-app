﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Web.Controllers.Utils;
using Web.Properties;
using DataContext.DTOs.Enums;
using Microsoft.AspNetCore.Authorization;
using System.Diagnostics;
using System;

namespace Web.Controllers
{
   [Authorize]
   public class SensorsController : BaseController
   {
      public SensorsController(IConnectionFactory connectionFactory) : base(connectionFactory)
      {
      }

      // GET: Sensors
      public ActionResult Index()
      {
         ViewData["Controller"] = "sensorsCtrl";
         return View("~/Views/Sensors/Index.cshtml");
      }


      // GET: Sensors
      public ActionResult Detail()
      {
         ViewData["Controller"] = "sensorsDetailCtrl";
         return View("~/Views/Sensors/Detail/Index.cshtml");
      }

      [HttpGet]
      public async Task<JsonResult> Names()
      {
         DataContext.Container.ChangeTracker.LazyLoadingEnabled = false;
         var Farms = await DataContext.GetFarms();
         var Houses = await DataContext.GetHouses();
         var Terminals = await DataContext.GetTerminals();
         var Devices = await DataContext.GetDevices();
         var ShowDemoDataDialog = false;
         DataContext.Container.ChangeTracker.LazyLoadingEnabled = true;

         if (!(Farms.Any() || Houses.Any() || Terminals.Any() || Devices.Any()) && !(CurrentUser.Company?.DisableDemoData ?? true))
         {
            ShowDemoDataDialog = true;
         }

         var result = Json(new
         {
            Farms = Farms.Select(s => Map(s, (x => x.Houses = Houses.Where(w => w.FarmId == s.Id).Select(v => v.Id).ToList()))),
            Houses = Houses.Select(s => Map(s, (x => x.Devices = Devices.Where(w => w.HouseId == s.Id).Select(v => v.Id).ToList()))),
            Terminals = Terminals.Select(s => Map(s, (x => x.Devices = Devices.Where(w => w.TerminalId == s.Id).Select(v => v.Id).ToList()))),
            Devices,
            ShowDemoDataDialog
         });
         return result;
      }

      private static T Map<T>(T val, Action<T> action)
      {
         action?.Invoke(val);
         return val;
      }

      [HttpGet]
      public async Task<JsonResult> DetailData(int? deviceId)
      {
         if (deviceId == null) return null;
         var device = await DataContext.GetDeviceById(deviceId.Value);
         if (device == null) return null;
         var ev = await DataContext.GetActiveDeviceEvent(deviceId.Value);

         var house = device.HouseId != null ? await DataContext.GetHouse(device.HouseId.Value) : null;
         var farm = house?.FarmId != null ? (await DataContext.GetFarm(house.FarmId.Value)) : null;
         if (ev == null) return Json(new { Device = device, House = house, Farm = farm });
         var timeSizes = (await DataContext.GetTimeSizes()).ToList();

         var results = await GetLastAndConvert(StatisticGroup.DEVICE.ToLowerName(), device.Uid, new[] { AggregationType.DAY, AggregationType.TIMESTAMP, AggregationType.AVERAGE });

         var data = new
         {
            TimeSizes = timeSizes,
            Curves = ConvertOut.Convert(await DataContext.GetCurvesByEvent(ev.Id)),
            Terminal = await DataContext.GetTerminalByDevice(deviceId.Value),
            Device = device,
            House = house,
            Farm = farm,
            Event = ev,
            Last = results
         };
         return Json(data);
      }

      [HttpGet]
      public async Task<JsonResult> Data()
      {
         var devices = (await DataContext.GetDevices()).ToList();
         var events = (await DataContext.GetDevicesEvent()).ToList();
         var results = ConvertOut.Convert(await DataContext.GetDevicesLast(events, devices), DataContext.Events.ToList(), DataContext.Houses.ToList()).ToList();
         var weightStats = results.Where(g => g.Sensors.Any(st => st.IsWeightSensorString())).ToList();


         var data = new
         {
            Devices = devices,
            Events = events,
            WeightStats = weightStats.GetUniqueLastValues(),
            Houses = await DataContext.GetHouses(),
            Stats = results.Where(g => g.Sensors.Any(st => !st.IsWeightSensorString())),
            Farms = await DataContext.GetFarms()
         };
         return Json(data);
      }

      [HttpPost]
      public async Task<bool> ChangeName(int deviceId, [FromBody]string name)
      {
         return await DataContext.UpdateDeviceName(deviceId, name);
      }

      [HttpPost]
      public async Task<JsonResult> CreateEvent(int deviceId)
      {
         return Json(await DataContext.StartDeviceEvent(deviceId, Resources.event_new));
      }
   }
}