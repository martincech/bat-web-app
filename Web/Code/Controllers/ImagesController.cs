﻿using System.IO;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Web.Controllers.Utils;
using UsersDataModel.DTO;
using Microsoft.AspNetCore.Authorization;

namespace Web.Controllers
{
   [Authorize]
   public class ImagesController : BaseController
   {
      private const string IMAGES_PATH = "Images";

      private const string USER_PATH = "User";
      private const string USER_DEFALT = "/Content/images/user.png";

      private const string BIRD_PATH = "Bird";
      private const string BIRD_DEFALT = "/Content/images/chicken.png";

      private const string FARM_PATH = "Farm";
      private const string FARM_DEFALT = "/Content/images/farm.png";

      private const string HOUSE_PATH = "House";
      private const string HOUSE_DEFALT = "/Content/images/house.png";

      private const string COMPANY_IMAGE = "company.png";
      private const string COMPANY_DEFALT = "/Content/images/company.png";

      public ImagesController(IConnectionFactory connectionFactory) : base(connectionFactory)
      {
      }

      [HttpGet]
      public ActionResult Users(string id)
      {
         return GetImage(GetImagePath(USER_PATH, id, id ?? CurrentUserId.ToString()), USER_DEFALT);
      }

      [HttpGet]
      public ActionResult Birds(string id)
      {
         return GetImage(GetImagePath(BIRD_PATH, CurrentUserId.ToString(), id), BIRD_DEFALT);
      }

      [HttpGet]
      public ActionResult Farms(string id)
      {
         return GetImage(GetImagePath(FARM_PATH, CurrentUserId.ToString(), id), FARM_DEFALT);
      }

      [HttpGet]
      public ActionResult Houses(string id)
      {
         return GetImage(GetImagePath(HOUSE_PATH, CurrentUserId.ToString(), id), HOUSE_DEFALT);
      }

      [HttpGet]
      public ActionResult Companies(string id)
      {
         if (id == null) id = UserContext.GetUserCompanyId(CurrentUserId.ToString());
         return GetImage(Path.Combine(WebRootPath, IMAGES_PATH, id ?? "", COMPANY_IMAGE), COMPANY_DEFALT);
      }

      [HttpPost]
      public ActionResult UploadUser(string id, [FromBody]string imageBase64)
      {
         return Upload(GetImagePath(USER_PATH, id, id ?? CurrentUserId.ToString()), imageBase64);
      }

      [HttpPost]
      public ActionResult UploadBird(string id, [FromBody]string imageBase64)
      {
         return Upload(GetImagePath(BIRD_PATH, CurrentUserId.ToString(), id), imageBase64);
      }

      [HttpPost]
      public ActionResult UploadFarm(string id, [FromBody]string imageBase64)
      {
         return Upload(GetImagePath(FARM_PATH, CurrentUserId.ToString(), id), imageBase64);
      }

      [HttpPost]
      public ActionResult UploadHouse(string id, [FromBody]string imageBase64)
      {
         return Upload(GetImagePath(HOUSE_PATH, CurrentUserId.ToString(), id), imageBase64);
      }

      [HttpPost]
      public ActionResult UploadCompany(string id, [FromBody]string imageBase64)
      {
         if (id == null) id = UserContext.GetUserCompanyId(CurrentUserId.ToString());

         var path = Path.Combine(WebRootPath, IMAGES_PATH, id);
         if (!Directory.Exists(path)) Directory.CreateDirectory(path);
         return Upload(Path.Combine(path, COMPANY_IMAGE), imageBase64);
      }

      private JsonResult Upload(string path, string imageBase64)
      {
         if (ImageUtil.Base64ToImage(path, imageBase64)) return Json("Upload OK");
         Response.StatusCode = (int)HttpStatusCode.BadRequest;
         return Json("Upload failed");
      }


      private string GetImagePath(string folder, string userId, string fileId)
      {
         var comapnyId = GetCompayId(userId ?? CurrentUserId.ToString());
         var path = Path.Combine(WebRootPath, IMAGES_PATH, comapnyId ?? "", folder);
         if (!Directory.Exists(path)) Directory.CreateDirectory(path);
         return Path.Combine(path, fileId + ".jpg");
      }

      private string GetCompayId(string userId)
      {
         if (User.IsInRole(RoleE.Owner.ToString()) ||
             UserContext.GetVisibleUsers().Select(s => s.ToString()).Contains(userId))
         {
            return UserContext.GetUserCompanyId(userId);
         }
         return null;
      }

      private ActionResult GetImage(string file, string defaultImage)
      {
         if (!System.IO.File.Exists(file)) return File(defaultImage, "image/png");
         return File(System.IO.File.OpenRead(file), "image/jpeg");
      }
   }
}