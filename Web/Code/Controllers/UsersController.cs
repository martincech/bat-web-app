﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Web.Properties;
using CognitoUsers;
using DataContext.Generator;
using UsersDataModel.DTO;
using Microsoft.AspNetCore.Authorization;

namespace Web.Controllers
{
   [Authorize(Roles = "Admin, Owner, Veit")]
   public class UsersController : BaseController
   {
      public UsersController(IConnectionFactory connectionFactory) : base(connectionFactory)
      {
      }

      // GET: Users
      public ActionResult Index()
      {
         ViewData["Controller"] = "usersCtrl";
         return View();
      }

      public async Task<JsonResult> Data()
      {
         var companyId = CurrentUser.CompanyId;
         var users = UserContext.GetUsers(true).Where(w => w.CompanyId == companyId).ToList();
         var cu = await UserManager.GetUsersAsync(users);
         var data = new
         {
            Users = cu.Select(s => CognitoUserManager.Map(s)),
            Images = Images(users.Select(s => s.Id.ToString()).ToList(), "User"),
            Devices = await DataContext.GetDevices(),
            CurrentUser.CompanyId
         };
         return Json(data);
      }

      [HttpPost]
      public async Task<JsonResult> Create([FromBody]UserDto user)
      {
         if (user == null) return null;
         var usr = await UserManager.CreateUserAsync(user.Email, CurrentUser.CompanyId);
         if (usr == null)
         {
            Response.StatusCode = (int)HttpStatusCode.BadRequest;
         }
         return Json(CognitoUserManager.Map(usr));
      }

      [HttpPost]
      public async Task<JsonResult> Update([FromBody]UserDto user)
      {
         var hasAutorization = user.Role != "Veit";
         if (user.Role == "Owner" && !User.IsInRole("Veit")) hasAutorization = false;
         if (user.Role == "Admin" && !(User.IsInRole("Veit") || User.IsInRole("Owner"))) hasAutorization = false;

         if (!hasAutorization)
         {
            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return Json(Resources.user_change_role);
         }

         user = CognitoUserManager.Map(await UserManager.UpdateUserAsync(user));

         return Json(Resources.user_updated);
      }

      [HttpPost]
      public async Task<List<string>> Delete([FromBody]List<string> userIds)
      {
         var result = new List<string>();
         if (userIds == null) return result;
         var users = UserContext.Users.Where(w => w.CompanyId == CurrentUser.CompanyId && w.Id != CurrentUserId);
         var del = users.Where(w => userIds.Contains(w.Id.ToString()));
         foreach (var u in del.ToList())
         {
            if (await UserManager.DeleteUserAsync(u.Id, u.UserName)) result.Add(u.Id.ToString());
         }
         return result;
      }

      [HttpPost]
      public async Task<bool> NewPassword(Guid id)
      {
         var user = UserManager.Users.ToList().FirstOrDefault(f => f.Id == id);
         if (user == null) return false;

         if (await UserManager.ResetUserPasswordAsync(user.Id.ToString()))
         {
            return true;
         }
         Response.StatusCode = (int)HttpStatusCode.BadRequest;
         return false;
      }     

      [Authorize(Roles = "Owner, Veit")]
      public async Task<JsonResult> DemoData(bool create)
      {
         if (create)
         {
            var dg = new DemoDataRegister(DataContext, UserContext);
            await dg.GenerateData(true);
         }
         else
         {
            await UserContext.DisableDemoData(true);
         }

         return Json(new { success = true });
      }
   }
}