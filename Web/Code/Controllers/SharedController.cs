﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers
{
   [Authorize]
   public class SharedController : Controller
   {
      // GET: Shared
      public ActionResult Export()
      {
         return PartialView("~/Views/Shared/_Export.cshtml");
      }

      // GET: Shared
      public ActionResult Delete()
      {
         return PartialView("~/Views/Shared/_Delete.cshtml");
      }

      // GET: Shared
      public ActionResult Paging()
      {
         return PartialView("~/Views/Shared/_Paging.cshtml");
      }


      // GET: Shared
      public ActionResult PrintTemplate()
      {
         return PartialView("~/Views/Shared/_PrintTemplate.cshtml");
      }

   }
}