﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace Web.Controllers
{
   [Authorize]
   public class AccountController : BaseController
   {
      public AccountController(IConnectionFactory connectionFactory) : base(connectionFactory)
      {
      }

      //
      // GET: /Account/Login

      public ActionResult Login(string returnUrl)
      {
         if (User.Identity.IsAuthenticated) return RedirectToAction("Index", "Compare");
         return Challenge();
      }

      //
      // GET: /Account/ForgotPasswordConfirmation
      [AllowAnonymous]
      public ActionResult ForgotPasswordConfirmation()
      {
         return View();
      }

      //
      // GET: /Account/ResetPassword
      [AllowAnonymous]
      public ActionResult ResetPassword(string code)
      {
         return code == null ? View("Error") : View();
      }

      //
      // POST: /Account/LogOff
      [HttpPost]
      [ValidateAntiForgeryToken]
      public async Task<ActionResult> LogOff()
      {
         await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
         return Redirect(CognitoSettings.LogoutUrl);
      }
   }
}