﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace Web.Controllers
{
   [Authorize]
   public class CompareController : BaseController
   {
      public CompareController(IConnectionFactory connectionFactory) : base(connectionFactory)
      {
      }

      // GET: Compare
      public ActionResult Index()
      {
         ViewData["Controller"] = "compareCtrl";
         return View();
      }

      [HttpGet]
      public async Task<JsonResult> Data()
      {
         var timeSizes = (await DataContext.GetTimeSizes()).ToList();         

         var data = new
         {
            TimeSizes = timeSizes,
            Curves = ConvertOut.Convert(await DataContext.GetCurves()),
            Events = (await DataContext.GetAllEvents()).Where(w => w.From < DateTime.Now).OrderBy(o => o.State).ThenByDescending(o => o.From)
         };
         return Json(data);
      }
   }
}