﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers
{
   [Authorize]
   public class Page404Controller : Controller
   {
      [AllowAnonymous]
      public ActionResult Index()
      {
         ViewData["Controller"] = "page404Ctrl";
         return View();
      }
   }
}