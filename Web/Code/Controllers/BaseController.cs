﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Controllers.Utils;
using DataContext.Context;
using DataContext.Convert;
using DataContext.DTOs;
using DataContext.DTOs.Enums;
using KafkaBatLibrary;
using UsersDataModel;
using NLog;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.IO;
using CognitoUsers;
using InfluxData.Net.InfluxDb;
using Microsoft.Extensions.Configuration;
using Veit.Cognito.Service;

namespace Web.Controllers
{
   public class BaseController : Controller
   {
      protected static readonly Logger LOG = LogManager.GetCurrentClassLogger();

      private const string DEFAULT_TEMPERATURE_UNIT = "c";
      private const string DEFAULT_WEIGHT_UNIT = "g";

      private readonly IConnectionFactory connectionFactory;
      protected string TemperatureUnit;
      protected string WeightUnit;
      protected int Decimals;

      public BaseController(IConnectionFactory connectionFactory)
      {
         this.connectionFactory = connectionFactory;
      }

      private User currentUser;
      protected User CurrentUser => currentUser ?? (currentUser = UserContext.User);

      protected string WebRootPath => connectionFactory.HostingEnvironment.WebRootPath;

      protected UserContext UserContext => connectionFactory.UserContext;

      protected BatContext DataContext => connectionFactory.BatContext;

      protected Guid CurrentUserId => UserContext.UserId;

      protected CognitoSettings CognitoSettings => connectionFactory.CognitoSettings;

      public CognitoUserManager UserManager => connectionFactory.CognitoUserManager;

      protected IKafkaSender KafkaSender => connectionFactory.KafkaSender;

      protected IInfluxDbClient InfluxClient => connectionFactory.InfluxClient;

      protected string GetConnectionString(string database)
         => string.Format(connectionFactory.Configuration.GetConnectionString("BatModelContainer"), database);

      private Converter convertIn;
      public Converter ConvertIn
      {
         get
         {
            if (convertIn == null)
            {
               convertIn = new Converter();
               convertIn.Temperature(TemperatureUnit, DEFAULT_TEMPERATURE_UNIT);
               convertIn.Weight(WeightUnit, DEFAULT_WEIGHT_UNIT);
            }
            return convertIn;
         }
      }

      private Converter convertOut;
      public Converter ConvertOut
      {
         get
         {
            if (convertOut == null)
            {
               convertOut = new Converter();
               convertOut.Temperature(DEFAULT_TEMPERATURE_UNIT, TemperatureUnit);
               convertOut.Weight(DEFAULT_WEIGHT_UNIT, WeightUnit);
            }
            return convertOut;
         }
      }

      public override void OnActionExecuting(ActionExecutingContext filterContext)
      {
         if (User.Identity.IsAuthenticated)
         {
            var cookies = filterContext.HttpContext.Request.Cookies;
            if (cookies != null)
            {
               TemperatureUnit = cookies.ContainsKey("temperatureUnit") ? cookies["temperatureUnit"] : DEFAULT_TEMPERATURE_UNIT;
               WeightUnit = cookies.ContainsKey("weightUnit") ? cookies["weightUnit"] : DEFAULT_WEIGHT_UNIT;

               int dec;
               if (!int.TryParse(cookies.ContainsKey("decimals") ? cookies["decimals"] : "2", out dec)) dec = 2;
               Decimals = dec;
            }
         }
         base.OnActionExecuting(filterContext);
      }

      [HttpPost]
      public async Task<JsonResult> FilterData([FromBody]IEnumerable<GroupFilter> filters)
      {
         return Json(await QueryAndConvertData(filters));
      }

      [HttpPost]
      public async Task<JsonResult> FilterLastData([FromBody]IEnumerable<GroupFilter> filters)
      {
         return Json(await QueryAndConvertData(filters, true));
      }

      protected Task<List<GroupData>> QueryAndConvertData(GroupFilter filter, bool onlyLast = false)
      {
         return QueryAndConvertData(new[] { filter }, onlyLast);
      }
      protected async Task<List<GroupData>> QueryAndConvertData(IEnumerable<GroupFilter> filter, bool onlyLast = false)
      {
         var result = new List<GroupData>();
         if (filter == null)
         {
            result.Add(new GroupData());
            return result;
         }
         foreach (var f in filter)
         {
            result.AddRange(await DataContext.ReadStats(f, onlyLast));
         }
         // convert units

         var proccessed = ConvertOut.Convert(result, DataContext.Events.ToList(), DataContext.Houses.ToList()).ToList();
         return (onlyLast ? proccessed.MergeSensorValues().GetUniqueLastValues() : proccessed.MergeSensorValues()).ToList();
      }

      protected Dictionary<string, string> Images(List<string> ids, string group)
      {
         var images = new Dictionary<string, string>();
         foreach (var id in ids)
         {
            if (images.ContainsKey(id)) continue;
            var file = string.Format("Images\\{0}\\{1}\\{2}.jpg", CurrentUser.CompanyId, group, id);
            var url = string.Format("/Images/{0}/{1}/{2}.jpg", CurrentUser.CompanyId, group, id);
            if (System.IO.File.Exists(Path.Combine(WebRootPath, file))) images.Add(id, url);
         }
         return images;
      }

      protected Task<List<GroupData>> GetLastAndConvert(string group, string id, IEnumerable<AggregationType> types = null)
      {
         return GetLastAndConvert(group, new[] { id }, types);

      }
      protected Task<List<GroupData>> GetLastAndConvert(string group, IEnumerable<string> id, IEnumerable<AggregationType> types = null)
      {
         var queries = new List<GroupFilter>
         {
            //WEIGHT
            new GroupFilter
            {
               Group = group,
               GroupIds = id.ToList(),
               Sensors = new List<string> {SensorType.WEIGHT.ToLowerName()},
               TimeSizes = new List<int> {AggregationInterval.Day.MapToMinutes()},
               Types = types == null?
                  new List<string>
                  {
                     AggregationType.DAY.ToLowerName(),
                     AggregationType.TIMESTAMP.ToLowerName(),
                     AggregationType.GAIN.ToLowerName(),
                     AggregationType.UNIFORMITY.ToLowerName(),
                     AggregationType.COUNT.ToLowerName(),
                     AggregationType.AVERAGE.ToLowerName()
                  } : types.Select(t => t.ToLowerName()).ToList()
            },
            // NON WEIGHT
            new GroupFilter
            {
               Group = group,
               GroupIds = id.ToList(),
               Sensors =
                  new List<string>
                  {
                     SensorType.TEMPERATURE.ToLowerName(),
                     SensorType.HUMIDITY.ToLowerName(),
                     SensorType.CO2.ToLowerName()
                  },
               TimeSizes = new List<int> {AggregationInterval.HalfHour.MapToMinutes()},
               Types = new List<string> {AggregationType.DAY.ToLowerName(),AggregationType.TIMESTAMP.ToLowerName() ,AggregationType.AVERAGE.ToLowerName()}
            }
         };

         return QueryAndConvertData(queries, true);
      }
   }
}