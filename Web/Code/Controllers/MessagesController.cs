﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace Web.Controllers
{
   [Authorize]
   public class MessagesController : BaseController
   {
      public MessagesController(IConnectionFactory connectionFactory) : base(connectionFactory)
      {
      }

      // GET: Messages
      public ActionResult Index()
      {
         ViewData["Controller"] = "messagesCtrl";
         return View();
      }
   }
}