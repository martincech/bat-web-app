﻿using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers
{
    public class RouterController : Controller
    {
        // GET: Crossroad
        public ActionResult Index()
        {
           if (User.Identity.IsAuthenticated) return RedirectToAction("Index", "Compare");
           return RedirectToAction("Index", "Home");
        }
    }
}