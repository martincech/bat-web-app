﻿using Microsoft.AspNetCore.Mvc;
using KafkaBatLibrary;

namespace Web.Controllers
{
   
   public class LogsController : BaseController
   {
      public LogsController(IConnectionFactory connectionFactory) : base(connectionFactory)
      {
      }

      [HttpPost]
      public bool Newsletter(string email)
      {
         UserContext.RegisterToNewsletter(email);
         return true;
      }
   }
}