﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DataContext.DTOs;
using Microsoft.AspNetCore.Authorization;

namespace Web.Controllers
{
   [Authorize]
   public class FarmsController : BaseController
   {
      public FarmsController(IConnectionFactory connectionFactory) : base(connectionFactory)
      {
      }

      // GET: MyFarm
      public ActionResult Index()
      {
         ViewData["Controller"] = "farmsCtrl";
         return View("~/Views/Setup/Farms/Index.cshtml");
      }

      [HttpGet]
      public async Task<JsonResult> Data()
      {
         var data = new
         {
            Farms = await DataContext.GetFarms(),
            Houses = await DataContext.GetHouses(),
         };
         return Json(data);
      }

      [HttpGet]
      public async Task<JsonResult> SetupData()
      {
         var farms = await DataContext.GetFarms();
         var data = new
         {
            Images = Images(farms.Select(s => s.Id.ToString()).ToList(), "Farm")
         };
         return Json(data);
      }

      [HttpPost]
      public async Task<JsonResult> Create([FromBody]string name)
      {
         return Json(await DataContext.CreateFarm(new Farm { Name = name }));
      }

      [HttpPost]
      public async Task<JsonResult> Update([FromBody]Farm farm)
      {
         return Json(await DataContext.UpdateFarm(farm));
      }

      [HttpPost]
      public async Task Delete([FromBody]List<int> farmIds)
      {
         await DataContext.DeleteFarm(farmIds);
      }

   }
}