﻿using System;

namespace Web.Controllers.Enums
{
   [Flags]
   public enum Notification
   {
     None = 0,
     Alarms = 1,
     Messages = 2
   }
}