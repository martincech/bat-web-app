﻿using System;

namespace Web.Controllers.Enums
{
   [Flags]
   public enum NotificationProvider
   {
      None = 0,
      Web = 1,
      Email = 2,
      Phone = 4
   }
}