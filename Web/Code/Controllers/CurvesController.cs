﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DataContext.DTOs;
using KafkaBatLibrary;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers
{
   [Authorize]
   public class CurvesController : BaseController
   {
      public CurvesController(IConnectionFactory connectionFactory) : base(connectionFactory)
      {
      }

      [HttpPost]
      public async Task AddPoint(int curveId, [FromBody]CurvePoint curvePoint)
      {
         var curve = await DataContext.GetCurve(curveId);
         if (curve == null) return;
         await DataContext.CreateCurvePoint(curveId, ConvertIn.Convert(curve.Type, curvePoint));
      }

      [HttpPost]
      public async Task<JsonResult> Create([FromBody]Curve curve)
      {
         return Json(ConvertOut.Convert(await DataContext.CreateCurve(ConvertIn.Convert(curve))));
      }

      [HttpGet]
      public async Task<JsonResult> Data()
      {
         var data = new
         {
            Curves = ConvertOut.Convert(await DataContext.GetCurves(false))
         };
         return Json(data);
      }

      [HttpPost]
      public async Task Delete([FromBody]ICollection<int> ids)
      {
         await DataContext.DeleteCurves(ids);
      }

      [HttpPost]
      public async Task DeletePoint(int curveId, int day)
      {
         await DataContext.DeleteCurvePoint(curveId, day);
      }
      // GET: Curves
      public ActionResult Index()
      {
         ViewData["Controller"] = "curvesCtrl";
         return View();
      }

      [HttpPost]
      public async Task Update([FromBody]Curve curve)
      {
         await DataContext.UpdateCurve(curve.Id, ConvertIn.Convert(curve));
      }
   }
}