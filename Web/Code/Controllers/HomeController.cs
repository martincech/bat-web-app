﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using Web.Models;

namespace Web.Controllers
{  
   public class HomeController : Controller
   {
      public ActionResult Index()
      {
         ViewData["Css"] = "homepage";
         return View();
      }

      [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
      public IActionResult Error()
      {
         return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
      }

      public ActionResult Send(string name, string email, string text)
      {
         throw new System.NotImplementedException();
      }
   }
}