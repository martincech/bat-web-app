﻿namespace Web.Controllers.Models
{
   public class IndexViewModel : EditSettingsViewModel
   {
      public string Email { get; set; }
      public string Company { get; set; }
   }

   public class EditSettingsViewModel
   {
      public string Name { get; set; }
      public string Comment { get; set; }
      public string PhoneNumber { get; set; }
   }
}