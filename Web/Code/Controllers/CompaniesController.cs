﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DataContext.Context;
using DataContext.DeviceGroup;
using UsersDataModel.DTO;
using UsersDataModel.Mapping;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using DataModel;

namespace Web.Controllers
{
   [Authorize(Roles = "Veit")]
   public class CompaniesController : BaseController
   {
      public CompaniesController(IConnectionFactory connectionFactory) : base(connectionFactory)
      {
      }

      // GET: Companies
      public ActionResult Index()
      {
         ViewData["Controller"] = "companiesCtrl";
         return View();
      }

      [HttpGet]
      public async Task<JsonResult> Data()
      {
         var cognitoUsers = await UserManager.GetAllCognitoUsers();
         return Json(new
         {
            Companies = UserContext.GetCompanies(),
            Users = UserContext.GetUsersName(cognitoUsers).Where(w => w.Id != CurrentUserId.ToString()),
            Terminals = UserContext.GetTerminals()
         });
      }

      [HttpPost]
      public async Task<JsonResult> Create([FromBody]string name)
      {
         var cp = UserContext.CreateCompany(new CompanyDto { Name = name, Database = Guid.NewGuid().ToString() });

         using (var newDataContext = GetCompanyBatContext(cp.Database))
         {
            await newDataContext.CreateDatabaseAsync();
         }
         return Json(cp.Map());
      }

      [HttpPost]
      public JsonResult Update([FromBody]CompanyDto company)
      {
         return Json(UserContext.UpdateCompany(company));
      }

      [HttpPost]
      public async Task<JsonResult> DisableDemoData(Guid companyId, bool disable)
      {
         return Json(await UserContext.DisableDemoData(companyId, disable));
      }

      [HttpPost]
      public async Task<JsonResult> Login(Guid companyId)
      {

         var result = Json(await UserContext.SetUserCompany(CurrentUserId, companyId));
         await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
         return result;
      }

      [HttpPost]
      public JsonResult UpdateGroups()
      {
         var databases = UserContext.Companies.Select(s => s.Database).ToList();
         return Json(GroupProducer.UpdateDatabase(databases, KafkaSender, (database) => new BatModelContainer(GetConnectionString(database))));
      }

      [HttpPost]
      public async Task Delete([FromBody]List<Guid> companyIds)
      {

         foreach (var id in companyIds)
         {
            var company = UserContext.Companies.FirstOrDefault(c => c.Id == id);
            if (company == null) continue;
            using (var batContextForComp = GetCompanyBatContext(company.Database))
            {
               await batContextForComp.DropDatabaseAsync();
            }
            await UserManager.DeleteCompanyAndItsUsers(id);
         }
      }

      [HttpPost]
      public async Task<JsonResult> DeleteUser(Guid userId)
      {
         return Json(await UserManager.DeleteUserAsync(userId, userId.ToString()));
      }

      [HttpPost]
      public async Task<JsonResult> CreateUser(Guid userId, Guid companyId, RoleE role)
      {
         var user = await UserManager.CreateLocalUserAsync(userId, companyId, role);
         var result = user != null ? await UserManager.GetUserAsync(user.Id) : null;
         return Json(result.Map());
      }

      [HttpPost]
      public async Task<JsonResult> UpdateUser(Guid userId, Guid companyId, RoleE role)
      {
         var result = await UserContext.SetUserCompany(userId, companyId);
         result = result && await UserContext.SetUserRole(userId, role);
         return Json(result);
      }

      private BatContext GetCompanyBatContext(string database)
         => new BatContext(new DataModel.BatModelContainer(GetConnectionString(database)), UserContext, InfluxClient, KafkaSender);

   }
}