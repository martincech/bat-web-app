﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Web.Controllers.Models;
using CognitoUsers;
using Microsoft.AspNetCore.Authorization;

namespace Web.Controllers
{
   [Authorize]
   public class ManageController : BaseController
   {    
      public ManageController(IConnectionFactory connectionFactory) : base(connectionFactory)
      {
      }

      // GET: /Manage/Index
      public async Task<ActionResult> Index()
      {
         ViewData["StatusMessage"] = "";

         var user = CognitoUserManager.Map(await UserManager.GetUserAsync(CurrentUserId));
         var model = new IndexViewModel
         {
            Name = user.Name,
            Email = user.Email,
            PhoneNumber = user.Phone,
            Comment = user.Comment,
            Company = CurrentUser.Company == null ? "" : CurrentUser.Company.Name
         };
         return View(model);
      }

      [HttpPost]
      [ValidateAntiForgeryToken]
      public async Task<ActionResult> EditSettings(EditSettingsViewModel settings)
      {
         var user = await UserManager.GetUserAsync(CurrentUserId);
         if (user == null)
         {
            return NotFound();
         }

         await UserManager.UpdateCognitoUserAttributesAsync(user.UserName, new System.Collections.Generic.Dictionary<string, string>
         {
            { "name", settings.Name },
            { "phone_number", settings.PhoneNumber },
            { "custom:note", settings.Comment },
         });

         return RedirectToAction("Index");
      }    
   }
}