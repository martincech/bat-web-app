﻿app.controller('farmsCtrl', ["$scope", "vLoader", "farms", "$timeout", "vName",
    function ($scope, vLoader, farms, $timeout, vName) {

        $scope.deleteArray = [];

        $scope.farms = [];
        $scope.houses = [];

        $scope.images = [];

        $scope.state = {
            operation: null
        }

        $scope.deleteArray = [];
        $scope.operations = {
            del: "delete"
        }
        $scope.ok = null;
        $scope.cancel = function () {
            $scope.deleteArray = [];
            $scope.state.operation = null;
        }
        $scope.state.operation = null;


        function updateMenu() {
            var scope = angular.element(document.getElementById('sensors-menu')).scope();
            if (scope == null) return;
            scope.farms = $scope.farms;
            scope.houses = $scope.houses;
        }


        $scope.delete = function () {
            if ($scope.state.operation !== $scope.operations.del) {
                $scope.state.operation = $scope.operations.del;
                $scope.ok = $scope.delete;
                return;
            }
            if ($scope.deleteArray.length === 0) {
                $scope.state.operation = null;
                return;
            }
            farms.delete($scope.deleteArray).success(function () {
                $scope.farms = $scope.farms.filter(function (farm) {
                    return $scope.deleteArray.indexOf(farm.Id) === -1;
                });

                $scope.houses.forEach(function (house) {
                    if ($scope.deleteArray.indexOf(house.FarmId) !== -1) house.FarmId = null;
                });
                $scope.deleteArray = [];
                $scope.state.operation = null;
                updateMenu();
            });
        }

        $scope.updateFarm = function () {
            if ($scope.activeFarm.Name === "" || $scope.activeFarm.Name == null) {
                $scope.activeFarm.Name = $scope.originalName || "Farm";
            }
            farms.update($scope.activeFarm).success(function () {
                $scope.editName = false;
                $scope.editNote = false;
                $scope.originalName = $scope.activeFarm.Name;
                updateMenu();
            });
        }

        $scope.create = function () {
            var name = vName.createUnique($scope.farms, "New Farm");
            farms.create(name).success(function (farm) {
                $scope.farms.unshift(farm);
                window.location.hash = "#/" + farm.Id;
                updateMenu();
            });
        }

        $scope.saveName = function () {
            $scope.editName = !$scope.editName;
            if (!$scope.editName) $scope.updateFarm();
        }

        $scope.saveNote = function () {
            $scope.editNote = !$scope.editNote;
            if (!$scope.editNote) $scope.updateFarm();
        }

        $scope.canAssignHouse = function (house) {
            if (house.FarmId === null || house.FarmId === $scope.activeFarm.Id) return true;
            return false;
        }

        $scope.assignHouse = function (house) {
            var index = $scope.activeFarm.Houses.indexOf(house.Id);
            if (index === -1) {
                house.FarmId = $scope.activeFarm.Id;
                $scope.activeFarm.Houses.push(house.Id);
            } else {
                house.FarmId = null;
                $scope.activeFarm.Houses.splice(index, 1);
            }
            $scope.updateFarm();
        }

        //#region Page load & hash change



        function applyHash() {
            var hashes = window.location.hash.match(/\d+/g) || [];
            $scope.activeFarm = $scope.farms.find(function (farm) { return farm.Id.toString() === hashes[0] });
            $scope.originalName = $scope.activeFarm ? $scope.activeFarm.Name : null;
        }

        farms.get().success(function (data) {
            $scope.farms = data.Farms;
            $scope.houses = data.Houses;
            applyHash();

        });

        window.addEventListener("hashchange", function () {
            applyHash();
            $scope.editName = false;
            $scope.editNote = false;
            setTimeout(function () {
                if ($scope.activeFarm) document.getElementById($scope.activeFarm.Id).scrollIntoView();
            }, 0);
            $scope.$apply();
        });

        $scope.fileUploadCallback = function (e, image) {
            var cropper = e.target.parentElement.parentElement.getElementsByTagName("img");
            cropper[0].src = image;
        }

        farms.setup().success(function (data) {
            $scope.images = data.Images;
        });
        //#endregion
    }]);