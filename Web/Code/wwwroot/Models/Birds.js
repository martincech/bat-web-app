﻿app.controller('birdsCtrl', ["$scope", "veit", "vLoader", "birds", "vName",
    function ($scope, veit, vLoader, birds, vName) {
        $scope.sexModes = veit.enums.sexModes;
        $scope.curveTypes = veit.enums.curveTypes;

        $scope.birds = {
            list: [],
            active: null,
            selected: []
        };

        $scope.state = {
            operation: null
        }

        $scope.edit = {
            name: null,
            description: null
        }

        $scope.cancel = function () {
            $scope.birds.selected = [];
            $scope.state.operation = null;
        }

        $scope.operations = {
            del: "delete"
        }

        $scope.save = function () {
            birds.update($scope.birds.active);
        }

        $scope.create = function () {
            var name = vName.createUnique($scope.birds.list, "New Bird");
            birds.create(name, $scope.sexMode).success(function (bird) {
                $scope.birds.list.unshift(bird);
                window.location.hash = $scope.path + bird.Id;
            });
        }

        $scope.delete = function () {
            $scope.state.operation = $scope.state.operation ? null : $scope.operations.del;
            if (!$scope.birds.selected.length) return;
            birds.delete($scope.birds.selected).success(function () {
                $scope.birds.list = $scope.birds.list.filter(function (bird) {
                    return $scope.birds.selected.indexOf(bird.Id) === -1;
                });
                $scope.birds.selected = [];
            });
        }
        $scope.ok = $scope.delete;

        $scope.changeName = function () {
            $scope.editing.name = !$scope.editing.name;
            if (!$scope.editing.name && $scope.birds.active.Name !== $scope.edit.name) {
                $scope.birds.active.Name = $scope.edit.name;
                $scope.save();
            }
        }

        $scope.changeDescription = function () {
            $scope.editingDescription = !$scope.editingDescription;
            if (!$scope.editingDescription && $scope.birds.active.Description !== $scope.edit.description) {
                $scope.birds.active.Description = $scope.edit.description;
                $scope.save();
            }
        }

        $scope.setSexMode = function (mode) {
            $scope.birds.active.SexMode = veit.enums.setSexMode($scope.birds.active.SexMode, mode);
            $scope.save();
        };

        var filter = [
            ["male", $scope.sexModes.male],
            ["female", $scope.sexModes.female],
            ["mixed", $scope.sexModes.mixed],
            ["undefined", $scope.sexModes.undefined]
        ];

        function applyHash() {
            var hash = veit.hash.parseHash(filter);
            $scope.path = hash[0];
            $scope.sexMode = hash[1];
            var id = (window.location.hash.match(/\d+/g) || [])[0];
            $scope.birds.active = $scope.birds.list.find(function (item) {
                return id === item.Id.toString();
            });
            if ($scope.birds.active) {
                $scope.edit.name = $scope.birds.active.Name;
                $scope.edit.description = $scope.birds.active.Description;
                setTimeout(function () { document.getElementById($scope.birds.active.Id).scrollIntoView() }, 0);
            }
        }

        function setFilter() {
            $scope.editing = {};
            applyHash();
            vLoader.loading(false);
        };

        birds.get().success(function (data) {
            $scope.curves = data.Curves;
            $scope.birds.list = data.Birds;
            $scope.images = data.Images;
            setFilter();
        });

        window.addEventListener('hashchange', function () {
            setFilter();
            $scope.$apply();
        });

        vLoader.loading(true);

        $scope.fileUploadCallback = function (e, image) {
            var cropper = e.target.parentElement.parentElement.getElementsByTagName("img");
            cropper[0].src = image;
        }
    }
]);