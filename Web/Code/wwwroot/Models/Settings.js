﻿app.controller('settingsCtrl', ["$scope", "$cookies", "vCookies", "$d3", "settings", function ($scope, $cookies, vCookies, $d3, settings) {
    var exampleValue = 1234.56;
    $scope.example = "1.66 kg";

    $scope.today = new Date();
    $scope.numberFormat = vCookies.getNumberFormat();

    $scope.dateFormats = vCookies.dateFormats;
    $scope.dateFormat = vCookies.getDateFormat();

    var defaultEventLength = 150;

    $scope.eventLength = defaultEventLength;
    settings.getEventLength().success(function (data) {
        $scope.eventLength = data.EventLength || defaultEventLength;
    });

    $scope.eventLenghtChanged = function () {
        settings.setEventLength($scope.eventLength);
    }

    function round(value, ndec) {
        var n = 10;
        for (var i = 1; i < ndec; i++) {
            n *= 10;
        }

        if (!ndec || ndec <= 0)
            return Math.round(value);
        else
            return Math.round(value * n) / n;
    }

    $scope.convert = {
        kg: function () {
            return round(exampleValue * 0.001, $scope.decimals);
        },
        lb: function () {
            return round(exampleValue * 0.0022046, $scope.decimals);
        },
        g: function () {
            return round(exampleValue, $scope.decimals);
        }
    }

    $scope.langs = vCookies.language;

    $scope.weigtUnits = vCookies.units.weight;
    $scope.temperatureUnits = vCookies.units.temperature;

    $scope.weightUnit = vCookies.units.weight.get();
    $scope.temperatureUnit = vCookies.units.temperature.get();
    $scope.decimals = vCookies.units.decimals.get();
    $scope.maxDecimals = vCookies.units.decimals[$scope.weightUnit];

    $scope.tutorial = vCookies.tutorial.enum;

    $scope.lang = $cookies.get("lang") || $scope.langs.en;
    $scope.showTutorial = $cookies.get("showTutorial") || $scope.tutorial.once;
    $scope.showHints = $cookies.get("showHints") || "true";

    $scope.example = $scope.convert[$scope.weightUnit]() + " " + $scope.weightUnit;

    $scope.changeCookie = function (name, value) {
        $cookies.put(name, value, { path: "/", expires: new Date("1.1.2035") });
        if (name === "lang") {
            $cookies.put(".AspNetCore.Culture", `c=${value}|uic=${value}`, { path: "/", expires: new Date("1.1.2035") });           
        }
        settings.update().then(function () {
            if (name === "lang") {
                location.reload();
            }
        });
    }

    $scope.changeWeight = function () {
        vCookies.units.weight.set($scope.weightUnit);
        $scope.changeDecimals();
        settings.update();
    }

    $scope.changeTemperature = function () {
        vCookies.units.temperature.set($scope.temperatureUnit);
        settings.update();
    }

    $scope.changeDecimals = function () {
        $scope.maxDecimals = vCookies.units.decimals[$scope.weightUnit];
        if ($scope.decimals > $scope.maxDecimals) $scope.decimals = $scope.maxDecimals;
        vCookies.units.decimals.set($scope.decimals);
        $scope.example = $scope.convert[$scope.weightUnit]() + " " + $scope.weightUnit;
        settings.update();
    }

    $scope.resetTutorial = function () {
        vCookies.tutorial.reset();
        settings.update();
    }

}]);