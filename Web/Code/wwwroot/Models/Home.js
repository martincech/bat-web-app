﻿app.controller('homeCtrl', ["$scope", '$http', function ($scope, $http) {

    $scope.name = "";
    $scope.email = "";
    $scope.text = "";

    $scope.sent = false;
    $scope.sending = false;

    $scope.send = function () {
        $scope.sending = true;
        $http({
            method: 'POST', data: {
                name: $scope.name,
                email: $scope.email,
                text: $scope.text
            },
            url: '/home/send'
        }).success(function () {
            $scope.sent = true;
            $scope.sending = false; 
            $scope.name = "";
            $scope.email = "";
            $scope.text = "";
            setTimeout(function () {
                $scope.apply(function () {
                    $scope.sent = false;
                });
            }, 1000);
        });
    }

    $scope.isValid = function() {
        if ($scope.email == null || $scope.email === "" || $scope.text == null || $scope.text === "") return false;
        return true;
    }

}]);