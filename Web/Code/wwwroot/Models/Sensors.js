app.controller('sensorsCtrl', [
    "$scope", "vEnums", "vArray", "vEvent", "events", "$d3", "vFile", "vLoader", "vCookies", "sensors", "vSensor", "sensorsDetail", "vExport", "vDate", "vStatTable",
    function ($scope, vEnums, vArray, vEvent, events, $d3, vFile, vLoader, vCookies, sensors, vSensor, sensorsDetail, vExport, vDate, vStatTable) {
        $scope.exporting = false;
        $scope.exportArray = [];

        $scope.decimals = vCookies.units.decimals.get();
        $scope.sexModes = vEnums.sexModes;
        $scope.types = vSensor.types;
        $scope.sensors = vSensor.sensors;
        $scope.sortTypes = vStatTable.sortTypes;

        $scope.filter = {};

        $scope.operations = {
            exp: "export"
        }
        $scope.state = {
            operation: null
        };

        $scope.export = function () {
            var devices = $scope.devices.filter(function (d) { return $scope.exportArray.indexOf(d.Id) !== -1 });
            vExport.lastToCsv(devices);
            $scope.state.operation = null;
            $scope.exportArray = [];
        }

        $scope.ok = $scope.export;
        $scope.cancel = function () {
            $scope.state.operation = null;
        }


        $scope.$on("treeDataSource", function (ev, args) {
            $scope.filterData = args;
        });

        $scope.$watch("state.operation", function (operation) {
            if (operation === null) {
                $scope.exportArray = [];
            } else {
                if ($scope.devices != null) $scope.exportArray = $scope.devices.map(function (d) { return d.Id });
            }
        });

        $scope.showStat = function (type, isWeight) {
            if (type === $scope.type) return;
            $scope.type = type;
            isWeight ? vStatTable.showWeightStat($scope.allDevices, $scope.type) : vStatTable.showStat($scope.allDevices, $scope.type);
            $scope.isReverse = !$scope.isReverse;
            $scope.sort($scope.sortTypes.value);
        }


        $scope.type = $scope.types.average;

        $scope.addToExport = function (deviceId) {
            vArray.toggle($scope.exportArray, deviceId);
        }

        $scope.getWidth = function (terminal) {
            if (terminal.Event == null) return null;
            var start = new Date(terminal.Event.From).getTime(),
                end = new Date(terminal.Event.To).getTime(),
                today = new Date().getTime();
            return { width: Math.round((today - start) * 100 / (end - start)) + '%' }
        }

        $scope.createEvent = function (id) {
            sensorsDetail.createEvent(id)
                .success(function (data) {
                    window.location = "/calendar/#/" + data.Id;
                });
        }

        $scope.clearFilter = function () {
            $scope.filter = {};
            window.location = "/sensors/#";
        }

        var now = new Date();
        var dayTicks = 1000 * 60 * 60 * 24;
        $scope.isActive = function (stats, sensor) {
            var date = vSensor.findValue(stats, [sensor, "time"]);
            if (date == null) return {};
            date = vDate.parse(date);
            var diff = now - date;
            return diff > dayTicks ? { opacity: 0.5 } : {};
        }

        function filter() {
            $scope.devices = $scope.allDevices;
            var path = window.location.hash.split('/');
            if (path.length < 3) return;
            var id = parseInt(path[2]);
            if (path[1] === 'house' && id) {
                $scope.devices = $scope.allDevices.filter(function (device) { return device.HouseId === id });
                $scope.filter.house = id;
            }
            if (path[1] === 'farm' && id) {
                var farm = $scope.farms.find(function (f) { return f.Id === id });
                if (farm) $scope.devices = $scope.allDevices.filter(function (device) { return farm.Houses.indexOf(device.HouseId) !== -1 });
                $scope.filter.farm = id;
            };

        }

        $scope.canStartEvent = function (device) {
            if (device.Event == null) return true;
            return false;
        }

        sensors.get()
            .success(function (data) {
                $scope.allDevices = data.Devices;
                var today = new Date();
                $scope.farms = data.Farms;
                $scope.houses = data.Houses;
                var last = vSensor.mapLast(data.WeightStats, data.Stats);
                $scope.allDevices.forEach(function (device) {
                    device.last = last[device.Uid];
                    device.Event = data.Events.find(function (w) { return w.Devices.indexOf(device.Id) !== -1 });
                    if (device.Event) {
                        device.Event.ActualDay = vDate.daysDiff(new Date(device.Event.From), today);
                        device.Event.TotalDays = vDate.daysDiff(new Date(device.Event.From), new Date(device.Event.To));
                    }
                    device.hasOtherData = vSensor.hasOtherData(device);
                    if (device.HouseId == null) return;
                    device.House = data.Houses.find(function (h) { return h.Id === device.HouseId });
                    if (device.House == null || device.House.FarmId == null) return;
                    device.Farm = data.Farms.find(function (f) { return device.House.FarmId === f.Id });
                });

                $scope.events = data.Events;
                filter();
                $scope.showStat($scope.types.weight.average, true);
                vLoader.loading(false);
            });

        window.addEventListener("hashchange", function () {
            $scope.filter = {};
            filter();
            if ($scope.sorting) {
                $scope.sorting($scope.devices);
                if ($scope.isReverse) $scope.devices.reverse();
            }
            $scope.$apply();
        });

        $scope.sort = function (sort) {
            if (!sort) return;
            $scope.devices = sort($scope.devices);
            if (sort === $scope.sorting) {
                $scope.isReverse = !$scope.isReverse;
            } else {
                $scope.isReverse = true;
            }
            if ($scope.isReverse) $scope.devices.reverse();
            $scope.sorting = sort;
        }


        $scope.isVisible = function (device) {
            var hasFarm = true;
            if ($scope.filter.farm) {
                var house = $scope.filterData.houses.find(function (h) { return h.FarmId === $scope.filter.farm && device.HouseId === h.Id });
                hasFarm = house != null;
            }
            var result = (hasFarm)
                && ($scope.filter.house ? $scope.filter.house === device.HouseId : true)
                && ($scope.filter.event ? $scope.filter.event === (device.Event ? device.Event.Id : -1) : true);
            return result;
        }

        $scope.$watch("filter.farm", function (newVal) {
            if (newVal == null) return;
            $scope.filter.event = null;
            window.location = "/sensors/#/farm/" + newVal;
        });

        $scope.$watch("filter.house", function (newVal) {
            if (newVal == null) return;
            $scope.filter.event = null;
            window.location = "/sensors/#/house/" + newVal;
        });


        $scope.getSort = function (sort) {
            if ($scope.sorting === sort) return $scope.isReverse ? "sorting_desc" : "sorting_asc";
            return "sorting";
        }

        //#endregion
        vLoader.loading(true);
    }
]);