﻿app.controller('housesSetupCtrl', ["$scope", "vLoader", "houses", "$timeout", "vName", "vArray", "vMessages",
    function ($scope, vLoader, houses, $timeout, vName, vArray, vMessages) {
        vMessages.loadResouces("^m_");
        $scope.deleteArray = [];
        $scope.farms = [];
        $scope.houses = [];
        $scope.images = [];

        function updateMenu() {
            var scope = angular.element(document.getElementById('sensors-menu')).scope();
            if (scope == null) return;
            scope.farms = $scope.farms;
            scope.houses = $scope.houses;
        }

        $scope.operations = {
            del: function () {
                if ($scope.deleteArray.length === 0) {
                    $scope.state.operation = null;
                    return;
                }
                houses.delete($scope.deleteArray).success(function () {
                    $scope.houses = $scope.houses.filter(function (farm) {
                        return $scope.deleteArray.indexOf(farm.Id) === -1;
                    });

                    $scope.deleteArray = [];
                    $scope.state.operation = null;
                    updateMenu();
                });
            }
        }
        $scope.ok = $scope.operations.del;
        $scope.cancel = function () {
            $scope.deleteArray = [];
            $scope.state.operation = null;
        }

        $scope.state = {
            operation: null
        };

        $scope.updateHouse = function (basicOnly) {
            if ($scope.house.Name == null || $scope.house.Name === "") {
                $scope.house.Name = $scope.originalName;
            }
            houses.update($scope.house, basicOnly).success(function () {
                $scope.originalName = $scope.house.Name;
                $scope.editName = false;
                $scope.editNote = false;
                updateMenu()
            });
        }

        $scope.create = function () {
            var name = vName.createUnique($scope.houses, "New House");
            houses.create(name).success(function (house) {
                $scope.houses.unshift(house);
                window.location.hash = "/" + house.Id;
            });
        }

        $scope.saveName = function () {
            $scope.editName = !$scope.editName;
            if (!$scope.editName) $scope.updateHouse(true);
        }

        $scope.saveNote = function () {
            $scope.editNote = !$scope.editNote;
            if (!$scope.editNote) $scope.updateHouse(true);
        }

        function assignDevice(device, add, events) {
            vArray.toggle($scope.house.Devices, device.Id);
            device.HouseId = $scope.house.Devices.indexOf(device.Id) === -1 ? null : $scope.house.Id;
            houses.updateDevice($scope.house.Id, device.Id, add).success(function () {
                for (var i = 0; i < events.length; i++) {
                    if (add) {
                        vArray.addUnique(events[i].Devices, device.Id);
                    } else {
                        vArray.remove(events[i].Devices, device.Id);
                    }
                }

                if (add) {
                    vMessages.showMessage("m_info", "m_house_add_device", vMessages.messageLevel.info, { device: device.Name, house: $scope.house.Name });
                } else {
                    vMessages.showMessage("m_info", "m_house_remove_device", vMessages.messageLevel.info, { device: device.Name, house: $scope.house.Name });
                }
            });
        }

        $scope.assignDevice = function (device) {
            if (device == null) return;
            var index = $scope.house.Devices.indexOf(device.Id);
            var events = $scope.events.filter(function (f) { return f.Devices.indexOf(device.Id) !== -1; });
            if (index !== -1) {

                if (events.length === 0 || $scope.house.Devices.length === 1) {
                    assignDevice(device, false, []);
                    return;
                }

                vMessages.showDialog('removeDeviceDialog.html', true, true, { device: device, house: $scope.house, events: events })
                    .then(function (result) {
                        if (result === true) assignDevice(device, false, events);
                    });
                return;
            }

            events = $scope.events.filter(function (e) { return $scope.house.Devices.filter(function (d) { return e.Devices.indexOf(d) !== -1; })[0] != null });
            assignDevice(device, true, events);
        }
        //#region Page load & hash change

        function applyHash() {
            var hashes = window.location.hash.match(/\d+/g) || [];
            $scope.house = $scope.houses.find(function (house) { return house.Id.toString() === hashes[0] });
            $scope.originalName = $scope.house ? $scope.house.Name : null;
            setTimeout(function () {
                if ($scope.house) document.getElementById($scope.house.Id).scrollIntoView();
            }, 0);
        }
        $scope.$on("treeDataSource", function (ev, args) {
            var data = angular.copy(args);
            $scope.farms = data.farms;
            $scope.houses = data.houses;

            $scope.houses.forEach(function (h) {
                if (h.FarmId == null) return;
                h.Farm = $scope.farms.find(function (f) { return f.Id === h.FarmId });
            });

            $scope.devices = data.devices;
            applyHash();
        });

        window.addEventListener("hashchange", function () {
            applyHash();
            $scope.editName = false;
            $scope.$apply();
        });

        $scope.fileUploadCallback = function (e, image) {
            var cropper = e.target.parentElement.parentElement.getElementsByTagName("img");
            cropper[0].src = image;
        }

        houses.setup().success(function (data) {
            $scope.images = data.Images;
            $scope.events = data.Events;
        });

        //#endregion
    }]);