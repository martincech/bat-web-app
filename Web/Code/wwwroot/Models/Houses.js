﻿app.controller('housesCtrl', [
    "$scope", "vEnums", "vArray", "vLoader", "vCookies", "vSensor", "houses", "vName", "vExport", "vDate", "vStatTable",
    function ($scope, vEnums, vArray, vLoader, vCookies, vSensor, houses, vName, vExport, vDate, vStatTable) {
        $scope.exporting = false;
        $scope.exportArray = [];

        $scope.decimals = vCookies.units.decimals.get();
        $scope.sexModes = vEnums.sexModes;
        $scope.types = vSensor.types;
        $scope.sensors = vSensor.sensors;
        $scope.sortTypes = vStatTable.sortTypes;

        $scope.operations = {
            exp: "export",
            del: "del"
        }
        $scope.state = { operation: null };

        $scope.ok = $scope.export;
        $scope.cancel = function () {
            $scope.state.operation = null;
        }

        $scope.filter = {};

        $scope.$on("treeDataSource", function (ev, args) {
            $scope.filterData = args;
        });


        $scope.$watch("state.operation", function (operation) {
            if (operation === null) $scope.exportArray = [];
        });

        $scope.showStat = function (type, isWeight) {
            if (type === $scope.type) return;
            $scope.type = type;
            isWeight ? vStatTable.showWeightStat($scope.allDevices, $scope.type) : vStatTable.showStat($scope.allDevices, $scope.type);
            $scope.isReverse = !$scope.isReverse;
            $scope.sort($scope.sortTypes.value);
        }


        $scope.type = $scope.types.average;

        $scope.addToExport = function (deviceId) {
            vArray.toggle($scope.exportArray, deviceId);
        }

        $scope.getDeviceType = function (type) {
            if (type === 3) return "fa-plug";
            if (type === 2) return "fa-balance-scale";
            if (type === 1) return "fa-microchip";
            return "fa-plug";
        }


        $scope.getWidth = function (terminal) {
            if (terminal.Event == null) return null;
            var start = new Date(terminal.Event.From).getTime(),
                end = new Date(terminal.Event.To).getTime(),
                today = new Date().getTime();
            return { width: Math.round((today - start) * 100 / (end - start)) + '%' }
        }

        var creatingEvent = false;

        $scope.createEvent = function (id) {
            var house = $scope.devices.find(function (d) { return d.Id === id });
            if (house == null || creatingEvent) return;
            creatingEvent = true;
            houses.createEvent(house.Id)
                .success(function (data) {
                    creatingEvent = false;
                    window.location = "/calendar/#/" + data.Id;
                });
        }

        $scope.create = function () {
            var name = vName.createUnique($scope.devices, "New House");
            houses.create(name).success(function (house) {
                $scope.devices.unshift(house);
            });
        }

        $scope.canStartEvent = function (house) {
            if (house.Event == null && house.Devices.length > 0) return true;
            return false;
        }


        $scope.ok = function () {
            if ($scope.state.operation === $scope.operations.del) {
                houses.delete($scope.exportArray).success(function () {
                    $scope.devices = $scope.devices.filter(function (d) {
                        return $scope.exportArray.indexOf(d.Id) === -1;
                    });
                    $scope.state.operation = null;
                    $scope.exportArray = [];
                });
            }
            if ($scope.state.operation === $scope.operations.exp) {
                var devices = $scope.devices.filter(function (d) { return $scope.exportArray.indexOf(d.Id) !== -1 });
                vExport.lastToCsv(devices);
                $scope.state.operation = null;
                $scope.exportArray = [];
            }
        }

        $scope.clearFilter = function () {
            $scope.filter = {};
            window.location = "/houses/#";
        }

        function filter() {
            $scope.devices = $scope.allDevices;
            var path = window.location.hash.split('/');
            if (path.length < 3) return;
            var id = parseInt(path[2]);
            if (path[1] === 'house' && id) $scope.devices = $scope.allDevices.filter(function (device) { return device.HouseId === id });
            if (path[1] === 'farm' && id) {
                var farm = $scope.farms.find(function (f) { return f.Id === id });
                if (farm) $scope.devices = $scope.allDevices.filter(function (device) { return farm.Houses.indexOf(device.Id) !== -1 });
                $scope.filter.farm = id;
            };
        }

        houses.get()
            .success(function (data) {
                $scope.allDevices = data.Houses;
                var today = new Date();
                var last = vSensor.mapLast(data.WeightStats, data.Stats);
                $scope.allDevices.forEach(function (device) {
                    device.last = last[device.Id];
                    device.Event = data.Events.find(function (w) { return vArray.intersect(w.Devices, device.Devices).length > 0 });
                    if (device.Event) {
                        device.Event.ActualDay = vDate.daysDiff(new Date(device.Event.From), today);
                        device.Event.TotalDays = vDate.daysDiff(new Date(device.Event.From), new Date(device.Event.To));
                    }
                    device.hasOtherData = vSensor.hasOtherData(device);
                    if (device.FarmId == null) return;
                    device.Farm = data.Farms.find(function (f) { return device.FarmId === f.Id });
                });
                $scope.farms = data.Farms;
                $scope.events = data.Events;
                filter();
                $scope.showStat($scope.types.weight.average, true);
                vLoader.loading(false);
            });

        window.addEventListener("hashchange", function () {
            $scope.filter = {};
            filter();
            if ($scope.sorting) {
                $scope.sorting($scope.devices);
                if ($scope.isReverse) $scope.devices.reverse();
            }
            $scope.$apply();
        });

        $scope.sort = function (sort) {
            if (!sort) return;
            $scope.devices = sort($scope.devices);
            if (sort === $scope.sorting) {
                $scope.isReverse = !$scope.isReverse;
            } else {
                $scope.isReverse = true;
            }
            if ($scope.isReverse) $scope.devices.reverse();
            $scope.sorting = sort;
        }

        $scope.isVisible = function (device) {
            var result = ($scope.filter.farm ? $scope.filter.farm === device.FarmId : true)
                && ($scope.filter.house ? $scope.filter.house === device.HouseId : true)
                && ($scope.filter.event ? $scope.filter.event === (device.Event ? device.Event.Id : -1) : true);
            return result;
        }

        $scope.$watch("filter.farm", function (newVal) {
            if (newVal == null) return;
            $scope.filter.house = null;
            $scope.filter.event = null;
            window.location = "/houses/#/farm/" + newVal;
        });

        $scope.getSort = function (sort) {
            if ($scope.sorting === sort) return $scope.isReverse ? "sorting_desc" : "sorting_asc";
            return "sorting";
        }

        //#endregion
        vLoader.loading(true);
    }
]);
