﻿app.controller('tutorialCtrl', ["$scope", "$cookies", "$d3", "vCookies", function ($scope, $cookies, $d3, vCookies) {

    $scope.header = "Welcome";
    $scope.page = "";
    $scope.step = 1;
    $scope.maxStep = 18;

    $scope.disabledNextSteps = [7, 8];

    $scope.paths = {
        compare: [1, 16],
        settings: [2, 3, 4, 5],
        curves: [6, 7, 8, 9],
        birds: [10, 11, 12],
        houses: [13],
        sensors: [14],
        users: [17, 18],
        calendar: [15]
    }

    function n(n) {
        return n > 9 ? "" + n : "0" + n;
    }

    function setHeader() {

        var eName = "tutor" + n($scope.step);
        var el = $d3.selectAll(".bubble." + eName);
        if (el[0].length) $scope.header = el.select("h3").text();
        $cookies.put("tutor", $scope.step, { expires: new Date(2035, 1), path: "/" });
        setTimeout(function () {
            var classes = $d3.select("body").attr("class");
            if (!classes) return;
            var regex = /(tutor)\d+/;

            if (classes.match(regex)) {
                classes = classes.replace(regex, eName);
            } else {
                classes += " " + eName;
            }
            $d3.select("body").attr("class", classes);

            if ($scope.step === 3) {
                document.getElementById("settings").scrollIntoView();
            }
            if ($scope.step === 4) {
                document.getElementById("tutor04").scrollIntoView();
            }
            if ($scope.step === 6) {
                $scope.$parent.showSubmenu = true;
            }
            if ($scope.step === 7) {
                $scope.$parent.showSubmenu = true;
            }
            if ($scope.step === 8) {
                $scope.$parent.showSubmenu = false;
            }
            if ($scope.step === 10) {
                $scope.$parent.showSubmenu = false;
            }
            if ($scope.step === 11) {
                $scope.$parent.showSubmenu = true;
            }
            if ($scope.step === 12) {
                $scope.$parent.showSubmenu = false;
            }

            $scope.$apply();
        }, 0);



        $d3.selectAll(".bubble").classed("hidden", function () {
            return !$d3.select(this).classed("tutor" + n($scope.step));
        });

        $d3.select("body").classed("bubble-hide", false);
    }

    function setLocation() {
        var newUrl = "/";
        for (var key in $scope.paths) {
            if ($scope.paths.hasOwnProperty(key)) {
                if ($scope.paths[key].indexOf($scope.step) !== -1) newUrl += key;
            }
        }
        window.location = newUrl;
    }

    function goToPage() {
        if (!$scope.page || $scope.page === "calendar") {
            if ($scope.paths["calendar"].indexOf($scope.step) === -1) setLocation();
        } else {
            if ($scope.paths[$scope.page].indexOf($scope.step) === -1) setLocation();
        }
    }



    $scope.hide = function () {
        $d3.select("body").classed("bubble-hide", true);
    }

    $scope.exit = function () {
        $d3.select("body").classed("tutorials", false);
        $cookies.put("showTutorial", "once", { path: "/", expires: new Date("1.1.2035") });
    }

    $scope.next = function () {
        ++$scope.step;
        goToPage();
        setHeader();
    }

    $scope.prev = function () {
        --$scope.step;
        goToPage();
        setHeader();
    }

    $scope.isNextDisabled = function () {
        return $scope.disabledNextSteps.indexOf($scope.step) !== -1;
    }

    $scope.$on("nextStep", function (context, steps) {
        if (steps == null || steps.indexOf($scope.step) === -1) return;
        $scope.next();
    });

    function pageLoad() {
        $scope.page = (window.location.pathname.split("/")[1] || "").toLowerCase();
        var hasTutorial = $scope.paths[$scope.page] != null && !vCookies.tutorial.visited();
        $d3.select("body").classed("tutorials", hasTutorial);
        $d3.select("body").classed("hints", vCookies.hints.enabled());
        if (!hasTutorial) return;
        var step = vCookies.tutorial.getStep();
        $scope.step = $scope.paths[$scope.page].indexOf(step) !== -1 ? step : $scope.step = $scope.paths[$scope.page][0];
        setHeader();
    }

    pageLoad();
}]);