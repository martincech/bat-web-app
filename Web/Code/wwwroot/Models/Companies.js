﻿app.controller('companiesCtrl', ["$scope", "$timeout", "veit", "vLoader", "companies", "vName", "vMessages",
    function ($scope, $timeout, veit, vLoader, companies, vName, vMessages) {

        //#region DELETE USER
        $scope.deleteArray = [];
        $scope.operations = {
            del: "delete"
        }

        $scope.edit = {};

        $scope.ok = null;
        $scope.cancel = function () {
            $scope.deleteArray = [];
            $scope.state.operation = null;
        }
        $scope.state = { operation: null };

        $scope.delete = function () {
            $scope.ok = $scope.delete;
            $scope.state.operation = $scope.state.operation ? null : $scope.operations.del;
            if (!$scope.deleteArray.length) return;
            vLoader.loading(true);
            companies.delete($scope.deleteArray).success(function () {
                $scope.companies = $scope.companies.filter(function (user) {
                    return $scope.deleteArray.indexOf(user.Id) === -1;
                });
                $scope.deleteArray = [];
                vLoader.loading(false);
            });
        }
        //#endregion

        //#region CREATE AND UPDATE USER
        $scope.changeName = function () {
            $scope.editingName = !$scope.editingName;
            if (!$scope.editingName) $scope.save();
        }

        $scope.changeDatabase = function () {
            $scope.editDatabase = !$scope.editDatabase;
            if (!$scope.editDatabase) $scope.save();
        }

        $scope.assignUser = function (user) {
            if ($scope.activeCompany.Users.indexOf(user.Id) !== -1) return;
            $scope.companies.forEach(function (company) {
                var index = company.Users.indexOf(user.Id);
                if (company === $scope.activeCompany && index === -1) {
                    $scope.activeCompany.Users.push(user.Id);
                    user.Company = $scope.activeCompany.Name;
                    return;
                }
                if (index !== -1) company.Users.splice(index, 1);
            });

            $scope.save();
        }

        $scope.assignTerminal = function (terminal) {
            var index = $scope.activeCompany.Terminals.indexOf(terminal.Uid);
            if (index !== -1) {
                $scope.activeCompany.Terminals.splice(index, 1);
                terminal.CompanyId = null;
            } else {
                $scope.activeCompany.Terminals.push(terminal.Uid);
                terminal.CompanyId = $scope.activeCompany.Id;
            }
            $scope.save();
        }



        $scope.fileUploadCallback = function (e, image) {
            var cropper = e.target.parentElement.parentElement.getElementsByTagName("img");
            cropper[0].src = image;
        }

        $scope.save = function () {
            companies.update($scope.activeCompany);
        }

        $scope.updateGroups = function () {
            vLoader.loading(true);
            companies.updateGroups().then(function (result) {
                vLoader.loading(false);
                var data = [];
                Object.keys(result.data).forEach(function (key) {
                    data.push(key + " : " + result.data[key]);
                });
                vMessages.showDialog("groupUpdate.html", null, null, { messages: data });
            });
        }

        $scope.create = function () {
            vLoader.loading(true);
            var name = vName.createUnique($scope.companies, "New Company");
            companies.create(name).success(function (company) {
                $scope.companies.unshift(company);
                vLoader.loading(false);
                location.href = "/companies/#/" + company.Id;
            });
        }

        $scope.login = function () {
            companies.login($scope.activeCompany.Id).success(function () {
                window.location.reload();
            });
        }

        $scope.userExist = function (userId) {
            return $scope.users.filter(function (usr) { return usr.Id === userId; })[0] != null;
        }

        $scope.deleteUser = function (user) {
            vLoader.loading(true);
            companies.deleteUser(user.Id).then(function () {
                $scope.users = $scope.users.filter(function (u) { return u.Id !== user.Id && u.Sub !== user.Id });
                vLoader.loading(false);
            });
        }

        $scope.createUser = function () {
            vLoader.loading(true);
            companies.createUser($scope.edit.user.Id, $scope.edit.user.CompanyId, $scope.edit.user.Role).then(function (user) {
                load();
                vLoader.loading(false);
            });
        }


        $scope.updateUser = function () {
            vLoader.loading(true);
            companies.updateUser($scope.edit.user.Id, $scope.edit.user.CompanyId, $scope.edit.user.Role).then(function (user) {
                load();
                vLoader.loading(false);
            });
        }

        $scope.isValidUser = function () {
            return $scope.edit.user.Id != null && $scope.edit.user.CompanyId != null && $scope.edit.user.Role != null;
        }
        //#endregion

        //#region PAGE INIT
        function filter() {
            var hash = window.location.hash.split("/") || [];
            $scope.activeCompany = $scope.companies.find(function (company) {
                return company.Id === hash[hash.length - 1];
            });
            vLoader.loading(false);
        }

        window.addEventListener('hashchange', function () {
            filter();
            $scope.$apply();
        });


        function load() {
            companies.get().success(function (data) {
                $scope.companies = data.Companies;
                $scope.users = data.Users;
                $scope.terminals = data.Terminals;
                filter();
            });
        }

        $scope.hasCognitoOnly = function () {
            if ($scope.users == null) return false;
            return $scope.users.filter(function (u) { return u.Id == null; }).length > 0;
        }

        $scope.hasCompanyless = function () {
            if ($scope.users == null) return false;
            return $scope.users.filter(function (u) { return u.CompanyId == null && u.Id != null; }).length > 0;
        }

        $scope.disableDemoData = function (company) {
            companies.disableDemoData(company.Id, !company.DisableDemoData).then(function () {
                company.DisableDemoData = !company.DisableDemoData;
            });
        }

        vLoader.loading(true);
        load();
        //#endregion
    }
]);