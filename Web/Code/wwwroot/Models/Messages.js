﻿"use strict";
app.controller('messagesCtrl', ['$scope', '$timeout', '$interpolate', function ($scope, $timeout, $interpolate) {
    var resources = {};
    $scope.messages = [];
    $scope.messageIcons = {
        "info": "fa-info-circle",
        "success": "fa-check-circle",
        "alert": "fa-exclamation-triangle"
    }

    $scope.resource = function (key, data) {
        var res = (resources[key] || key);
        return $interpolate(res)(data);
    }

    function shift() {
        $scope.messages.shift();
    }

    $scope.$on('showMessage', function (scope, data) {
        $timeout(function() {
             $scope.messages.push(data);
        },0);
        $timeout(shift, 5000);
    });

    $scope.$on('loadResources', function (scope, res) {
        if (res == null) return;
        resources = angular.merge({}, resources, res);
    });
}]);