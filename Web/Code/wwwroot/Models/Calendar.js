﻿app.controller('calendarCtrl', ["$scope", "$d3", "$timeout", "veit", "d3Calendar", "vEvent", "vLoader", "events", "vDate", "vEnums", "vHash", "vArray",
function ($scope, $d3, $timeout, veit, d3CalendarFactory, vEvent, vLoader, events, vDate, vEnums, vHash) {
    $scope.eventStates = veit.enums.eventStates;
    $scope.sexModes = veit.enums.sexModes;
    var d3Calendar = d3CalendarFactory.create();

    var filter = [
     ["active", vEnums.eventStates.active],
     ["finished", vEnums.eventStates.closed],
     ["scheduled", vEnums.eventStates.stopped]
    ];

    $scope.timeSizes = {
        halfHour: 30,
        day: 1440
    }

    $scope.state = {
        operation: null
    }

    $scope.timeSize = $scope.timeSizes.day;

    function filterEvents() {

        $scope.events.forEach(function (ev) {
            ev.visible = ev.State === $scope.eventState || $scope.eventState == null;
        });
    }

    function redrawEvents() {
        filterEvents();
        if (d3Calendar.redraw) {
            d3Calendar.redraw($scope.events);
        } else {
            d3Calendar.createCalendar($scope.events, document.getElementById("graph-print"), null, null);
            var area = $scope.timeArea();
            d3Calendar.setDomain($scope.events, area.from, area.to);
        }
    }

    d3Calendar.rowChanged = function (event) {
        $scope.update(["Row"], event);
    }

    $scope.timeAreas = {
        all: function () {
            var result = {};
            var filtered = vEvent.filterByState($scope.events, $scope.eventState);
            result.from = $d3.min(filtered, function (d) { return d.From });
            result.to = $d3.max(filtered, function (d) { return d.To });
            return result;
        },
        year: function () {
            var dToday = new Date();
            var result = {};
            result.from = $d3.time.month.offset(dToday, -6),
            result.to = $d3.time.month.offset(dToday, +6);
            return result;
        },
        month: function () {
            var dToday = new Date();
            var result = {};
            result.from = $d3.time.day.offset(dToday, -15),
            result.to = $d3.time.day.offset(dToday, +15);
            return result;
        }
    }

    $scope.timeArea = $scope.timeAreas.year;

    $scope.setTimeArea = function (newValue) {
        $scope.timeArea = newValue;
        var result = newValue();
        if (d3Calendar.setDomain && result) d3Calendar.setDomain($scope.events, result.from, result.to);
    };

    $scope.moveEnabled = false;
    $scope.toggleMove = function () {
        $scope.moveEnabled = !$scope.moveEnabled;
        if ($scope.moveEnabled) { d3Calendar.enableZoom() }
        else { d3Calendar.disableZoom() }
    }

    function setEvent(data) {
        if (data == null) return;
        data.From = vDate.parse(data.From);
        data.To = vDate.parse(data.To);
        $scope.events.unshift(data);
        window.location.hash = "#/" + data.Id;
    }

    $scope.copyEvent = function () {
        events.create(vEvent.copy($scope.events, $scope.event)).success(setEvent);
    }

    $scope.createEvent = function () {
        events.create(vEvent.create($scope.events, $scope.eventLength)).success(setEvent);
    }

    $scope.$watch("selected", function (dArray) {
        if (dArray == null) return;
        if ($scope.state.operation === $scope.operations.exp || $scope.operation === $scope.operations.del) d3Calendar.setSelected(dArray);
    }, true);

    function selectMultiple(id) {
        veit.array.toggle($scope.selected, id);
        $scope.$apply();
        return $scope.selected;
    }

    function selectSingle(id) {
        var hash = $scope.path + id;
        window.location.hash = hash;
        return [id];
    }

    d3Calendar.onClick = selectSingle;


    $scope.setSelected = function (array, item) {
        veit.array.toggle(array, item);
        d3Calendar.setSelected(array, true);
    }

    $scope.operations = {
        del: function deleteEvent() {
            events.delete($scope.selected).success(function () {
                $scope.events = $scope.events.filter(function (item) {
                    return $scope.selected.indexOf(item.Id) === -1;
                });
                $scope.state.operation = null;
                d3Calendar.redraw($scope.events);
                if ($scope.events.indexOf($scope.event) === -1) {
                    window.location = $scope.path;
                }
            });
        },
        exp: function () {
            events.exportEvents($scope.selected, $scope.timeSize);
            $scope.state.operation = $scope.state.operation ? null : $scope.operations.exp;
        }
    }
 

    $scope.$watch("state.operation", function (operation) {
        $scope.selected = [];
        d3Calendar.onClick = operation ? selectMultiple : selectSingle;
        d3Calendar.setSelected(operation || $scope.event == null ? [] : [$scope.event.Id]);
    });

    $scope.houseEnabled = function (house) {
        if (house == null || house.Devices.length === 0) return false;
        var enabled = true;
        $scope.devices.forEach(function (device) {
            if (house.Devices.indexOf(device.Id) !== -1) enabled = enabled && device.Enabled;
        });
        return enabled;
    }

    $scope.getHouseDevices = function (house) {
        return $scope.devices.filter(function (item) { return house.Devices.indexOf(item.Id) !== -1 });
    }

    $scope.birdChanged = function () {
        $scope.event.Bird = $scope.birds.find(function (bird) { return bird.Id === $scope.event.BirdId });
        if ($scope.event.Bird != null) {
            $scope.event.SexMode = $scope.event.Bird.SexMode;
        } else {
            $scope.event.SexMode = $scope.sexModes.undefined;
        }
        //$scope.updateEvent();
        $scope.update();
    }

    $scope.stopEvent = function () {
        $scope.event.To = new Date();
        $scope.updateEvent(true);
    }

    $scope.getAge = function () {
        return vEvent.getAge($scope.event);
    }

    $scope.eventsIntersect = function () {
        return $scope.events.find(function (ev) {
            if (ev.Row !== $scope.event.Row || ev.Id === $scope.event.Id) return false;
            if ($scope.event.From <= ev.From && $scope.event.To >= ev.From) return true;
            if ($scope.event.From <= ev.To && $scope.event.To >= ev.To) return true;
            if ($scope.event.From <= ev.From && $scope.event.To >= ev.To) return true;
            if ($scope.event.From >= ev.From && $scope.event.To <= ev.To) return true;
            return false;
        });
    }

    function setAvailiable() {
        $scope.houses.forEach(function (house) {
            house.Enabled = $scope.houseEnabled(house);
        });

        $scope.farms.forEach(function (farm) {
            farm.Enabled = $scope.houses.filter(function (house) { return house.FarmId === farm.Id && !house.Enabled }).length === 0;
        });
    }

    var dateChanged = null;

    $scope.dateChanged = function (updateTo) {
        if ($scope.event.From == null || $scope.event.To == null) return;
        if (dateChanged != null) $timeout.cancel(dateChanged);
        dateChanged = $timeout(function () {
            if (!$scope.event.From || !$scope.event.To) return;
            if (updateTo) {
                var diff = $scope.eventOriginal.To - $scope.eventOriginal.From;
                $scope.event.To = $d3.time.second.offset($scope.event.From, diff / 1000);
            }
            if (($scope.event.To - $scope.event.From) < 86400000) return;
            var intersect = $scope.eventsIntersect();
            if (intersect == null && $scope.event.intersect) $scope.event.intersect.intersect = null;
            $scope.event.intersect = intersect;
            if ($scope.event.intersect == null) {
                $scope.updateEvent(true);
            } else {
                $scope.event.intersect.intersect = $scope.event;
                if (d3Calendar.redraw) {
                    redrawEvents();
                }
            }
            vEvent.setAvailableDevices($scope.event, $scope.events, $scope.devices);
            setAvailiable();
            dateChanged = null;
        }, 500);
    }

    $scope.print = function () {
        var graphToPrint = d3CalendarFactory.create();
        graphToPrint.print($scope.events);
    }

    $scope.ageChanged = function () {
        $scope.update();
    }

    $scope.nameChanged = function () {
        if ($scope.editingName) {
            $scope.update();
            d3Calendar.updateName($scope.event.Id, $scope.event.Name);
        }
        $scope.editingName = !$scope.editingName;
    }

    $scope.update = function () {
        if ($scope.event.intersect != null) return;
        events.update($scope.event, true);
    }

    $scope.updateEvent = function (redraw) {
        if ($scope.event.intersect != null) return;
        redraw = redraw || vEvent.setNameIfDefault($scope.event, $scope.farms, $scope.houses, $scope.devices);
        vLoader.loading(true);
        events.update($scope.event).success(function () {
            vLoader.loading(false);
            if (!$scope.event.From || !$scope.event.To) return;
            $scope.event.State = vEvent.getState($scope.event);
            if (redraw && d3Calendar.redraw) {
                redrawEvents();
                vEvent.setAvailableDevices($scope.event, $scope.events, $scope.devices);
            }
            $scope.eventOriginal = angular.copy($scope.event);

        });
    }



    function setActiveEvent() {
        var path = vHash.parseHash(filter);
        $scope.path = path[0];
        $scope.eventState = path[1];
        $scope.filter.State = $scope.eventState;
        $scope.event = $scope.events.filter(function (item) {
            item.expanded = path[2] === item.Id.toString();
            return item.expanded;
        })[0];

        if ($scope.event == null) {
            var event = vEvent.getClosestToEnd($scope.events, $scope.eventState);
            if (event) {
                window.location.hash = $scope.path + event.Id;
                return;
            }
        }

        $scope.eventOriginal = angular.copy($scope.event);
        filterEvents();
        if ($scope.event != null) $scope.event.intersect = $scope.eventsIntersect();
        redrawEvents();
        $scope.setSelected($scope.state.operation ? $scope.selected : $scope.event ? [$scope.event.Id] : []);
        vEvent.setAvailableDevices($scope.event, $scope.events, $scope.devices);
        setAvailiable();
        vLoader.loading(false);
        setTimeout(function () {
            var wrapper = document.getElementById("events-list");
            var elem = document.getElementById(path[2]);
            if (wrapper == null || elem == null) return;
            if (!(elem.offsetTop > wrapper.scrollTop && elem.offsetTop < wrapper.scrollTop + wrapper.clientHeight)) wrapper.scrollTop = elem.offsetTop;
        }, 0);
    }

    window.addEventListener("hashchange", function () {
        $scope.editingName = false;
        setActiveEvent();
        $scope.$apply();
    });

    events.get().success(function (data) {
        $scope.filter = {};
        $scope.events = vEvent.convertDates(data.Events);
        $scope.birds = data.Birds;
        $scope.curves = data.Curves;
        $scope.terminals = data.Terminals;
        $scope.devices = data.Devices;
        $scope.houses = data.Houses;
        $scope.farms = data.Farms;
        vEvent.mapLocation($scope.devices, $scope.houses, $scope.farms);
        $scope.houses = $scope.houses.sort(function (a, b) {
            return +(a.farmName > b.farmName) || +(a.farmName === b.farmName) - 1;
        });
        $scope.eventLength = data.EventLength || 150;
        setActiveEvent();
    });

    $scope.wizardStart = function () {
        $scope.$broadcast("wizardStart", {});
    }

    vLoader.loading(true);
}])
.controller("wizardCtrl", ["$scope", "vEvent", "vDate", "events", function ($scope, vEvent, vDate, events) {
    $scope.wizardStep = 0;
    $scope.wizardEvent = null;
    var evs = [];

    function setEvent(data) {
        if (data == null) return;
        data.From = vDate.parse(data.From);
        data.To = vDate.parse(data.To);
        $scope.events.unshift(data);
        window.location.hash = "#/" + data.Id;
    }

    function setAvailiable() {
        $scope.wizardHouses.forEach(function (house) {
            house.Enabled = $scope.houseEnabled(house);
        });

        $scope.wizardFarms.forEach(function (farm) {
            farm.Enabled = $scope.wizardHouses.filter(function (house) { return house.FarmId === farm.Id && !house.Enabled }).length === 0;
        });
    }

    $scope.$on("wizardStart", function (scope) {
        var parent = scope.targetScope;
        if (parent == null) return;
        $scope.wizardEvent = vEvent.create(parent.events, parent.eventLength);
        $scope.wizardEvent.eventLength = parent.eventLength;
        $scope.wizardDevices = angular.copy(parent.devices);
        $scope.wizardFarms = angular.copy(parent.farms);
        $scope.wizardHouses = angular.copy(parent.houses);
        vEvent.mapLocation($scope.wizardDevices, $scope.wizardHouses, $scope.wizardFarms);
        evs = angular.copy(parent.events);
        vEvent.setAvailableDevices($scope.wizardEvent, evs, $scope.wizardDevices);
        setAvailiable();
    });

    $scope.wizardGo = function (step) {
        if (!$scope.wizardStepValid(step - 1)) return;
        $scope.wizardStep = step;
    }
    $scope.wizardCancel = function () {
        $scope.wizardStep = 0;
        $scope.wizardEvent = null;
    }
    $scope.wizardFinish = function () {
        events.create($scope.wizardEvent).success(function (data) {
            $scope.wizardCancel();
            setEvent(data);
        });
    }

    function addDays(date, days) {
        if (days == null || days < 1) days = $scope.eventLength;
        return new Date(date.getTime() + (days * (1000 * 60 * 60 * 24)));
    }

    $scope.houseEnabled = function (house) {
        if (house == null || house.Devices.length === 0) return false;
        var enabled = true;
        $scope.wizardDevices.forEach(function (device) {
            if (house.Devices.indexOf(device.Id) !== -1) enabled = enabled && device.Enabled;
        });
        return enabled;
    }

    $scope.wizardUpdateEnd = function () {
        if ($scope.wizardEvent.eventLength == null || $scope.wizardEvent.From == null) return;
        $scope.wizardEvent.To = addDays($scope.wizardEvent.From, $scope.wizardEvent.eventLength);
        vEvent.setAvailableDevices($scope.wizardEvent, evs, $scope.wizardDevices);
        setAvailiable();
    }

    $scope.wizardStepValid = function (step) {
        if ($scope.wizardEvent == null) return false;
        if (step < 1) return $scope.wizardEvent.Name !== "";
        var valid = $scope.wizardStepValid(step - 1);
        if (step === 1) return valid;
        if (step === 2) return valid && $scope.wizardEvent.From != null;
        if (step === 3) return valid && $scope.wizardEvent.eventLength > 0;
        if (step === 4) return valid;
        return valid;
    }

    $scope.wizardStepStyle = function () {
        return { 'transform': 'translateX(' + (-20 * $scope.wizardStep) + '%)' };
    }
}])
.directive('eventLocation', [function () {
    var controller = ["$scope", function ($scope) {

        $scope.assignDevice = function (device) {
            if (!device.Enabled) return;
            var index = $scope.event.Devices.indexOf(device.Id);
            if (index === -1) {
                $scope.event.Devices.push(device.Id);
            } else {
                $scope.event.Devices.splice(index, 1);
            }
            if ($scope.updateevent != null) $scope.updateevent();
        }

        $scope.houseEnabled = function (house) {
            if (house == null || house.Devices.length === 0) return false;
            var enabled = true;
            $scope.devices.forEach(function (device) {
                if (house.Devices.indexOf(device.Id) !== -1) enabled = enabled && device.Enabled;
            });
            return enabled;
        }

        $scope.houseActive = function (house) {
            var enabled = false;
            $scope.event.Devices.forEach(function (deviceId) {
                if (house.Devices.indexOf(deviceId) !== -1) enabled = true;
            });
            return enabled;
        }

        function assignHouse(house, isActive) {
            house.Devices.forEach(function (deviceId) {
                var index = $scope.event.Devices.indexOf(deviceId);
                if (isActive && index !== -1) $scope.event.Devices.splice(index, 1);
                if (!isActive && index === -1) $scope.event.Devices.push(deviceId);
            });
        }

        $scope.assignHouse = function (house) {
            if (!house.Enabled) return;
            var isActive = $scope.houseActive(house);
            assignHouse(house, isActive);
            if ($scope.updateevent != null) $scope.updateevent();
        }

        $scope.farmActive = function (farm) {
            if (farm == null || farm.Houses.length === 0) return false;
            return $scope.houses.filter(function (house) {
                if (house.FarmId !== farm.Id) return false;
                return $scope.houseActive(house);
            }).length > 0;
        }

        $scope.assignFarm = function (farm) {
            if (!farm.Enabled) return;
            var isActive = $scope.farmActive(farm);
            $scope.houses.forEach(function (house) {
                if (house.FarmId !== farm.Id) return;
                assignHouse(house, isActive);
            });
            if ($scope.updateevent != null) $scope.updateevent();
        }
    }];

    return {
        restrict: 'E',
        templateUrl: "eventLocation.html",
        scope: {
            uid: "@",
            event: "=",
            houses: "=",
            farms: "=",
            devices: "=",
            updateevent: "="
        },
        controller: controller
    }
}]);
