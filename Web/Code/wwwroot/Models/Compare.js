﻿"use strict";
app.controller('compareCtrl', ["$scope", "compare", "compareGraph", "vEnums", "$colors", "vSensor", "$timeout", "vArray", "$filter", "vDate",
function ($scope, compare, compareGraphFactory, vEnums, $colors, vSensor, $timeout, vArray, $filter, vDate) {

    var compareGraph = compareGraphFactory.create();

    $scope.sexModes = vEnums.sexModes;
    $scope.sexMode = null;
    $scope.from = null;
    $scope.to = null;

    $scope.showSubmenu = true;

    $scope.stats = [];

    $scope.events = {
        states: vEnums.eventStates,
        active: [],
        activePreview: null,
        activeEnabled: true,
        past: [],
        pastPreview: null,
        pastEnabled: false,
        selected: [],
        allSelected: []
    }

    function hasSensor(sensors, sensor) {
        if (sensors == null || sensors.length === 0 || sensor == null) return false;
        return sensors.find(function (s) {
            return s.toLowerCase().indexOf(sensor.toLowerCase()) !== -1;
        });
    }

    function getStatData() {
        var eventsId = $scope.events.selected.map(function (m) { return m.Id });
        if ($scope.events.activePreview && $scope.events.activeEnabled) eventsId.push($scope.events.activePreview.Id);
        if ($scope.events.pastPreview && $scope.events.pastEnabled) eventsId.push($scope.events.pastPreview.Id);

        var allEventsId = ($scope.allEvents || []) .map(function (m) { return m.Id });

        eventsId = eventsId.filter(function (n) {
            return allEventsId.indexOf(n) !== -1;
        });
        return $scope.stats.filter(function (stat) {
            return eventsId.indexOf(parseInt(stat.GroupId)) !== -1
                && stat.TimeSize === $scope.timeSize.Minutes
                && hasSensor(stat.Sensors, $scope.sensor.typeName);
        });
    }

    function draw() {
        compareGraph.create(getStatData(), $scope.type, $scope.allEvents, $scope.sensor.type.decimals, $scope.sensor, $scope.advanced);
        if ($scope.sensor === $scope.sensors.weightMale && $scope.type === $scope.types.average) {
            if (compareGraph.addCurve) compareGraph.addCurve($scope.curve);
        }
        if ($scope.events.allSelected.indexOf($scope.fitEvent) !== -1) {
            if (compareGraph.setFitTo) compareGraph.setFitTo($scope.fitEvent);
        } else {
            if (compareGraph.setFit) compareGraph.setFit();
        }
    }

    $scope.print = function () {
        var printG = compareGraphFactory.create();
        printG.print(getStatData(), $scope.type, $scope.allEvents, $scope.sensor.type.decimals, $scope.sensor);
        if ($scope.sensor === $scope.sensors.weightMale && $scope.type === $scope.types.average) {
            if (printG.addCurve) printG.addCurve($scope.curve);
        }
        if ($scope.events.allSelected.indexOf($scope.fitEvent) !== -1) {
            if (printG.setFitTo) printG.setFitTo($scope.fitEvent);
        } else {
            if (printG.setFit) printG.setFit();
        }
    }

    function drawGraph() {
        if (compareGraph.isCreated()) {
            compareGraph.show(getStatData());
            if ($scope.events.allSelected.indexOf($scope.fitEvent) !== -1) {
                compareGraph.setFitTo($scope.fitEvent);
            } else {
                compareGraph.setFit();
            }
        } else {
            draw();
        }
        setActiveCurve($scope.fitEvent || $scope.events.activePreview || $scope.events.pastPreview);
    }

    $scope.sensors = vSensor.sensors;
    $scope.sensor = $scope.sensors.weightMale;

    $scope.types = vSensor.types.weight;
    $scope.type = $scope.types.average;

    function hasData() {
        var evs = [];
        var selected = $scope.events.selected.slice();
        if ($scope.events.pastPreview && selected.indexOf($scope.events.pastPreview) === -1 && $scope.events.pastEnabled) selected.unshift($scope.events.pastPreview);
        if ($scope.events.activePreview && selected.indexOf($scope.events.activePreview) === -1 && $scope.events.activeEnabled) selected.unshift($scope.events.activePreview);


        $scope.events.allSelected = selected;

        var lastEv = selected.filter(function (s) {
            return s.last == null;
        });

        if (lastEv.length !== 0) {
            compare.filterLastData(
               lastEv,
               1440,
               vSensor.sensors.weightMale.typeName,
               vSensor.sensors.weightMale.type.list()
           ).success(function (data) {
               data.forEach(function (d) {
                   var ev = $scope.events.allSelected.find(function (a) {
                       return a.Id.toString() === d.GroupId;
                   });
                   if (ev == null) return;
                   ev.last = d;
               });
           });
        }

        selected.forEach(function (ev) {
            var data = $scope.stats.filter(function (stat) {
                return parseInt(stat.GroupId) === ev.Id
                    && stat.TimeSize === $scope.timeSize.Minutes
                    && hasSensor(stat.Sensors, $scope.sensor.typeName);
            })[0];
            if (!data) evs.push(ev);
        });

        if (evs.length === 0) {
            drawGraph();
            $scope.loading = false;
        } else {
            $scope.loading = true;
            compare.filterData(
                evs,
                $scope.timeSize.Minutes,
                $scope.sensor.typeName,
                $scope.sensor.type.list()
            ).success(function (data) {
                data.forEach(function (d) {
                    $scope.stats.push(d);
                });
                drawGraph();
                $scope.loading = false;
            });
        }
    }

    function applyFilters(from, to) {
        if ($scope.allEvents == null) return;
        var events = $scope.allEvents.filter(function (ev) {
            var result = true;
            if (from != null) result = new Date(ev.To) > from;
            if (to != null) result = result && (new Date(ev.From) < to);
            if ($scope.sexMode != null) result = result && ev.SexMode === $scope.sexMode;
            return result;
        });

        $scope.events.active = events.filter(function (e) { return e.State === vEnums.eventStates.active });
        $scope.events.past = events.filter(function (e) { return e.State === vEnums.eventStates.closed });

        $scope.delta = 0;
        $scope.previewEvent(vEnums.eventStates.active);
        $scope.previewEvent(vEnums.eventStates.closed);
    }

    $scope.date = {};
    $scope.clearFilter = function () {
        $scope.sexMode = null;
        $scope.date.from = null;
        $scope.date.to = null;
        applyFilters();
    }

    $scope.filterEvents = function (from, to) {
        applyFilters(from, to);
    }

    $scope.resize = function () {
        $scope.advanced = !$scope.advanced;
        setTimeout(draw, $scope.advanced ? 0 : 300);
    }

    $scope.isToday = function (date) {
        if (date == null) return false;
        var pDate = vDate.parse(date);
        if (pDate == null) return false;
        var d = new Date();
        d.setDate(d.getDate() - 1);
        return pDate > d;
    }

    $scope.changeSexMode = function (sexMode) {
        $scope.sexMode = vEnums.setSexMode($scope.sexMode, sexMode);
        if ($scope.sexMode === $scope.sexModes.undefined) $scope.sexMode = null;
        applyFilters($scope.from, $scope.to);
    }

    function setActiveCurve(ev) {
        if (ev == null || ev.Bird == null) {
            $scope.curveId = "";
            compareGraph.addCurve(null, $scope.sensor !== $scope.sensors.weightMale);
            return;
        }
        var curveId = null;
        if ($scope.sensor === $scope.sensors.weightMale) {
            if (ev.SexMode === $scope.sexModes.male) curveId = ev.Bird.MaleCurveId;
            if (ev.SexMode === $scope.sexModes.female) curveId = ev.Bird.FemaleCurveId;
            if (ev.Bird.FemaleCurveId != null && ev.SexMode === $scope.sexModes.mixed) curveId = ev.Bird.FemaleCurveId;
            if (ev.Bird.MaleCurveId != null && ev.SexMode === $scope.sexModes.mixed) curveId = ev.Bird.MaleCurveId;
            if (ev.Bird.UndefinedCurveId != null && ev.SexMode === $scope.sexModes.undefined) curveId = ev.Bird.UndefinedCurveId;
        } else {
            if ($scope.sensor === $scope.sensors.humidity) curveId = ev.Bird.HumidityCurveId || "";
            if ($scope.sensor === $scope.sensors.temperature) curveId = ev.Bird.TemperatureCurveId || "";
            if ($scope.sensor === $scope.sensors.co2) curveId = ev.Bird.Co2CurveId || "";
        }

        if (curveId != null) {
            $scope.curveId = curveId.toString();
            $scope.showCurve();
        }
    }

    $scope.showSensor = function (sensor) {
        $scope.sensor = sensor;
        if ($scope.sensor.type !== $scope.types) {
            $scope.type = vSensor.types.sensor.average;
        }
        if ($scope.sensor.type === $scope.types && $scope.type === vSensor.types.sensor.value) {
            $scope.type = vSensor.types.weight.average;
        }
        compareGraph.forceRedraw();
        $scope.drawFitCurve = true;
        hasData();
        setActiveCurve($scope.fitEvent || $scope.events.activePreview || $scope.events.pastPreview);
    }

    $scope.show = function (type) {
        $scope.type = type;
        compareGraph.forceRedraw();
        drawGraph();
    }
    $scope.moveGraph = false;
    $scope.graphMove = function () {
        $scope.moveGraph = !$scope.moveGraph;
        if (compareGraph.setMove) compareGraph.setMove($scope.moveGraph);
    }

    $scope.graphFit = function () {
        $scope.fitEvent = null;
        if (compareGraph.setFit) compareGraph.setFit();
    }



    $scope.graphFitTo = function (ev) {
        if ($scope.fitEvent !== ev) {
            $scope.fitEvent = ev;
            if (compareGraph.setFitTo) compareGraph.setFitTo(ev);
            setActiveCurve(ev);
        } else {
            $scope.fitEvent = null;
            if (compareGraph.setFitTo) compareGraph.setFitTo(ev);
            if (compareGraph.setFit) compareGraph.setFit();
        }
    }

    $scope.scrollCurve = function () {
        var curves = $scope.curves.filter($scope.isCurveVisible);
        if (curves.length === 0) return;
        var index = curves.indexOf($scope.curve);
        var dIndex = index - $scope.delta;
        if (dIndex === curves.length) return;
        if (dIndex < 0) {
            $scope.curve = null;
            $scope.curveId = "";
        }
        $scope.curve = curves[dIndex];
        if ($scope.curve) $scope.curveId = $scope.curve.Id.toString();
        compareGraph.addCurve($scope.curve);
    }

    $scope.showCurve = function () {
        $scope.curve = $scope.curves.find(function (c) { return c.Id.toString() === $scope.curveId });
        compareGraph.addCurve($scope.type === $scope.types.average ? $scope.curve : null, $scope.sensor !== $scope.sensors.weightMale);
    }

    $scope.changeTimeSize = function (timeSize) {
        $scope.timeSize = timeSize;
        compareGraph.forceRedraw();
        hasData();
    }


    function loadLastForActive() {
        compare.filterLastData(
              $scope.events.active,
              1440,
              vSensor.sensors.weightMale.typeName,
              vSensor.sensors.weightMale.type.list()
          ).success(function (data) {
              data.forEach(function (d) {
                  var ev = $scope.events.active.find(function (a) {
                      return a.Id.toString() === d.GroupId;
                  });
                  if (ev == null) return;
                  ev.last = d;
              });

          });
    }

    compare.get().success(function (data) {
        $scope.allEvents = data.Events;
        $scope.curves = data.Curves;

        $scope.events.active = $scope.allEvents.filter(function (e) { return e.State === vEnums.eventStates.active });
        $scope.events.past = $scope.allEvents.filter(function (e) { return e.State === vEnums.eventStates.closed });

        $scope.events.activePreview = $scope.events.active[0];
        $scope.events.pastPreview = $scope.events.past[0];

        $scope.timeSizes = data.TimeSizes;
        $scope.timeSize = data.TimeSizes[1];
        $scope.allEvents.forEach(function (ev, i) {
            ev.Color = $colors.getByIndex(i);
        });
        hasData();
        loadLastForActive();
    });

    $scope.getSelected = function () {
        if ($scope.allEvents == null) return [];
        var events = $scope.events.selected.slice();
        if (events.indexOf($scope.events.pastPreview) === -1 && $scope.events.pastPreview && $scope.events.activeEnabled) events.unshift($scope.events.pastPreview);
        if (events.indexOf($scope.events.activePreview) === -1 && $scope.events.activePreview && $scope.events.pastEnabled) events.unshift($scope.events.pastPreview);
        return events;
    }

    $scope.getPosition = function (state) {
        if (state === -1) {
            if ($scope.slectedPosition < 0) $scope.slectedPosition = 0;
            if ($scope.slectedPosition > $scope.events.allSelected.length - 1) $scope.slectedPosition = $scope.events.allSelected.length - 1;

            return -$scope.slectedPosition * 57;
        }
        if ($scope.allEvents == null) return 0;
        var index = 0;
        if (state === vEnums.eventStates.active) {
            index = $filter('filter')($scope.events.active, $scope.searchText).indexOf($scope.events.activePreview);
        }
        if (state === vEnums.eventStates.closed) {
            index = $filter('filter')($scope.events.past, $scope.searchText).indexOf($scope.events.pastPreview);
        }
        return index === -1 ? 0 : 33 * index;
    }

    $scope.isCurveVisible = function (curve) {
        if ($scope.sensor === $scope.sensors.weightMale && vEnums.curveTypes.isWeight(curve.Type)) return true;
        return $scope.sensor.curveType === curve.Type;
    }

    $scope.filterByText = function (state) {
        return function (ev) {
            var index = 0;
            if ($scope.searchText) index = ev.Name.toLowerCase().indexOf($scope.searchText.Name.toLowerCase());
            return ev.State === state && index !== -1;
        }
    }

    $scope.swipeAction = function (delta) {
        $scope.delta = delta;
        $scope.showEvent();
    }

    $scope.setPreview = function (state) {
        if (state === vEnums.eventStates.active) $scope.events.activeEnabled = !$scope.events.activeEnabled;
        if (state === vEnums.eventStates.closed) $scope.events.pastEnabled = !$scope.events.pastEnabled;
        hasData();
    }

    $scope.hasPreview = function (state) {
        if (state === vEnums.eventStates.active) return $scope.events.activeEnabled;
        if (state === vEnums.eventStates.closed) return $scope.events.pastEnabled;
        return false;
    }

    $scope.loading = false;

    $scope.slectedPosition = 0;
    $scope.move = function (state, delta) {
        if (state === -1) {
            $scope.slectedPosition = $scope.slectedPosition - delta;
            return;
        }
        $scope.delta = delta;
        $scope.previewEvent(state);
    }

    $scope.moveTop = function (state) {
        if (state === -1) {
            $scope.slectedPosition = 0;
            return;
        }
        if (state === vEnums.eventStates.active) $scope.events.activePreview = $scope.events.active[0];
        if (state === vEnums.eventStates.closed) $scope.events.pastPreview = $scope.events.past[0];
        hasData();
    }

    $scope.moveBottom = function (state) {
        if (state === -1) {
            $scope.slectedPosition = $scope.events.allSelected.length - 1;
            return;
        }
        if (state === vEnums.eventStates.active) $scope.events.activePreview = $scope.events.active[$scope.events.active.length - 1];
        if (state === vEnums.eventStates.closed) $scope.events.pastPreview = $scope.events.past[$scope.events.past.length - 1];
        hasData();
    }

    $scope.scrollSelected = function () {
        $scope.slectedPosition = $scope.slectedPosition - $scope.delta;
    }

    $scope.$watch("searchText", function () {
        $scope.delta = 0;
        if ($scope.allEvents == null) return;
        $scope.previewEvent(vEnums.eventStates.active);
        $scope.previewEvent(vEnums.eventStates.closed);
    }, true);

    $scope.previewEvent = function (state) {
        if ($scope.loading) return;
        if ($scope.slectedPosition >= $scope.events.allSelected.length) $scope.slectedPosition = 0;
        var events = [];
        var event = null;
        if (state === vEnums.eventStates.active) {
            events = $scope.events.active;
            event = $scope.events.activePreview;
        }
        if (state === vEnums.eventStates.closed) {
            events = $scope.events.past;
            event = $scope.events.pastPreview;
        }
        events = $filter('filter')(events, $scope.searchText);
        var index = 0;
        if (event != null) {
            index = events.indexOf(event);
            if (index === -1) {
                index = 0;
            } else {
                index = index - $scope.delta;
                if (index < 0) index = 0;
                if (index > events.length - 1) index = events.length - 1;
            }
        }

        if (state === vEnums.eventStates.active) $scope.events.activePreview = events[index];
        if (state === vEnums.eventStates.closed) $scope.events.pastPreview = events[index];
        hasData();
    }


    $scope.$watch("events.activePreview", function (newEv) {
        if ($scope.fitEvent != null) return;
        if ($scope.events.activeEnabled) setActiveCurve(newEv);
    });


    $scope.$watch("events.pastPreview", function (newEv) {
        if ($scope.fitEvent != null) return;
        if ($scope.events.pastEnabled) setActiveCurve(newEv);
    });

    $scope.showEvent = function (event, isSelected) {
        if (event == null) return;
        if (event === $scope.events.activePreview && !isSelected) {
            $scope.events.activeEnabled = false;
        } else if (event === $scope.events.pastPreview && !isSelected) {
            $scope.events.pastEnabled = false;
        } else {
            vArray.toggle($scope.events.selected, event);
        }

        hasData();
    }

    var graphPromise = null;

    function resizeGraph() {
        if (graphPromise != null) $timeout.cancel(graphPromise);
        graphPromise = $timeout(draw, 500);
    }

    window.addEventListener('resize', resizeGraph);
    $scope.$watch("hideSubmenu", resizeGraph);

}])
.directive('ngMouseWheel', function () {
    return function (scope, element, attrs) {
        element.bind("DOMMouseScroll mousewheel onmousewheel", function (ev) {
            var event = window.event || ev; // old IE support
            var delta = Math.max(-1, Math.min(1, (event.wheelDelta || -event.detail)));
            scope.$apply(function () {
                scope.delta = delta;
                scope.$parent.delta = delta;
                scope.$eval(attrs.ngMouseWheel);
            });

            event.returnValue = false;

            if (event.preventDefault) {
                event.preventDefault();
            }
        });
    };
});