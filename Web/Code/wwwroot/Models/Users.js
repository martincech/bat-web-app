﻿
app.controller('usersCtrl', ["$scope", "vLoader", "vValid", "users", "vName", "vMessages",
    function ($scope, vLoader, vValid, users, vName, vMessages) {

        //#region DELET & CREATE USER
        $scope.deleteArray = [];
        $scope.operations = {
            del: "delete"
        }
        $scope.ok = null;
        $scope.cancel = function () {
            $scope.deleteArray = [];
            $scope.state.operation = null;
        }
        $scope.state = { operation: null };

        $scope.delete = function () {
            $scope.ok = $scope.delete;
            $scope.state.operation = $scope.state.operation ? null : $scope.operations.del;
            if (!$scope.deleteArray.length) return;

            $scope.users = $scope.users.filter(function (item) {
                return $scope.deleteArray.indexOf(item.Id) === -1 && item.Id.indexOf("NEW") === -1;
            });
            $scope.deleteArray = $scope.deleteArray.filter(function (user) {
                return user.indexOf("NEW") === -1;
            });
            users.delete($scope.deleteArray).success(function () {
                $scope.users = $scope.users.filter(function (user) {
                    return $scope.deleteArray.indexOf(user.Id) === -1;
                });
                $scope.deleteArray = [];
            });
        }
        //#endregion

        //#region CREATE AND UPDATE USER
        $scope.changeName = function () {
            $scope.editingName = !$scope.editingName;
            if (!$scope.editingName) $scope.save();
        }

        $scope.changeEmail = function () {
            $scope.editEmail = !$scope.editEmail;
            if (!$scope.editEmail) $scope.save();
        }

        $scope.changePhone = function () {
            $scope.editPhone = !$scope.editPhone;
            if (!$scope.editPhone) $scope.save();
        }

        $scope.changeJob = function () {
            $scope.editJob = !$scope.editJob;
            if (!$scope.editJob) $scope.save();
        }

        $scope.assignDevice = function (device) {
            var index = $scope.activeUser.Devices.indexOf(device.Uid);
            if (index === -1) {
                $scope.activeUser.Devices.push(device.Uid);
            } else {
                $scope.activeUser.Devices.splice(index, 1);
            }
            $scope.save();
        }

        $scope.houseActive = function (house) {
            return house.Devices.filter(function (deviceId) {
                var device = $scope.devices.find(function (d) { return d.Id === deviceId });
                if (device == null) return false;
                return $scope.activeUser.Devices.indexOf(device.Uid) !== -1;
            }).length > 0;
        }

        function assignHouse(house, isActive) {
            house.Devices.forEach(function (deviceId) {
                var device = $scope.devices.find(function (d) { return d.Id === deviceId });
                if (device == null) return;
                var index = $scope.activeUser.Devices.indexOf(device.Uid);
                if (isActive && index !== -1) $scope.activeUser.Devices.splice(index, 1);
                if (!isActive && index === -1) $scope.activeUser.Devices.push(device.Uid);
            });
        }

        $scope.assignHouse = function (house) {
            var isActive = $scope.houseActive(house);
            assignHouse(house, isActive);
            $scope.save();
        }

        $scope.farmActive = function (farm) {
            if (farm == null || farm.Houses.length === 0) return false;
            return $scope.houses.filter(function (house) {
                if (house.FarmId !== farm.Id) return false;
                return $scope.houseActive(house);
            }).length > 0;
        }

        $scope.assignFarm = function (farm) {
            var isActive = $scope.farmActive(farm);
            $scope.houses.forEach(function (house) {
                if (house.FarmId !== farm.Id) return;
                assignHouse(house, isActive);
            });
            $scope.save();
        }


        $scope.fileUploadCallback = function (e, image) {
            var cropper = e.target.parentElement.parentElement.getElementsByTagName("img");
            cropper[0].src = image;
        }

        $scope.save = function () {
            function emailExists() {
                vLoader.loading(false);
                $scope.activeUser.EmailExist = true;
            }
            $scope.activeUser.EmailExist = false;
            $scope.activeUser.InvalidEmail = false;
            if (!vValid.email.isValid($scope.activeUser.Email)) {
                $scope.activeUser.InvalidEmail = true;
                return;
            }
            vLoader.loading(true);
            if ($scope.activeUser.IsNew) {
                users.create($scope.activeUser)
                    .success(function (user) {
                        vLoader.loading(false);
                        var index = $scope.users.indexOf($scope.activeUser);
                        $scope.users.splice(index, 1);
                        $scope.users.unshift(user);
                        location.href = "/users/#/" + user.Id;
                    }).error(emailExists);
            } else {
                users.update($scope.activeUser).success(function () {
                    vLoader.loading(false);
                }).error(emailExists);
            }
        }

        $scope.emailChanged = function () {
            $scope.activeUser.EmailExist = false;
            if (!vValid.email.isValid($scope.activeUser.Email)) {
                $scope.activeUser.InvalidEmail = true;
            } else {
                $scope.activeUser.InvalidEmail = false;
            }
        }

        $scope.create = function () {
            if ($scope.users.filter(function (item) { return item.IsNew; }).length > 0) return;
            var name = vName.createUnique($scope.users, "New User");
            var user = { Email: "", Id: "NEW" + new Date().getTime(), Name: name, Role: "Worker", Devices: [], IsNew: true };
            $scope.users.unshift(user);
            $scope.editEmail = true;
            setTimeout(function () { location.href = "/users/#/" + user.Id; });
        }
        //#endregion


        $scope.loginAs = function () {
            users.loginAs($scope.activeUser.Id).success(function () {
                setTimeout(function () { window.location = "/compare"; });
            });
        }

        $scope.sendNewPass = function (event) {
            if (event.target.className.indexOf("disabled") !== -1) return;
            event.target.classList.add("disabled");
            users.newPassword($scope.activeUser.Id).success(function () {
                $scope.activeUser.Invited = true;
                setTimeout(function () { event.target.classList.remove("disabled") }, 2000);
            });
        }

        $scope.isValidPhone = function (phoneNumber) {
            if (phoneNumber == null || phoneNumber === "") return true;
            if (phoneNumber.length < 4 || phoneNumber.length > 19) return false;
            var regex = new RegExp("^\\+[0-9]*$");
            return regex.test(phoneNumber);
        }

        //#region PAGE INIT
        function filter() {
            var hash = window.location.hash.split("/");
            if (hash) $scope.activeUser = $scope.users.find(function (user) {
                return user.Id === hash[hash.length - 1];
            });
            if ($scope.activeUser == null) return;
            $scope.activeUser.InvalidEmail = !vValid.email.isValid($scope.activeUser.Email);
        }

        window.addEventListener('hashchange', function () {
            filter();
            $scope.$apply();
        });

        $scope.$on("treeDataSource", function (ev, argsO) {
            var args = angular.copy(argsO);
            $scope.houses = args.houses;
            $scope.farms = args.farms;
            $scope.devices = args.devices;
            $scope.houses.forEach(function (house) {
                if (house.FarmId == null) return;
                house.Farm = $scope.farms.find(function (f) { return house.FarmId === f.Id });
                house.filter = house.Farm ? house.Farm.Name + " " + house.Name : house.Name;
            });

            $scope.devices.forEach(function (device) {
                if (device.HouseId == null) return;
                device.House = $scope.houses.find(function (h) { return h.Id === device.HouseId });
                device.filter = device.House ? device.House.filter + " " + device.Name : device.Name;
            });
        });

        users.get().success(function (data) {
            $scope.users = data.Users;
            $scope.companyId = data.CompanyId;
            $scope.images = data.Images;
            filter();
        });
        //#endregion
    }]);