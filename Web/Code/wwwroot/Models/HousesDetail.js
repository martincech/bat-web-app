﻿app.controller('housesDetailCtrl', [
    "$scope", "vEnums", "vLoader", "vCookies", "sensors", "vSensor", "sensorGraph", "vDevice", "vDate", "houses", "vArray", "vExport","vStatTable",
    function ($scope, vEnums, vLoader, vCookies, sensors, vSensor, sensorGraphFactory, vDevice, vDate, houses, vArray, vExport, vStatTable) {

        $scope.decimals = vCookies.units.decimals.get();
        $scope.sexModes = vEnums.sexModes;
        $scope.eventStates = vEnums.eventStates;
        $scope.types = vSensor.types;
        $scope.sensors = vSensor.sensors;
        var sensorGraph = sensorGraphFactory.create();

        //GRAPH
        $scope.graphParams = {};

        $scope.toggleMove = function() {
            $scope.moveEnabled = !$scope.moveEnabled;
            sensorGraph.setMove($scope.moveEnabled);
        };

        $scope.timeAreas = {
            day: new Date(new Date() - 24 * 60 * 60 * 1000),
            week: new Date(new Date() - 7 * 24 * 60 * 60 * 1000),
            all: null
        }

        $scope.setTimeArea = function(area) {
            $scope.timeArea = area;
            sensorGraph.setFit($scope.timeArea);
        }

        $scope.graphFit = function() {
            sensorGraph.setFit($scope.timeArea);
        }

        $scope.canStartEvent = function () {
            if ($scope.device == null) return true;
            if ($scope.event == null && $scope.device.Devices.length > 0) return true;
            return false;
        }

        $scope.changeName = function() {
            $scope.editName = !$scope.editName;
            if ($scope.editName) {
                $scope.device.originalName = $scope.device.Name;
                return;
            }
            if ($scope.device.Name === "" || $scope.device.Name === $scope.device.originalName) {
                $scope.device.Name = $scope.device.originalName;
                return;
            }
            houses.changeName($scope.device.Id, $scope.device.Name);
        }

//VIEW
       
        $scope.samplesPage = [];
         function showTable() {
             var samples = vSensor.findSamples($scope.sampleGroup, $scope.sensor.name).slice();
            samples.reverse();
            $scope.samples = samples;
         }


        $scope.getValue = function(statRow, type, decimals, setToFloor) {
            if ($scope.sampleGroup == null) return null;
            var index = $scope.sampleGroup.Types.indexOf(type);
            if (index === -1 || statRow[index] == null) return null;
            if (setToFloor) return Math.floor(statRow[index]);
            if (!statRow[index].toFixed) return statRow[index];
            return statRow[index].toFixed(decimals);
        }

        $scope.parseDate = vDate.parse;

        $scope.sampleGroups = [];
        $scope.sampleGroup = null;

        function loadData() {
            vLoader.loading(true);
            if ($scope.device != null && $scope.event != null)
                houses.filterData(
                    $scope.device.Id,
                    $scope.timeSize.Minutes,
                    [$scope.sensor.typeName],
                    $scope.sensor.type.list(),
                    $scope.event.From,
                    $scope.event.To).success(function(sampleGroups) {
                    sampleGroups.forEach(function(sampleGroup) {
                        $scope.sampleGroups.push(sampleGroup);
                    });
                    $scope.sampleGroup = sampleGroups[0];
                    $scope.view();
                    vLoader.loading(false);
                });
        }

        function setSampleGroup() {
            $scope.sampleGroup = vSensor.findGroups(
                $scope.sampleGroups,
                $scope.device.Uid,
                $scope.timeSize.Minutes,
                $scope.sensor.name)[0];
            return $scope.sampleGroup;
        }

        $scope.setTimeSize = function(timeSize) {
            $scope.timeSize = timeSize;
            if (setSampleGroup()) {
                $scope.view();
            } else {
                loadData();
            }
        }

        $scope.showSensor = function(sensor) {
            if ($scope.sensor === sensor) return;
            $scope.sensor = sensor;
            setSampleGroup();
            if ($scope.sampleGroup == null || $scope.sampleGroup.length === 0) {
                loadData();
            } else {
                $scope.view();
            }
            if ($scope.sensor.type === vSensor.sensors.weightMale.type) {
                $scope.showStat($scope.types.weight.average,true);
            } else {
                $scope.showStat($scope.sensor);
            }
        }

        $scope.isVisible = function() {
            return true;
        }

        $scope.print = function () {
            var pg = sensorGraphFactory.create();
            pg.print(
                    [$scope.sampleGroup],
                    $scope.sensor.type === vSensor.types.weight ? vSensor.types.weight.average : vSensor.types.sensor.average,
                    [$scope.event],
                    $scope,
                    $scope.sensor,
                    [vSensor.types.sensor.max, vSensor.types.sensor.min, vSensor.types.weight.gain],
                    $scope.timeArea);
            var curve = $scope.curves.find(function (curve) {
                return $scope.sensor.curveType === curve.Type;
            });
            pg.addCurve(curve, $scope.sensor.type === vSensor.types.weight);

        }

        $scope.views = {
            values: function() {},
            graph: function() {
                if ($scope.event == null || $scope.event.State !== 0) return;
                setTimeout(function() {
                    sensorGraph.draw(
                        [$scope.sampleGroup],
                        $scope.sensor.type === vSensor.types.weight ? vSensor.types.weight.average : vSensor.types.sensor.average,
                        [$scope.event],
                        $scope,
                        $scope.sensor,
                        [vSensor.types.sensor.max, vSensor.types.sensor.min, vSensor.types.weight.gain],
                        $scope.timeArea);
                    var curve = $scope.curves.find(function(curve) {
                        return $scope.sensor.curveType === curve.Type;
                    });
                    sensorGraph.addCurve(curve, $scope.sensor.type === vSensor.types.weight);
                }, 0);
            },
            data: showTable,
            sensors: function() {

            }
        }
        //$scope.view = $scope.views.values;
        $scope.showView = function(showView) {
            $scope.view = showView;
            showView();
        }

        $scope.export = function() {
            vExport.sensorToCsv($scope.sensor, $scope.sampleGroup);
        }


        var creatingEvent = false;
        $scope.createEvent = function() {
            if ($scope.device.Devices.length === 0 || creatingEvent) return;
            creatingEvent = true;
            houses.createEvent($scope.device.Id)
                .success(function(data) {
                    creatingEvent = false;
                    window.location = "/calendar/#/" + data.Id;
                });
        }

        $scope.isVisible = function(sensor) {
            if ($scope.activeSensors == null) return false;
            return $scope.activeSensors.indexOf(sensor) !== -1;
        }

        function setSensorsLastValues(data) {
            $scope.activeSensors.forEach(function(sensor) {
                var sg = vSensor.findGroups(data.Last, null, null, sensor.name)[0];
                if (sg == null) return;
                var last = vSensor.findSamples(sg, sensor.name)[0];
                if (last == null) return;
                var timeStampIndex = sg.Types.indexOf(vSensor.types.time.time);
                var averageIndex = sg.Types.indexOf(vSensor.types.sensor.average);
                var dayIndex = sg.Types.indexOf(vSensor.types.time.day);
                var curve = data.Curves.find(function(c) { return c.Type === sensor.curveType });
                sensor.model.date = vDate.parse(last[timeStampIndex]);
                sensor.model.value = Number(last[averageIndex].toFixed(sensor.type.decimals));

                var point = null;
                var points = [];
                if (curve != null) {
                    points = curve.CurvePoints.filter(function(p) { return p.Day <= last[dayIndex] });
                }
                point = points[points.length - 1];
                if (sensor.type === vSensor.types.weight) {
                    if (point) {
                        sensor.model.max = (((100 + point.Max) / 100) * point.Value).toFixed($scope.decimals);
                        sensor.model.min = (((100 - point.Min) / 100) * point.Value).toFixed($scope.decimals);
                    }
                } else {
                    //point = points[points.length - 1];
                    if (point) {
                        sensor.model.max = point.Max.toFixed(0);
                        sensor.model.min = point.Min.toFixed(0);
                    }
                }

            });
        }

        function setAvailableSensors() {
            $scope.activeSensors = $scope.activeSensors.filter(function(sensor) {
                if (sensor.type === vSensor.types.sensor) return true;
                if ($scope.event.SexMode === vEnums.sexModes.mixed && sensor.inMixed) return true;
                return $scope.event.SexMode === sensor.sex;
            });
            if ($scope.activeSensors.indexOf($scope.sensor) === -1) $scope.sensor = $scope.activeSensors[0];
            if ($scope.sensor.type === vSensor.types.weight) {
                $scope.weightSensor = $scope.sensor;
            } else {
                $scope.weightSensor = $scope.sensors.weightFemale;
            }
        }

        $scope.showStat = function (type, isWeight) {
            if (type === $scope.type) return;
            $scope.type = type;
            isWeight ? vStatTable.showWeightStat($scope.devices, $scope.type) : vStatTable.showStat($scope.devices, $scope.type);
            $scope.isReverse = !$scope.isReverse;
            //$scope.sort($scope.sortTypes.value);
        }

        function loadDevice() {
            var loc = window.location.hash.match(/\d+/g);
            if (loc == null) {
                window.location = "/houses/";
                return;
            }
            var id = loc[0];

            houses.detail(id).success(function(data) {
                var today = new Date();
                $scope.sampleGroups = [];
                if (data == null || data.House == null) {
                    window.location = "/houses/";
                    return;
                };
                $scope.curves = data.Curves;
                $scope.bird = data.Event ? data.Event.Bird : null;
                $scope.birds = [data.Bird];
                $scope.device = data.House;
                $scope.allDevices = data.Devices;
                $scope.devices = data.Devices.filter(function (d) { return $scope.device.Devices.indexOf(d.Id) !== -1 });
                $scope.activeSensors = vDevice.devices.none.sensors.slice();
                $scope.farmm = data.Farm; 
                vDevice.reset($scope.activeSensors);
                $scope.event = data.Event;
                (((data.Last || [])[0] || {}).Sensors || []).forEach(function (s) { sensors[s] = true });
                $scope.hasOtherData = vSensor.hasOtherData({ Event: data.Event, last: sensors });
                if ($scope.event == null || $scope.event.State !== vEnums.eventStates.active) $scope.view = $scope.views.values;
                vLoader.loading(false);
                if ($scope.event == null || $scope.device.Devices == null || $scope.device.Devices.length === 0 || $scope.event.State !== vEnums.eventStates.active) return;
                $scope.timeSizes = $scope.timeSizes || data.TimeSizes.sort(function(a, b) { return b.Minutes - a.Minutes });
                $scope.timeSize = $scope.timeSize || $scope.timeSizes[0];
                setAvailableSensors();
                setSensorsLastValues(data);
                loadData();
                var last = vSensor.mapLast(data.WeightStats, data.Stats);
                $scope.devices.forEach(function(device) {
                    device.last = last[device.Uid];
                    device.Event = data.Event;
                    if (device.Event) {
                        device.Event.ActualDay = vDate.daysDiff(new Date(device.Event.From), today);
                        device.Event.TotalDays = vDate.daysDiff(new Date(device.Event.From), new Date(device.Event.To));
                    }
                });
                if ($scope.view == null) {
                    $scope.view = $scope.views.graph;
                }

                if ($scope.sensor.type === vSensor.sensors.weightMale.type) {
                    $scope.showStat($scope.types.weight.average,true);
                } else {
                    $scope.showStat($scope.sensor);
                }
            });
        }

        function initPage() {
            vLoader.loading(true);
            $scope.data = [];
            $scope.samples = [];
            loadDevice();
        }

        window.addEventListener('hashchange', function() {
            initPage();
        });

        loadDevice();
        vLoader.loading(true);
    }
]);