﻿app.filter('pagging', function () {
    return function (items, params) {
        var res = items.filter(function (item, index) {
            return index >= params.from && index < params.to;
        });
        return res;
    };
});
app.controller('curvesCtrl', ["$scope", "$http", "$d3", "$timeout", "vHash", "vCurve", "vValid", "vEnums", "vArray", "vFile", "$modal", "vLoader", "curves", "vName",
    function ($scope, $http, $d3, $timeout, vHash, vCurve, vValid, vEnums, vArray, vFile, $modal, vLoader, curves, vName) {


        $scope.state = {
            operation: null,
            newCurveType: 0
        }

        $scope.oneAtATime = true;

        $scope.curveTypes = vEnums.curveTypes;
        $scope.curveType = undefined;

        $scope.curves = null;
        $scope.activeCurve = null;

        $scope.downloadArray = [];
        $scope.deleteArray = [];

        $scope.oldName = "";
        $scope.editingName = false;

        $scope.error = { day: true };

        //#region Apply hash

        var filter = [
            ["", undefined],
            ["weight", $scope.curveTypes.weight],
            ["temperature", $scope.curveTypes.temperature],
            ["humidity", $scope.curveTypes.humidity],
            ["co2", $scope.curveTypes.co2]
        ];

        function applyHash() {
            var hash = vHash.parseHash(filter);
            $scope.path = hash[0];
            $scope.curveType = hash[1];

            var hashes = window.location.hash.match(/\d+/g);
            $scope.hash = "";
            $scope.newPoint = null;
            if (hashes) $scope.hash = hashes.length === 2 ? hashes[1] : hashes[0];
            if (hashes && hashes.length === 1 && window.location.hash.indexOf("co2") !== -1) return;
            $scope.curves.forEach(function (item) {
                if ($scope.hash === item.Id.toString()) $scope.activeCurve = item;
                item.expanded = $scope.hash === item.Id.toString();
            });

            if ($scope.activeCurve) {
                setTimeout(function () { document.getElementById($scope.activeCurve.Id).scrollIntoView(); }, 0);
            }
        }

        function filterByHash() {
            $scope.editingName = false;
            $scope.activeCurve = null;
            $scope.item = null;
            applyHash();
        }

        window.addEventListener("hashchange", function () {
            filterByHash();
            $scope.$apply();
        });

        $scope.filterCurves = function (curve) {
            if ($scope.curveType == null) return true;
            if ($scope.curveType > 2) return $scope.curveType === curve.Type;
            if ($scope.curveType === -1 && (curve.Type < 3 || curve.Type === 5)) return true;
            return curve.Type < 3;
        }

        //#endregion

        $scope.saveCurveType = function (type) {
            if ($scope.activeCurve.Type === type) {
                $scope.activeCurve.Type = $scope.curveTypes.birdUndefined;
            } else {
                $scope.activeCurve.Type = type;
            }
            curves.update($scope.activeCurve);
        }

        $scope.saveCurveName = function () {
            $scope.editingName = !$scope.editingName;
            if ($scope.oldName === $scope.activeCurve.Name) return;
            curves.update($scope.activeCurve).then(function () {
                $scope.oldName = $scope.activeCurve.Name;
            });
        }

        window.addEventListener('resize', function (event) {
            if (!$scope.activeCurve) return;
            if ($scope.redraw) $scope.redraw();
        });

        $scope.createCurve = function (curve) {
            if (curve == null) {
                var type = $scope.curveType;
                if (type == null) type = $scope.state.newCurveType;
                curve = { Name: vName.createUnique($scope.curves, "New Curve"), Type: type, CurvePoints: [vCurve.getDefaultPoint(type), vCurve.getDefaultPoint(type, true)] };
            }

            if (curve.Type === $scope.curveTypes.weight) curve.Type = $scope.curveTypes.birdFemale;
            curves.create(curve).then(function (data) {
                $scope.state.operation = null;
                $scope.editEnable = true;
                setTimeout(function () {
                    data.data.expanded = true;
                    $scope.curves.unshift(data.data);
                    window.location.hash = $scope.path + data.data.Id;
                }, 0);
            });
        }

        function goToPoint(point) {
            $scope.item = point;
        }

        function addPoint(newPoint) {
            var point = angular.copy(newPoint);
            $scope.activeCurve.CurvePoints.push(point);
            $scope.activeCurve.CurvePoints.sort(function (a, b) { return a.Day - b.Day });

            return point;
        }

        $scope.editCurve = function () {
            $scope.editEnable = !$scope.editEnable;
            if ($scope.editEnable) {
                $scope.newPoint = null;
                $scope.activeCurve.CurvePoints = $scope.activeCurve.CurvePoints.filter(function (p) {
                    return !p.new;
                });
            }

            if (!$scope.editEnable) {
                $scope.updatePoint();
            }
        }

        var dayChangedTimeout = null;

        $scope.dayChanged = function () {
            $timeout.cancel(dayChangedTimeout);
            if (this.day == null) return;
            var day = this.day;
            dayChangedTimeout = $timeout(function () {
                $scope.newPoint.Day = day;
            }, 500);
        }

        $scope.selectPoint = function (point) {
            $scope.newPoint = angular.copy(point);
            $scope.validateDay();
        }

        $scope.$watch("newPoint.Day", function (value) {
            if (value == null) return;
            $scope.day = value;
            $scope.activeCurve.CurvePoints = $scope.activeCurve.CurvePoints.filter(function (p) {
                return !p.new;
            });

            var point = $scope.activeCurve.CurvePoints.find(function (p) { return p.Day === value });
            if (point == null) {
                point = addPoint($scope.newPoint);
                point.new = true;
            }
            goToPoint(point);
        });

        $scope.cancelEdit = function () {
            var index = $scope.activeCurve.CurvePoints.indexOf($scope.item);

            $scope.activeCurve.CurvePoints = $scope.activeCurve.CurvePoints.filter(function (p) {
                return !p.new;
            });
            if (index === -1) index = 0;
            if (index >= $scope.activeCurve.CurvePoints.length) index = $scope.activeCurve.CurvePoints.length - 1;
            goToPoint($scope.activeCurve.CurvePoints[index]);
            $scope.newPoint = null;
        }

        $scope.updatePoint = function () {
            if ($scope.newPoint == null) return;
            curves.addPoint($scope.activeCurve.Id, $scope.newPoint).success(function () {
                var point = $scope.activeCurve.CurvePoints.find(function (item) {
                    return item.Day === $scope.newPoint.Day;
                });
                if (point != null) {
                    point.Value = $scope.newPoint.Value;
                    point.Min = $scope.newPoint.Min;
                    point.Max = $scope.newPoint.Max;
                    if (point.new && $scope.activeCurve.CurvePoints.indexOf(point) === $scope.activeCurve.CurvePoints.length - 1) {
                        $scope.createPoint();
                    } else {
                        $scope.newPoint = null;
                    }
                    point.new = null;
                }
            });
        }

        $scope.createPoint = function () {
            if ($scope.activeCurve.CurvePoints.length === 0) {
                $scope.newPoint = vCurve.getDefaultPoint($scope.activeCurve.Type);
            } else {
                $scope.newPoint = angular.copy($scope.activeCurve.CurvePoints[$scope.activeCurve.CurvePoints.length - 1]);
                $scope.newPoint.Day = $scope.newPoint.Day + 1;
            }
            $scope.validateDay();
        }


        $scope.deletePoint = function (day) {
            var index = 0;
            curves.deletePoint($scope.activeCurve.Id, day).success(function () {
                $scope.activeCurve.CurvePoints = $scope.activeCurve.CurvePoints.filter(function (item, i) {
                    if (item.Day === day) index = i;
                    return item.Day !== day;
                });
                if (index >= $scope.activeCurve.CurvePoints.length) index = $scope.activeCurve.CurvePoints.length - 1;
                goToPoint($scope.activeCurve.CurvePoints[index]);
                $scope.validateDay();
            });
        }

        $scope.validateDay = function () {
            if ($scope.activeCurve == null || $scope.activeCurve.CurvePoints == null) {
                $scope.error.day = true;
                return;
            }
            $scope.error.day = !vValid.curves.isValid($scope.activeCurve.Type, $scope.newPoint);
        }

        //#endregion

        //#region Operation Export/Delete

        $scope.cancel = function () {
            $scope.deleteArray = [];
            $scope.downloadArray = [];
            $scope.state.operation = null;
        }

        $scope.deleteCurve = function () {
            curves.delete($scope.deleteArray).success(function () {
                $scope.curves = $scope.curves.filter(function (curve) {
                    return $scope.deleteArray.indexOf(curve.Id) === -1;
                });
                if ($scope.curves.indexOf($scope.activeCurve) === -1) $scope.activeCurve = null;
                $scope.cancel();
            });
        }

        $scope.exportCurves = function () {
            var csv = vCurve.curvesToCsv($scope.curves, $scope.downloadArray);
            $scope.downloadArray = [];
            vFile.save("curves.csv", csv);
            $scope.cancel();
        }

        $scope.copy = function () {
            if (!$scope.activeCurve) return;
            var curve = angular.copy($scope.activeCurve);
            curve.Name += "-Copy";
            $scope.createCurve(curve);
            $scope.cancel();
        }

        $scope.operations = {
            create: $scope.createCurve,
            del: $scope.deleteCurve,
            exp: $scope.exportCurves
        }

        $scope.state.operation = null;
        $scope.state.newCurveType = $scope.curveTypes.birdFemale;


        $scope.$watch("state.operation", function (operation) {
            $scope.newLimit = null;
            if (operation == null) return;
            if (operation === $scope.operations.create && $scope.curveType != null) operation();
        });

        //#endregion
        curves.get()
            .success(function (data) {
                $scope.curves = data.Curves.reverse();
                filterByHash();
                vLoader.loading(false);
            });
        vLoader.loading(true);
    }]);
