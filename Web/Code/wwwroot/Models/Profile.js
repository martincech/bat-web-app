﻿"use strict";
app.controller('profileCtrl', ['$scope', 'profile', 'vEnums', function ($scope, profile, vEnums) {
    $scope.company = null;
    $scope.terminals = [];
    $scope.terminalUid = '';

    $scope.update = function () {
        profile.update($scope.company).success(function (data) {
        });
    }

    $scope.new = {
        name: '',
        uid: ''
    }

    $scope.addTerminal = function () {
        if ($scope.new.uid.length < 1 || $scope.terminalExist()) return;
        profile.addTerminal($scope.new.uid, $scope.new.name).success(function (data) {
            if (data.Id == null) {
                alert(data);
                return;
            }
            data.Type = "<i class='fa fa-television'></>";
            $scope.terminals.push(data);
            $scope.new.uid = "";
            $scope.new.name = "";
        }).error(function (d) {
            alert(d);
        });
    };

    $scope.terminalExist = function () {
        return $scope.terminals.find(function (item) { return item.Uid === $scope.new.uid }) != null;
    }

    function removeDevice(array) {
        var index = array.indexOf($scope.deleteDevice);
        $scope.deleteDevice = null;
        if (index === -1) return;
        array.splice(index, 1);
    }

    $scope.delete = function () {
        if ($scope.showDevices) {
            profile.deleteDevice($scope.deleteDevice.Uid).success(function () {
                removeDevice($scope.devices);
            });
        } else {
            profile.deleteTerminal($scope.deleteDevice.Uid).success(function () {
                removeDevice($scope.terminals);
            });
        }
    };

    $scope.del = function (device) {
        $scope.deleteDevice = device;
    }

    function setList(array) {
        if ($scope.showDevices) {
            $scope.devices = array;
            return;
        }
        $scope.terminals = array;
    }

    function getList() {
        if ($scope.showDevices) return $scope.devices;
        return $scope.terminals;
    }

    $scope.show = function (props) {
        $scope.col = props;
        var array = getList();
        array.forEach(function (a) {
            var val = a[props[0]];
            if (props[1]) {
                if (val != null) {
                    val = val[props[1]];
                } else {
                    val = null;
                }
            }
            a.Value = val;
        });
        $scope.sort.by();
    }

    function sortBy(propName, subProp) {
        var array = getList();
        var result = array.sort(function (a, b) {
            var valA = a[propName];
            var valB = b[propName];
            if (subProp && valA != null) valA = valA[subProp];
            if (subProp && valB != null) valB = valB[subProp];
            if (valA == null) return -1;
            if (valB == null) return 1;
            if (valA < valB) return -1;
            if (valA > valB) return 1;
            return 0;
        });
        if ($scope.sort.desc) result.reverse();
        setList(result);
    }

    $scope.cols = {
        type: ["Type"],
        uid: ["Uid"],
        terminal: ["Terminal", "Name"],
        house: ["House", "Name"]
    }

    $scope.col = $scope.cols.uid;

    $scope.sorts = {
        name: function () { sortBy($scope.cols.house[1]) },
        type: function () { sortBy($scope.cols.type[0]) },
        uid: function () { sortBy($scope.cols.uid[0]) },
        terminal: function () { sortBy($scope.cols.terminal[0], $scope.cols.terminal[1]) },
        house: function () { sortBy($scope.cols.house[0], $scope.cols.house[1]) },
        value: function () { sortBy("Value") }
    }

    $scope.sort = {
        desc: false,
        by: $scope.sorts.name
    }

    $scope.sortBy = function (sort) {
        if (sort === $scope.sort.by) {
            $scope.sort.desc = !$scope.sort.desc;
        }
        $scope.sort.by = sort;
        sort();
    }

    $scope.showTable = function (showDevices) {
        $scope.col = $scope.cols.uid;
        $scope.filter = null;
        $scope.showDevices = showDevices;
        $scope.sort.by();
        $scope.show($scope.col);
    }

    $scope.getSort = function (sort) {
        if ($scope.sort.by === sort) return $scope.sort.desc ? "sorting_desc" : "sorting_asc";
        return "sorting";
    }

    $scope.$on("treeDataSource", function (ev, argsO) {
        var args = angular.copy(argsO);
        $scope.houses = args.houses;
        $scope.farms = args.farms;
        $scope.devices = args.devices;
        $scope.terminals = args.terminals;
        $scope.terminals.forEach(function (t) {
            t.Type = "<i class='fa fa-television'></>";
        });
        $scope.devices.forEach(function (d) {
            d.Type = "<i class='fa " + vEnums.deviceType.getIcon(d.Type) + "'></>";
        });
        $scope.devices.forEach(function (d) {
            d.House = $scope.houses.find(function (h) { return d.HouseId === h.Id });
            d.Terminal = $scope.terminals.find(function (h) { return d.TerminalId === h.Id });
            d.Farm = $scope.farms.find(function (f) { return f.Houses.indexOf(d.HouseId) !== -1 });
        });

        profile.get().success(function (data) {
            $scope.company = data.Company;
            $scope.showTable(false);
        });
    });
}]);