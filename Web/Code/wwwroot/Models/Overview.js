﻿app.controller('overviewCtrl', ["$scope", "$http", "$timeout", "veit", "vLoader",
    function ($scope, $http, $timeout, veit, vLoader) {

        //CLICK & DRAG REGION

        $scope.models = {
            selected: null,
            templates: [
                {
                    type: "flock", 
                    id: 1, 
                    icon: "object-group",
                    columns: [[], []]
                },
                {                    
                    type: "text",
                    id: 2,
                    icon: "commenting"
                },
                {
                    type: "gauge",
                    id: 3,                   
                    icon: "tachometer"
                },
                {
                    type: "progressbar",
                    id: 4,                   
                    icon: "tasks"
                }
            ],
            halls: {
                "Southwest": [
                    {
                        "type": "flock",
                        "id": 1,
                        "columns": [
                            [
                                {
                                    "type": "text",
                                    "id": "1"
                                },
                                {
                                    "type": "gauge",
                                    "id": "2"
                                }
                            ],
                            [
                                {
                                    "type": "progressbar",
                                    "id": "3"
                                }
                            ]
                        ]
                    },
                    {
                        "type": "text",
                        "id": "4"
                    },
                    {
                        "type": "gauge",
                        "id": "5"
                    },
                    {
                        "type": "text",
                        "id": "6"
                    }
                ],
                "Northeast": [
                    {
                        "type": "progressbar",
                        "id": 7
                    },
                    {
                        "type": "text",
                        "id": "8"
                    },
                    {
                        "type": "flock",
                        "id": "2",
                        "columns": [
                            [
                                {
                                    "type": "text",
                                    "id": "9"
                                },
                                {
                                    "type": "gauge",
                                    "id": "10"
                                },
                                {
                                    "type": "text",
                                    "id": "11"
                                }
                            ],
                            [
                                {
                                    "type": "text",
                                    "id": "12"
                                },
                                {
                                    "type": "flock",
                                    "id": "3",
                                    "columns": [
                                        [
                                            {
                                                "type": "text",
                                                "id": "13"
                                            }
                                        ],
                                        [
                                            {
                                                "type": "text",
                                                "id": "14"
                                            }
                                        ]
                                    ]
                                },
                                {
                                    "type": "text",
                                    "id": "15"
                                },
                                {
                                    "type": "text",
                                    "id": "16"
                                }
                            ]
                        ]
                    },
                    {
                        "type": "text",
                        "id": 16
                    }
                ]
            }
        };

        $scope.$watch('models.halls', function(model) {
            $scope.modelAsJson = angular.toJson(model, true);
        }, true);

}]);