﻿/*-- ANGULAR --*/
var app = angular.module('BatApp', [
    'ngSanitize',
    'mm.foundation',
    'sticky',
    //'dndLists',
    '720kb.datepicker',
    'imageCropper',
    //'angular-carousel',
    'ngScrollbar',
    'd3Module',
    'veitModule',
    'ngCookies'
]).config([
    "$locationProvider", function($locationProvider) {
        $locationProvider.html5Mode({
            rewriteLinks: false
        });
    }
]).run(["vTreeMenu", function(vTreeMenu) { vTreeMenu.load();}])