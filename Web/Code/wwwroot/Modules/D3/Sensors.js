﻿angular.module("d3Module")
.factory('sensorGraph', ['$d3', "$colors", "vDownSampling", "vDate", "$d3Utils", "vCurve", function ($d3, $colors, vDownSampling, vDate, $d3Utils, vCurve) {

    function create() {
        var move = false;
        var margin = { top: 0, bottom: 30, left: 0, right: 60 };

        var colors = $colors.colorList;

        function updateZoom(svg, x, y, draw) {
            $d3Utils.updateZoom(svg.select('g.events foreignObject canvas'), x, y, move ? draw : null);
            $d3Utils.updateZoom(svg.select('rect.zoom.x.box'), x, null, move ? draw : null);
            $d3Utils.updateZoom(svg.select('rect.zoom.y.box'), null, y, move ? draw : null);
        }

        function addZoomBox(svg, height, width) {
            $d3Utils.add.zoomBox(svg, width, 0, margin.right, height, "zoom y box");
            $d3Utils.add.zoomBox(svg, 0, height, width, margin.bottom, "zoom x box");
        }

        function addLayout(svg, width, height, sensor) {
            svg.attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom);

            $d3Utils.add.clipPath(svg, width, height);
            $d3Utils.add.axis(svg, width, 0, "y axis");
            $d3Utils.add.axis(svg, 0, height, "x axis");

            return $d3Utils.add.contentHolder(svg, "events");
        }

        var factory = {};


        factory.print = function (dataSource, property, events, $scope, sensor, tipValues, from) {
            var svg = $d3Utils.print(false, "sensorsDetailCtrl");

            if (dataSource == null || dataSource.length === 0) return;

            var width = 1050;
            var height = 680;
            move = true;
            factory.drawNS(dataSource, property, events, $scope, sensor, tipValues, from, svg, width, height);
        }

        factory.draw = function (dataSource, property, events, $scope, sensor, tipValues, from) {
            var svg = $d3.select("#print-svg");
            svg.selectAll("*").remove();

            if (dataSource == null || dataSource.length === 0) return;


            var width = svg.node().parentNode.clientWidth - margin.right - margin.left;
            var height = 400;
            factory.drawNS(dataSource, property, events, $scope, sensor, tipValues, from, svg, width, height);
        }

        factory.drawNS = function (dataSource, property, events, $scope, sensor, tipValues, from, svg, width, height) {


            var data = dataSource.slice();
            sensor.original = angular.copy(sensor.model);
            var x = $d3.time.scale()
                .range([0, width]);

            var y = $d3.scale.linear()
                .range([height, 0]);



            var xaxis = $d3.svg.axis().scale(x)
                .orient("bottom")
                .ticks(width / 150)
                .tickFormat($d3Utils.dateFormat);

            var yaxis = $d3.svg.axis().scale(y)
                .orient("right")
                .tickSize(-width)
                .tickPadding(10);

            var areaWeight = d3.svg.area()
                .x(function (d) { return x(d.TimeStamp); })
                .y0(function (d) { return y(((100 + d.Max) / 100) * d.Value); })
                .y1(function (d) { return y(((100 - d.Min) / 100) * d.Value); }).interpolate("linear");

            var areaSensor = d3.svg.area()
                .x(function (d) { return x(d.TimeStamp); })
                .y0(function (d) { return y(d.Min); })
                .y1(function (d) { return y(d.Max); }).interpolate("linear");

            var area = areaWeight;

            var curveLine = $d3.svg.line()
                .x(function (d) {
                    return x(d.TimeStamp);
                })
                .y(function (d) {
                    return y(d.Value);
                })
                .interpolate("linear");


            var line = $d3.svg.line()
                .x(function (d) {
                    return x(d[domain.x.index]);
                })
                .y(function (d) {
                    return y(d[domain.y.index]);
                })
                .interpolate("linear");


            var tips = [];
            if (tipValues != null) {
                tipValues.forEach(function (tv) {
                    var index = data[0].Types.indexOf(tv);
                    if (index !== -1) {
                        tips.push({ type: tv.toLowerCase(), index: index });
                    }
                });
            }

            var domain = $d3Utils.getDomain(dataSource, "timestamp", property, from, new Date());
            $d3Utils.setDomain(x, y, domain);

            var content = addLayout(svg, width, height, sensor);
            var curveLimit = content.append("g").attr("class", "curve");
            var canvas = $d3Utils.add.canvas(content, width, height);
            var canvasContext = canvas.node().getContext("2d");

            var tip = svg.append("g").attr("class", "mouse-tip");
            $d3Utils.add.line(tip, 0, 0, 0, 0, 1, colors.darkGrey, "pointer-line pointer-line-x");
            $d3Utils.add.line(tip, 0, 0, 0, 0, 1, colors.darkGrey, "pointer-line pointer-line-y");
            $d3Utils.add.circle(tip, 0, 0, 4, colors.red, "circle-point");

            var tipX = $d3Utils.add.foDiv(tip, "point-tip point-tip-x")[0];
            var tipY = $d3Utils.add.foDiv(tip, "point-tip point-tip-y")[0];
            var tipMouse = $d3Utils.add.foDiv(tip, "point-tip");
            var tipPoint = tipMouse[0];
            var tipContent = tipMouse[1];
            $d3Utils.add.div(tipContent, "point-value");

            tips.forEach(function (ttip) {
                $d3Utils.add.div(tipContent, "point-" + ttip.type);
            });



            var bisectDate = $d3.bisector(function (d) { return d[domain.x.index]; }).right;
            var bisectDay = $d3.bisector(function (d) { return d.TimeStamp.getTime() - 12 * 60 * 60 * 1000; }).left;

            canvas.on("mousemove", function () {
                tip.style("opacity", 0);
                if (sensor == null) return;

                var mouse = d3.mouse(this);
                var mDay = x.invert(mouse[0]);


                var sensorIndex = data[0]
                    .Sensors
                    .map(function (m) { return m.toLowerCase() })
                    .indexOf(sensor.name);

                if (sensorIndex === -1) {
                    sensorIndex = 0;
                };
                var sensorData = data[0].Values[sensorIndex];
                var values = sensorData[bisectDate(sensorData, mDay)];
                if (values == null) return;
                var yValue = values[domain.y.index];
                var xValue = values[domain.x.index];

                $d3Utils.add.setLine(tip, x(xValue), y(yValue), x(xValue), y(y.domain()[0]), ".pointer-line.pointer-line-x", 1);
                $d3Utils.add.setLine(tip, x(xValue), y(yValue), x(x.domain()[1]), y(yValue), ".pointer-line.pointer-line-y", 1);
                var min = null;
                var max = null;
                var curvePoints = curveLimit.selectAll("path.area").data()[0];
                if (curvePoints) {
                    var point = curvePoints.CurvePoints[bisectDay(curvePoints.CurvePoints, xValue) - 1];
                    if (point) {
                        if (sensor.typeName !== "weight") {
                            min = point.Min.toFixed(0);
                            max = point.Max.toFixed(0);
                        } else {
                            max = parseFloat((((100 + point.Max) / 100) * point.Value).toFixed(sensor.type.decimals));
                            min = parseFloat((((100 - point.Min) / 100) * point.Value).toFixed(sensor.type.decimals));
                        }
                    }
                }

                $d3Utils.move(tipPoint, x(xValue), y(yValue));

                tipContent.select(".point-value").text(yValue.toFixed(sensor.type.decimals));
                tips.forEach(function (ttip) {
                    var val = values[ttip.index];
                    if (val == null) return;
                    tipContent.select(".point-" + ttip.type).classed("negative", values[ttip.index] < 0).text(val.toFixed(sensor.type.decimals));
                });

                $d3Utils.move(tipX, x(xValue), y(y.domain()[0])).select("div").text(xValue.toLocaleString());
                $d3Utils.move(tipY, x(x.domain()[1]), y(yValue)).select("div").text(yValue.toFixed(1));

                $scope.$apply(function () {
                    sensor.model.date = values[1];
                    sensor.model.value = parseFloat(yValue.toFixed(sensor.type.decimals));
                    sensor.model.max = max || sensor.model.max;
                    sensor.model.min = min || sensor.model.min;
                });
                $d3Utils.move(tip.select("circle"), x(values[domain.x.index]), y(values[domain.y.index]));
                tip.style("opacity", 1);
            });

            canvas.on("mouseleave", function () {
                tip.style("opacity", 0);
                if (sensor.model == null) return;
                $scope.$apply(function () {
                    sensor.model.date = sensor.original.date;
                    sensor.model.value = sensor.original.value == null ? null : sensor.original.value;
                    sensor.model.min = sensor.original.min;
                    sensor.model.max = sensor.original.max;
                });
            });

            var minIndex = data[0].Types.indexOf("min");
            var maxIndex = data[0].Types.indexOf("max");

            function draw() {
                if ($d3.event && $d3.event.sourceEvent) $d3.event.sourceEvent.preventDefault();
                svg.select("g.x.axis").call(xaxis);
                svg.select("g.y.axis").call(yaxis);


                curveLimit.select("path.line").attr("d", function (d) {
                    return curveLine(d.CurvePoints);
                });

                curveLimit.select("path.area").attr("d", function (d) {
                    return area(d.CurvePoints);
                });

                canvasContext.clearRect(0, 0, width, height);
                var xDm = x.domain();
                var yDm = y.domain();
                data.forEach(function (dd) {
                    dd.Values.forEach(function (d) {
                        var visible = d.filter(function (x) {
                            if (xDm[0] >= (x[domain.x.index].getTime() + 1000 * 60 * 60 * 24) || xDm[1] <= x[domain.x.index] - 1000 * 60 * 60 * 24) return false;
                            return true;
                        });

                        var down = vDownSampling.largestTriangleThreeBuckets(visible, 1000, domain.x.index, domain.y.index);
                        for (var i = 0; i < down.length; i++) {
                            var downRow = down[i];
                            var xValue = downRow[domain.x.index];
                            var yValue = downRow[domain.y.index];

                            if (yDm[0] >= yValue || yDm[1] <= yValue) continue;
                            $d3Utils.canvas.drawCircle(canvasContext, x(xValue), y(yValue), "black", 2);
                            if (maxIndex > 0) $d3Utils.canvas.drawLine(canvasContext, x(xValue), y(yValue), x(xValue), y(downRow[maxIndex]), colors.green);
                            if (minIndex > 0) $d3Utils.canvas.drawLine(canvasContext, x(xValue), y(yValue), x(xValue), y(downRow[minIndex]), colors.red);
                        }
                        $d3Utils.canvas.drawPath(canvasContext, line(down), colors.black, 2);
                    });
                });

                tip.style("opacity", 0);
                updateZoom(svg, x, y, draw);
                svg.selectAll("text").attr("font-size", 12);
                svg.selectAll("text.icon").attr("font-size", 14);
            }


            draw();
            $d3Utils.add.unit(svg.select(".y.axis"), 0, 12, sensor.axis);
            $d3Utils.add.unit(svg.select(".x.axis"), width - margin.left, 5, vCurve.axis.date);
            svg.selectAll("text").attr("font-size", 12);
            svg.selectAll("text.icon").attr("font-size", 14);
            addZoomBox(svg, height, width);
            factory.show = function (array) {
                data = array.slice();
                domain = $d3Utils.getDomain(data, "timestamp", property, from, new Date());
                draw();
            }

            factory.setMove = function (mv) {
                move = mv;
                updateZoom(svg, x, y, draw);
            }

            factory.setFit = function (from) {
                domain = $d3Utils.getDomain(data, "timestamp", property, from, new Date());
                $d3Utils.setDomain(x, y, domain);
                draw();
            }

            factory.addCurve = function (cv, isWeight) {
                curveLimit.selectAll("*").remove();
                if (cv == null) return;
                var from = vDate.parse(events[0].From);
                from.setHours(23, 59, 59, 0);

                var addDay = 0;
                if (events[0]) {
                    addDay = addDay - events[0].StartAge;
                }
                var vals = data[0].Values[0];
                if (vals == null || vals[0] == null) {
                    vCurve.setDate(cv.CurvePoints, from, addDay);
                } else {
                    var day = vals[vals.length - 1][0];
                    var date = vals[vals.length - 1][1];
                    vCurve.setDateByDay(cv.CurvePoints, date, day);
                }

                if (isWeight) {
                    area = areaWeight;
                } else {
                    area = areaSensor;
                }
                if (domain.y.min == null) return;
                curveLimit.selectAll("path.area")
                    .data([cv])
                    .enter()
                    .append("path")
                    .attr("class", "area")
                    .attr("stroke", colors.lightGrey)
                    .attr("opacity", 0.2)
                    .attr("d", function (d) {
                        return area(d.CurvePoints);
                    });
                if (!isWeight) return;
                curveLimit.selectAll("path.line")
                    .data([cv])
                    .enter()
                    .append("path")
                    .attr("class", "line")
                    .attr("opacity", 0.7)
                    .attr("stroke", colors.black)
                    .attr("d", function (d) {
                        return curveLine(d.CurvePoints);
                    });
            }
        }
        return factory;
    }
    return {
        create: function () { return new create() }
    }
}]);