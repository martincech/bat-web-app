﻿angular.module("d3Module")
    .factory('statGraph', [
        '$d3', function ($d3) {
            var factory = {};

            var margin = {
                top: 60,
                bottom: 30,
                left: 0,
                right: 60
            };
            var draw = null;
            var width = 960;
            var height = 400;
            var xaxis = $d3.svg.axis();
            var yaxis = $d3.svg.axis();
            var move = true;

            function addZoomBox(svg) {
                svg.append("svg:rect")
                    .attr("class", "zoom y box")
                    .attr("width", margin.right)
                    .attr("height", height)
                    .attr("transform", "translate(" + width + "," + 0 + ")")
                    .style("visibility", "hidden")
                    .attr("pointer-events", "all");

                svg.append("svg:rect")
                    .attr("class", "zoom x box")
                    .attr("width", width)
                    .attr("height", height - margin.top - margin.bottom)
                    .attr("transform", "translate(" + 0 + "," + (height) + ")")
                    .style("visibility", "hidden")
                    .attr("pointer-events", "all");


                svg.append("rect")
                    .attr("class", "zoom xy box")
                    .attr("width", width)
                    .attr("height", height)
                    .style("visibility", "hidden")
                    .attr("pointer-events", "all");
            }

            factory.draw = function (data, property, showMinMax) {

                var svg = $d3.select("svg");
                svg.selectAll("*").remove();
                if (data == null || data.length === 0) return;

                data.forEach(function (sample) {
                    sample.TimeStamp = new Date(sample.TimeStamp);
                    sample.Value = sample[property];
                });

                data = data.filter(function (sample) { return sample.Value != null });

                var x = $d3.time.scale()
                    .range([0, width]);

                var y = $d3.scale.linear()
                    .range([height, 0]);

                xaxis.scale(x)
                    .orient("bottom")
                    .ticks(width / 150);

                yaxis.scale(y)
                    .orient("right")
                    .tickSize(-width)
                    .tickPadding(10);

                var line = $d3.svg.line()
                    .x(function (d) {
                        return x(d.TimeStamp);
                    })
                    .y(function (d) {
                        return y(d.Value);
                    })
                    .interpolate("linear");


                svg
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                svg.append("clipPath")
                    .attr("id", "clip")
                    .append("rect")
                    .attr("x", x(0))
                    .attr("y", y(1))
                    .attr("width", x(1) - x(0))
                    .attr("height", y(0) - y(1));


                svg.append("g")
                    .attr("class", "y axis")
                    .attr("transform", "translate(" + width + ",0)");

                svg.append("g")
                    .attr("class", "x axis")
                    .attr("transform", "translate(0," + height + ")");

                svg.append("path")
                    .attr("class", "line")
                    .attr("stroke-width", 3)
                    .attr("d", line(data))
                    .attr("clip-path", "url(#clip)");


                var limits = svg.append("g")
                    .attr("class", "limits")
                    .attr("clip-path", "url(#clip)");

                addZoomBox(svg);

                var points = svg.append("g")
                    .attr("class", "points")
                    .attr("clip-path", "url(#clip)");;

                function zoom_update() {
                    var xyzoom = d3.behavior.zoom()
                        .x(x)
                        .y(y)
                        .on("zoom", draw ? draw : null);
                    var xzoom = d3.behavior.zoom()
                        .x(x)
                        .on("zoom", draw ? draw : null);
                    var yzoom = d3.behavior.zoom()
                        .y(y)
                        .on("zoom", draw ? draw : null);

                    if (move) {
                        svg.select('rect.zoom.xy.box').call(xyzoom);
                        svg.select('rect.zoom.x.box').call(xzoom);
                        svg.select('rect.zoom.y.box').call(yzoom);
                    } else {
                        svg.select('rect.zoom.xy.box').on('.zoom', null);
                        svg.select('rect.zoom.x.box').on('.zoom', null);
                        svg.select('rect.zoom.y.box').on('.zoom', null);
                    }
                }

                draw = function () {
                    svg.select("g.x.axis").call(xaxis);
                    svg.select("g.y.axis").call(yaxis);
                 
                    svg.select("path.line").attr("d", line);

                    if (showMinMax === true) {
                        limits.selectAll("line.max")
                            .attr("x1", function (d, i) {
                                return x(d.TimeStamp);
                            })
                            .attr("y1", function (d, i) {
                                return y(d.Max);
                            })
                            .attr("x2", function (d, i) {
                                return x(d.TimeStamp);
                            })
                            .attr("y2", function (d, i) {
                                return y(d.Value) - 1.5;
                            });

                        limits.selectAll("line.min")
                            .attr("x1", function (d, i) {
                                return x(d.TimeStamp);
                            })
                            .attr("y1", function (d, i) {
                                return y(d.Min);
                            })
                            .attr("x2", function (d, i) {
                                return x(d.TimeStamp);
                            })
                            .attr("y2", function (d, i) {
                                return y(d.Value) + 1.5;
                            });
                    }
                    points.selectAll("circle.point")
                        .attr("cx", function (d, i) {
                            return x(d.TimeStamp);
                        }).attr("cy", function (d, i) {
                            return y(d.Value);
                        });

                    zoom_update();
                }


                var maxD = $d3.max(data, function (d) { return d.TimeStamp; });
                var minD = $d3.min(data, function (d) { return d.TimeStamp; });

                if (minD === maxD) {
                    maxD = new Date(minD + 86400000);
                }

                x.domain([minD, maxD]);

                var minY = $d3.min(data, function (d) { return d.Value; });
                var maxY = $d3.max(data, function (d) { return d.Value; });
                if (showMinMax === true) {
                    minY = $d3.min(data, function (d) { return d.Min; });
                    maxY = $d3.max(data, function (d) { return d.Max; });
                }

                y.domain([minY > 0 ? minY * 0.9 : minY * 1.1, maxY * 1.1]);

             
                svg.select("path.line").data([data]);

                if (showMinMax === true) {
                    limits.selectAll(".max")
                        .data(data)
                        .enter().append("line")
                        .attr("stroke", "red")
                        .attr("class", "max")
                        .attr("stroke-width", 2);

                    limits.selectAll(".min")
                        .data(data)
                        .enter().append("line")
                        .attr("stroke", "green")
                        .attr("class", "min")
                        .attr("stroke-width", 2);
                }


                points.selectAll(".point")
                    .data(data)
                    .enter().append("circle")
                    .attr("fill", "black")
                    .attr("class", "point")
                    .attr("r", 4)
                    .on("click", function (d) {

                    });

                var lock = svg.append("text")
                    .attr("x", 5)
                    .attr("y", 45)
                    .attr('class', 'lock')
                    .style("opacity", 0.3)
                    .attr("font-family", "FontAwesome")
                    .attr('font-size', '40px')
                    .style("cursor", "pointer")
                    .on("mouseenter", function () { lock.style("opacity", 1) })
                    .on("mouseleave", function () { lock.style("opacity", 0.3) })
                    .on("click", function () {
                        move = !move;
                        lock.text(move ? '\uf09c' : '\uf023');
                        zoom_update();
                    })
                    .text(move ? '\uf09c' : '\uf023');
                draw();
            }

            return factory;
        }
    ]);
