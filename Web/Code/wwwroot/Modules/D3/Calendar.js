﻿angular.module('d3Module')
    .factory('d3Calendar', [
        '$d3', '$d3Utils', '$colors', function ($d3, $d3Utils, $colors) {


            function d3Calendar() {
                var factory = {};

                var layout = {
                    width: 600,
                    height: 354,
                    container: { width: 600, height: 354 },
                    margin: { top: 5, right: 1, bottom: 20, left: 1 },
                    row: { height: 26, text: 22, icon: 24, ratio: 1, count: 12, margin: 1 },
                    calc: function (y) {
                        layout.row.height = y(1) - y(0) - layout.row.margin * 2;
                        layout.row.ratio = layout.row.height * 0.17;
                        layout.row.text = layout.row.height - layout.row.ratio;
                        layout.row.icon = layout.row.height;
                    }
                }

                var instance = {
                    values: [],
                    svg: null,
                    locked: true,
                    zoom: null,
                    element: null,
                    filter: {
                        from: null,
                        to: null,
                        selected: []
                    }
                }

                var axis = {
                    x: { zoom: null, domain: null, scale: null, axis: null },
                    y: { zoom: null, domain: null, scale: null, axis: null },
                    xy: { zoom: null, domain: null, scale: null }
                }

                var timeOut = null;
                window.onresize = function () {
                    if (timeOut != null)
                        clearTimeout(timeOut);

                    timeOut = setTimeout(function () {
                        factory.createCalendar(instance.values, instance.element, instance.filter.from, instance.filter.to);
                        factory.setSelected(instance.filter.selected, true);
                    }, 50);
                };


                function updateZoom() {
                    axis.xy.zoom = $d3.behavior.zoom()
                        .x(axis.x.domain)
                        .y(axis.y.domain)
                        .on("zoom", instance.zoom);

                    axis.x.zoom = $d3.behavior.zoom()
                        .x(axis.x.domain)
                        .on("zoom", instance.zoom);

                    axis.y.zoom = $d3.behavior.zoom()
                        .y(axis.y.domain)
                        .on("zoom", instance.zoom);

                    if (!instance.locked) {
                        instance.svg.select('rect.pane').call(axis.xy.zoom);
                        instance.svg.select('g.x.axis rect').call(axis.x.zoom);
                        instance.svg.select('g.y.axis rect').call(axis.y.zoom);
                    } else {
                        instance.svg.select('rect.pane').on(".zoom", null);
                        instance.svg.select('g.x.axis rect').on(".zoom", null);
                        instance.svg.select('g.y.axis rect').on(".zoom", null);
                    }
                    instance.svg.select('rect.pane').style("cursor", !instance.locked ? "move" : "default");
                    instance.svg.select('g.x.axis rect').style("cursor", !instance.locked ? "move" : "default");
                    instance.svg.select('g.y.axis rect').style("cursor", !instance.locked ? "move" : "default");
                    instance.svg.selectAll('.event-group').classed("can-drag", !instance.locked);
                    //$d3.selectAll(".event-group g").attr("class", function (dd) { return dd === d ? "draggable" : "g-event" });
                }


                factory.enableZoom = function () {
                    instance.locked = false;
                    updateZoom();
                }

                factory.disableZoom = function () {
                    instance.locked = true;
                    updateZoom();
                }

                factory.rowChanged = null;

                factory.setDomain = function (values, from, to) {
                    instance.filter.from = from;
                    instance.filter.to = to;
                    instance.values = values;
                    factory.createCalendar(instance.values, instance.element, from, to);
                }

                factory.redraw = null;

                factory.setSelected = function (ids, zoomToEvent) {
                    if (!instance.svg) return;
                    instance.filter.selected = ids;
                    instance.svg.selectAll(".event-bg").style("opacity", function (d) { return instance.filter.selected.indexOf(d.Id) !== -1 ? 1 : 0.5 });
                    instance.svg.selectAll(".event-group g").attr("class", function (d) { return instance.filter.selected[0] === d.Id ? "draggable" : "g-event" });
                    if (zoomToEvent) {
                        var yDomain = axis.y.domain.domain();
                        var ydomaiDiff = yDomain[0] - yDomain[1];
                        var xDomain = axis.x.domain.domain();
                        var xdomaiDiff = xDomain[1] - xDomain[0];

                        var selected = instance.values.find(function (ev) { return ev.expanded === true });
                        if (selected) {
                            if (selected.Row > yDomain[0] || selected.Row < yDomain[1]) {
                                yDomain[1] = selected.Row - Math.floor(ydomaiDiff / 2);
                                yDomain[0] = yDomain[1] + ydomaiDiff;
                                axis.y.domain.domain(yDomain);
                            }
                            if (new Date(selected.From) - xDomain[0] < 0 || xDomain[1] - new Date(selected.From) <= 1000 * 60 * 60 * 24 * 30) {
                                xDomain[0] = new Date(selected.From);
                                xDomain[1] = $d3.time.second.offset(xDomain[0], xdomaiDiff / 1000);
                                axis.x.domain.domain(xDomain);
                            }
                        }
                        instance.zoom();
                    }
                }

                factory.onClick = null;

                var states = [
                    { color: $colors.colorList.green, text: "Active", icon: "\uf144" },
                    { color: $colors.colorList.red, text: "Paused", icon: "\uf28b" },
                    { color: $colors.colorList.blue, text: "Ready", icon: "\uf28b" },
                    { color: $colors.colorList.darkGrey, text: "Closed", icon: "\uf1da" }
                ];


                function createLayout() {
                    instance.svg = $d3.select(instance.element).append("svg")
                        .attr("class", "print-svg")
                        .attr("width", layout.width + layout.margin.left + layout.margin.right)
                        .attr("height", layout.height + layout.margin.top + layout.margin.bottom)
                        .attr("shape-rendering", "crispEdges")
                        .append("g")
                        .attr("transform", "translate(" + layout.margin.left + "," + layout.margin.top + ")");

                    instance.svg.append("rect")
                        .attr("class", "pane")
                        .attr("width", layout.width)
                        .attr("height", layout.height).attr("width", layout.width + layout.margin.left)
                        .attr("height", layout.height + layout.margin.top);


                    instance.svg.append("clipPath")
                        .attr("id", "clip")
                        .append("rect")
                        .attr("width", layout.width - 1)
                        .attr("height", layout.height).attr("transform", "translate(1,0)");


                    instance.svg.append("g")
                        .attr("class", "x axis")
                        .attr("transform", "translate(0," + layout.height + ")")
                        .call(axis.x.axis)
                        .append("rect")
                        .attr("class", "axis-bottom")
                        .attr("width", layout.width + layout.margin.right)
                        .attr("height", layout.margin.bottom);

                    instance.svg.append("g")
                        .attr("class", "y axis")
                        .call(axis.y.axis);
                    //.append("rect")
                    //.attr("class", "axis-right")
                    //.attr("x", layout.width)
                    //.attr("width", layout.margin.right)
                    //.attr("height", layout.height);

                    return instance.svg;
                }


                factory.print = function (events) {
                    var svg = $d3Utils.print(true, "calendarCtrl");
                    layout.row.count = 36;
                    instance.locked = false;
                    // var div = $d3.select(svg[0][0].parentNode).append("div").style("width", 1050).style("height", 700);
                    factory.createCalendar(events, svg, null, null, 1085, 650);
                }

                factory.updateName = function (id, name) {
                    instance.svg.select("#name_" + id).text(name);
                }

                factory.createCalendar = function (val, ele, from, to, width, height) {
                    if (!ele) return;
                    instance.values = val;
                    instance.element = ele;
                    instance.filter.from = from;
                    instance.filter.to = to;
                    instance.element.innerHTML = "";
                    if (!instance.values) {
                        return;
                    };
                    layout.container.height = height == null ? layout.container.height : height;
                    layout.container.width = width == null ? instance.element.clientWidth : width;
                    layout.width = layout.container.width - layout.margin.left - layout.margin.right;
                    layout.height = layout.container.height - layout.margin.top - layout.margin.bottom;
                    layout.row.height = layout.height / layout.row.count - layout.row.margin * 2;


                    var dToday = new Date(),
                        startDate = $d3.time.month.offset(dToday, -6),
                        endDate = $d3.time.month.offset(dToday, +6);

                    if (from && to) {
                        startDate = instance.filter.from,
                            endDate = instance.filter.to;
                    }

                    var yMin = 0,
                        yMax = 6;

                    if (instance.values.length) {
                        yMin = $d3.min(instance.values.filter(function (f) { return f.visible }), function (d) { return d.Row }) || 0;
                        yMax = yMin + layout.row.count;
                    }

                    axis.x.domain = $d3.time.scale()
                        .domain([startDate, endDate])
                        .range([0, layout.width]);

                    axis.y.domain = $d3.scale.linear()
                        .domain([yMax, yMin])
                        .range([layout.height, 0]);

                    layout.calc(axis.y.domain);

                    axis.x.axis = $d3.svg.axis()
                        .scale(axis.x.domain)
                        .orient("bottom")
                        .ticks(Math.ceil(layout.width / 150))
                        .tickSize(-layout.height)
                        .tickFormat($d3Utils.dateFormat);

                    axis.y.axis = $d3.svg.axis()
                        .scale(axis.y.domain)
                        .orient("left")
                        .ticks(0)
                        .tickSize(-layout.width)
                        .tickFormat("");

                    instance.svg = createLayout();

                    var today = new Date();

                    var pos = 0;
                    var init = 0;
                    var moved = false;

                    function dragstarted(d) {
                        // $d3.event.sourceEvent.stopPropagation();
                        if (instance.locked || !d.expanded) return;
                        pos = 0;
                        moved = false;
                        init = parseInt($d3.select(this).select("rect").attr("y"));;
                        $d3.event.sourceEvent.stopPropagation();
                        $d3.select(this).classed("dragging", true);
                    }

                    function dragged(d) {
                        if (instance.locked || !d.expanded) return;
                        if ($d3.event.sourceEvent.movementY == null) return;
                        pos += $d3.event.sourceEvent.movementY;
                        var newY = Math.round(axis.y.domain.invert(pos + init));

                        var dFrom = new Date(d.From);
                        var dTo = new Date(d.To);
                        var evs = instance.values.filter(function (ev) {

                            if (ev === d)
                                return false;
                            if (ev.Row !== newY) return false;
                            var evFrom = new Date(ev.From);
                            var evTo = new Date(ev.To);
                            var result = false;

                            if (evFrom >= dFrom && evFrom <= dTo) result = true;
                            if (evTo >= dFrom && evTo <= dTo) result = true;

                            if (evFrom >= dFrom && evTo <= dTo) result = true;
                            if (evFrom <= dFrom && evTo >= dTo) result = true;

                            return result;

                        })[0];
                        if (evs != null) return;

                        if (d.Row !== newY) moved = true;
                        d.Row = newY;
                        $d3.select(this)
                            .selectAll("rect")
                            .attr("y", axis.y.domain(newY) + layout.row.margin / 2);
                        $d3.select(this)
                            .selectAll("text")
                            .attr("y", axis.y.domain(newY) + layout.row.text);

                    }

                    function dragended(d) {
                        instance.svg.select('rect.pane').style("cursor", !instance.locked ? "move" : "default");
                        if (instance.locked || !d.expanded) return;
                        $d3.select(this).classed("dragging", false);
                        if (factory.rowChanged && moved) factory.rowChanged(d);
                        moved = false;
                    }

                    var drag = $d3.behavior.drag()
                        .origin(function (d) { return d; })
                        .on("dragstart", dragstarted)
                        .on("drag", dragged)
                        .on("dragend", dragended);


                    var group = instance.svg.append("g").attr("class", "event-group")
                        .attr("clip-path", "url(#clip)");

                    var redraw = function (data) {

                        instance.values = data;
                        group.selectAll("g").remove();
                        var item = group.selectAll("g")
                            .data(data)
                            .enter()
                            .append("g")
                            .attr("class", function (d) { return instance.filter.selected[0] === d.Id ? "draggable" : "g-event" })
                            .filter(function (d) { return d.visible })
                            .on('mouseup', function (d, i) {
                                if (moved) return;
                                var res = [];
                                if (factory.onClick) res = factory.onClick(d.Id);
                                factory.setSelected(res, axis.y.domain);
                                if (res.length === 1) {
                                    $d3.selectAll(".event-group g").attr("class", function (dd) { return dd === d ? "draggable" : "g-event" });
                                } else {
                                    $d3.selectAll(".event-group g").attr("class", function () { return "g-event"; });
                                }
                            }).call(drag);

                        item.append("rect")
                            .attr("class", "event-bg")
                            .attr("fill", function (d) {
                                if (d.intersect != null) {
                                    console.log(d);
                                    return $colors.colorList.red;
                                }
                                return states[d.State].color;
                            });

                        item.append("text")
                            .attr("class", "event-name")
                            .attr("id", function (d) { return "name_" + d.Id; })
                            .attr("fill", "white")
                            .attr("clip-path", function (d) { return "url(#clip_" + d.Id + ")" })
                            .attr("font-family", "Roboto,Arial,sans-serif")
                            .attr("font-weight", "300")
                            .text(function (d) { return d.Name });

                        item.append("text")
                            .attr("class", "event-icon")
                            .attr("fill", "white")
                            .attr("clip-path", function (d) { return "url(#clip_" + d.Id + ")" })
                            .attr("font-family", "FontAwesome")
                            .attr("font-weight", "300")
                            .text(function (d) { return states[d.State].icon });

                        item.append("clipPath")
                            .attr("id", function (d) { return "clip_" + d.Id })
                            .append("rect");
                        if (instance.zoom) instance.zoom();
                    }

                    factory.redraw = redraw;
                    factory.redraw(instance.values);
                    instance.svg.append("line")
                        .attr("class", "event-today")
                        .attr("clip-path", "url(#clip)")
                        .attr("x1", axis.x.domain(today)) //<<== change your code here
                        .attr("y1", 0)
                        .attr("x2", axis.x.domain(today)) //<<== and here
                        .attr("y2", layout.height)
                        .style("stroke-width", 2)
                        .style("stroke", "red")
                        .style("fill", "none");

                    instance.zoom = function () {
                        if (d3.event && d3.event.sourceEvent) d3.event.sourceEvent.preventDefault();
                        updateZoom();
                        layout.calc(axis.y.domain);

                        instance.svg.select(".x.axis").call(axis.x.axis);
                        instance.svg.select(".y.axis").call(axis.y.axis);

                        group.selectAll("rect")
                            .attr("width", function (d, i) { return axis.x.domain(d.To) - axis.x.domain(d.From) })
                            .attr("height", function (d, i) { return layout.row.height > 0 ? layout.row.height : 0 })
                            .style("opacity", function (d) { return instance.filter.selected.indexOf(d.Id) !== -1 ? 1 : 0.5 })
                            .attr("x", function (d, i) { return axis.x.domain(d.From) })
                            .attr("y", function (d, i) { return axis.y.domain(d.Row) + layout.row.margin });


                        group.selectAll("text")
                            .attr("x", function (d) { return axis.x.domain(d.From) + (d3.select(this).classed('event-name') ? layout.row.text + layout.row.ratio : layout.row.ratio) })
                            .attr("y", function (d) { return axis.y.domain(d.Row) + layout.row.text })
                            .attr("font-size", function (d) { return layout.row.text });

                        instance.svg.select("line.event-today")
                            .attr("x1", axis.x.domain(today))
                            .attr("x2", axis.x.domain(today));

                        instance.svg.selectAll(".y.axis line").style("stroke", "red");
                    }

                    instance.zoom();
                }

                return factory;
            }


            return {
                create: function () { return new d3Calendar(); }
            }
        }
    ]);
