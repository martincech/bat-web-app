﻿"use strict";
angular.module("d3Module")
.factory('compareGraph', ['$d3', "$colors", "vDownSampling", "$d3Utils", "vCurve", function ($d3, $colors, vDownSampling, $d3Utils, vCurve) {

    function compareGraph() {
        var svg = $d3.select("svg");
        var isCreated = false;
        var move = false;

        var margin = {
            top: 0,
            bottom: 30,
            left: 0,
            right: 60
        };

        var instance = {
            data: null,
            events: null,
            visible: null,
            property: null,
            domain: null,
            width: null,
            height: null,
            move: false,
            x: null,
            y: null,
            xaxis: null,
            yaxis: null,
            decimals: null
        }

        var layout = {
            content: null,
            curve: null,
            fo: null,
            canvas: null,
            tip: null,
            tipPoint: null,
            tipContent: null
        }

        function getX(row) {
            return instance.x(row[instance.domain.x.index]);
        }

        function getY(row) {
            return instance.y(row[instance.domain.y.index]);
        }

        var area = d3.svg.area()
            .x(function (d) { return instance.x(d.Day); })
            .y0(function (d) { return instance.y(((100 + d.Max) / 100) * d.Value); })
            .y1(function (d) { return instance.y(((100 - d.Min) / 100) * d.Value); });

        var areaSensors = d3.svg.area()
            .x(function (d) { return instance.x(d.Day); })
            .y0(function (d) { return instance.y(d.Max); })
            .y1(function (d) { return instance.y(d.Min); });

        var curveLine = $d3.svg.line()
            .x(function (d) {
                return instance.x(d.Day);
            })
            .y(function (d) {
                return instance.y(d.Value);
            })
            .interpolate("linear");


        var line = $d3.svg.line()
            .x(function (d) {
                return getX(d);
            })
            .y(function (d) {
                return getY(d);
            })
            .interpolate("linear");

        var bisectDate = $d3.bisector(function (d) { return d[instance.domain.x.index]; }).right;

        function updateZoomFor(selector, x, y) {
            var zoom = $d3.behavior.zoom();
            if (x) zoom = zoom.x(x);
            if (y) zoom = zoom.y(y);
            zoom.on("zoom", move ? draw : null);
            move ? svg.select(selector).call(zoom) : svg.select(selector).on('.zoom', null);
            svg.select(selector).style("cursor", move ? "move" : "default");
        }

        function updateZoom() {
            updateZoomFor('g.events foreignObject canvas', instance.x, instance.y);
            updateZoomFor('rect.zoom.x.box', instance.x, null);
            updateZoomFor('rect.zoom.y.box', null, instance.y);
        }

        function drawCircles(data, ctx, color) {
            var yDm = instance.y.domain();
            for (var i = 0; i < data.length; i++) {
                var sxs = data[i];
                if (yDm[0] >= sxs[instance.domain.y.index] || yDm[1] <= sxs[instance.domain.y.index]) continue;
                instance.visible.push(sxs);
                ctx.beginPath();
                ctx.arc(getX(sxs), getY(sxs), 2, 0, Math.PI * 2, true);
                ctx.fillStyle = color;
                ctx.closePath();
                ctx.fill();
            }
        }

        function drawLine(data, ctx, color) {
            var p = new Path2D(line(data));
            ctx.strokeStyle = color;
            ctx.lineWidth = 2;
            ctx.stroke(p);
        }

        function getColorByEventId(id) {
            var event = instance.events.find(function (ev) {
                return parseInt(id) === ev.Id;
            });
            return $colors.getByIndex(instance.events.indexOf(event));
        }

        function draw() {
            if ($d3.event != null && $d3.event.sourceEvent != null && $d3.event.sourceEvent.preventDefault != null) $d3.event.sourceEvent.preventDefault();
            layout.curve.selectAll("circle").remove();
            svg.select("g.x.axis").call(instance.xaxis);
            svg.select("g.y.axis").call(instance.yaxis);


            layout.curve.select("path.line").attr("d", function (d) {
                return curveLine(d.CurvePoints);
            });

            layout.curve.select("path.area").attr("d", function (d) {
                return area(d.CurvePoints);
            });

            var canvasContext = layout.canvas.node().getContext("2d");
            canvasContext.clearRect(0, 0, instance.width, instance.height);
            instance.visible = [];
            instance.data.forEach(function (eventStats) {
                var color = getColorByEventId(eventStats.GroupId);
                eventStats.Values.forEach(function (sensor) {
                    var visible = sensor.filter(function (row) {
                        if (instance.x.domain()[0] >= row[instance.domain.x.index] + 1 || instance.x.domain()[1] <= row[instance.domain.x.index] - 1) return false;
                        return true;
                    });
                    var downsampled = vDownSampling.largestTriangleThreeBuckets(visible, 1000, instance.domain.x.index, instance.domain.y.index);
                    drawLine(downsampled, canvasContext, color);
                    drawCircles(downsampled, canvasContext, color);
                });
            });

            updateZoom();
            svg.selectAll("text").attr("font-size", 12);
            svg.selectAll("text.icon").attr("font-size", 14);
        }

        function setDomainFor(axis, axisLimits, minDiff) {
            var diff = (axisLimits.max - axisLimits.min) * 0.05;
            if (diff < minDiff) diff = minDiff;
            axis.domain([axisLimits.min - diff, axisLimits.max + diff]);
        }

        function setDomain() {
            setDomainFor(instance.x, instance.domain.x, 1);
            setDomainFor(instance.y, instance.domain.y, 0.1);
        }

        function addZoomBox() {
            svg.append("svg:rect")
                .attr("class", "zoom y box")
                .attr("width", margin.right)
                .attr("height", instance.height)
                .attr("transform", "translate(" + instance.width + "," + 0 + ")")
                .style("visibility", "hidden")
                .attr("pointer-events", "all");

            svg.append("svg:rect")
                .attr("class", "zoom x box")
                .attr("width", instance.width)
                .attr("height", margin.bottom)
                .attr("transform", "translate(" + 0 + "," + (instance.height) + ")")
                .style("visibility", "hidden")
                .attr("pointer-events", "all");
        }

        function addLayout() {
            svg.attr("width", instance.width + margin.left + margin.right)
               .attr("height", instance.height + margin.top + margin.bottom)
               .append("g")
               .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            svg.append("clipPath")
                .attr("id", "clip")
                .append("rect")
                .attr("x", 0)
                .attr("y", 0)
                .attr("width", instance.width)
                .attr("height", instance.height);

            svg.append("g")
                .attr("class", "y axis")
                .attr("transform", "translate(" + instance.width + ",0)");

            svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + instance.height + ")");

            return svg.append("g")
                .attr("class", "events")
                .attr("clip-path", "url(#clip)");
        }

        function getDomainLimits(data, property) {
            var limits = { x: { min: 10000, max: -10000, index: null }, y: { min: 10000, max: -10000, index: null } };
            for (var i = 0; i < data.length; i++) {
                var dIndex = data[i].Types.indexOf("day");
                var vIndex = data[i].Types.indexOf(property);
                if (dIndex == null || vIndex == null) continue;
                if (limits.x.index == null) limits.x.index = dIndex;
                if (limits.y.index == null) limits.y.index = vIndex;
                data[i].Values.forEach(function (sensor) {
                    sensor.forEach(function (row) {
                        if (row[vIndex] < limits.y.min) limits.y.min = row[vIndex];
                        if (row[vIndex] > limits.y.max) limits.y.max = row[vIndex];

                        if (row[dIndex] < limits.x.min) limits.x.min = row[dIndex];
                        if (row[dIndex] > limits.x.max) limits.x.max = row[dIndex];
                    });
                });
            }
            return limits;
        }

        function show(array) {
            instance.data = array.slice();
            //var setNewDomain = false;
            //if (instance.domain.y.min === 10000) setNewDomain = true;
            instance.domain = getDomainLimits(instance.data, instance.property);

            //if (instance.data.length === 1 || setNewDomain) {
            //    setDomain();
            //}

            addCurve(instance.cv, instance.isNotWeight);
        }

        function setMove(mv) {
            move = mv;
            updateZoom();
        }

        function setFit() {
            if (instance.domain == null) return;
            setDomain();
            draw();
        }

        function setFitTo(event) {
            if (event == null) {
                setFit();
                return;
            }
            var evData = instance.data.find(function (d) { return d.GroupId === event.Id.toString() });
            if (evData == null || evData.Values[0] == null || (evData.Values[0].length === 0)) {
                setFit();
                return;
            }

            var evDomain = angular.copy(instance.domain);
            var evX = $d3.extent(evData.Values[0], function (d) { return d[evDomain.x.index] });
            var evY = $d3.extent(evData.Values[0], function (d) { return d[evDomain.y.index] });
            evDomain.x.min = evX[0] || 0;
            evDomain.x.max = evX[1] || 100;
            evDomain.y.min = evY[0] || 0;
            evDomain.y.max = evY[1] || 100;
            setDomainFor(instance.x, evDomain.x, 1);
            setDomainFor(instance.y, evDomain.y, 0.1);
            draw();
        }

        function addCurve(cv) {
            if (layout.curve == null) return;
            layout.curve.selectAll("*").remove();
            instance.cv = cv;
            if (cv == null || cv.CurvePoints[0] == null) return;



            var curveYMin = cv.CurvePoints[0].Value;
            var curveXMin = cv.CurvePoints[0].Day;
            var curveYMax = cv.CurvePoints[cv.CurvePoints.length - 1].Value;
            var curveXMax = cv.CurvePoints[cv.CurvePoints.length - 1].Day;

            if (instance.isNotWeight) {
                curveYMin = $d3.min(cv.CurvePoints, function (c) { return c.Min; });
                curveYMax = $d3.max(cv.CurvePoints, function (c) { return c.Max; });
            }


            if (instance.domain.y.min > curveYMin) instance.domain.y.min = curveYMin;
            if (instance.domain.y.max < curveYMax) instance.domain.y.max = curveYMax;
            if (instance.domain.x.min > curveXMin) instance.domain.x.min = curveXMin;
            if (instance.domain.x.max < curveXMax) instance.domain.x.max = curveXMax;
            setDomainFor(instance.x, instance.domain.x, 10);
            setDomainFor(instance.y, instance.domain.y, 1);
            draw();

            layout.curve.selectAll("path.area")
                .data([cv])
                .enter()
                .append("path")
                .attr("class", "area")
                .attr("stroke", "black")
                 .attr("opacity", 0.2)
                .attr("d", function (d) {
                    return instance.isNotWeight ? areaSensors(d.CurvePoints) : area(d.CurvePoints);
                });

            if (!instance.isNotWeight) {
                layout.curve.selectAll("path.line")
                    .data([cv])
                    .enter()
                    .append("path")
                    .attr("class", "line")
                    .attr("opacity", 0.7)
                    .attr("stroke", "black")
                    .attr("d", function (d) {
                        return curveLine(d.CurvePoints);
                    });
            }
        }

        function mouseMove() {
            layout.curve.selectAll("circle").remove();
            layout.tipContent.selectAll("*").remove();
            layout.tip.style("opacity", 0);
            instance.visible = instance.visible.sort(function (a, b) { return a[instance.domain.x.index] - b[instance.domain.x.index] });
            var mouse = d3.mouse(this);

            var mDay = instance.x.invert(mouse[0]);
            var i = bisectDate(instance.visible, mDay);
            var visible = instance.visible.slice(i);
            if (!visible || visible.length === 0) return;
            var firstVisible = visible[0];
            var validData = visible.filter(function (f) {
                return firstVisible[instance.domain.x.index] === f[instance.domain.x.index];
            });

            var sorted = validData.sort(function (a, b) { return b[instance.domain.y.index] - a[instance.domain.y.index] });
            if (sorted.length > 0) {
                layout.tipPoint.attr("width", 200).attr("height", 400).attr({ transform: "translate(" + 0 + "," + 120 + ")" });

                $d3Utils.add.setLine(
                    layout.tip,
                    getX(sorted[0]),
                    getY(sorted[0]),
                    getX(sorted[0]),
                    instance.y(instance.y.domain()[0]),
                    ".pointer-line.pointer-line-x", 1);
            }
            var unit = svg.select("text.unit").text();
            var day = svg.select("text.text").text();

            layout.tipContent.append("xhtml:div")
                   .attr("class", "point-value")
                   .style("background-color", "black")
                   .text(day + " " + Math.floor(mDay >= 0 ? mDay : 0));

            sorted.forEach(function (f) {
                var statGroup = instance.data.find(function (group) {
                    return group.Values.find(function (row) { return row.indexOf(f) !== -1; });
                });
                var color = null;
                if (statGroup) {
                    color = getColorByEventId(statGroup.GroupId);
                }
                layout.tipContent.append("xhtml:div")
                    .attr("class", "point-value")
                    .style("background-color", color)
                    .text(f[instance.domain.y.index] != null ? f[instance.domain.y.index].toFixed(instance.decimals) + unit : '');
            });

            layout.curve.selectAll("circle").data(validData).enter().append("circle")
                .attr("fill", "red")
                .attr("cx", function (d) { return getX(d) })
                .attr("cy", function (d) { return getY(d) })
                .attr("r", 4);
            layout.tip.style("opacity", 1);
        }

        function mouseLeave() {
            layout.curve.selectAll("circle").remove();
            layout.tipPoint.attr("width", 0).attr("height", 0);
            layout.tip.style("opacity", 0);
        }


        function print(dataSource, property, events, decimals, sensor) {
            svg = $d3Utils.print(false, "compareCtrl");
            move = true;
            if (dataSource == null || dataSource.length === 0) return;

            instance.property = property;
            instance.decimals = decimals;
            instance.events = events;
            instance.data = dataSource.slice();
            instance.domain = getDomainLimits(instance.data, instance.property);

            //instance.width = 2339;
            //instance.height = 1654;

            instance.width = 750;
            instance.height = 680;

            instance.x = $d3.scale.linear().range([0, instance.width]);
            instance.y = $d3.scale.linear().range([instance.height, 0]);

            setDomain();

            instance.xaxis = $d3.svg.axis().scale(instance.x).orient("bottom").ticks(instance.width / 150);
            instance.yaxis = $d3.svg.axis().scale(instance.y).orient("right").tickSize(-instance.width).tickPadding(10);

            layout.content = addLayout();
            layout.curve = layout.content.append("g").attr("class", "curve");
            var fo = layout.content.append("svg:foreignObject").attr("width", instance.width).attr("height", instance.height);
            layout.canvas = fo.append("xhtml:canvas").attr("width", instance.width).attr("height", instance.height);


            layout.tip = svg.append("g").attr("class", "mouse-tip");
            $d3Utils.add.line(layout.tip, 0, 0, 0, 0, 1, $colors.colorList.darkGrey, "pointer-line pointer-line-x");

            layout.tipPoint = layout.tip.append("foreignObject").attr("width", 200).attr("height", 400);
            layout.tipContent = layout.tipPoint.append("xhtml:div").attr("class", "point-tip");

            layout.canvas.on("mousemove", mouseMove);
            layout.canvas.on("mouseleave", mouseLeave);

            var axis = property !== "average" ? vCurve.axis[property] : sensor.axis;
            $d3Utils.add.unit(svg.select(".y.axis"), 0, 12, axis);
            $d3Utils.add.unit(svg.select(".x.axis"), instance.width - margin.left, 5, vCurve.axis.day);

            draw();
            addZoomBox();

            svg.selectAll("text").attr("font-size", 12);
            svg.selectAll("text.icon").attr("font-size", 14);
            isCreated = true;
        }

        function create(dataSource, property, events, decimals, sensor, filter) {
            svg.selectAll("*").remove();
            if (dataSource == null || dataSource.length === 0) return;

            instance.property = property;
            instance.decimals = decimals;
            instance.events = events;
            instance.data = dataSource.slice();
            instance.domain = getDomainLimits(instance.data, instance.property);

            instance.width = svg.node().parentNode.clientWidth - margin.right - margin.left;
            //instance.height = window.innerHeight - 295 - $d3.select("#compare-filter").node().getBoundingClientRect().height;
            instance.height = window.innerHeight - 190 - 100;
            if (filter) instance.height = window.innerHeight - 190 - 180;
            //console.log($d3.select("#compare-header").node().getBoundingClientRect().height);
            instance.isNotWeight = sensor.typeName !== "weight";
            if (instance.isNotWeight) instance.height = instance.height + 48;

            instance.x = $d3.scale.linear().range([0, instance.width]);
            instance.y = $d3.scale.linear().range([instance.height, 0]);

            setDomain();

            instance.xaxis = $d3.svg.axis().scale(instance.x).orient("bottom").ticks(instance.width / 150);
            instance.yaxis = $d3.svg.axis().scale(instance.y).orient("right").tickSize(-instance.width).tickPadding(10);

            layout.content = addLayout();
            layout.curve = layout.content.append("g").attr("class", "curve");
            var fo = layout.content.append("svg:foreignObject").attr("width", instance.width).attr("height", instance.height);
            layout.canvas = fo.append("xhtml:canvas").attr("width", instance.width).attr("height", instance.height);


            layout.tip = svg.append("g").attr("class", "mouse-tip");
            $d3Utils.add.line(layout.tip, 0, 0, 0, 0, 1, $colors.colorList.darkGrey, "pointer-line pointer-line-x");

            layout.tipPoint = layout.tip.append("foreignObject").attr("width", 200).attr("height", 400);
            layout.tipContent = layout.tipPoint.append("xhtml:div").attr("class", "point-tip");

            layout.canvas.on("mousemove", mouseMove);
            layout.canvas.on("mouseleave", mouseLeave);

            var axis = property !== "average" ? vCurve.axis[property] : sensor.axis;
            $d3Utils.add.unit(svg.select(".y.axis"), 0, 12, axis);
            $d3Utils.add.unit(svg.select(".x.axis"), instance.width - margin.left, 5, vCurve.axis.day);

            draw();
            addZoomBox();

            svg.selectAll("text").attr("font-size", 12);
            svg.selectAll("text.icon").attr("font-size", 14);
            isCreated = true;
        }

        return {
            create: create,
            addCurve: addCurve,
            setMove: setMove,
            setFit: setFit,
            show: show,
            setFitTo: setFitTo,
            forceRedraw: function () { isCreated = false; },
            isCreated: function () { return isCreated },
            print: print
        };

    }

    return {
        create: function () { return new compareGraph() }
    }
}]);