﻿var d3Module = angular.module('d3Module', [])
.factory('$d3', [function () {
    return window.d3;
}])
.factory('$colors', function () {
    //var colors = [
    //   'brown', 'darkgoldenrod', 'burlywood', 'cadetblue', 'chartreuse', 'chocolate',
    //   'coral', 'cornflowerblue', 'crimson', 'darkblue', 'darkcyan', 'darkgray', 'darkgreen', 'darkkhaki',
    //   'darkmagenta', 'darkolivegreen', 'darkorange', 'darkorchid', 'darkred', 'darksalmon', 'darkseagreen', 'darkslategray',
    //   'darkturquoise', 'darkviolet', 'deeppink', 'dimgray', 'dimgrey', 'dodgerblue', 'firebrick', 'forestgreen', 'fuchsia',
    //   'gainsboro', 'gold', 'gray', 'green', 'greenyellow', 'grey', 'hotpink', 'indianred', 'indigo', 'khaki', 'lawngreen',
    //   'lightblue', 'lightcoral', 'lightgreen', 'lightgrey', 'lightpink', 'lightsalmon', 'lightseagreen', 'lightskyblue',
    //   'lightslategray', 'lightsteelblue', 'lightyellow', 'limegreen', 'linen', 'magenta', 'maroon', 'mediumaquamarine',
    //   'mediumblue', 'mediumorchid', 'mediumseagreen', 'mediumslateblue', 'mediumspringgreen', 'mediumturquoise',
    //   'mediumvioletred', 'midnightblue', 'mistyrose', 'navajowhite', 'navy', 'olive', 'olivedrab', 'orange', 'orangered',
    //   'orchid', 'palegoldenrod', 'palegreen', 'paleturquoise', 'palevioletred', 'papayawhip', 'peachpuff', 'peru', 'pink',
    //   'plum', 'powderblue', 'purple', 'red', 'rosybrown', 'royalblue', 'saddlebrown', 'salmon', 'sandybrown', 'seagreen',
    //   'sienna', 'silver', 'skyblue', 'slateblue', 'slategray', 'springgreen', 'steelblue', 'tan', 'teal', 'thistle', 'tomato',
    //    'turquoise', 'violet', 'wheat', 'yellow', 'yellowgreen'];

    /*
     * Curve colors
     */
    var colors = ['#e66101', '#fdb863', '#b2abd2', '#5e3c99',
                 '#d01c8b', '#f1b6da', '#b8e186', '#4dac26',
                 '#a6611a', '#dfc27d', '#80cdc1', '#018571',
                 '#ca0020', '#f4a582', '#92c5de', '#0571b0',
                 '#7b3294', '#c2a5cf', '#a6dba0', '#008837']
    ;

    /*
     * System colors
     */
    var colorList = {
        red: "#dc0000",
        green: "#8bc34a",
        blue: "#03a9f4",
        black: "#333333",
        darkGrey: "#666666",
        mediumGrey: "#cacaca",
        lightGrey: "#e5e5e5"
    };



    return {
        getByIndex: function (index) {
            if (index == null) return "black";
            var colorIndex = index % colors.length;
            return colors[colorIndex];
        },
        colors: colors,
        colorList: colorList
    };
})
.factory('$d3Utils', ["$d3", "vDate", "vSensor", "vCookies", "$sce", "$templateRequest", function ($d3, vDate, vSensor, vCookies, $sce, $templateRequest) {


    var locale = {};
    locale[vCookies.language.de] = $d3.locale({
        "decimal": ",",
        "thousands": ".",
        "grouping": [3],
        "currency": ["€", ""],
        "dateTime": "%a %b %e %X %Y",
        "date": "%d.%m.%Y",
        "time": "%H:%M:%S",
        "periods": ["AM", "PM"],
        "days": ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"],
        "shortDays": ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
        "months": ["Jänner", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
        "shortMonths": ["Jän", "Feb", "Mär", "Apr", "Mai", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Dez"]
    });

    var tickFormat = locale[vCookies.language.get()] == null ? null : locale[vCookies.language.get()].timeFormat.multi([
        ["%H:%M", function (d) { return d.getMinutes(); }],
        ["%H:%M", function (d) { return d.getHours(); }],
        ["%a %d", function (d) { return d.getDay() && d.getDate() !== 1; }],
        ["%b %d", function (d) { return d.getDate() !== 1; }],
        ["%B", function (d) { return d.getMonth(); }],
        ["%Y", function () { return true; }]
    ]);


    function addAxis(element, width, height, axisClass) {
        return element.append("svg:g")
            .attr("class", axisClass)
            .attr("transform", "translate(" + width + "," + height + ")");
    }

    function addClipPath(element, width, height) {
        return element.append("svg:clipPath")
            .attr("id", "clip")
            .append("rect")
            .attr("x", 0)
            .attr("y", 0)
            .attr("width", width)
            .attr("height", height);
    }

    function addZoomBox(element, x, y, width, height, boxClass) {
        return element.append("svg:rect")
            .attr("class", boxClass)
            .attr("width", width)
            .attr("height", height)
            .attr("transform", "translate(" + x + "," + y + ")")
            .style("visibility", "hidden")
            .attr("pointer-events", "all");
    }

    function addContentHolder(element, contentClass) {
        return element.append("svg:g")
            .attr("class", contentClass)
            .attr("clip-path", "url(#clip)");
    }

    function addCanvas(element, width, height) {
        return element.append("foreignObject")
            .attr("width", width)
            .attr("height", height)
            .append("xhtml:canvas")
            .attr("width", width)
            .attr("height", height);
    }

    function addDiv(element, divClass) {
        return element.append("xhtml:div").attr("class", divClass);
    }

    function addFoDiv(element, divClass) {
        var wrapper = element.append("foreignObject").attr("width", 1).attr("height", 1);
        return [wrapper, addDiv(wrapper, divClass)];
    }

    function move(element, x, y) {
        return element.attr({
            transform: "translate(" + x + "," + y + ")"
        });
    }

    function addLine(element, x1, y1, x2, y2, width, color, lineClass) {
        return element.append("line")
            .style("stroke", color)
            .style("stroke-width", width + "px")
            .attr("class", lineClass)
            .attr("x1", x1)
            .attr("x2", x2)
            .attr("y1", y1)
            .attr("y2", y2);
    }

    function addCircle(element, cx, cy, r, color, circleClass) {
        return element.append("svg:circle")
            .attr("class", circleClass)
            .attr("fill", color)
            .attr("cx", cx)
            .attr("cy", cy)
            .attr("r", r);
    }

    function setLine(element, x1, y1, x2, y2, lineClass) {
        return element.select(lineClass)
            .attr("x1", x1)
            .attr("x2", x2)
            .attr("y1", y1)
            .attr("y2", y2);
    }

    function addUnit(element, x, y, axis) {
        var g = element.append("g").attr({ transform: "translate(" + x + "," + y + ")" });
        g.append("text").attr("class", axis.icon ? "icon" : "text")
            .attr("font-family", axis.icon ? "icomoon" : "")
            .text(axis.icon ? axis.icon : axis.text);

        var box = g.node().getBBox();
        g.append("text").attr("class", "unit").attr("x", box.width).text(" " + axis.unit + "");
        box = g.node().getBBox();
        g.insert("svg:rect", "text").attr("y", -box.height)
            .attr("width", box.width).attr("height", box.height + 2)
            .attr("fill", "white");
        return g;
    }

    function canvasDrawLine(context, x1, y1, x2, y2, color) {
        context.beginPath();
        context.moveTo(x1, y1);
        context.lineTo(x2, y2);
        context.stroke();
        context.strokeStyle = color;
        context.closePath();
        context.fill();
    }

    function canvasDrawPath(context, line, color, width) {
        var p = new Path2D(line);
        context.strokeStyle = "black";
        context.lineWidth = width;
        context.stroke(p);
        context.closePath();
    }

    function canvasDrawCircle(context, x, y, color, r) {
        context.beginPath();
        context.arc(x, y, r, 0, Math.PI * r, true);
        context.fillStyle = color;
        context.closePath();
        context.fill();
    }

    function updateZoom(element, xAxis, yAxis, action) {
        element.style("cursor", action ? "move" : "default");
        if (action == null) {
            element.on(".zoom", null);
            return;
        }
        var zoom = $d3.behavior.zoom();
        if (xAxis) zoom.x(xAxis);
        if (yAxis) zoom.y(yAxis);
        zoom.on("zoom", action);
        element.call(zoom);
    }


    function getDomain(data, xAttr, yAttr, from, to) {
        var limits = { x: { min: null, max: null, index: null }, y: { min: 10000, max: -10000, index: null } };
        var xValues = [];
        var yValues = [];
        for (var i = 0; i < data.length; i++) {
            var types = data[i].Types;

            var xIndex = types.indexOf(xAttr);
            var yIndex = types.indexOf(yAttr);

            var minIndex = types.indexOf("min");
            var maxIndex = types.indexOf("max");

            if (xIndex == null || yIndex == null) continue;
            if (limits.x.index == null) limits.x.index = xIndex;
            if (limits.y.index == null) limits.y.index = yIndex;
            data[i].Values.forEach(function (samples) {
                samples.forEach(function (sample) {
                    if (xAttr === vSensor.types.time.timeStamp) sample[xIndex] = vDate.parse(sample[xIndex]);
                    xValues.push(sample[xIndex]);
                    yValues.push(sample[yIndex]);
                    if (minIndex !== -1) yValues.push(sample[minIndex]);
                    if (maxIndex !== -1) yValues.push(sample[maxIndex]);
                });
            });
        }
        var x = $d3.extent(xValues);
        var y = $d3.extent(yValues);
        limits.x.min = from || x[0] || new Date();
        limits.x.max = to || x[1] || new Date();
        limits.y.min = y[0];
        limits.y.max = y[1];
        return limits;
    }


    function setDomain(x, y, domain) {
        var diffX = (domain.x.max - domain.x.min) * 0.05;
        var diffY = (domain.y.max - domain.y.min) * 0.05;
        if (diffY < 0.1) diffY = 0.1;
        var xMax = 0;
        if (domain.x.max instanceof Date) xMax = new Date((new Date(domain.x.max)).getTime() + diffX);
        x.domain([domain.x.min - diffX, xMax]);
        y.domain([domain.y.min - diffY, domain.y.max + diffY]);
    }



    var titleSelector = "print-title";
    var legendSelector = "print-legend";

    var templateUrl = $sce.getTrustedResourceUrl('/shared/printtemplate');
    function addElement(targetElement, sourceElement) {
        if (targetElement == null || sourceElement == null) return;
        return targetElement.appendChild(sourceElement.cloneNode(true));
    }

    var template = "";
    $templateRequest(templateUrl).then(function (temp) {
        template = temp;
    });

    function print(parent, bodyClass) {
        var printWindow = window.open();
        printWindow.document.write(template);
        addElement(printWindow.document.getElementById(titleSelector), document.getElementsByClassName(titleSelector)[0]);
        addElement(printWindow.document.getElementById(legendSelector), document.getElementsByClassName(legendSelector)[0]);
        if (bodyClass) printWindow.document.body.classList.add(bodyClass);
        printWindow.document.close();
        if (parent) {
            var parentElement = printWindow.document.getElementById("print-svg");
            parentElement.innerHTML = "";
            return parentElement;
        }
       return $d3.select(printWindow.document.body).select("svg");
    }

    /*
    * SVG manipulation utils 
    */
    var add = {
        axis: addAxis,
        clipPath: addClipPath,
        zoomBox: addZoomBox,
        contentHolder: addContentHolder,
        canvas: addCanvas,
        div: addDiv,
        foDiv: addFoDiv,
        line: addLine,
        circle: addCircle,
        setLine: setLine,
        unit: addUnit
    }

    /*
    * Canvas simple draw functions 
    */
    var canvas = {
        drawLine: canvasDrawLine,
        drawPath: canvasDrawPath,
        drawCircle: canvasDrawCircle
    }

    return {
        add: add,
        canvas: canvas,
        move: move,
        updateZoom: updateZoom,
        getDomain: getDomain,
        setDomain: setDomain,
        dateFormat: tickFormat,
        print: print
    };
}])
.directive('veitPrint', ["$templateRequest", "$sce", function ($templateRequest, $sce) {
    var titleSelector = "print-title";
    var svgSelector = "print-svg";
    var canvasSelector = "print-canvas";
    var legendSelector = "print-legend";

    var templateUrl = $sce.getTrustedResourceUrl('/shared/printtemplate');
    function addElement(targetElement, sourceElement) {
        if (targetElement == null || sourceElement == null) return;
        return targetElement.appendChild(sourceElement.cloneNode(true));
    }

    return {
        restrict: 'A',
        link: function (scope, element) {
            element.bind("click", function () {

                var title = document.getElementsByClassName(titleSelector)[0];
                var svg = document.getElementsByClassName(svgSelector)[0];
                var legend = document.getElementsByClassName(legendSelector)[0];

                var canvasElement = document.getElementsByTagName("canvas")[0];
                var canvas = document.createElement("img");
                if (canvasElement != null) {
                    canvas.src = canvasElement.toDataURL();
                }

                var printWindow = window.open();
                $templateRequest(templateUrl).then(function (template) {

                    printWindow.document.write(template);
                    addElement(printWindow.document.getElementById(titleSelector), title);
                    addElement(printWindow.document.getElementById(svgSelector), svg);
                    addElement(printWindow.document.getElementById(canvasSelector), canvas);
                    addElement(printWindow.document.getElementById(legendSelector), legend);
                    printWindow.document.close();

                    printWindow.focus();
                    setTimeout(function () { printWindow.print(); }, 100);
                });
            });
        }
    };
}]);