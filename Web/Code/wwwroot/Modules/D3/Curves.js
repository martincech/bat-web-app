﻿angular.module('d3Module')
    .directive("curvesGraph", [function () {
        return {
            restrict: 'E',
            scope: {
                curve: '=',
                tooltips: '='
            },
            controller: ["$scope", "$element", "$d3", "$d3Utils", "vCurve", "vResources", function ($scope, $element, $d3, $d3Utils, vCurve, vResources) {

                function draw(data) {
                    var elemWidth = $element[0].parentNode.clientWidth - 30;

                    // Set the dimensions of the canvas / graph
                    var margin = { top: 10, right: 10, bottom: 20, left: 40 },
                        width = elemWidth - margin.left - margin.right,
                        height = 400 - margin.top - margin.bottom;

                    // Set the ranges
                    var x = $d3.scale.linear().range([0, width]);
                    var y = $d3.scale.linear().range([height, 0]);

                    // Define the axes
                    var xAxis = $d3.svg.axis().scale(x)
                        .orient("bottom").ticks(10);

                    var yAxis = $d3.svg.axis().scale(y)
                        .orient("left").ticks(5);

                    var valueline = $d3.svg.line()
                        .x(function (d) { return x(d.Day); })
                        .y(function (d) { return y(d.Value); })
                        .interpolate("linear");

                    $d3.select($element[0]).selectAll("svg").remove();

                    // Adds the svg canvas
                    var svg = $d3.select($element[0])
                        .append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .append("g")
                        .attr("transform",
                            "translate(" + margin.left + "," + margin.top + ")");


                    var yMax = data.max;
                    var yMin = data.min;

                    var xDomainData = data.target;
                    if (!(xDomainData && xDomainData.length)) {
                        xDomainData = data.max;
                    }
                    if (!(xDomainData && xDomainData.length)) {
                        xDomainData = data.min;
                    }

                    if (!(xDomainData && xDomainData.length)) {
                        return;
                    }

                    var yMi = $d3.min(yMin, function (d) { return d.Value; });
                    var yMa = $d3.max(yMax, function (d) { return d.Value; });
                    var diff = (yMa - yMi) * 0.05;

                    // Scale the range of the data
                    x.domain($d3.extent(xDomainData, function (d) { return d.Day; }));
                    y.domain([yMi - diff, yMa + diff]);

                    // Add the X Axis
                    var xaxis = svg.append("g")
                          .attr("class", "x axis")
                          .attr("transform", "translate(0," + height + ")")
                          .call(xAxis);
                   
                    $d3Utils.add.unit(xaxis, width - 30, 5, data.xAxis);

                    // Add the Y Axis
                    var yaxis = svg.append("g")
                         .attr("class", "y axis")
                         .call(yAxis);

                    $d3Utils.add.unit(yaxis, -40, 5, data.yAxis);

                    function drawLine(lineData) {
                        if (lineData && lineData.length) {
                            svg.append("path")
                                .attr("class", "line")
                                .attr("d", valueline(lineData));
                        }
                    }

                    drawLine(data.target);
                    drawLine(data.max);
                    drawLine(data.min);


                    var areaAboveTrafficLine = $d3.svg.area()
                        .x(valueline.x())
                        .y0(valueline.y())
                        .y1(0);
                    var areaBelowTrafficLine = $d3.svg.area()
                        .x(valueline.x())
                        .y0(valueline.y())
                        .y1(height);
                    var areaAboveRateLine = $d3.svg.area()
                        .x(valueline.x())
                        .y0(valueline.y())
                        .y1(0);
                    var areaBelowRateLine = $d3.svg.area()
                        .x(valueline.x())
                        .y0(valueline.y())
                        .y1(height);

                    var defs = svg.append('defs');

                    defs.append('clipPath')
                        .attr('id', 'clip-traffic')
                        .append('path')
                        .datum(data.max)
                        .attr('d', areaAboveTrafficLine);

                    defs.append('clipPath')
                        .attr('id', 'clip-rate')
                        .append('path')
                        .datum(data.min)
                        .attr('d', areaAboveRateLine);


                    svg.append('path')
                        .datum(data.max)
                        .attr('d', areaBelowTrafficLine)
                        .attr('clip-path', 'url(#clip-rate)')
                        .style("opacity", "0.2");

                    // RATE IS ABOVE TRAFFIC
                    svg.append('path')
                        .datum(data.min)
                        .attr('d', areaBelowRateLine)
                        .attr('clip-path', 'url(#clip-traffic)')
                        .style("opacity", "0.2");

                    var g = svg.append("svg:g");


                    if (data.target && data.target.length) {
                        g.selectAll("scatter-dots")
                            .data(data.target)
                            .enter().append("svg:circle")
                            .attr("class", "scatter")
                            .attr("cx", function (d, i) { return x(d.Day); })
                            .attr("cy", function (d) { return y(d.Value); })
                            .attr("r", 4)
                            .on("mouseover", function (d) {
                                var min = data.min.filter(function (item) { return item.Day === d.Day })[0];
                                var max = data.max.filter(function (item) { return item.Day === d.Day })[0];
                                $scope.tooltips = {
                                    day: d.Day,
                                    value: d.Value,
                                    min: min.Value.toFixed(3),
                                    max: max.Value.toFixed(3),
                                    style: {
                                        left: ($d3.event.pageX + 5) + "px",
                                        top: ($d3.event.pageY + 5) + "px",
                                        position: "fixed"
                                    }
                                };
                                $scope.$apply();
                            })
                            .on("mouseout", function (d) {
                                $scope.tooltips = null;
                                $scope.$apply();
                            });
                    }

                    svg.selectAll("text").attr("font-size", 12);
                    svg.selectAll("text.icon").attr("font-size", 14);

                }



                function parseData() {
                    var data = $scope.curve.CurvePoints;

                    var values = new Array();
                    var mins = new Array();
                    var maxs = new Array();

                    var graphData;

                    if ($scope.curve.Type < 3) {
                        var lastMin = null;
                        var lastMax = null;


                        for (var i = 0; i < data.length; i++) {
                            var item = data[i];
                            if (item.Value) {
                                values.push({ Day: item.Day, Value: item.Value });
                            }
                            if ((item.Min || lastMin) && item.Value) {
                                lastMin = item.Min || lastMin;
                                var min = item.Value * (1 - lastMin / 100);
                                mins.push({ Day: item.Day, Value: min });
                            }
                            if ((item.Max || lastMax) && item.Value) {
                                lastMax = item.Max || lastMax;
                                var max = item.Value * (1 + lastMax / 100);
                                maxs.push({ Day: item.Day, Value: max });
                            }
                        };

                        graphData = {
                            target: values,
                            min: mins,
                            max: maxs,
                            xAxis: vCurve.axis.day,
                            yAxis:  vCurve.axis.weight
                        };


                        draw(graphData);
                    } else {
                        for (var c = 0; c < data.length; c++) {
                            mins.push({ Day: data[c].Day, Value: data[c].Min });
                            maxs.push({ Day: data[c].Day, Value: data[c].Max });
                        };

                        graphData = {
                            target: null,
                            min: mins,
                            max: maxs,
                            xAxis: vCurve.axis.day,
                            yAxis: vCurve.getAxisByType($scope.curve.Type)
                        };
                        draw(graphData);
                    }
                }


                $scope.$parent.$parent.$parent.redraw = parseData;

                $scope.$watch("curve.CurvePoints", function () {
                    setTimeout(parseData, 0);
                }, true);
            }]
        };
    }]);