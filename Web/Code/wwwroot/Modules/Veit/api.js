﻿angular.module('veitModule', [])
    .factory("events", ["$http", function ($http) {
        return {
            get: function () {
                return $http({ method: 'GET', url: '/calendar/data' });
            },
            create: function (event) {
                return $http({ method: 'POST', data: event, url: '/calendar/create' });
            },
            update: function (event, basicOnly) {
                var ev = basicOnly ? { Id: event.Id, Row: event.Row, Name: event.Name, BirdId: event.BirdId, SexMode: event.SexMode, StartAge: event.StartAge } : event;
                return $http({ method: 'POST', data: ev, url: '/calendar/update?basicOnly=' + !!basicOnly });
            },
            exportEvents: function (ids, sampleSize) {
                if (!ids || !ids.length) return;
                var url = "/calendar/export?timeSize=" + sampleSize;
                ids.forEach(function (item) {
                    url += "&";
                    url += "eventIds=" + item;
                });
                window.location = url;
            },
            delete: function (ids) {
                return $http({ method: 'POST', data: ids, url: '/calendar/delete' });
            }
        }
    }])
    .factory("sensors", ["$http", function ($http) {
        return {
            get: function () {
                return $http({ method: 'GET', url: '/sensors/data' });
            },
            names: function () {
                return $http({ method: 'GET', url: '/sensors/names' });
            },
            download: function () {
                return $http({ method: 'GET', url: '/sensors/download' });
            }
        }
    }])
    .factory("sensorsDetail", ["$http", function ($http) {
        return {
            get: function (id) {
                return $http({ method: 'GET', url: '/sensors/detaildata?deviceId=' + id });
            },
            filterData: function (deviceId, timeSize, sensors, types, from, to) {
                var filter = {
                    Group: "device",
                    GroupIds: [deviceId],
                    TimeSizes: [timeSize],
                    Sensors: sensors,
                    Types: types,
                    From: from,
                    To: to
                }
                return $http({
                    method: 'POST',
                    data: [filter],
                    url: '/compare/filterdata'
                });
            },
            changeName: function (deviceId, name) {
                return $http({
                    method: 'POST',
                    data: {
                        deviceId: deviceId,
                        name: name
                    },
                    url: '/sensors/changename'
                });
            },
            createEvent: function (deviceId) {
                return $http({
                    method: 'POST',
                    data: {
                        deviceId: deviceId
                    },
                    url: '/sensors/createevent'
                });
            }
        }
    }])
    .factory("birds", ["$http", function ($http) {
        return {
            get: function () {
                return $http({ method: 'GET', url: '/birds/data' });
            },
            create: function (birdName, sexmode) {
                return $http({ method: 'POST', data: { name: birdName, sexMode: sexmode | 0 }, url: '/birds/create' });
            },
            update: function (bird) {
                return $http({ method: 'POST', data: bird, url: '/birds/update' });
            },
            delete: function (ids) {
                return $http({ method: 'POST', data: ids, url: '/birds/delete' });
            }
        }
    }])
    .factory("curves", ["$http", function ($http) {
        return {
            get: function () {
                return $http({ method: 'GET', url: '/curves/data' });
            },
            create: function (curve) {
                return $http({ method: 'POST', data: curve, url: '/curves/create' });
            },
            update: function (curve) {
                return $http({ method: 'POST', data: curve, url: '/curves/update' });
            },
            delete: function (ids) {
                return $http({ method: 'POST', data: ids, url: '/curves/delete' });
            },
            addPoint: function (curveId, curvePoint) {
                return $http({ method: 'POST', params: { curveId: curveId }, data: curvePoint, url: '/curves/addPoint' });
            },
            deletePoint: function (curveId, day) {
                return $http({ method: 'POST', params: { curveId: curveId, day: day }, url: '/curves/deletePoint' });
            }
        }
    }])
    .factory("users", ["$http", function ($http) {
        return {
            get: function () {
                return $http({ method: 'GET', url: '/users/data' });
            },
            create: function (user) {
                user.Id = null;
                return $http({ method: 'POST', data: user, url: '/users/create' });
            },
            update: function (user) {
                return $http({ method: 'POST', data: user, url: '/users/update' });
            },
            delete: function (ids) {
                return $http({ method: 'POST', data: ids, url: '/users/delete' });
            },
            loginAs: function (userId) {
                return $http({ method: 'GET', url: '/users/singas?id=' + userId });
            },
            newPassword: function (userId) {
                return $http({ method: 'POST', params: { id: userId }, url: '/users/newpassword' });
            },
        }
    }])
    .factory("companies", ["$http", function ($http) {
        return {
            get: function () {
                return $http({ method: 'GET', url: '/companies/data' });
            },
            create: function (companyName) {
                return $http({ method: 'POST', data: "\"" + companyName + "\"", url: '/companies/create' });
            },
            update: function (company) {
                return $http({ method: 'POST', data: company, url: '/companies/update' });
            },
            login: function (companyId) {
                return $http({ method: 'POST', params: { companyId: companyId} , url: '/companies/login' });
            },
            updateGroups: function () {
                return $http({ method: 'POST', data: null, url: '/companies/updateGroups' });
            },
            delete: function (ids) {
                return $http({ method: 'POST', data: ids, url: '/companies/delete' });
            },
            deleteUser: function (userId) {
                return $http({ method: 'POST', params: { userId: userId }, url: '/companies/deleteuser' });
            },
            createUser: function (userId, companyId, role) {
                return $http({ method: 'POST', params: { userId: userId, companyId: companyId, role: role }, url: '/companies/createuser' });
            },
            updateUser: function (userId, companyId, role) {
                return $http({ method: 'POST', params: { userId: userId, companyId: companyId, role: role }, url: '/companies/updateuser' });
            },
            disableDemoData: function (companyId, disable) {
                return $http({ method: 'POST', params: { companyId: companyId, disable: disable }, url: '/companies/disabledemodata' });
            }
        }
    }])
    .factory("farms", ["$http", function ($http) {
        return {
            get: function () {
                return $http({ method: 'GET', url: '/farms/data' });
            },
            setup: function () {
                return $http({ method: 'GET', url: '/farms/setupdata' });
            },
            create: function (farmName) {
                return $http({ method: 'POST', data: "\"" + farmName + "\"", url: '/farms/create' });
            },
            update: function (farm) {
                return $http({ method: 'POST', data: farm, url: '/farms/update' });
            },
            delete: function (ids) {
                return $http({ method: 'POST', data: ids, url: '/farms/delete' });
            }
        }
    }])
    .factory("compare", ["$http", function ($http) {
        return {
            get: function () {
                return $http({ method: 'GET', url: '/compare/data' });
            },
            filterData: function (events, timeSize, sensor, types) {
                var filters = [];
                events.forEach(function (ev) {
                    var filter = {
                        Group: "event",
                        GroupIds: [ev.Id],
                        TimeSizes: [timeSize],
                        Sensors: [sensor],
                        Types: types,
                        From: ev.From,
                        To: ev.To

                    }
                    filters.push(filter);
                });

                return $http({
                    method: 'POST',
                    data: filters,
                    url: '/compare/filterdata'
                });
            },
            filterLastData: function (events, timeSize, sensor, types) {
                var filters = [];
                events.forEach(function (ev) {
                    var filter = {
                        Group: "event",
                        GroupIds: [ev.Id],
                        TimeSizes: [timeSize],
                        Sensors: [sensor],
                        Types: types,
                        From: ev.From,
                        To: ev.To

                    }
                    filters.push(filter);
                });

                return $http({
                    method: 'POST',
                    data: filters,
                    url: '/compare/filterlastdata'
                });
            }
        }
    }])
    .factory("profile", ["$http", function ($http) {
        return {
            get: function () {
                return $http({ method: 'GET', url: '/profile/data' });
            },
            update: function (company) {
                return $http({ method: 'POST', data: company, url: '/profile/update' });
            },
            addTerminal: function (uid, name) {
                return $http({ method: 'POST', data: { uid: uid, name: name }, url: '/profile/addterminal' });
            },
            deleteTerminal: function (uid) {
                return $http({ method: 'POST', params: { uid: uid }, url: '/profile/deleteterminal' });
            },
            deleteDevice: function (uid) {
                return $http({ method: 'POST', params: { uid: uid }, url: '/profile/deletedevice' });
            }
        }
    }])
    .factory("houses", ["$http", function ($http) {
        return {
            get: function () {
                return $http({ method: 'GET', url: '/houses/data' });
            },
            setup: function () {
                return $http({ method: 'GET', url: '/houses/setupdata' });
            },
            detail: function (id) {
                return $http({ method: 'GET', url: '/houses/detaildata?houseId=' + id });
            },
            create: function (houseName) {
                return $http({ method: 'POST', data: "\"" + houseName + "\"", url: '/houses/create' });
            },
            update: function (house) {
                var hs = { Id: house.Id, Name: house.Name, Location: house.Location };
                return $http({ method: 'POST', data: hs, url: '/houses/update' });
            },
            updateDevice: function (houseId, deviceId, add) {
                return $http({ method: 'POST', params: { houseId: houseId, deviceId: deviceId, add: add }, url: '/houses/updatedevice' });
            },
            delete: function (ids) {
                return $http({ method: 'POST', data: ids, url: '/houses/delete' });
            },
            filterData: function (deviceId, timeSize, sensors, types, from, to) {
                var filter = {
                    Group: "house",
                    GroupIds: [deviceId],
                    TimeSizes: [timeSize],
                    Sensors: sensors,
                    Types: types,
                    From: from,
                    To: to
                }
                return $http({
                    method: 'POST',
                    data: [filter],
                    url: '/compare/filterdata'
                });
            },
            changeName: function (houseId, name) {
                return $http({
                    method: 'POST',
                    params: {
                        houseId: houseId,
                        name: name
                    },
                    data: "\"" + name + "\"",
                    url: '/houses/changename'
                });
            },
            createEvent: function (houseId) {
                return $http({
                    method: 'POST',
                    params: {
                        houseId: houseId
                    },
                    url: '/houses/createevent'
                });
            }
        }
    }])
    .factory("settings", ["$http", function ($http) {
        return {
            getEventLength: function () {
                return $http({ method: 'GET', url: '/settings/geteventlength' });
            },
            setEventLength: function (eventLength) {
                return $http({ method: 'POST', data: { eventLength: eventLength }, url: '/settings/seteventlength' });
            },
            update: function () {
                return $http({ method: 'POST', url: '/settings/update' });
            },
            resources: function (regex) {
                return $http({ method: 'GET', url: '/settings/resources?regex=' + encodeURIComponent(regex) });
            }
        }
    }]);