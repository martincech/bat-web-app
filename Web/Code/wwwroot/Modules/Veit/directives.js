﻿angular.module('veitModule')
    .directive('rightMenu', function () {
        return {
            restrict: 'A',
            link: function (scope, element) {
                element.bind("click", function () {
                    var canvas = document.getElementsByClassName("js-off-canvas-exit")[0];
                    if (!canvas) return;
                    setTimeout(function () { canvas.classList.remove("is-visible") }, 0);
                });
            }
        };
    })
    .directive('rnStepper', function () {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, iElement, iAttrs, ngModel) {

                function round(val) {
                    return Math.round(val * 1000) / 1000;
                }
                var plus = angular.element('<button ng-click="increment()" class="inc spin button">' +
                    '+' +
                    '</button>');

                var minus = angular.element('<button ng-click="decrement()" class="dec spin button">' +
                    '-' +
                    '</button>');
                iElement.after(minus);
                iElement.after(plus);

                var min = parseInt(iElement[0].min);
                var max = parseInt(iElement[0].max);

                plus.on("click", function (e) {
                    if (max != null && ngModel.$viewValue >= max) {
                        ngModel.$setViewValue(round(max) + "");
                    } else {
                        ngModel.$setViewValue(round(++ngModel.$viewValue) + "");
                    }
                    ngModel.$render();
                });

                minus.on("click", function (e) {
                    if (max != null && ngModel.$viewValue <= min) {
                        ngModel.$setViewValue(round(min) + "");
                    } else {
                        ngModel.$setViewValue(round(--ngModel.$viewValue) + "");
                    }
                    ngModel.$render();
                });
            }
        };
    })
    .directive('convertToNumber', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                ngModel.$parsers.push(function (val) {
                    return parseInt(val, 10);
                });
                ngModel.$formatters.push(function (val) {
                    return '' + val;
                });
            }
        };
    })
    .directive("wUnit", ["vCookies", function (vCookies) {
        return {
            restrict: "E",
            link: function (scope, elem, attrs) {
                elem[0].innerHTML = vCookies.units.weight.get();
            }
        }
    }])
    .directive("tUnit", ["vCookies", function (vCookies) {
        return {
            restrict: "E",
            link: function (scope, elem, attrs) {
                var temp = vCookies.units.temperature.get();
                switch (temp) {
                    case vCookies.units.temperature.c:
                        temp = "°C";
                        break;
                    case vCookies.units.temperature.f:
                        temp = "°F";
                        break;
                    case vCookies.units.temperature.k:
                        temp = "K";
                        break;
                    default:
                        temp = "°C";
                        break;
                }
                elem[0].outerHTML = temp;
            }
        }
    }])
    .directive('veitExport', ['vArray', function (vArray) {
        return {
            restrict: 'A',
            templateUrl: "/shared/export",
            scope: {
                item: "=item",
                array: "=array"
            },
            link: function (scope) {
                scope.setSelected = function () {
                    vArray.toggle(scope.array, scope.item);
                }
            }
        }
    }])
    .directive('veitDelete', ['vArray', function (vArray) {
        return {
            restrict: 'A',
            templateUrl: "/shared/delete",
            scope: {
                item: "=item",
                array: "=array"
            },
            link: function (scope) {
                scope.setSelected = function () {
                    vArray.toggle(scope.array, scope.item);
                }
            }
        }
    }])
    .directive("veitHashPath", ["$location", "$d3", function ($location, $d3) {
        return {
            restrict: "A",
            link: function (scope, elem, attrs) {
                var path = window.location.pathname.split("/").join("").toLocaleLowerCase();
                if (path !== attrs.veitHashPath) return;
                function setActive() {
                    var hash = window.location.hash.replace("#/", "").replace("#", "").split("/");
                    var lis = $d3.select(elem[0]).selectAll("li")[0];
                    if (!isNaN(hash[0])) hash.unshift("");
                    lis.forEach(function (li) {
                        var isActive = false;
                        if (li.hasAttribute("path") && hash.indexOf(li.getAttribute("path")) === 0) {
                            isActive = true;
                        }
                        d3.select(li).classed("active", isActive);
                    });
                }

                if (elem[0].hasAttribute("delay")) {
                    setTimeout(setActive, 100);
                } else {
                    setActive();
                }
                scope.$watch(function () { return location.hash }, function () {
                    setActive();
                });
            }
        }
    }])
    .directive("veitPath", ["$location", "$d3", "$rootScope", function ($location, $d3, $rootScope) {

        var devices = [];
        var houses = [];

        $rootScope.$on("treeDataSource", function (ev, args) {
            devices = args.devices;
            houses = args.houses;
        });

        function isActive(element, paths) {
            if (element.hasAttribute("path") && paths.length === 0 && element.getAttribute("path") === "") return true;
            return element.hasAttribute("path") && paths.indexOf(element.getAttribute("path")) !== -1;
        }

        function find(array, id) {
            return array.find(function (h) { return id.toString() === h.Id.toString() });
        }

        function getPath(farmId, houseId, deviceId) {
            if (deviceId != null) {
                var device = find(devices, deviceId);
                if (device != null) houseId = device.HouseId;
            }
            if (houseId != null) {
                var house = find(houses, houseId);
                if (house != null) farmId = house.FarmId;
            }
            return ["f" + farmId, "h" + houseId, "d" + deviceId];
        }

        return {
            restrict: "A",
            link: function (scope, elem, attrs) {
                var path = window.location.pathname.split("/").join("").toLocaleLowerCase();
                if (attrs.veitPath.toLowerCase().split(",").indexOf(path) === -1) return;

                function setActive() {
                    var hash = window.location.hash.replace("#/", "").replace("#", "").split("/");
                    var lis = $d3.select(elem[0]).selectAll("li")[0];
                    var paths = [];

                    if (hash[0] === "farm") paths = getPath(hash[1]);
                    if (hash[0] === "house") paths = getPath(null, hash[1]);
                    if (path.indexOf("detail") !== -1) {

                        if (isNaN(hash[0])) return;
                        if (path.indexOf("sensors") !== -1) paths = getPath(null, null, hash[0]);
                        if (path.indexOf("houses") !== -1) paths = getPath(null, hash[0]);
                    }

                    lis.forEach(function (li) {
                        d3.select(li).classed("active", isActive(li, paths));
                    });
                }

                if (elem[0].hasAttribute("delay")) {
                    setTimeout(setActive, 100);
                } else {
                    setActive();
                }
                scope.$watch(function () { return location.hash }, function () {
                    setActive();
                });
            }
        }
    }])
    .directive('veitToggleMenu', ["$cookies", function ($cookies) {
        return {
            restrict: 'A',
            link: function (scope, elem) {
                elem.bind("click", function () {
                    var cookie = $cookies.get('leftSideMenu');
                    if (cookie) {
                        $cookies.remove('leftSideMenu', { path: "/" });
                    } else {
                        $cookies.put('leftSideMenu', true, { path: "/" });
                    }
                });
            }
        }
    }])
    .directive('veitImageBase', ["$http", "$modal", function ($http, $modal) {
        function base64(file, callback) {
            var coolFile = {};
            function readerOnload(e) {
                var base64 = btoa(e.target.result);
                coolFile.base64 = base64;
                callback(coolFile);
            };

            var reader = new FileReader();
            reader.onload = readerOnload;
            coolFile.filetype = file.type;
            coolFile.size = file.size;
            coolFile.filename = file.name;
            reader.readAsBinaryString(file);
        }

        return {
            restrict: 'A',
            scope: {
                callback: '&',
                url: '='
            },
            link: function (scope, element) {

                element[0].addEventListener("change", function (e) {
                    if (e.target.files.length === 0) return;
                    base64(e.target.files[0], function (data) {
                        var mInstance = $modal.open({
                            templateUrl: 'imageCrop.html',
                            controller: ['$scope', '$modalInstance', function (sc, $modalInstance) {
                                sc.image = data.base64;
                                sc.cancel = function () {
                                    $modalInstance.dismiss('cancel');
                                }
                                sc.ok = function () {
                                    scope.image = sc.api.crop();
                                    $modalInstance.close();
                                }
                                sc.getApiFunction = function (api) {
                                    sc.api = api;
                                }
                            }]
                        });

                        mInstance.result.then(function () {
                            $http({
                                method: 'POST',
                                data: "\"" + scope.image + "\"",
                                url: scope.url
                            }).success(function () {
                                if (scope.callback()) {
                                    scope.callback()(e, scope.image);
                                } else {
                                    window.location.reload();
                                }
                            });
                        });
                    });
                });
            }
        }
    }])
    .directive('veitPaging', [function () {
        return {
            restrict: 'E',
            scope: {
                pageSize: '=',
                items: '=',
                visible: '=',
                item: '='
            },
            templateUrl: "/Shared/Paging",
            controller: ["$scope", function ($scope) {
                var allItems = [];
                $scope.page = {
                    index: null,
                    size: 0,
                    max: 0
                }

                $scope.$watch("items", function (items) {
                    $scope.page = {
                        index: $scope.page.index || 0,
                        size: $scope.pageSize || 6,
                        max: items.length
                    }
                    allItems = (items || []).slice();
                    if ($scope.page.index >= allItems.length / $scope.page.size) $scope.page.index = Math.floor(allItems.length / $scope.page.size);
                    $scope.goTo($scope.page.index);

                });

                $scope.$watch("item", function (item) {
                    if (item == null) return;
                    var index = allItems.indexOf(item);
                    if (index === -1) return;
                    $scope.goTo(Math.floor(index / $scope.page.size));
                });

                $scope.hasNext = function (index) {
                    return (($scope.page.index + index) * $scope.page.size) < $scope.page.max;
                }

                $scope.hasPrev = function (index) {
                    return $scope.page.index - index >= 0;
                }

                $scope.goTo = function (index) {
                    $scope.page.index = index;
                    var startIndex = $scope.page.size * $scope.page.index;
                    $scope.visible = allItems.slice(startIndex, startIndex + $scope.page.size);
                }

                $scope.next = function () {
                    if (!$scope.hasNext(1)) return;
                    $scope.goTo($scope.page.index + 1);
                }

                $scope.prev = function () {
                    if (!$scope.hasPrev(1)) return;
                    $scope.goTo($scope.page.index - 1);
                }

                $scope.last = function () {
                    return Math.floor(($scope.page.max - 1) / $scope.page.size);
                }
            }]
        }
    }])
    .directive('sexIcon', ["vEnums", function (vEnums) {
        return {
            restrict: 'A',
            scope: {
                sexIcon: '='
            },
            link: function (scope, element) {
                element.addClass(vEnums.sexModes.getIcon(scope.sexIcon));
            }
        }
    }])
    .filter('sexIcon', ["vEnums", function (vEnums) {
        function filter(type) {
            return vEnums.sexModes.getIcon(type);
        }
        filter.$stateful = true;
        return filter;
    }])
    .directive('deviceIcon', ["vEnums", function (vEnums) {
        return {
            restrict: 'A',
            scope: {
                deviceIcon: '='
            },
            link: function (scope, element) {
                scope.$watch('deviceIcon', function (newValue, oldValue) {
                    if (oldValue != null) element.removeClass(vEnums.deviceType.getIcon(oldValue));
                    if (newValue != null) element.addClass(vEnums.deviceType.getIcon(newValue));
                });
            }
        }
    }])
    .directive('veitGauge', [function () {
        return {
            restrict: 'E',
            scope: {
                model: '=',
                title: '@',
                sensor: '@',
                unit: '@'
            },
            replace: true,
            templateUrl: "_gaugeTemplate.html",
            controller: ['$scope', "vCookies", "vEnums", "$locale", function ($scope, vCookies, vEnums, $locale) {

                var limits = {
                    weight: { min: null, max: null },
                    temperature: { min: null, max: null },
                    humidity: { min: null, max: null },
                    co2: { min: null, max: null }
                }

                $scope.turn = 0;
                $scope.floor = '';
                $scope.decimals = '';
                $scope.sex = null;

                function setValue(model) {
                    if (model == null) return;
                    if (model["sex"] != null) {
                        if (model["sex"] === vEnums.sexModes.male) $scope.sex = "mars";
                        if (model["sex"] === vEnums.sexModes.female) $scope.sex = "venus";
                        if (model["sex"] === vEnums.sexModes.undefined) $scope.sex = "question";
                    }
                    if (model.value == null) return;
                    $scope.floor = Math.floor(model.value);
                    var value = model.value.toString().split(".")[1];
                    $scope.decimal = value == null || model.decimals === 0 ? "" : $locale["NUMBER_FORMATS"]["DECIMAL_SEP"] + value.substring(0, model.decimals);

                    var limit = limits[$scope.sensor];
                    if (limit == null) return;
                    $scope.min = limit.min;
                    $scope.max = limit.max;
                    if (model.min != null) $scope.min = model.min;
                    if (model.max != null) $scope.max = model.max;
                    if ($scope.min == null || $scope.max == null) {
                        $scope.turn = 0;
                        return;
                    }
                    $scope.turn = (model.value - $scope.min) / ($scope.max - $scope.min) * 0.4 + 0.05;
                    if (model.value < $scope.min) $scope.turn = 0.05;
                    if (model.value > $scope.max) $scope.turn = 0.5;
                }

                $scope.$watch('model', function (model) {
                    $scope.floor = '';
                    $scope.decimal = '';
                    $scope.turn = 0;
                    if (model == null) return;
                    if (Array.isArray(model)) {
                        setValue(model[0]);
                    } else {
                        setValue(model);
                    }

                }, true);

                $scope.unit = vCookies.units.getName($scope.sensor);
                $scope.icon = '_' + $scope.sensor + 'Icon.html';
            }]
        }
    }])
    .directive('veitEdit', [function () {

        var maxDepth = 3;
        function getInput(elem, i) {
            if (i === maxDepth) return null;
            var res = elem.parentElement.getElementsByTagName("textarea")[0];
            if (res == null) res = elem.parentElement.getElementsByTagName("input")[0];
            if (res == null) return getInput(elem.parentElement, i + 1);
            return res;
        }

        return {
            restrict: 'A',
            link: function (scope, elem) {
                elem[0].addEventListener("click", function () {
                    var input = getInput(elem[0], 0);
                    if (input == null) return;
                    setTimeout(function () { input.select() }, 0);
                });
            }
        }
    }])
    .filter('curveIcon', ["vEnums", function (vEnums) {
        function filter(type) {
            return vEnums.curveTypes.getIcon(type);
        }
        filter.$stateful = true;
        return filter;
    }])
    .filter('value', ["vSensor", function (vSensor) {
        function filter(sampleGroup, path) {
            return vSensor.findValue(sampleGroup, path);
        }
        filter.$stateful = true;
        return filter;
    }])
    .directive('focusOn', ["$timeout", function ($timeout) {
        return {
            restrict: 'A',
            priority: -100,
            link: function ($scope, $element, $attr) {
                $scope.$watch($attr.focusOn,
                    function (focusVal) {
                        $timeout(function () {
                            focusVal ? $element[0].focus() :
                                $element[0].blur();
                        });
                    }
                );

            }
        };
    }])
    .directive('eventIcon', ["vEnums", function (vEnums) {
        return {
            restrict: 'A',
            scope: {
                eventIcon: '='
            },
            link: function (scope, element) {
                element.addClass(vEnums.eventStates.getIcon(scope.eventIcon));
            }
        }
    }])
    .filter('eventIcon', ["vEnums", function (vEnums) {
        function filter(state) {
            return vEnums.eventStates.getIcon(state);
        }
        filter.$stateful = true;
        return filter;
    }])
    .directive('veitLang', ['$cookies', function ($cookies) {
        return {
            restrict: 'A',
            scope: {
                veitLang: '@',
                isLink: '='
            },
            link: function (scope, element) {
                var lang = $cookies.get('lang') || 'en-US';
                if (scope.isLink === true) {
                    if (lang === scope.veitLang) {
                        element.remove();
                    } else {
                        element[0].addEventListener('click', function () {
                            $cookies.put('lang', scope.veitLang);
                            $cookies.put(".AspNetCore.Culture", `c=${scope.veitLang}|uic=${scope.veitLang}`, { path: "/", expires: new Date("1.1.2035") });
                            location.reload();
                        });
                    }
                } else {
                    if (lang !== scope.veitLang) {
                        element.remove();
                    }
                }
            }
        }
    }])
    .directive('tutorSteps', ['$rootScope', function ($rootScope) {
        return {
            restrict: 'A',
            scope: {
                tutorSteps: '='
            },
            link: function (scope, element) {
                element[0].addEventListener("click", function (e) {
                    $rootScope.$broadcast("nextStep", scope.tutorSteps);
                });
            }
        }
    }])
    .directive('generateDialog', ["$http", "$modal", "$rootScope", "vLoader", function ($http, $modal, $rootScope, vLoader) {

        function sendCreate(crate) {
            return $http({ method: 'POST', params: { create: crate }, url: "/users/demodata" });
        }

        function showModal() {
            var mInstance = $modal.open({
                templateUrl: 'generateDialog.html',
                controller: ['$scope', '$modalInstance', function (sc, $modalInstance) {
                    sc.cancel = function () {
                        sendCreate(false);
                        $modalInstance.dismiss('cancel');
                    }
                    sc.ok = function () {
                        $modalInstance.close();
                    }
                }]
            });

            mInstance.result.then(function () {
                vLoader.loading(true);
                sendCreate(true).success(function () {
                    vLoader.loading(false);
                    window.location.reload();
                });
            });
        }

        return {
            restrict: 'E',
            scope: {
                url: "@"
            },
            link: function (scope, element) {
                $rootScope.$on("generateDataDialog", function () {
                    showModal();
                });
            }
        }
    }]);