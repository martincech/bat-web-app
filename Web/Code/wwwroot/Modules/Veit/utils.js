﻿angular.module('veitModule')
.controller('terminalList', [
        "$scope", "vEnums", function ($scope, vEnums) {
            $scope.devices = [];
            $scope.houses = [];
            $scope.farms = [];

            $scope.house = {};
            $scope.farms = {};

            $scope.$on("treeData", function (ev, args) {
                $scope.devices = args.devices;
                $scope.houses = args.houses;
                $scope.farms = args.farms;

                $scope.house = args.house;
                $scope.farm = args.farm;
            });
            $scope.getDeviceType = vEnums.deviceType.getIcon;
        }
])
.controller('housesList', [
    "$scope", function ($scope) {
        $scope.houses = [];
        $scope.farms = [];

        $scope.house = {};
        $scope.farm = {};

        $scope.$on("treeDataSource", function (ev, argsO) {
            var args = angular.copy(argsO);
            $scope.houses = args.houses;
            $scope.farms = args.farms;
            $scope.farms.forEach(function (f) {
                f.Houses = $scope.houses.filter(function (h) {
                    return h.FarmId === f.Id;
                });
            });
            $scope.farm.Houses = args.houses.filter(function (h) { return h.FarmId == null });

        });
    }
])
.controller('newsletterCtrl', [
    "$scope", "$http", "$timeout", "$cookies", function ($scope, $http, $timeout, $cookies) {

        $scope.hidden = false;
        $scope.show = false;
        $scope.email = "";
        $scope.isEmailValid = false;
        var guid = $cookies.get("guid");

        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

        $scope.$watch("email", function (newValue) {
            $scope.isEmailValid = validateEmail(newValue);
        });

        $scope.close = function () {
            $scope.show = !$scope.show;
            if ($scope.show) {
                $cookies.remove("newsletters");
            } else {
                $cookies.put("newsletters", "no", { path: "/", expires: new Date(2040, 1) });
            }
        }

        $scope.register = function () {
            if (!$scope.isEmailValid) return;
            $http({
                url: '/logs/newsletter',
                method: "POST",
                data: { 'email': $scope.email, 'guid': guid }
            }).then(function () {
                $cookies.put("newsletters", "yes", { path: "/", expires: new Date(2040, 1) });
                $scope.show = false;
                $scope.hidden = true;
            });
        }

        var newsletter = $cookies.get("newsletters");
        if (newsletter === "yes") $scope.hidden = true;
        if (newsletter) return;

        var ts = $cookies.get("ts");
        var seconds = (new Date()).getTime() / 1000;
        if (ts == null) {
            $cookies.put("ts", seconds, { path: "/", expires: new Date(2040, 1) });
        } else {
            seconds = parseInt(ts);
        }
        var diff = ((new Date()).getTime() / 1000) - seconds;

        $timeout(function () {
            $scope.show = true;
        }, (30 - diff) * 1000);
    }
])
.factory("vLoader", [
    "$timeout", "vMessages", function ($timeout, vMessages) {
        var factory = {};
        var elementId = "content-loader";
        var classes = ["loading", "hidden"];

        var timeout = null;

        function addClass(elm, className) {
            if (elm.classList.contains(className)) return;
            elm.classList.add(className);
        }

        function removeClass(elm, className) {
            if (!elm.classList.contains(className)) return;
            elm.classList.remove(className);
        }

        function loadFail() {
            timeout = null;
            vMessages.showDialog("errorDialog.html", true);
            factory.loading(false);
        }

        factory.loading = function (isLoading) {
            var loaderElement = document.getElementById(elementId);
            if (!loaderElement) return;
            addClass(loaderElement, isLoading ? classes[0] : classes[1]);
            removeClass(loaderElement, isLoading ? classes[1] : classes[0]);
            if (timeout != null) $timeout.cancel(timeout);
            if (isLoading) {
                timeout = $timeout(loadFail, 30000);
            } else {
                timeout = null;
            }
        }

        factory.isLoading = function () {
            var loaderElement = document.getElementById(elementId);
            if (!loaderElement) return false;
            return loaderElement.classList.contains(classes[0]);
        }
        return factory;
    }
])
.factory('vFile', function () {
    var factory = {};


    function hex2Ascii(hexx) {
        var hex = hexx.toString();
        var str = '';
        for (var i = 0; i < hex.length; i += 2)
            str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
        return str;
    }

    factory.arrayToCSV = function (objArray, withoutDate) {
        var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        var str = '';
        if (!array || !array.length) return null;
        var date = "exported;" + new Date().toISOString().slice(0, 10);
        date += '\r\n';
        date += '\r\n';
        var header = array[0];
        for (var key in header) {
            if (header.hasOwnProperty(key)) {
                if (str !== '') str += ";";
                str += key;
            }
        }
        str += '\r\n';
        for (var i = 0; i < array.length; i++) {
            var line = '';
            for (var index in array[i]) {
                if (line !== '') line += ';';

                line += array[i][index];
            }

            str += line + '\r\n';
        }
        if (!withoutDate) str = date + str;
        return str.split(".").join(",");
    }

    factory.save = function (fileName, text) {
        var blob = new Blob([text], { type: "text/csv;charset=utf-8;" });
        saveAs(blob, fileName);
        return true;
    }

    factory.load = function (input, callback) {

        var file, fr;

        function returnResult() {
            var n, aByte, byteStr;

            var markup = [];
            var result = fr.result;
            for (n = 0; n < result.length; ++n) {
                aByte = result.charCodeAt(n);
                byteStr = aByte.toString(16);
                if (byteStr.length < 2) {
                    byteStr = "0" + byteStr;
                }

                byteStr = hex2Ascii(byteStr);
                markup.push(byteStr);
            }
            callback(markup.join(""));
        }

        if (typeof window.FileReader !== 'function') {
            alert("The file API isn't supported on this browser yet.");
            return;
        }

        if (!input) {
            alert("Um, couldn't find the fileinput element.");
        } else if (!input.files) {
            alert("This browser doesn't seem to support the `files` property of file inputs.");
        } else if (!input.files[0]) {
            return;
        } else {
            file = input.files[0];
            fr = new FileReader();
            fr.onload = returnResult;
            fr.readAsText(file);
        }
        input.value = "";
    }

    factory.onLoad = function (inputId, callback) {
        var inputElement = document.getElementById(inputId);

        function fileSelected() {
            factory.load(inputElement, callback);
        }

        inputElement.addEventListener("change", fileSelected, false);
    }

    return factory;
})
.factory('vArray', function () {
    var factory = {};

    factory.first = function (array, item, prop) {
        var result = array.filter(function (d) {
            return prop(d) === item;
        });
        return result ? result[0] : null;
    }

    factory.addUnique = function (array, item) {
        if (!array || array.indexOf(item) !== -1) return;
        array.push(item);
    }

    factory.remove = function (array, item) {
        if (!array) return;
        var j = 0;
        for (var i = 0, l = array.length; i < l; i++) {
            if (array[i] !== item) {
                array[j++] = array[i];
            }
        }
        array.length = j;
    }

    factory.toggle = function (array, item) {
        if (!array) return;
        var index = array.indexOf(item);
        if (index === -1) {
            array.push(item);
        } else {
            array.splice(index, 1);
        }
    }

    factory.diff = function (a, b) {
        var i, la = a.length, lb = b.length, res = [];
        if (!la) return b;
        else if (!lb) return a;
        for (i = 0; i < la; i++) {
            if (b.indexOf(a[i]) === -1) res.push(a[i]);
        }
        for (i = 0; i < lb; i++) {
            if (a.indexOf(b[i]) === -1) res.push(b[i]);
        }
        return res;
    }

    factory.intersect = function (a, b) {
        var ai = 0, bi = 0;
        var result = [];

        while (ai < a.length && bi < b.length) {
            if (a[ai] < b[bi]) {
                ai++;
            } else if (a[ai] > b[bi]) {
                bi++;
            } else /* they're equal */ {
                result.push(a[ai]);
                ai++;
                bi++;
            }
        }
        return result;
    }

    return factory;
})
.factory('vEnums', function () {
    var factory = {};

    factory.eventStates = {
        active: 0,
        paused: 1,
        stopped: 2,
        closed: 3,
        all: undefined,
        getIcon: function (state) {
            if (state === factory.eventStates.active) return "fa-play-circle";
            if (state === factory.eventStates.stopped) return "fa-pause-circle";
            if (state === factory.eventStates.closed) return "fa-history";
            return "fa-question";
        }
    }

    factory.curveTypes = {
        isWeight: function (type) {
            return type === factory.curveTypes.birdFemale
                || type === factory.curveTypes.birdMale
                || type === factory.curveTypes.birdUndefined;
        },
        weight: -1,
        birdFemale: 0,
        birdMale: 1,
        birdUndefined: 2,
        humidity: 3,
        temperature: 4,
        detection: 5,
        co2: 6,
        getIcon: function (type) {
            if (factory.curveTypes.isWeight(type)) return 'icon-weight';
            if (type === factory.curveTypes.temperature) return 'icon-temperature';
            if (type === factory.curveTypes.humidity) return 'icon-humidity';
            if (type === factory.curveTypes.co2) return 'icon-co2';
            return 'fa-question';
        }
    }

    factory.sexModes = {
        undefined: 0,
        male: 1,
        female: 2,
        mixed: 3,
        getIcon: function (type) {
            if (factory.sexModes.male === type || type === "male") return "fa-mars";
            if (factory.sexModes.female === type || type === "female") return "fa-venus";
            if (factory.sexModes.mixed === type || type === "mixed") return "fa-venus-mars";
            return "fa-question";
        }
    }

    factory.setSexMode = function (actual, selected) {
        if (actual === selected) {
            actual = factory.sexModes.undefined;
        } else if (actual === factory.sexModes.undefined) {
            actual = selected;
        } else if (actual === factory.sexModes.mixed) {
            actual -= selected;
        } else {
            actual += selected;
        }
        return actual;
    }

    factory.stepModes = {
        enter: 0,
        leave: 1,
        both: 2
    }

    factory.setStepMode = function (actual, selected) {
        if (actual === selected) return actual;
        if (actual === factory.stepModes.both) {
            actual = +!selected;
        } else {
            actual = factory.stepModes.both;
        }
        return actual;
    }

    factory.calibrationStates = {
        stop: 0,
        start: 1,
        firstStableSample: 2,
        calibrated: 3
    }

    factory.statTypes = {
        Temperature: 1,
        Humidity: 2,
        Co2: 4,
        BirdWeight: 8,
        Ammonia: 16,
        RawWeight: 32,
        Gain: 64,
        Target: 128,
        Uniformity: 256,
        TargetDifference: 512
    }

    factory.sampleTypes = {
        Temperature: 0,
        Humidity: 1,
        Co2: 2,
        Ammonia: 3,
        RawWeight: 4,
        BirdWeight: 5
    }

    factory.algorithms = {
        slow: 0,
        fast: 1
    }

    factory.sampleSizes = {
        All: 1,
        HalfHour: 2,
        Hour: 4,
        Day: 8
    }

    factory.weighModes = {
        automatic_slow: 0,
        automatic_fast: 1,
        curve: 2
    }

    factory.deviceType = {
        none: 0,
        sensorPack: 1,
        bat2: 2,
        externalSensor: 3,
        bat2cable: 4,
        externalTemperature: 5,
        externalHumidity: 6,
        externalCo2: 7,
        externalWeight: 8,
        bat3: 9,
        getIcon: function (type) {
            if (type === factory.deviceType.none) return 'fa-question';
            if (type === factory.deviceType.sensorPack) return 'fa-microchip';
            if (type === factory.deviceType.bat2) return 'fa-balance-scale';
            if (type === factory.deviceType.externalSensor) return 'fa-microchip';
            if (type === factory.deviceType.bat2cable) return 'fa-balance-scale';
            if (type === factory.deviceType.externalTemperature) return 'icon-temperature';
            if (type === factory.deviceType.externalHumidity) return 'icon-humidity';
            if (type === factory.deviceType.externalCo2) return 'icon-co2';
            if (type === factory.deviceType.externalWeight) return 'fa-balance-scale';
            if (type === factory.deviceType.bat3) return 'fa-balance-scale';
            return 'fa-home';
        }
    }

    return factory;
})
.factory('vDate', [
    '$d3', function ($d3) {
        var factory = {};
        factory.parse = function (dateString) {
            if (dateString == null) return null;
            if (dateString instanceof Date) return dateString;
            var iso = $d3.time.format.utc("%Y-%m-%dT%H:%M:%S");
            var date = iso.parse(dateString.split("Z")[0]);
            if (date == null) {
                return new Date(dateString);
            }
            return $d3.time.minute.offset(date, date.getTimezoneOffset());
        }

        var oneDay = 1000 * 60 * 60 * 24;
        factory.daysDiff = function (date1, date2) {
            if (date1 == null || date2 == null || !date1.getTime || !date2.getTime) return 0;
            return Math.round((date2.getTime() - date1.getTime()) / oneDay);
        }
        return factory;
    }
])
.factory('vCookies', [
    "$cookies", '$window', function ($cookies, $window) {
        var factory = {};

        var rootPath = "/";
        var maxExpirationDate = new Date((new Date()).getFullYear() + 2, 1);

        var setCookie = function (name, value, expirationDate, path) {
            $cookies.put(name, value, { expires: expirationDate, path: path });
        }

        factory.languages = ["en-US", "de-DE"];
        factory.language = {
            en: factory.languages[0],
            de: factory.languages[1],
            default: factory.languages[0],
            get: function () {
                return $cookies.get("lang") || factory.language.en;
            },
            set: function (lang) {
                if (languages.indexOf(lang) !== -1) setCookie("lang", lang, maxExpirationDate, rootPath);
            }
        }
        var index = factory.languages.indexOf(factory.language.get());
        if (index === -1) index = 0;
        factory.tutorial = {};
        factory.tutorial.enum = {
            once: "once",
            all: "all",
            never: "never"
        }

        factory.tutorial.visited = function () {


            var path = $window.location.pathname.split("/")[1] || "";
            var result = $cookies.get("visitedPages") || "";
            var visited = true;
            if (result.indexOf(path) === -1) {
                visited = false;
                result += path + ",";
                setCookie("visitedPages", result, maxExpirationDate, rootPath);
            }
            if ($cookies.get("showTutorial") === factory.tutorial.enum.never) return true;
            if ($cookies.get("showTutorial") === factory.tutorial.enum.all) return false;

            //return true;
            //to enable recently visited page tutorial remove this previous line
            return visited;
        }

        factory.tutorial.getStep = function () {
            var step = $cookies.get("tutor");
            return step == null ? null : parseInt(step);
        }

        factory.tutorial.reset = function () {
            $cookies.put("visitedPages", "", { expires: new Date(2035, 1), path: "/" });
        }

        factory.hints = {};
        factory.hints.enabled = function () {
            if ($cookies.get("showHints") == null) return true;
            return $cookies.get("showHints") === "true";
        }

        factory.dateFormats = [
            "MM/dd/yyyy hh:mm:ss a",
            "dd.MM.yyyy HH:mm:ss"
        ];

        factory.getDateFormat = function () {
            return $cookies.get("dateFormat") || factory.dateFormats[index];
        }


        factory.numberFormats = [
            ".",
            ","
        ];

        factory.getNumberFormat = function () {
            return $cookies.get("numberFormat") || factory.numberFormats[index];
        }

        var units = {
            weight: null,
            decimals: null,
            temperature: null
        }

        var unitNames = {
            weight: function () {
                return factory.units.weight.get();
            },
            temperature: function () {
                var unit = factory.units.temperature.get();
                switch (unit) {
                    case factory.units.temperature.c:
                        unit = "°C";
                        break;
                    case factory.units.temperature.f:
                        unit = "°F";
                        break;
                    case factory.units.temperature.k:
                        unit = "K";
                        break;
                    default:
                        unit = "°C";
                        break;
                }
                return unit;
            },
            humidity: function () {
                return "%";
            },
            co2: function () {
                return "ppm";
            }
        }

        factory.units = {}
        factory.units = {
            decimals: {
                g: 2,
                kg: 3,
                lb: 3,
                get: function () {
                    if (units.decimals == null) {
                        var decimals = $cookies.get("decimals");
                        units.decimals = decimals != null ? parseInt(decimals) : factory.units.decimals[factory.units.weight.get()];
                    }
                    return units.decimals;
                },
                set: function (value) {
                    if (value == null) return;
                    units.decimals = value;

                    setCookie("decimals", value, maxExpirationDate, rootPath);
                }
            },
            weight: {
                g: "g",
                kg: "kg",
                lb: "lb",
                get: function () {
                    if (units.weight == null) units.weight = $cookies.get("weightUnit") || factory.units.weight.g;
                    return units.weight;
                },
                set: function (value) {
                    if (value == null) return;
                    units.weight = value;
                    setCookie("weightUnit", value, maxExpirationDate, rootPath);
                }
            },
            temperature: {
                c: "c",
                k: "k",
                f: "f",
                get: function () {
                    if (units.temperature == null) units.temperature = $cookies.get("temperatureUnit") || factory.units.temperature.c;
                    return units.temperature;
                },
                set: function (value) {
                    if (value == null) return;
                    units.temperature = value;
                    setCookie("temperatureUnit", value, maxExpirationDate, rootPath);
                }
            },
            getName: function (sensor) {
                var func = unitNames[sensor];
                if (func == null) return "";
                return func();
            }
        }
        return factory;
    }
])
.factory('vHash', [
    "$window", function ($window) {
        var factory = {};
        factory.parseHash = function (paths) {
            var result = ["", undefined];
            var hash = $window.location.hash.toLocaleLowerCase();
            paths.forEach(function (item) {
                if (hash.indexOf("#/" + item[0]) !== -1) result = item;
            });
            var numbers = (window.location.hash.match(/\d+/g) || []);
            var id = numbers[numbers.length - 1];
            return ["#/" + result[0] + (result[0] !== "" ? "/" : ""), result[1], id];
        }
        return factory;
    }
])
.factory('vUnits', [
    "vCookies", function (vCookies) {
        var factory = {};
        factory.gToKg = function (weight) {
            if (isNaN(weight)) return null;
            var res = weight / 1000;
            return Math.round(res * 1000) / 1000;
        }

        factory.gToLb = function (weight) {
            if (isNaN(weight)) return null;
            var res = weight / 453.59237;
            return Math.round(res * 1000) / 1000;
        }

        factory.cToK = function (temp) {
            if (isNaN(temp)) return null;
            return temp * 1.8 + 32;
        }

        factory.cToF = function (temp) {
            if (isNaN(temp)) return null;
            return temp + 273.15;
        }

        factory.valueToType = function (value, type) {
            if (type === vCookies.units.weight.g) return value;
            if (type === vCookies.units.weight.kg) return factory.gToKg(value);
            if (type === vCookies.units.weight.lb) return factory.gToLb(value);

            if (type === vCookies.units.temperature.c) return value;
            if (type === vCookies.units.temperature.f) return factory.cToF(value);
            if (type === vCookies.units.temperature.k) return factory.cToK(value);

            return value;
        }

        return factory;
    }
])
.factory('vValid', [
    "vEnums", function (vEnums) {
        var f = {};
        f.curves = {};

        function hasMinMaxAndValidDay(point) {
            if (point.Day == null || point.Day < 0 || point.Min == null || point.Max == null) return false;
            return true;
        }

        function isMinSmallerThanMax(point) {
            return point.Min < point.Max;
        }

        f.curves.isValidWeigt = function (point) {
            if (point == null) return false;
            if (point.Value == null || !hasMinMaxAndValidDay(point)) return false;
            if (point.Value < 0 || point.Min < 0 || point.Max < 0) return false;
            return true;
        }

        f.curves.isValidTemperature = function (point) {
            if (point == null) return false;
            if (!hasMinMaxAndValidDay(point)) return false;
            if (!isMinSmallerThanMax(point)) return false;
            return true;
        }

        f.curves.isValidHumidity = function (point) {
            if (point == null) return false;
            if (!hasMinMaxAndValidDay(point)) return false;
            if (point.Min < 0 || point.Min > 100 || point.Max < 0 || point.Max > 100) return false;
            if (!isMinSmallerThanMax(point)) return false;
            return true;
        }

        f.curves.isValidCo2 = function (point) {
            if (point == null) return false;
            if (!hasMinMaxAndValidDay(point)) return false;
            if (point.Min < 0 || point.Max < 0) return false;
            if (!isMinSmallerThanMax(point)) return false;
            return true;
        }

        f.curves.isValid = function (curveType, point) {
            var func;
            if (curveType === vEnums.curveTypes.temperature) {
                func = f.curves.isValidTemperature;
            } else if (curveType === vEnums.curveTypes.humidity) {
                func = f.curves.isValidHumidity;
            } else if (curveType === vEnums.curveTypes.co2) {
                func = f.curves.isValidCo2;
            } else {
                func = f.curves.isValidWeigt;
            }
            return func(point);
        }

        f.email = {
            regex: new RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i)
        };
        f.email.isValid = function (email) {
            return f.email.regex.test(email);
        }

        return f;
    }
])
.factory('veit', [
    "vFile", "vArray", "vEnums", 'vCookies', 'vHash', 'vValid', function (vFile, vArray, vEnums, vCookies, vHash, vValid) {
        var factory = {};
        factory.file = vFile;
        factory.array = vArray;
        factory.enums = vEnums;
        factory.cookies = vCookies;
        factory.hash = vHash;
        factory.valid = vValid;
        return factory;
    }
])
.factory('vCurve', [
    "vEnums", "vCookies", "vUnits", "vArray", "vResources", function (vEnums, vCookies, vUnits, vArray, vResources) {
        var factory = {};

        function copyPoint(point) {
            return { Day: point.Day, Min: point.Min, Max: point.Max, Value: point.Value };
        }

        var defPoints = [
            { Id: vEnums.curveTypes.birdFemale, Point: { Day: 1, Min: 5, Max: 5, Value: 40 }, Point2: { Day: 7, Min: 5, Max: 5, Value: 80 } },
            { Id: vEnums.curveTypes.birdMale, Point: { Day: 1, Min: 5, Max: 5, Value: 60 }, Point2: { Day: 7, Min: 5, Max: 5, Value: 100 } },
            { Id: vEnums.curveTypes.birdUndefined, Point: { Day: 1, Min: 10, Max: 10, Value: 50 }, Point2: { Day: 7, Min: 5, Max: 5, Value: 90 } },
            { Id: vEnums.curveTypes.detection, Point: { Day: 1, Min: 30, Max: 30, Value: 50 }, Point2: { Day: 7, Min: 5, Max: 5, Value: 90 } },
            { Id: vEnums.curveTypes.humidity, Point: { Day: 1, Min: 20, Max: 35, Value: null }, Point2: { Day: 7, Min: 15, Max: 75, Value: null } },
            { Id: vEnums.curveTypes.co2, Point: { Day: 1, Min: 250, Max: 1500, Value: null }, Point2: { Day: 7, Min: 250, Max: 2500, Value: null } },
            { Id: vEnums.curveTypes.temperature, Point: { Day: 1, Min: 25, Max: 30, Value: null }, Point2: { Day: 7, Min: 15, Max: 40, Value: null } },
            { Id: vEnums.curveTypes.weight, Point: { Day: 1, Min: 5, Max: 5, Value: 50 }, Point2: { Day: 7, Min: 5, Max: 5, Value: 100 } }
        ];

        factory.getDefaultPoint = function (curveType, next) {
            var result = factory.find(defPoints, curveType);
            var dPoint = result ? result.Point : defPoints[0].Point;
            if (next) dPoint = result ? result.Point2 : defPoints[0].Point2;
            var point = copyPoint(dPoint);
            var temperature = vCookies.units.temperature.get();
            var weight = vCookies.units.weight.get();
            switch (curveType) {
                case vEnums.curveTypes.temperature:
                    point.Min = vUnits.valueToType(point.Min, temperature);
                    point.Max = vUnits.valueToType(point.Max, temperature);
                    break;
                case vEnums.curveTypes.humidity:
                    break;
                default:
                    point.Value = vUnits.valueToType(point.Value, weight);
                    break;
            }
            return point;
        }

        factory.copy = function (curve) {
            if (curve == null) return null;
            return JSON.parse(JSON.stringify(curve));
        }

        factory.find = function (array, id) {
            if (!array || array.constructor !== Array || id == null) return null;
            return array.find(function (d) { return d.Id === id });
        }

        factory.setDateByDay = function (curvePoints, date, day) {
            if (curvePoints == null || date == null || day == null) return;
            var from = new Date(date.getTime() - day * 24 * 60 * 60 * 1000);
            factory.setDate(curvePoints, from, 0);
        }

        factory.setDate = function (curvePoints, from, addDay) {
            if (curvePoints == null || from == null || addDay == null) return;
            curvePoints.forEach(function (point) {
                var daysInMilisec = (point.Day + addDay) * 24 * 60 * 60 * 1000;
                point.TimeStamp = new Date(from.getTime() + daysInMilisec);
            });
        }

        function getCurvePoints(item) {
            var curve = {
                rows: [],
                points: []
            };

            var rows = ["day", "target", "min", "max"];

            var header = { day: "Day", target: "Target [g]", min: "Min [%]", max: "Max [%]" };
            if (item.Type === vEnums.curveTypes.humidity) {
                rows.splice(1, 1);
            }
            if (item.Type === vEnums.curveTypes.co2) {
                rows.splice(1, 1);
            }
            if (item.Type === vEnums.curveTypes.temperature) {
                rows.splice(1, 1);
                header.min = "Min [°C]";
                header.max = "Max [°C]";
            }
            curve.rows = rows;
            curve.points.push(header);
            item.CurvePoints.forEach(function (data) {
                var item = { day: data.Day, min: data.Min, max: data.Max }
                if (rows.length === 4)
                    item.target = data.Value;
                curve.points.push(item);
            });
            return curve;
        }

        function curveToCsv(items, rows) {
            var csv = "";
            if (!items || !rows) return csv;
            rows.forEach(function (row) {
                var line = "";
                if (csv !== "") csv += "\r\n";
                items.forEach(function (item) {
                    if (line !== "") line += ";";
                    line += item[row];
                });
                csv += line;
            });
            return csv.split(".").join(",");
        }

        factory.curvesToCsv = function (curves, ids) {
            if (curves == null || ids === null) return "";
            var csv = "";
            curves.forEach(function (item) {
                if (ids.indexOf(item.Id) !== -1) {
                    var curve = getCurvePoints(item);
                    if (csv !== "") csv += '\r\n\r\n';
                    csv += item.Name;
                    csv += '\r\n';
                    csv += curveToCsv(curve.points, curve.rows);
                }
            });
            return csv;
        }

        var axisIcons = {
            weight: '\ue904',
            gain: '\ue905',
            target: '\ue906',
            uniformity: '\ue903',
            sigma: '\ue90a',
            cv: '\ue909',
            count: '\ue908',
            temperature: '\ue902',
            humidity: '\ue900',
            co2: '\ue907'
        }

        var units = {
            uniformity: "%",
            target: vCookies.units.getName("weight"),
            gain: vCookies.units.getName("weight")
        }

        function getUnitAxis(axis) {
            return {
                icon: axisIcons[axis],
                text: vResources.get(axis),
                unit: units[axis] || vCookies.units.getName(axis)
            };
        }

        factory.axis = {
            date: getUnitAxis("date"),
            day: getUnitAxis("day"),
            weight: getUnitAxis("weight"),
            temperature: getUnitAxis("temperature"),
            humidity: getUnitAxis("humidity"),
            co2: getUnitAxis("co2"),
            gain: getUnitAxis("gain"),
            target: getUnitAxis("target"),
            uniformity: getUnitAxis("uniformity"),
            sigma: getUnitAxis("sigma"),
            cv: getUnitAxis("cv"),
            count: getUnitAxis("count")
        }

        factory.getAxisByType = function (curveType) {
            if (vEnums.curveTypes.isWeight(curveType)) return factory.axis.weight;
            if (vEnums.curveTypes.temperature === curveType) return factory.axis.temperature;
            if (vEnums.curveTypes.humidity === curveType) return factory.axis.humidity;
            if (vEnums.curveTypes.co2 === curveType) return factory.axis.co2;
            return "";
        }

        factory.isWeightCurve = function (curve) {
            if (curve == null) return false;
            var type = curve.Type;
            return type === vEnums.curveTypes.birdFemale
                || type === vEnums.curveTypes.birdMale
                || type === vEnums.curveTypes.birdUndefined
                || type === vEnums.curveTypes.detection;
        }

        return factory;
    }
])
.factory('vEvent', [
    "vEnums", "vCurve", "vName", "vDate", "$filter", function (vEnums, vCurve, vName, vDate, $filter) {
        var factory = {};

        var defEvent = {
            Name: "Event",
            InitialWeight: vCurve.getDefaultPoint(vEnums.curveTypes.weight).Value,
            InitialWeightFemale: vCurve.getDefaultPoint(vEnums.curveTypes.birdFemale).Value,
            DetectionRange: 30,
            DetectionRangeFemale: 30,
            SexMode: vEnums.sexModes.undefined,
            StepMode: vEnums.stepModes.both,
            WeightMode: vEnums.weighModes.automatic,
            UniformityRange: 20,
            Stabilization: {
                FiltrationAveragingWindow: 5,
                Range: 3,
                WindowSampleCount: 5
            }
        }

        factory.create = function (events, eventLenght) {
            var today = (new Date()).setHours(0, 0, 0, 0);;
            var newEvent = angular.copy(defEvent);
            var maxRow = 0;
            if (events.length > 0) maxRow = Math.max.apply(null, events.map(function (ev) { return ev.Row }));
            newEvent.Row = maxRow + 1;
            newEvent.From = new Date(today);
            newEvent.To = new Date(today + eventLenght * 24 * 60 * 60 * 1000);
            newEvent.Name = vName.createUnique(events, "New Event");
            newEvent.Devices = [];
            return newEvent;
        }

        factory.copy = function (events, event) {
            if (events == null || events.length === 0) return angular.copy(event);
            if (event == null) return event;
            var maxDate = Math.max.apply(null, events.filter(function (ev) { return ev.Row === event.Row }).map(function (ev) { return ev.To }));
            var dateDiff = event.To - event.From;
            var eventCopy = angular.copy(event);
            var dayTicks = 24 * 60 * 60 * 1000;
            eventCopy.From = new Date(maxDate + dayTicks);
            eventCopy.To = new Date(maxDate + dateDiff + dayTicks);
            return eventCopy;
        }

        factory.setAvailableDevices = function (event, events, devices) {
            if (!event) return;

            devices.forEach(function (device) {
                var enabled = true;
                events.forEach(function (ev) {
                    if (event === ev) return;
                    if (ev.Devices.indexOf(device.Id) !== -1) {
                        if (event.From <= ev.From && event.To >= ev.From) enabled = false;
                        if (event.From <= ev.To && event.To >= ev.To) enabled = false;
                        if (event.From <= ev.From && event.To >= ev.To) enabled = false;
                        if (event.From >= ev.From && event.To <= ev.To) enabled = false;
                    }
                });
                device.Enabled = enabled;
                if (event.Devices == null) event.Devices = [];
                if (!enabled && event.Devices.indexOf(device.Id) !== -1) event.Devices.splice(event.Devices.indexOf(device.Id), 1);
            });
        }

        factory.getDefault = function () {
            return angular.copy(defEvent);
        }

        factory.getAge = function (event) {
            if (event == null || event.From == null || event.To == null) return 0;
            var today = new Date();
            var from = vDate.parse(event.From);
            var to = vDate.parse(event.To);
            var diff = 0;
            if (event.State === vEnums.eventStates.active) {
                diff = Math.abs(today.getTime() - from.getTime());
            }
            if (event.State === vEnums.eventStates.closed) {
                diff = Math.abs(to.getTime() - from.getTime());
            }
            return Math.ceil(diff / (1000 * 3600 * 24));
        }

        factory.getClosestToEnd = function (events, state) {
            var visibleEvents = $filter("filter")(events, { State: state });
            if (visibleEvents.length !== 0) {
                var today = new Date();
                var activeEvents = visibleEvents.filter(function (e) { return (new Date(e.To)) >= today });
                if (activeEvents.length > 0) return activeEvents.sort(function (a, b) { return (new Date(a.To)) - (new Date(b.To)) })[0];
            }
            return visibleEvents[0];
        }

        factory.convertDates = function (curves) {
            if (!curves) return [];
            curves.forEach(function (c) {
                c.From = vDate.parse(c.From);
                c.To = vDate.parse(c.To);
            });
            return curves;
        }

        factory.getState = function (ev) {
            var today = new Date();
            var state = vEnums.eventStates.active;
            if (ev.From < today && ev.To < today) state = vEnums.eventStates.closed;
            if (ev.From <= today && ev.To >= today) state = vEnums.eventStates.active;
            if (ev.From > today && ev.To > today) state = vEnums.eventStates.stopped;
            return state;
        }

        factory.filterByState = function (events, state) {
            if (!events) return [];
            if (state == null) return events;
            return events.filter(function (d) { return d.State === state });
        }


        factory.setNameIfDefault = function (event, farms, houses, devices) {
            var name = vName.createUnique([], "New Event");
            if (event.Name.indexOf(name) !== -1 && event.Devices.length !== 0) {
                var device = devices.find(function (d) { return event.Devices.indexOf(d.Id) !== -1 && d.HouseId != null });
                if (device != null) {
                    var house = houses.find(function (d) { return d.Id === device.HouseId });
                    var farm = farms.find(function (d) { return d.Id === house.FarmId });
                    event.Name = farm != null ? farm.Name + " " + house.Name : house.Name;
                    return true;
                }
            }
            return false;
        }

        factory.mapLocation = function(devices, houses, farms) {
            if (devices == null || houses == null) return;
            devices.forEach(function (device) {
                if (device.HouseId == null) return;
                device.House = houses.find(function (h) { return h.Id === device.HouseId });
                if (device.House == null || device.House.FarmId == null || farms == null) return;
                device.Farm = farms.find(function (f) { return device.House.FarmId === f.Id });
            });
            if (farms == null) return;
            houses.forEach(function (h) {
                if (h.FarmId == null) return;
                var farm = farms.find(function (f) { return f.Id === h.FarmId });
                if (farm != null) h.farmName = farm.Name;
            });
        }

        return factory;
    }
])
.factory('vStatTable', [
    "vSensor", function () {
        var factory = {};
        var sexes = ["male", "female", "undefined"];

        function weight(last) {
            if (last == null) return {};
            return last.weight_male || last.weight_female || last.weight_undefined || last.weight || {};
        }

        factory.sortTypes = {
            name: function (devices) {
                return devices.sort(function (a, b) {
                    var nameA = a.Name;
                    var nameB = b.Name;
                    if (a.House) nameA = a.House.Name + " " + nameA;
                    if (b.House) nameB = b.House.Name + " " + nameB;

                    if (a.Farm) nameA = a.Farm.Name + " " + nameA;
                    if (b.Farm) nameB = b.Farm.Name + " " + nameB;

                    if (nameA < nameB) return -1;
                    if (nameA > nameB) return 1;
                    return 0;
                });
            },
            gain: function (devices) { return factory.sort(devices, function (last) { return weight(last).gain }) },
            uniformity: function (devices) { return factory.sort(devices, function (last) { return weight(last).uniformity }) },
            target: function (devices) { return factory.sort(devices, function (last) { return weight(last).target }) },
            average: function (devices) { return factory.sort(devices, function (last) { return weight(last).average }) },
            temperature: function (devices) { return factory.sort(devices, function (last) { return (last.temperature || {}).average }) },
            humidity: function (devices) { return factory.sort(devices, function (last) { return (last.humidity || {}).average }) },
            co2: function (devices) { return factory.sort(devices, function (last) { return (last.co2 || {}).average }) },
            value: function (devices) {
                return devices.sort(function (a, b) {
                    if (a.Values[0] == null) return -1;
                    if (b.Values[0] == null) return 1;
                    return a.Values[0].value - b.Values[0].value;
                });
            },
            event: function (devices) {
                return devices.sort(function (a, b) {
                    if (a.Event == null) return -1;
                    if (b.Event == null) return 1;
                    return a.Event.ActualDay - b.Event.ActualDay;
                });
            }
        };


        factory.showWeightStat = function (devices, type) {
            devices.forEach(function (device) {
                device.Values = [];
                sexes.forEach(function (sex) {
                    var value = ((device.last || {})["weight_" + sex] || {})[type];
                    if (value != null) device.Values.push({ sex: sex, value: value });
                });
            });
        }
        factory.showStat = function (devices, type) {
            devices.forEach(function (device) {
                device.Values = [];
                var value = ((device.last || {})[type.name] || {})[type.type.average]; // vSensor.findValue(device.Stats, [type.name, type.type.average]);
                if (value != null) device.Values.push({ value: value });
            });
        }


        factory.sort = function (devices, getValue) {
            return devices.sort(function (a, b) {
                var valA = getValue(a.last || {});
                var valB = getValue(b.last || {});
                if (valA == null) return -1;
                if (valB == null) return 1;
                return valA - valB;
            });
        }

        return factory;
    }
])
.factory('vSensor', [
    "vEnums", "vCookies", "vCurve", "vDate", function (vEnums, vCookies, vCurve, vDate) {
        var factory = {};

        factory.types = {
            time: {
                day: "day",
                timeStamp: "timestamp",
                time: "time"
            },
            weight: {
                average: "average",
                gain: "gain",
                uniformity: "uniformity",
                cv: "cv",
                sigma: "sigma",
                target: "target",
                count: "count",
                decimals: vCookies.units.decimals.get(),
                list: function () {
                    return [
                        factory.types.time.day,
                        factory.types.time.timeStamp,
                        factory.types.weight.average,
                        factory.types.weight.gain,
                        factory.types.weight.uniformity,
                        factory.types.weight.cv,
                        factory.types.weight.sigma,
                        factory.types.weight.target,
                        factory.types.weight.count
                    ];
                }
            },
            sensor: {
                average: "average",
                min: "min",
                max: "max",
                count: "count",
                decimals: 1,
                list: function () {
                    return [
                        factory.types.time.day,
                        factory.types.time.timeStamp,
                        factory.types.sensor.average,
                        factory.types.sensor.min,
                        factory.types.sensor.max,
                        factory.types.sensor.count
                    ];
                }
            },
            mixed: {
                list: function () {
                    return [
                        factory.types.time.day,
                        factory.types.time.timeStamp,
                        factory.types.weight.average,
                        factory.types.weight.gain,
                        factory.types.weight.uniformity,
                        factory.types.weight.cv,
                        factory.types.weight.sigma,
                        factory.types.weight.target,
                        factory.types.sensor.min,
                        factory.types.sensor.max,
                        factory.types.weight.count
                    ];
                }
            }
        }

        factory.decimals = {
            get: function (sensor, type) {
                if (sensor == null || type == null) return 0;
                if (type === factory.types.weight.average) {
                    if (sensor === factory.sensors.co2) return 0;
                    return sensor.type === factory.types.weight
                        ? factory.types.weight.decimals : factory.types.sensor.decimals;
                }
                return factory.decimals[type];
            }
        };

        factory.decimals[factory.types.weight.gain] = factory.types.weight.decimals;
        factory.decimals[factory.types.weight.target] = factory.types.weight.decimals;
        factory.decimals[factory.types.weight.uniformity] = 1;
        factory.decimals[factory.types.weight.cv] = 1;
        factory.decimals[factory.types.weight.sigma] = 1;
        factory.decimals[factory.types.weight.count] = 0;
        factory.decimals[factory.types.time.day] = 0;
        factory.decimals[factory.types.sensor.min] = factory.types.sensor.decimals;
        factory.decimals[factory.types.sensor.max] = factory.types.sensor.decimals;

        factory.sensors = {
            weightMale: {
                name: "weight_male",
                typeName: "weight",
                inMixed: true,
                sex: vEnums.sexModes.male,
                curveType: vEnums.curveTypes.birdMale,
                axis: vCurve.axis.weight,
                type: factory.types.weight,
                model: { min: null, max: null, value: null, date: null, sex: vEnums.sexModes.male, decimals: factory.types.weight.decimals }
            },
            weightFemale: {
                name: "weight_female",
                typeName: "weight",
                inMixed: true,
                sex: vEnums.sexModes.female,
                curveType: vEnums.curveTypes.birdFemale,
                axis: vCurve.axis.weight,
                type: factory.types.weight,
                model: { min: null, max: null, value: null, date: null, sex: vEnums.sexModes.female, decimals: factory.types.weight.decimals }
            },
            weightUndefined: {
                name: "weight_undefined",
                typeName: "weight",
                inMixed: false,
                sex: vEnums.sexModes.undefined,
                curveType: vEnums.curveTypes.birdUndefined,
                axis: vCurve.axis.weight,
                type: factory.types.weight,
                model: { min: null, max: null, value: null, date: null, sex: vEnums.sexModes.undefined, decimals: factory.types.weight.decimals }
            },
            temperature: {
                name: "temperature",
                typeName: "temperature",
                inMixed: true,
                curveType: vEnums.curveTypes.temperature,
                axis: vCurve.axis.temperature,
                type: factory.types.sensor,
                model: { min: null, max: null, value: null, date: null, sex: null, decimals: factory.types.sensor.decimals }
            },
            humidity: {
                name: "humidity",
                typeName: "humidity",
                inMixed: true,
                curveType: vEnums.curveTypes.humidity,
                axis: vCurve.axis.humidity,
                type: factory.types.sensor,
                model: { min: null, max: null, value: null, date: null, sex: null, decimals: factory.types.sensor.decimals }
            },
            co2: {
                name: "co2",
                typeName: "co2",
                inMixed: true,
                curveType: vEnums.curveTypes.co2,
                axis: vCurve.axis.co2,
                type: factory.types.sensor,
                model: { min: null, max: null, value: null, date: null, sex: null, decimals: 0 }
            },
            list: function () {
                return [factory.sensors.weightMale.typeName, factory.sensors.temperature.typeName, factory.sensors.humidity.typeName, factory.sensors.co2.typeName];
            }
        };

        factory.hasSensor = function (sensors, sensor) {
            if (sensors == null || sensors.length === 0 || sensor == null) return false;
            return sensors.find(function (s) {
                return s.toLowerCase().indexOf(sensor.toLowerCase()) !== -1;
            });
        }

        factory.findGroups = function (sampleGrups, groupId, timeSize, sensor) {
            if (sampleGrups == null || sampleGrups.length === 0) return [];
            return sampleGrups.filter(function (sampleGroup) {
                var result = true;
                if (groupId != null) result = sampleGroup.GroupId === groupId;
                if (timeSize != null) result = result && sampleGroup.TimeSize === timeSize;
                if (sensor != null) result = result && factory.hasSensor(sampleGroup.Sensors, sensor);
                return result;
            });
        };

        factory.findSamples = function (sampleGrup, sensor) {
            if (sampleGrup == null || sampleGrup.Values == null || sensor == null) return [];
            var sensorItem = sampleGrup.Sensors.find(function (s) {
                return s.toLowerCase().indexOf(sensor.toLowerCase()) !== -1;
            });
            if (sensorItem == null) return [];
            return sampleGrup.Values[sampleGrup.Sensors.indexOf(sensorItem)];
        }

        factory.findValue = function (sampleGroup, path) {
            if (sampleGroup == null || path == null || path.length < 2) return null;
            var sensor = sampleGroup.Sensors.find(function (s) {
                return s.toLowerCase().split("_").indexOf(path[0].toLowerCase()) !== -1;
            });
            var sensorIndex = sampleGroup.Sensors.indexOf(sensor);
            var type = sampleGroup.Types.find(function (s) { return s.toLowerCase().indexOf(path[1].toLowerCase()) !== -1 });
            var valueIndex = sampleGroup.Types.indexOf(type);
            if (sensorIndex === -1 || valueIndex === -1) return null;
            var values = sampleGroup.Values[sensorIndex][0];
            return values != null ? values[valueIndex] : null;
        }

        factory.resetSensor = function (sensor) {
            if (sensor == null || sensor.model == null) return;
            sensor.model.min = null;
            sensor.model.max = null;
            sensor.model.value = null;
            sensor.model.date = null;
        }

        function processGroup(statGroup) {
            var obj = {};
            if (statGroup == null) return obj;
            var now = new Date();
            var day = 1000 * 60 * 60 * 24;
            for (var i = 0; i < statGroup.Sensors.length; i++) {
                var sensor = statGroup.Sensors[i];
                var values = statGroup.Values.length > i ? statGroup.Values[i] : [];
                if (values == null || values.length === 0) continue;
                obj[sensor] = {};
                for (var y = 0; y < statGroup.Types.length; y++) {
                    var type = statGroup.Types[y];
                    if (values[0] != null && values[0].length > y) obj[sensor][type] = values[0][y];
                }
                if (obj[sensor]["time"]) obj[sensor].isOld = (now - vDate.parse(obj[sensor]["time"])) > day;
            }
            return obj;
        }

        factory.mapLast = function (weightStats, sensorStats) {
            var array = [];
            array = array.concat(weightStats || []);
            array = array.concat(sensorStats || []);
            if (array.length === 0) return {};
            var result = {};
            for (var i = 0; i < array.length; i++) {
                var data = processGroup(array[i]);
                if (data == null) continue;
                var obj = {};
                obj[array[i].GroupId] = data;
                angular.merge(result, obj);
            }
            return result;
        }

    factory.hasOtherData = function (device) {
        if (device.Event == null || device.Event.Bird == null || device.last == null) return false;
        var sex = device.Event.Bird.SexMode;
        if (sex === vEnums.sexModes.female && device.last[factory.sensors.weightMale.name] != null) return true;
        if (sex === vEnums.sexModes.male && device.last[factory.sensors.weightFemale.name] != null) return true;
        if (sex === vEnums.sexModes.undefined && device.last[factory.sensors.weightFemale.name] != null) return true;
        if (sex === vEnums.sexModes.mixed && device.last[factory.sensors.weightUndefined.name] != null) return true;
        return false;
    }

        return factory;
    }
])
.factory('vDevice', [
    "vSensor", "vEnums", function (vSensor, vEnums) {
        var factory = {}
        var devices = [
            {
                name: "none",
                type: vEnums.deviceType.none,
                sensors: [
                    vSensor.sensors.weightFemale,
                    vSensor.sensors.weightMale,
                    vSensor.sensors.weightUndefined,
                    vSensor.sensors.temperature,
                    vSensor.sensors.humidity,
                    vSensor.sensors.co2
                ]
            },
            {
                name: "sensorPack",
                type: vEnums.deviceType.sensorPack,
                sensors: [
                    vSensor.sensors.weightFemale,
                    vSensor.sensors.weightMale,
                    vSensor.sensors.weightUndefined,
                    vSensor.sensors.temperature,
                    vSensor.sensors.humidity,
                    vSensor.sensors.co2
                ]
            },
            {
                name: "bat2",
                type: vEnums.deviceType.bat2,
                sensors: [
                    vSensor.sensors.weightFemale,
                    vSensor.sensors.weightMale,
                    vSensor.sensors.weightUndefined
                ]
            },
            {
                name: "externalSensor",
                type: vEnums.deviceType.externalSensor,
                sensors: [
                    vSensor.sensors.weightFemale,
                    vSensor.sensors.weightMale,
                    vSensor.sensors.weightUndefined,
                    vSensor.sensors.temperature,
                    vSensor.sensors.humidity,
                    vSensor.sensors.co2
                ]
            },
            {
                name: "bat2cable",
                type: vEnums.deviceType.bat2cable,
                sensors: [
                    vSensor.sensors.weightFemale,
                    vSensor.sensors.weightMale,
                    vSensor.sensors.weightUndefined
                ]
            },
            {
                name: "externalTemperature",
                type: vEnums.deviceType.externalTemperature,
                sensors: [
                    vSensor.sensors.temperature
                ]
            },
            {
                name: "externalHumidity",
                type: vEnums.deviceType.externalHumidity,
                sensors: [
                    vSensor.sensors.humidity
                ]
            },
            {
                name: "externalCo2",
                type: vEnums.deviceType.externalCo2,
                sensors: [
                    vSensor.sensors.co2
                ]
            },
            {
                name: "externalWeight",
                type: vEnums.deviceType.externalWeight,
                sensors: [
                    vSensor.sensors.weightFemale,
                    vSensor.sensors.weightMale,
                    vSensor.sensors.weightUndefined
                ]
            },
            {
                name: "bat3",
                type: vEnums.deviceType.bat3,
                sensors: [
                    vSensor.sensors.weightFemale,
                    vSensor.sensors.weightMale,
                    vSensor.sensors.weightUndefined
                ]
            }
        ];
        factory.devices = {
            none: devices[0],
            sensorPack: devices[1],
            bat2: devices[2],
            externalSensor: devices[3],
            bat2cable: devices[4],
            externalTemperature: devices[5],
            externalHumidity: devices[6],
            externalCo2: devices[7],
            externalWeight: devices[8],
            bat3: devices[9]
        };

        factory.getByType = function (type) {
            return devices.find(function (device) {
                return type === device.type;
            });
        }
        factory.reset = function (sensors) {
            if (sensors == null) return;
            sensors.forEach(function (sensor) {
                vSensor.resetSensor(sensor);
            });
        }
        return factory;
    }
])
.factory('vDownSampling', [
    function () {
        function largestTriangleThreeBuckets(data, threshold, xAccessor, yAccessor) {

            var floor = Math.floor,
                abs = Math.abs,
                dataLength = data.length,
                sampled = [],
                sampledIndex = 0,
                every = (dataLength - 2) / (threshold - 2), // Bucket size. Leave room for start and end data points
                a = 0, // Initially a is the first point in the triangle
                maxAreaPoint,
                maxArea,
                area,
                nextA,
                i,
                avgX = 0,
                avgY = 0,
                avgRangeStart,
                avgRangeEnd,
                avgRangeLength,
                rangeOffs,
                rangeTo,
                pointAX,
                pointAY;

            if (threshold >= dataLength || threshold === 0) {
                return data; // Nothing to do
            }

            sampled[sampledIndex++] = data[a]; // Always add the first point

            for (i = 0; i < threshold - 2; i++) {

                // Calculate point average for next bucket (containing c)
                avgX = 0;
                avgY = 0;
                avgRangeStart = floor((i + 1) * every) + 1;
                avgRangeEnd = floor((i + 2) * every) + 1;
                avgRangeEnd = avgRangeEnd < dataLength ? avgRangeEnd : dataLength;

                avgRangeLength = avgRangeEnd - avgRangeStart;

                for (; avgRangeStart < avgRangeEnd; avgRangeStart++) {
                    avgX += data[avgRangeStart][xAccessor] * 1; // * 1 enforces Number (value may be Date)
                    avgY += data[avgRangeStart][yAccessor] * 1;
                }
                avgX /= avgRangeLength;
                avgY /= avgRangeLength;

                // Get the range for this bucket
                rangeOffs = floor((i + 0) * every) + 1;
                rangeTo = floor((i + 1) * every) + 1;

                // Point a
                pointAX = data[a][xAccessor] * 1; // enforce Number (value may be Date)
                pointAY = data[a][yAccessor] * 1;

                maxArea = area = -1;

                for (; rangeOffs < rangeTo; rangeOffs++) {
                    // Calculate triangle area over three buckets
                    area = abs((pointAX - avgX) * (data[rangeOffs][yAccessor] - pointAY) -
                        (pointAX - data[rangeOffs][xAccessor]) * (avgY - pointAY)
                    ) * 0.5;
                    if (area > maxArea) {
                        maxArea = area;
                        maxAreaPoint = data[rangeOffs];
                        nextA = rangeOffs; // Next a is this b
                    }
                }

                sampled[sampledIndex++] = maxAreaPoint; // Pick this point from the bucket
                a = nextA; // This a is the next a (chosen b)
            }

            sampled[sampledIndex++] = data[dataLength - 1]; // Always add last

            return sampled;
        }

        return {
            largestTriangleThreeBuckets: largestTriangleThreeBuckets
        };
    }
])
.factory('vTreeMenu', [
    "$rootScope", "sensors", function ($rootScope, sensors) {
        var tree = {
            devices: [],
            houses: [],
            farms: []
        }

        function load() {
            sensors.names().success(function (source) {
                if (source.Devices == null) return;
                $rootScope.$broadcast("treeDataSource", {
                    devices: source.Devices,
                    houses: source.Houses,
                    farms: source.Farms,
                    terminals: source.Terminals
                });
                var data = angular.copy(source);
                tree.devices = data.Devices;
                tree.houses = data.Houses.filter(function (h) {
                    if (h.Devices.length === 0) return false;
                    h.Devices = data.Devices.filter(function (d) {
                        return h.Devices.indexOf(d.Id) !== -1;
                    });
                    return true;
                });
                tree.farms = data.Farms.filter(function (f) {
                    f.Houses = tree.houses.filter(function (h) {
                        return f.Houses.indexOf(h.Id) !== -1;
                    });
                    return f.Houses.length !== 0;
                });
                tree.house = {
                    Devices: tree.devices.filter(function (d) { return d.HouseId == null })
                }
                tree.farm = {
                    Houses: tree.houses.filter(function (d) { return d.FarmId == null })
                }

                $rootScope.$broadcast("treeData", tree);
                if (source.ShowDemoDataDialog) $rootScope.$broadcast("generateDataDialog", {});
            });
        }

        return {
            load: load
        }
    }
])
.factory('vExport', [
    'vFile', 'vSensor', 'vDate', '$filter', 'vResources', 'vCookies', function (vFile, vSensor, vDate, $filter, vResources, vCookies) {

        var sep = ";";
        var separator = "sep=;\n";
        var decimalSeparator = vCookies.getNumberFormat();
        var dateFormat = vCookies.getDateFormat();

        function formatDate(value) {
            return $filter('date')(vDate.parse(value), dateFormat);
        }

        function formatNumber(value, decimals) {
            if (value == null || !value.toFixed) return "";
            return value.toFixed(decimals == null ? 0 : decimals).replace(".", decimalSeparator);
        }

        function format(column, value) {
            if (column == null || value == null) return "";
            if (column.type === vSensor.types.time.timeStamp) return formatDate(value);
            return formatNumber(value, column.decimals);
        }

        function getHeaderText(type, sensor, text) {
            if (type === "day" || type === "timestamp" || type === "name" || type === "flock") return vResources.get(type);
            return vResources.get(text || type) + "[" + vResources.getUnit(sensor, type) + "]";
        }

        function getHeader(columns, sensor) {
            var row = "";
            for (var i = 0; i < columns.length; i++) {
                row = row + getHeaderText(columns[i].type, sensor) + ";";
            }
            return row + "\n";
        }

        function getRow(columns, row) {
            var csvRow = "";
            for (var i = 0; i < columns.length; i++) {
                var val = format(columns[i], row[columns[i].index]);
                csvRow = csvRow + val + ";";
            }
            return csvRow + "\n";
        }

        function getAllRows(columns, rows) {
            var csvRows = "";
            for (var i = 0; i < rows.length; i++) {
                csvRows = csvRows + getRow(columns, rows[i]);
            }
            return csvRows;
        }

        function getColumns(sensor, types, sampleGroup) {
            var columns = [];
            for (var i = 0; i < types.length; i++) {
                var index = sampleGroup.Types.indexOf(types[i]);
                if (index === -1) continue;
                var decimals = vSensor.decimals.get(sensor, types[i]);
                columns.push({ type: types[i], index: index, decimals: decimals });
            }

            return columns;
        }

        function sensorToCsv(sensor, sampleGroup, types) {
            if (sensor == null) return;
            var columns = getColumns(sensor, types || sensor.type.list(), sampleGroup);
            var csv = separator + getHeader(columns, sensor);
            var rows = vSensor.findSamples(sampleGroup, sensor.name);
            csv = csv + getAllRows(columns, rows);
            var fileName = sampleGroup.Group + "_" + sampleGroup.GroupId + "_" + sensor.name + "_" + sampleGroup.TimeSize + ".csv";
            vFile.save(fileName, csv);
        }

        /*
* Export for list of devices/houses
*/

        var columns = [
            { type: "name" },
            { type: "gain", sensor: vSensor.sensors.weightMale, decimals: vSensor.decimals.get(vSensor.sensors.weightMale, "gain") },
            { type: "uniformity", sensor: vSensor.sensors.weightMale, decimals: vSensor.decimals.get(vSensor.sensors.weightMale, "uniformity") },
            { type: "target", sensor: vSensor.sensors.weightMale, decimals: vSensor.decimals.get(vSensor.sensors.weightMale, "target") },
            { type: "average", sensor: vSensor.sensors.weightMale, decimals: vSensor.decimals.get(vSensor.sensors.weightMale, "average") },
            { type: "average", sensor: vSensor.sensors.temperature, decimals: vSensor.decimals.get(vSensor.sensors.temperature, "average") },
            { type: "average", sensor: vSensor.sensors.humidity, decimals: vSensor.decimals.get(vSensor.sensors.humidity, "average") },
            { type: "average", sensor: vSensor.sensors.co2, decimals: vSensor.decimals.get(vSensor.sensors.co2, "average") },
            { type: "flock" }
        ];

        function getListValue(sampleGroup, type, sensor, decimals) {
            if (sampleGroup == null || type == null || sampleGroup.Values.length === 0) return "";
            var index = sampleGroup.Types.indexOf(type);
            if (index === -1) return "";
            if (sensor.type === vSensor.types.weight) {

                return sampleGroup.Values.map(function (v) {
                    if (v[0] == undefined) return "";
                    return formatNumber(v[0][index], decimals);
                }).join(" / ");
            } else {
                var sensorIndex = sampleGroup.Sensors.indexOf(sensor.name);
                if (sensorIndex === -1) return "";
                var val = sampleGroup.Values[sensorIndex][index];
                if (val == null) return "";
                return formatNumber(val[0], decimals) || "";
            }
        }

        function getListRow(device) {
            var csv = device.Name + sep;
            for (var i = 1; i < columns.length; i++) {
                if (columns[i].sensor == null) continue;
                var values = columns[i].sensor.type === vSensor.types.weight ? device.WeightStats : device.Stats;
                csv = csv + getListValue(values, columns[i].type, columns[i].sensor, columns[i].decimals) + sep;
            }
            return csv + (device.Event ? device.Event.Name : "") + sep + "\n";
        }

        function tableHeader() {
            var csv = "";
            for (var i = 0; i < columns.length; i++) {
                var col = columns[i];
                var text = col.sensor == null ? null : col.sensor.typeName;
                if (text != null && col.sensor.type === vSensor.types.weight) text = null;
                csv = csv + getHeaderText(col.type, col.sensor, text) + ";";
            }
            return csv + "\n";
        }

        function listToCsv(devices) {
            if (devices == null || devices.length === 0) return;
            var csv = separator + tableHeader();
            for (var i = 0; i < devices.length; i++) {
                csv = csv + getListRow(devices[i]);
            }
            vFile.save("list.csv", csv);
        }


        function lastRowValue(last, sensors, type, decimals) {
            if (last == null) return sep;
            var values = [];
            for (var i = 0; i < sensors.length; i++) {
                if (last[sensors[i]] && last[sensors[i]][type]) values.push(last[sensors[i]][type].toFixed(decimals));
            }
            return (values.join(" / ") + sep).replace(".", decimalSeparator);
        }

        var weights = ["weight_male", "weight_female", "weight_undefined"];
        var numberDecimals = vSensor.decimals.get("weight", "average");

        function lastRowToCsv(device) {
            var row = "";

            row += (device.Farm ? device.Farm.Name + " " : "") + (device.House ? device.House.Name + " " : "") + device.Name + sep;
            row += lastRowValue(device.last, weights, "gain", numberDecimals);
            row += lastRowValue(device.last, weights, "uniformity", 0);
            row += lastRowValue(device.last, weights, "target", numberDecimals);
            row += lastRowValue(device.last, weights, "average", numberDecimals);
            row += lastRowValue(device.last, ["temperature"], "average", 0);
            row += lastRowValue(device.last, ["humidity"], "average", 0);
            row += lastRowValue(device.last, ["co2"], "average", 0);
            row += (device.Event || {}).Name + sep;
            return row + "\n";
        }

        function lastToCsv(devices) {
            var csv = separator + tableHeader();
            csv += formatDate(new Date()) + sep + "\n";
            for (var i = 0; i < devices.length; i++) {
                csv = csv + lastRowToCsv(devices[i]);
            }
            vFile.save("list.csv", csv);
        }

        return {
            sensorToCsv: sensorToCsv,
            listToCsv: listToCsv,
            lastToCsv: lastToCsv
        }
    }
])
.factory('vName', [
    'vResources', function (vResources) {
        function createUnique(items, baseName) {
            if (items == null || items.length === 0) return baseName;
            baseName = vResources.get(baseName);
            var names = items.map(function (i) { return i.Name });
            var same = names.filter(function (name) {
                if (name == null) return false;
                return name.indexOf(baseName) === 0;
            });
            if (same.length === 0) return baseName;
            var nums = same.map(function (name) {
                var nums = name.match(/\d/g);
                if (nums == null) return -1;
                return parseInt(nums.join(""));
            });

            if (nums == null || nums.length === 0) return baseName;
            var num = Math.max.apply(null, nums) + 1;
            if (num === 0) return baseName + " 1";
            return baseName + " " + num;
        }

        return {
            createUnique: createUnique
        }
    }
])
.factory('vResources', [
    'vCookies', function (vCookies) {

        var defaultLanguage = vCookies.language.default;

        var resources = {};

        resources[vCookies.language.en] = {
            day: "Day",
            date: "Date",
            timestamp: "Date",
            weight: "Weight",
            average: "Average",
            cv: "Coefficient of variation",
            temperature: "Temperature",
            humidity: "Humidity",
            sigma: "Standard deviation",
            co2: "Co2",
            count: "Count",
            min: "Minimum",
            max: "Maximum",
            gain: "Gain",
            target: "Target",
            uniformity: "Uniformity",
            flock: "Flock",
            name: "Name"
        };

        resources[vCookies.language.de] = {
            day: "Tag",
            date: "Datum",
            timestamp: "Datum",
            weight: "Gewicht",
            average: "Mittlere",
            cv: "Variationskoeffizient",
            temperature: "Temperatur",
            humidity: "Feuchtigkeit",
            sigma: "Standardabweichung",
            co2: "Co2",
            count: "Count",
            min: "Minimum",
            max: "Maximal",
            gain: "Zunahme",
            target: "Zielwert",
            uniformity: "Einheitlichkeit",
            flock: "Flock",
            name: "Name",
            "New Farm": "Neue Farm",
            "New House": "Neues Haus",
            "New Bird": "Neuer Vogel",
            "New Curve": "Neue Kurve",
            "New User": "Neuer Benutzer",
            "New Event": "Neues Event"
        };

        var weightUnit = vCookies.units.getName("weight");
        var temperatureUnit = vCookies.units.getName("temperature");

        var units = {
            weight: {
                average: weightUnit,
                gain: weightUnit,
                min: weightUnit,
                max: weightUnit,
                target: weightUnit,
                uniformity: "%",
                cv: "%"
            },
            temperature: {
                average: temperatureUnit,
                min: temperatureUnit,
                max: temperatureUnit
            },
            humidity: {
                average: "%",
                min: "%",
                max: "%"
            },
            co2: {
                average: "ppm",
                min: "ppm",
                max: "ppm"
            }
        }

        var local = resources[vCookies.language.get()] || resources[defaultLanguage];

        function getTextByKey(key) {
            if (key == null || key === "" || local == null) return "";
            return local[key] || resources[defaultLanguage][key] || key;
        }

        function getUnit(sensor, type) {
            if (sensor == null || type == null || sensor.typeName == null) return "-";
            var typeGroup = units[sensor.typeName];
            if (typeGroup == null) return "-";
            var unit = typeGroup[type];
            return unit || "-";
        }

        return {
            get: getTextByKey,
            getUnit: getUnit
        };
    }
])
.factory('vMessages', ['$rootScope', '$modal', 'settings', function ($rootScope, $modal, settings) {

    function isFunction(object) {
        return !!(object && object.constructor && object.call && object.apply);
    }

    var factory = {};

    factory.messageLevel = {
        info: "info",
        success: "success",
        alert: "alert"
    }
    
    factory.showDialog = function (dialogId, okAction, cancelAction, context) {
        if (dialogId == null || dialogId === "") return {};
        return $modal.open({
            templateUrl: dialogId,
            controller: [
                '$scope', '$modalInstance', function (scope, $modalInstance) {

                    if (okAction)
                        scope.ok = function () {
                            if (isFunction(okAction)) okAction();
                            $modalInstance.close(true);
                        }

                    if (cancelAction)
                        scope.cancel = function () {
                            if (isFunction(cancelAction)) cancelAction();
                            $modalInstance.close(false);
                        }

                    if (context != null) angular.extend(scope, context);
                }
            ]
        }).result;
    };

    factory.showMessage = function (title, text, level, data) {
        var validLevel = factory.messageLevel[level] || factory.messageLevel.info;
        $rootScope.$broadcast('showMessage', { title: title, text: text, level: validLevel, data: data });
    }

    factory.loadResouces = function (regex) {
        if (regex == null) return;
        settings.resources(regex).success(function (resources) {
            $rootScope.$broadcast('loadResources', resources);
        });
    }
    return factory;
}]);
