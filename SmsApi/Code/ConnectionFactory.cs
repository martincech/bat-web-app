﻿using DataContext.Context;
using DataModel;
using InfluxData.Net.Common.Enums;
using InfluxData.Net.InfluxDb;
using KafkaBatLibrary;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using UsersDataModel;

namespace SmsApi
{
   public interface IConnectionFactory
   {
      IKafkaSender KafkaSender { get; }

      IInfluxDbClient InfluxClient { get; }

      UserContext UserContext { get; }
      BatContext BatContext { get; }

      IHostingEnvironment HostingEnvironment { get; }
      IConfiguration Configuration { get; }    
   }


   public class ConnectionFactory : IConnectionFactory
   {

      private readonly string influxUrl;

      private readonly BatModelContainer batModel;
      private readonly ClaimsPrincipal claimsPrincipal;
      private readonly UserModelContainer userModel;

      private BatContext batContext;
      private UserContext userContext; 
      private IInfluxDbClient influxClient;

      public ConnectionFactory(
         UserModelContainer userModel,
         BatModelContainer batModel,
         IKafkaSender kafkaSender,
         ClaimsPrincipal claimsPrincipal,
         IConfiguration configuration,
         IHostingEnvironment hostingEnvironment)
      {
         this.userModel = userModel;
         this.batModel = batModel;
         this.claimsPrincipal = claimsPrincipal;

         KafkaSender = kafkaSender;

         Configuration = configuration;
         HostingEnvironment = hostingEnvironment;
        
         influxUrl = configuration["Influx"];
      }

      public UserContext UserContext =>
         userContext ?? (userContext = new UserContext(userModel, claimsPrincipal));

      public BatContext BatContext =>
         batContext ?? (batContext = new BatContext(batModel, UserContext, InfluxClient, KafkaSender));

      public IKafkaSender KafkaSender { get; private set; }

      public IConfiguration Configuration { get; private set; }

      public IHostingEnvironment HostingEnvironment { get; private set; }  

      public IInfluxDbClient InfluxClient
         => influxClient ?? (influxClient = new InfluxDbClient(influxUrl, "name", "pass", InfluxDbVersion.v_1_0_0));
   }
}
