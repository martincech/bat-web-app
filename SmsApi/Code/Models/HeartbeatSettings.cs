﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmsApi.Models
{
   public class HeartbeatSettings
   {
      public string Bucket { get; set; }
      public string File { get; set; }
      public string Region { get; set; }
      public string IAMId { get; set; }
      public string IAMSecret { get; set; }
   }
}
