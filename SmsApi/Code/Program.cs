﻿using System;
using App.Metrics;
using App.Metrics.AspNetCore;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace SmsApi
{
   public class Program
   {
      public static void Main(string[] args)
      {

         var logger = NLog.Web.NLogBuilder.ConfigureNLog("NLog.config").GetCurrentClassLogger();
         try
         {
            logger.Debug("init main");
            CreateWebHostBuilder(args).Build().Run();
         }
         catch (Exception ex)
         {
            //NLog: catch setup errors
            logger.Error(ex, "Stopped program because of exception");
            throw;
         }
         finally
         {
            // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
            NLog.LogManager.Shutdown();
         }
      }

      public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
          WebHost.CreateDefaultBuilder(args)
         .ConfigureMetricsWithDefaults((webHostBuilderContext, builder) =>
         {
            var influx = webHostBuilderContext.Configuration["Influx"];
            var db = webHostBuilderContext.Configuration["MetricsOptions:InfluxDbName"];
            builder.Report.ToInfluxDb(influx, db);
         })
         .UseMetrics()
         .UseStartup<Startup>();
   }
}
