﻿using System.Collections.Generic;
using System.Linq;
using Veit.Bat.Common.Sample.Statistics;

namespace KafkaBatLibrary.Map
{
   public static class StatisticDataExtension
   {
      public const string SEX_MALE = "male";
      public const string SEX_FEMALE = "female";
      public const string SEX_UNDEFINED = "undefined";

      public static List<DailyWeight> Map(this StatisticSample stat)
      {
         var dailyWeights = new List<DailyWeight>();
         if (stat == null) return dailyWeights;
         if (stat.MaleData != null) dailyWeights.Add(Map(stat.MaleData, stat));
         if (stat.FemaleData != null) dailyWeights.Add(Map(stat.FemaleData, stat));
         return dailyWeights;
      }

      public static List<DailyWeight> Map(this List<StatisticSample> stats)
      {
         if (stats == null) return new List<DailyWeight>();
         return stats.SelectMany(Map).ToList();
      }

      private static DailyWeight Map(StatData sample, StatisticSample stat)
      {
         if (stat == null || sample == null) return null;
         return new DailyWeight
         {
            Average = sample.Average,
            Count = sample.Count,
            Cv = sample.Cv,
            Day = stat.DayNumber,
            Gain = sample.Gain,
            Sigma = sample.Sigma,
            Uid = stat.PhoneNumber,
            Uniformity = sample.Uniformity,
            TimeStamp = stat.TimeStamp,
            Sex = GetSex(sample, stat)
         };
      }

      private static string GetSex(StatData sample, StatisticSample stat)
      {
         if (sample == stat.FemaleData && stat.MaleData != null) return SEX_FEMALE;
         if (sample == stat.MaleData && stat.FemaleData != null) return SEX_MALE;
         return SEX_UNDEFINED;
      }
   }
}
