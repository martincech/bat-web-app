﻿using Veit.Bat.Common.Sample.Statistics;

namespace SmsApi.DTOs
{
   public class TerminalStatisticSample
   {
      public string TerminalUid { get; set; }

      public StatisticSample Data { get; set; }
   }
}