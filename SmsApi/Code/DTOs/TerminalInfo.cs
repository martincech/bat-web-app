﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmsApi.DTOs
{
   public class TerminalInfo
   {
      public string Uid { get; set; }
      public string Name { get; set; }
   }
}