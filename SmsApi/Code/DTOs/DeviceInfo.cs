﻿using Veit.Bat.Common;

namespace SmsApi.DTOs
{
   public class DeviceInfo
   {
      public string TerminalUid { get; set; }
      public string DeviceUid { get; set; }
      public DeviceType? DeviceType { get; set; }
   }
}