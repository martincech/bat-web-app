﻿using DataModel;
using KafkaBatLibrary;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using System.Security.Claims;
using TokenService;
using System.Linq;
using UsersDataModel;
using System.Collections.Generic;
using Swashbuckle.AspNetCore.Swagger;
using SmsApi.Models;
using NLog;
using Veit.Cognito.Service;

namespace SmsApi
{
    public class Startup
    {
        private string batModelConnectionString;
        private string userModelConnectionString;

        private CognitoSettings cognitoSettings;
        private HeartbeatSettings heartbeatSettings;

        private static readonly Logger LOG = LogManager.GetCurrentClassLogger();

        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;

            batModelConnectionString = Configuration.GetConnectionString("BatModelContainer");
            userModelConnectionString = Configuration.GetConnectionString("UserModelContainer");
            cognitoSettings = Configuration.GetSection("Cognito").Get<CognitoSettings>();
            heartbeatSettings = Configuration.GetSection("Heartbeat").Get<HeartbeatSettings>();
            LOG.Info($"API - ENVIROMENT : {Environment.EnvironmentName}");
        }

        public IConfiguration Configuration { get; }
        public IHostingEnvironment Environment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddMvc()
            .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
            .AddMetrics(); ;

            //Swagger generator that builds SwaggerDocument objects directly from your routes, controllers, and models.
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "BAT CLOUD API", Version = "v1" });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme { In = "header", Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"", Name = "Authorization", Type = "apiKey" });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> {
                { "Bearer", Enumerable.Empty<string>() },
               });
            });

            //Fetch JSON Web Key Set used for JWT verification
            var keys = JwtSettingsExtensions.GetJsonWebKeys(cognitoSettings.Issuer);


            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = keys.LastOrDefault().Value.TokenValidationParameters;
                options.Events = new JwtBearerEvents
                {
                    OnTokenValidated = async ctx =>
                 {
                     var success = false;
                     var scopes = ctx.Principal.Claims.First(f => f.Type == "scope");
                     if (scopes != null && cognitoSettings.Scopes.ToLowerInvariant().Split(" ").Except(scopes.Value.ToLowerInvariant().Split(" ")).Count() == 0)
                     {
                         var userId = ctx.Principal.FindFirstValue(ClaimTypes.NameIdentifier);
                         var db = ctx.HttpContext.RequestServices.GetRequiredService<UserModelContainer>();

                         var user = await db.Users.FirstOrDefaultAsync(a => a.Id.ToString() == userId);
                         if (user != null && user.Company != null)
                         {
                             var role = user.Roles.FirstOrDefault();
                             var claims = new List<Claim>
                           {
                           new Claim(ClaimTypes.Role, role?.Name),
                           new Claim("Database", user.Company?.Database)
                           };
                             var appIdentity = new ClaimsIdentity(claims);
                             ctx.Principal.AddIdentity(appIdentity);
                             success = true;
                         }
                     }

                     if (!success)
                     {
                         ctx.Fail($"Failed to authorize user.");
                     }
                 }
                };

            });

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<ClaimsPrincipal>(provider => provider.GetService<IHttpContextAccessor>().HttpContext.User);

            services.AddSingleton<IKafkaSender>(new KafkaSender(Configuration["Kafka"], Path.Combine(Environment.ContentRootPath, "Cache", "API.kafkaCache")));
            //Register user database context service 
            services.AddDbContext<UserModelContainer>(o => o.UseLazyLoadingProxies().UseSqlServer(userModelConnectionString));
            //Register company database context service 
            services.AddDbContext<BatModelContainer>((provider, options) =>
            {
                var principal = provider.GetService<ClaimsPrincipal>();
                var database = principal.HasClaim(c => c.Type == "Database") ? principal.FindFirstValue("Database") : null;
                options.UseSqlServer(string.Format(batModelConnectionString, database));
            });

            services.AddSingleton(heartbeatSettings);
            services.AddScoped(c => new S3Bucket.S3File(heartbeatSettings.IAMId, heartbeatSettings.IAMSecret, heartbeatSettings.Region));

            services.AddScoped<IConnectionFactory, ConnectionFactory>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            //app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "BAT CLOUD API V1");
            });

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
