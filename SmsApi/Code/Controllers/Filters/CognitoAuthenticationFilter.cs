﻿using System.Security.Claims;

namespace SmsApi.Controllers.Filters
{
   public class CognitoAuthenticationFilter
   {
      internal const string API_KEY_HEADER = "ApiKey";
      internal const string API_KEY_PASSWORD = "/XEu`c{893V;8:)y";

      internal const string ROLE_CLAIM_TYPE = ClaimTypes.Role;
      internal const string DATABASE_CLAIM_TYPE = "database";
      internal const string JTI_CLAIM_TYPE = "jti";
   }
}
