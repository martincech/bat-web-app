﻿namespace SmsApi.Controllers.Filters
{
   /// <summary>
   /// User data
   /// </summary>
   public class ApiKey
   {
      /// <summary>
      /// User role
      /// </summary>
      public string Role { get; set; }
      /// <summary>
      /// Company database
      /// </summary>
      public string Database { get; set; }
      /// <summary>
      /// Associated access token id
      /// </summary>
      public string TokenId { get; set; }
   }
}
