﻿using System.Security.Principal;

namespace SmsApi.Controllers.Filters
{
   public class TokenAuthenticationIdentity : GenericIdentity
   {
      public TokenAuthenticationIdentity(string token, string apiKey)
         : base(token, "Bearer")
      {
         Token = token;
         ApiKey = apiKey;
      }

      /// <summary>
      /// Access token authentication
      /// </summary>
      public string Token { get; set; }

      /// <summary>
      /// Custom header with user role 
      /// </summary>
      public string ApiKey { get; set; }
   }
}