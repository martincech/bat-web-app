﻿using Newtonsoft.Json;
using NLog;
using System.Configuration;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Heartbeat;
using S3Bucket;
using Microsoft.AspNetCore.Http;
using SmsApi.Models;

namespace SmsApi.Controllers
{
   [Route("api/v1/[controller]")]
   public class HeartbeatController : Controller
   {
      private readonly HeartbeatSettings settings;
      private readonly S3File s3Storage;
      protected static readonly Logger LOG = LogManager.GetCurrentClassLogger();        

      public HeartbeatController(S3File s3Storage, HeartbeatSettings settings)
      {        
         this.s3Storage = s3Storage;
         this.settings = settings;
      }

      [HttpPost("")]
      public async Task<IActionResult> Post([FromBody]UserInfo info)
      {
         LOG.Info(info);
         var etag = Request.Headers.FirstOrDefault(f => f.Key == "ETag").Value;      
         var result = await s3Storage.ReadAsync(settings.Bucket, settings.File, etag.FirstOrDefault());

         if (result.Value == null)
         {
            return StatusCode(StatusCodes.Status304NotModified);
         }
         this.HttpContext.Response.Headers.Add("ETag", result.Key);
         return new FileContentResult(Encoding.UTF8.GetBytes(result.Value), "application/json");
      }

      [HttpPut("")]
      [Authorize(Roles = "Veit")]
      public async Task<string> Put([FromBody]HeartbeatInfo heartbeat)
      {
         if (heartbeat == null) return "Heartbeat cannot be null.";      
         return await s3Storage.WriteAsync(settings.Bucket, settings.File, JsonConvert.SerializeObject(heartbeat));
      }
   }
}