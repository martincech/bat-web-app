﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using KafkaBatLibrary;
using KafkaBatLibrary.Map;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SmsApi.DTOs;
using Veit.Bat.Common;

namespace SmsApi.Controllers
{
   public class StatsController : BaseApiController
   {
      public StatsController(IConnectionFactory connectionFactory) : base(connectionFactory)
      {
      }

      [HttpPost("")]
      public async Task<ActionResult> Post([FromBody]TerminalStatisticSample terminalData)
      {
         var smsData = terminalData?.Data;
         LOG.Debug("Recived : {0}", JsonConvert.SerializeObject(terminalData));
         if (smsData == null || smsData.TimeStamp < new DateTime(1970) || terminalData.TerminalUid == null) return BadRequest("Invalid statistic data.");

         var terminal = await GetTerminal(terminalData.TerminalUid);
         if (terminal == null) return Unauthorized();

         try
         {
            smsData.PhoneNumber = CreateValidUid(smsData.PhoneNumber);
            await DataContext.CreateDeviceIfNotExist(terminalData.TerminalUid, smsData.PhoneNumber, DeviceType.Bat2Cable, smsData.ScaleName);
            if (SendToKafka(smsData.Map())) return Ok("Data was saved successfully");
         }
         catch (Exception e)
         {
            LOG.Error(e, JsonConvert.SerializeObject(smsData));
         }
         return BadRequest(new Exception("Failed to save statistic data"));
      }

      private bool SendToKafka(List<DailyWeight> DailyWeights)
      {
         return KafkaSender.Send(DailyWeights);
      }

      public static string CreateValidUid(string phoneNumber)
      {
         return (!string.IsNullOrEmpty(phoneNumber) && phoneNumber.StartsWith("+")) ? phoneNumber.Substring(1) : phoneNumber;
      }
   }
}
