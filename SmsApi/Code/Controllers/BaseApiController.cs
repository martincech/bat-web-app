﻿using DataContext.Context;
using KafkaBatLibrary;
using UsersDataModel;
using NLog;
using UsersDataModel.DTO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace SmsApi.Controllers
{
   [Route("api/v1/[controller]")]
   [Authorize]
   public class BaseApiController : Controller
   {
      private readonly IConnectionFactory connectionFactory;
      private User currentUser;
      private BatContext dataContext;
      private UserContext userContext;
    
      protected static readonly Logger LOG = LogManager.GetCurrentClassLogger();

      public BaseApiController(IConnectionFactory connectionFactory)
      {        
         this.connectionFactory = connectionFactory;
      }

      protected BatContext DataContext => connectionFactory.BatContext;

      protected UserContext UserContext => connectionFactory.UserContext;

      protected IKafkaSender KafkaSender => connectionFactory.KafkaSender;
          

      protected User CurrentUser
      {
         get { return currentUser ?? (currentUser = UserContext.User); }
      }

      protected async Task<TerminalDto> GetTerminal(string terminalUid)
      {
         var terminal = await UserContext.GetOrCreateTerminal(terminalUid, CurrentUser.CompanyId);
         if (terminal != null) await DataContext.CreateTerminalIfNotExist(terminalUid);
         return terminal;
      }
   }
}