﻿using Microsoft.AspNetCore.Mvc;
using SmsApi.DTOs;
using System.Threading.Tasks;
using System.Web.Http;

namespace SmsApi.Controllers
{
   public class TerminalController : BaseApiController
   {
      public TerminalController(IConnectionFactory connectionFactory) : base(connectionFactory) { }

      [HttpPost("")]
      public async Task<ActionResult> Post([FromBody]TerminalInfo terminalInfo)
      {
         if (terminalInfo == null || string.IsNullOrEmpty(terminalInfo.Uid)) return BadRequest("Invalid parameters.");
         var terminal = await GetTerminal(terminalInfo.Uid);
         if (terminal == null) return Unauthorized();
         return Ok();
      }
   }
}
