﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SmsApi.DTOs;
using Veit.Bat.Common;

namespace SmsApi.Controllers
{  
   public class DeviceController : BaseApiController
   {
      private static readonly SemaphoreSlim LOCK_OBJECT = new SemaphoreSlim(1, 1);

      public DeviceController(IConnectionFactory connectionFactory) : base(connectionFactory)
      {
      }

      [HttpPost("")]
      // POST: api/Device
      public async Task<ActionResult> Post([FromBody]DeviceInfo deviceInfo)
      {
         LOG.Debug("Device connected : {0}", JsonConvert.SerializeObject(deviceInfo));
         if (deviceInfo == null
            || deviceInfo.TerminalUid == null
            || deviceInfo.DeviceUid == null
            || deviceInfo.DeviceType == null) return BadRequest("Invalid parameters.");
         await LOCK_OBJECT.WaitAsync();
         try
         {
            var terminal = await GetTerminal(deviceInfo.TerminalUid);
            if (terminal == null) return Forbid();

            var deviceType = deviceInfo.DeviceType;
            var deviceUid = StatsController.CreateValidUid(deviceInfo.DeviceUid);

            if (!string.IsNullOrEmpty(deviceUid) && deviceType != null)
            {
               if (deviceType == DeviceType.Bat2Cable) deviceType = DeviceType.Bat3;
               if (deviceUid.Contains("\"")) return BadRequest("Invalid device UID");
               var device = await DataContext.CreateDeviceIfNotExist(deviceInfo.TerminalUid, deviceUid, deviceType.Value);
               if (device == null) return BadRequest(string.Format("Failed to create device {0}.", deviceUid));
               UserContext.AddDeviceToWorker(deviceUid);
            }
         }
         catch (Exception e)
         {
            LOG.Error(e, "An error occurred during creation of device {0}.", deviceInfo.DeviceUid);
            return BadRequest(string.Format("Failed to create device {0}.", deviceInfo.DeviceUid));
         }
         finally
         {
            LOCK_OBJECT.Release();
         }
         return Ok();
      }
   }
}
