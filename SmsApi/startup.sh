#!/bin/bash -xe
for f in *.in; do cat $f | envsubst > `basename $f .in`; done

cp NLog.${ENVIRONMENT}.config NLog.config
exec dotnet SmsApi.dll
