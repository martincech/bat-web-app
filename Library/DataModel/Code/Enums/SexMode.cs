namespace DataModel
{
    public enum SexMode : byte
    {
        Male = 0,
        Female = 1,
        Undefined = 2,
        Mixed = 3
    }
}
