namespace DataModel
{
    public enum StepMode : byte
    {
        Enter = 0,
        Leave = 1,
        Both = 2
    }
}
