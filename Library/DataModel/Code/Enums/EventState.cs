namespace DataModel
{
    public enum EventState : byte
    {
        Pause = 0,
        Unpause = 1
    }
}
