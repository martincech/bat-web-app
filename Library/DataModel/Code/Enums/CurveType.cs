namespace DataModel
{
    public enum CurveType : int
    {
        BirdFemale = 0,
        BirdMale = 1,
        BirdUndefined = 2,
        Humidity = 3,
        Temperature = 4,
        Detection = 5,
        Co2 = 6
    }
}
