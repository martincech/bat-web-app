using Microsoft.EntityFrameworkCore;
using System;

namespace DataModel
{
   public interface IBatModelContainer: IDisposable
   {
      DbSet<Device> Devices { get; set; }
      DbSet<Event> Events { get; set; }
      DbSet<House> Houses { get; set; }
   }
}