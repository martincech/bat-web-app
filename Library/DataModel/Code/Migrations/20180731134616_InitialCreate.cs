﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataModel.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Birds",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    SexMode = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Birds", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Curves",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Curves", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Farms",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Location = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Farms", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Terminals",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Uid = table.Column<string>(maxLength: 200, nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Terminals", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TimeSizes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Minutes = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TimeSizes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BirdCurve",
                columns: table => new
                {
                    Birds_Id = table.Column<int>(nullable: false),
                    Curves_Id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BirdCurve", x => new { x.Birds_Id, x.Curves_Id });
                    table.ForeignKey(
                        name: "FK_BirdCurve_Birds_Birds_Id",
                        column: x => x.Birds_Id,
                        principalTable: "Birds",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BirdCurve_Curves_Curves_Id",
                        column: x => x.Curves_Id,
                        principalTable: "Curves",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CurvePoints",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CurveId = table.Column<int>(nullable: false),
                    Day = table.Column<int>(nullable: false),
                    Value = table.Column<double>(nullable: true),
                    Min = table.Column<double>(nullable: true),
                    Max = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CurvePoints", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CurvePoints_Curves_CurveId",
                        column: x => x.CurveId,
                        principalTable: "Curves",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Events",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    From = table.Column<DateTime>(nullable: false),
                    To = table.Column<DateTime>(nullable: false),
                    RowNumber = table.Column<int>(nullable: false),
                    State = table.Column<byte>(nullable: false),
                    StartAge = table.Column<int>(nullable: false),
                    Stabilization_Range = table.Column<double>(nullable: false),
                    Stabilization_WindowSampleCount = table.Column<int>(nullable: false),
                    Stabilization_FiltrationAveragingWindow = table.Column<int>(nullable: false),
                    StepMode = table.Column<byte>(nullable: false),
                    SexMode = table.Column<byte>(nullable: false),
                    CurveId = table.Column<int>(nullable: true),
                    CurveFemaleId = table.Column<int>(nullable: true),
                    UniformityRange = table.Column<double>(nullable: false),
                    WeighMode = table.Column<byte>(nullable: false),
                    InitialWeight = table.Column<double>(nullable: false),
                    InitialWeightFemale = table.Column<double>(nullable: false),
                    DetectionRange = table.Column<double>(nullable: false),
                    DetectionRangeFemale = table.Column<double>(nullable: false),
                    BirdId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Events", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Events_Birds_BirdId",
                        column: x => x.BirdId,
                        principalTable: "Birds",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Events_Curves_CurveFemaleId",
                        column: x => x.CurveFemaleId,
                        principalTable: "Curves",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Events_Curves_CurveId",
                        column: x => x.CurveId,
                        principalTable: "Curves",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Houses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FarmId = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: false),
                    Location = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Houses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Houses_Farms_FarmId",
                        column: x => x.FarmId,
                        principalTable: "Farms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Devices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Uid = table.Column<string>(maxLength: 200, nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    HouseId = table.Column<int>(nullable: true),
                    TerminalId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Devices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Devices_Houses_HouseId",
                        column: x => x.HouseId,
                        principalTable: "Houses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Devices_Terminals_TerminalId",
                        column: x => x.TerminalId,
                        principalTable: "Terminals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DeviceTimeSize",
                columns: table => new
                {
                    DeviceTimeSize_TimeSize_Id = table.Column<int>(nullable: false),
                    TimeSizes_Id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeviceTimeSize", x => new { x.DeviceTimeSize_TimeSize_Id, x.TimeSizes_Id });
                    table.ForeignKey(
                        name: "FK_DeviceTimeSize_Devices_DeviceTimeSize_TimeSize_Id",
                        column: x => x.DeviceTimeSize_TimeSize_Id,
                        principalTable: "Devices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DeviceTimeSize_TimeSizes_TimeSizes_Id",
                        column: x => x.TimeSizes_Id,
                        principalTable: "TimeSizes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EventDevice",
                columns: table => new
                {
                    Events_Id = table.Column<int>(nullable: false),
                    Devices_Id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventDevice", x => new { x.Events_Id, x.Devices_Id });
                    table.ForeignKey(
                        name: "FK_EventDevice_Devices_Devices_Id",
                        column: x => x.Devices_Id,
                        principalTable: "Devices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EventDevice_Events_Events_Id",
                        column: x => x.Events_Id,
                        principalTable: "Events",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BirdCurve_Curves_Id",
                table: "BirdCurve",
                column: "Curves_Id");

            migrationBuilder.CreateIndex(
                name: "IX_CurvePoints_CurveId",
                table: "CurvePoints",
                column: "CurveId");

            migrationBuilder.CreateIndex(
                name: "IX_Devices_HouseId",
                table: "Devices",
                column: "HouseId");

            migrationBuilder.CreateIndex(
                name: "IX_Devices_TerminalId",
                table: "Devices",
                column: "TerminalId");

            migrationBuilder.CreateIndex(
                name: "IX_DeviceTimeSize_TimeSizes_Id",
                table: "DeviceTimeSize",
                column: "TimeSizes_Id");

            migrationBuilder.CreateIndex(
                name: "IX_EventDevice_Devices_Id",
                table: "EventDevice",
                column: "Devices_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Events_BirdId",
                table: "Events",
                column: "BirdId");

            migrationBuilder.CreateIndex(
                name: "IX_Events_CurveFemaleId",
                table: "Events",
                column: "CurveFemaleId");

            migrationBuilder.CreateIndex(
                name: "IX_Events_CurveId",
                table: "Events",
                column: "CurveId");

            migrationBuilder.CreateIndex(
                name: "IX_Houses_FarmId",
                table: "Houses",
                column: "FarmId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BirdCurve");

            migrationBuilder.DropTable(
                name: "CurvePoints");

            migrationBuilder.DropTable(
                name: "DeviceTimeSize");

            migrationBuilder.DropTable(
                name: "EventDevice");

            migrationBuilder.DropTable(
                name: "TimeSizes");

            migrationBuilder.DropTable(
                name: "Devices");

            migrationBuilder.DropTable(
                name: "Events");

            migrationBuilder.DropTable(
                name: "Houses");

            migrationBuilder.DropTable(
                name: "Terminals");

            migrationBuilder.DropTable(
                name: "Birds");

            migrationBuilder.DropTable(
                name: "Curves");

            migrationBuilder.DropTable(
                name: "Farms");
        }
    }
}
