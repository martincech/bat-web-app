using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;

namespace DataModel
{
   public class BatModelContainer : DbContext, IDesignTimeDbContextFactory<BatModelContainer>, IBatModelContainer
   {
      private readonly string connectionString;

      public BatModelContainer() { }

      public BatModelContainer(string connectionString)
         : base()
      {
         this.connectionString = connectionString;
      }

      public BatModelContainer(DbContextOptions<BatModelContainer> options) : base(options)
      {
      }

      public virtual DbSet<Bird> Birds { get; set; }
      public virtual DbSet<CurvePoint> CurvePoints { get; set; }
      public virtual DbSet<Curve> Curves { get; set; }
      public virtual DbSet<Device> Devices { get; set; }
      public virtual DbSet<Event> Events { get; set; }
      public virtual DbSet<Farm> Farms { get; set; }
      public virtual DbSet<House> Houses { get; set; }
      public virtual DbSet<Terminal> Terminals { get; set; }
      public virtual DbSet<TimeSize> TimeSizes { get; set; }


      public IQueryable<Bird> BirdsLazy => Birds
         .Include(i => i.Events)
         .Include(i => i.BirdCurves)
            .ThenInclude(i => i.Curve);

      public IQueryable<Event> EventsLazy => Events
         .Include(i => i.Bird)
              .ThenInclude(i => i.BirdCurves)
                  .ThenInclude(i => i.Curve)
         .Include(i => i.EventDevices)
            .ThenInclude(i => i.Device);

      public IQueryable<Curve> CurvesLazy => Curves
         .Include(i => i.CurvePoints)
         .Include(i => i.BirdCurves);

      public IQueryable<Device> DevicesLazy => Devices
          .Include(i => i.House)
          .Include(i => i.Terminal)
          .Include(i => i.DeviceTimeSizes)
          .Include(i => i.EventDevices)
              .ThenInclude(i => i.Event);

      public IQueryable<Farm> FarmsLazy => Farms
        .Include(i => i.Houses);

      public IQueryable<House> HousesLazy => Houses
        .Include(i => i.Farm)
        .Include(i => i.Devices);

      public IQueryable<Terminal> TerminalsLazy => Terminals
       .Include(i => i.Devices);


      public BatModelContainer CreateDbContext(string[] args)
      {
         var builder = new DbContextOptionsBuilder<BatModelContainer>();
         builder.UseSqlServer(string.Format("data source=localhost,1433;initial catalog={0};User Id=sa;Password=Passw0rd!;multipleactiveresultsets=True;application name=Web", "VeitBat3Data"));

         return new BatModelContainer(builder.Options);
      }

      protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
      {
         if (!optionsBuilder.IsConfigured)
         {
            optionsBuilder.UseSqlServer(connectionString);
         }
      }


      protected override void OnModelCreating(ModelBuilder modelBuilder)
      {
         modelBuilder.Entity<BirdCurve>()
            .HasKey(bc => new { bc.BirdId, bc.CurveId });

         modelBuilder.Entity<DeviceTimeSize>()
            .HasKey(bc => new { bc.DeviceId, bc.TimeSizeId });

         modelBuilder.Entity<EventDevice>()
            .HasKey(bc => new { bc.EventId, bc.DeviceId });

      }
   }
}
