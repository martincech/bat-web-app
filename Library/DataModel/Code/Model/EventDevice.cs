﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataModel
{
   public class EventDevice
   {
      [Column("Events_Id")]
      public int EventId { get; set; }
      public virtual Event Event { get; set; }

      [Column("Devices_Id")]
      public int DeviceId { get; set; }
      public virtual Device Device { get; set; }
   }
}
