using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace DataModel
{
   public partial class Terminal
   {
      public int Id { get; set; }

      [Required]
      [MaxLength(200)]
      public string Uid { get; set; }

      public string Name { get; set; }

      public virtual ICollection<Device> Devices { get; set; }
   }
}
