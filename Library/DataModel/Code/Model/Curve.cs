using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace DataModel
{
   public partial class Curve
   {
      public int Id { get; set; }

      [Required]
      public string Name { get; set; }

      public CurveType Type { get; set; }

      public virtual ICollection<CurvePoint> CurvePoints { get; set; }

      public virtual ICollection<BirdCurve> BirdCurves { get; set; }

      [NotMapped]
      public IEnumerable<Bird> Birds => BirdCurves?.Select(s => s.Bird);
   }
}
