using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace DataModel
{
   public partial class Bird
   {
      public int Id { get; set; }

      [Required]
      public string Name { get; set; }

      [Required]
      public string Description { get; set; }

      public SexMode SexMode { get; set; }
           
      public virtual ICollection<BirdCurve> BirdCurves { get; set; }

      public virtual ICollection<Event> Events { get; set; }

      [NotMapped]
      public virtual IEnumerable<Curve> Curves => BirdCurves?.Select(s => s.Curve);
   }
}
