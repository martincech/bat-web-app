using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataModel
{
   public partial class TimeSize
   {
      public int Id { get; set; }

      public int Minutes { get; set; }

      [NotMapped]
      public virtual ICollection<Device> Devices { get; set; }
   }
}
