using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace DataModel
{
   public partial class Event
   {     
      public int Id { get; set; }

      [Required]
      public string Name { get; set; }

      public DateTime From { get; set; }

      public DateTime To { get; set; }

      public int RowNumber { get; set; }

      public EventState State { get; set; }

      public int StartAge { get; set; }

      public double Stabilization_Range { get; set; }

      public int Stabilization_WindowSampleCount { get; set; }

      public int Stabilization_FiltrationAveragingWindow { get; set; }

      public StepMode StepMode { get; set; }

      public SexMode SexMode { get; set; }

      public int? CurveId { get; set; }

      public int? CurveFemaleId { get; set; }

      public double UniformityRange { get; set; }

      public WeighMode WeighMode { get; set; }

      public double InitialWeight { get; set; }

      public double InitialWeightFemale { get; set; }

      public double DetectionRange { get; set; }

      public double DetectionRangeFemale { get; set; }

      public int? BirdId { get; set; }

      public virtual Bird Bird { get; set; }

      public virtual Curve Curve { get; set; }

      public virtual Curve CurveFemale { get; set; }

      public virtual ICollection<EventDevice> EventDevices { get; set; }

      [NotMapped]
      public IEnumerable<Device> Devices => EventDevices?.Select(s => s.Device);
   }
}
