using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace DataModel
{
   public partial class Farm
   {
      public int Id { get; set; }

      [Required]
      public string Name { get; set; }

      public string Location { get; set; }

      public virtual ICollection<House> Houses { get; set; }
   }
}
