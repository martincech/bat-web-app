using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace DataModel
{
   public partial class Device
   {  
      public int Id { get; set; }

      [Required]
      [MaxLength(200)]
      public string Uid { get; set; }

      public string Name { get; set; }

      public DeviceType Type { get; set; }

      public int? HouseId { get; set; }

      public int? TerminalId { get; set; }

      public virtual House House { get; set; }

      public virtual Terminal Terminal { get; set; }

      public virtual ICollection<DeviceTimeSize> DeviceTimeSizes { get; set; }

      public virtual ICollection<EventDevice> EventDevices { get; set; }

      [NotMapped]
      public IEnumerable<TimeSize> TimeSizes => DeviceTimeSizes?.Select(s => s.TimeSize);

      [NotMapped]
      public IEnumerable<Event> Events => EventDevices?.Select(s => s.Event);
   }
}
