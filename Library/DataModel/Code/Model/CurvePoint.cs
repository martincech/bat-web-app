using System.ComponentModel.DataAnnotations.Schema;

namespace DataModel
{
   public partial class CurvePoint
   {
      public int Id { get; set; }
    
      public int CurveId { get; set; }

      public int Day { get; set; }

      public double? Value { get; set; }

      public double? Min { get; set; }

      public double? Max { get; set; }
   
      public virtual Curve Curve { get; set; }
   }
}
