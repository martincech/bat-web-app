﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataModel
{
   public class DeviceTimeSize
   {
      [Column("DeviceTimeSize_TimeSize_Id")]
      public int DeviceId { get; set; }
      public virtual Device Device { get; set; }

      [Column("TimeSizes_Id")]
      public int TimeSizeId { get; set; }
      public virtual TimeSize TimeSize { get; set; }

   }
}
