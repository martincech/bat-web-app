using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace DataModel
{
   public partial class House
   {
      public int Id { get; set; }

      public int? FarmId { get; set; }

      [Required]
      public string Name { get; set; }

      public string Location { get; set; }

      public virtual ICollection<Device> Devices { get; set; }

      public virtual Farm Farm { get; set; }
   }
}
