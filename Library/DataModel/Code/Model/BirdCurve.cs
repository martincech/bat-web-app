﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataModel
{
   public class BirdCurve
   {
      [Column("Birds_Id")]
      public int BirdId { get; set; }
      public virtual Bird Bird { get; set; }

      [Column("Curves_Id")]
      public int CurveId { get; set; }
      public virtual Curve Curve { get; set; }
   }
}
