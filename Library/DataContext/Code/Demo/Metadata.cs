﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataContext.Demo
{
   public class Metadata
   {

      public const string SCRIPT = @"SET IDENTITY_INSERT [dbo].[Birds] ON 
      
      INSERT [dbo].[Birds] ([Id], [Name], [Description], [SexMode]) VALUES (1, N'Tradition-brown-mixed', N'Tradition brown chicks. They grows cca 45 days.', 3)
      
      INSERT [dbo].[Birds] ([Id], [Name], [Description], [SexMode]) VALUES (2, N'Lite-brown-female', N'Brown chicks lite. They grows cca 42 days for 1200g', 1)
      
      INSERT [dbo].[Birds] ([Id], [Name], [Description], [SexMode]) VALUES (3, N'Tradition-white-male', N'White chicks classic. They grows cca 52 days for 1350g', 0)
      
      INSERT [dbo].[Birds] ([Id], [Name], [Description], [SexMode]) VALUES (4, N'Lite-white-undefined', N'White chicks lite.', 2)
      
      SET IDENTITY_INSERT [dbo].[Birds] OFF
      
      SET IDENTITY_INSERT [dbo].[Curves] ON 
      
      INSERT [dbo].[Curves] ([Id], [Name], [Type]) VALUES (1, N'LOHMANN BROWN-CLASSIC', 1)
      
      INSERT [dbo].[Curves] ([Id], [Name], [Type]) VALUES (2, N'LOHMANN LSL-CLASSIC', 0)
      
      INSERT [dbo].[Curves] ([Id], [Name], [Type]) VALUES (3, N'Brown-Extra', 2)
      
      INSERT [dbo].[Curves] ([Id], [Name], [Type]) VALUES (4, N'Brown-Lite', 2)
      
      INSERT [dbo].[Curves] ([Id], [Name], [Type]) VALUES (5, N'Classic-Female', 0)
      
      INSERT [dbo].[Curves] ([Id], [Name], [Type]) VALUES (6, N'Classic-Male', 1)
      
      INSERT [dbo].[Curves] ([Id], [Name], [Type]) VALUES (7, N'Cobb 500 - Female IN SEASON', 0)
      
      INSERT [dbo].[Curves] ([Id], [Name], [Type]) VALUES (8, N'Cobb 500 - Female OUT SEASON', 0)
      
      INSERT [dbo].[Curves] ([Id], [Name], [Type]) VALUES (9, N'Cobb 500 - Male', 1)
      
      INSERT [dbo].[Curves] ([Id], [Name], [Type]) VALUES (10, N'Cobb 500 - Male MX', 1)
      
      INSERT [dbo].[Curves] ([Id], [Name], [Type]) VALUES (11, N'Humidity-Classic', 3)
      
      INSERT [dbo].[Curves] ([Id], [Name], [Type]) VALUES (12, N'Humidity-Extra', 3)
      
      INSERT [dbo].[Curves] ([Id], [Name], [Type]) VALUES (13, N'Temperature-Extra', 4)
      
      INSERT [dbo].[Curves] ([Id], [Name], [Type]) VALUES (14, N'Temperature-Pullets', 4)
      
      INSERT [dbo].[Curves] ([Id], [Name], [Type]) VALUES (15, N'Cobb 500 - Male MX', 6)
      
      INSERT [dbo].[Curves] ([Id], [Name], [Type]) VALUES (16, N'Co2-Extra', 6)
      
      SET IDENTITY_INSERT [dbo].[Curves] OFF
      
      SET IDENTITY_INSERT [dbo].[Events] ON 
      
      INSERT [dbo].[Events] ([Id], [Name], [From], [To], [RowNumber], [State], [StartAge], [Stabilization_Range], [Stabilization_WindowSampleCount], [Stabilization_FiltrationAveragingWindow], [StepMode], [SexMode], [CurveId], [CurveFemaleId], [UniformityRange], [WeighMode], [InitialWeight], [InitialWeightFemale], [DetectionRange], [DetectionRangeFemale], [BirdId]) VALUES (1, N'Brown classic #1', DATEADD(day,-350 ,GETDATE()), DATEADD(day,-200 ,GETDATE()), 0, 1, 0, 5, 10, 10, 0, 3, NULL, NULL, 10, 2, 50, 50, 30, 30, 1)
      
      INSERT [dbo].[Events] ([Id], [Name], [From], [To], [RowNumber], [State], [StartAge], [Stabilization_Range], [Stabilization_WindowSampleCount], [Stabilization_FiltrationAveragingWindow], [StepMode], [SexMode], [CurveId], [CurveFemaleId], [UniformityRange], [WeighMode], [InitialWeight], [InitialWeightFemale], [DetectionRange], [DetectionRangeFemale], [BirdId]) VALUES (2, N'Brown classic #2',  DATEADD(day,-198 ,GETDATE()),  DATEADD(day, -50 ,GETDATE()), 0, 1, 0, 5, 10, 10, 0, 3, NULL, NULL, 10, 2, 50, 50, 30, 30, 1)
      
      INSERT [dbo].[Events] ([Id], [Name], [From], [To], [RowNumber], [State], [StartAge], [Stabilization_Range], [Stabilization_WindowSampleCount], [Stabilization_FiltrationAveragingWindow], [StepMode], [SexMode], [CurveId], [CurveFemaleId], [UniformityRange], [WeighMode], [InitialWeight], [InitialWeightFemale], [DetectionRange], [DetectionRangeFemale], [BirdId]) VALUES (3, N'Brown classic #3',  DATEADD(day,-48 ,GETDATE()),  DATEADD(day, 100 ,GETDATE()), 0, 1, 0, 5, 10, 10, 0, 3, NULL, NULL, 10, 2, 50, 50, 30, 30, 1)
      
      INSERT [dbo].[Events] ([Id], [Name], [From], [To], [RowNumber], [State], [StartAge], [Stabilization_Range], [Stabilization_WindowSampleCount], [Stabilization_FiltrationAveragingWindow], [StepMode], [SexMode], [CurveId], [CurveFemaleId], [UniformityRange], [WeighMode], [InitialWeight], [InitialWeightFemale], [DetectionRange], [DetectionRangeFemale], [BirdId]) VALUES (4, N'Brown classic #4',  DATEADD(day, 102 ,GETDATE()),  DATEADD(day, 250 ,GETDATE()), 0, 1, 0, 5, 10, 10, 0, 3, NULL, NULL, 10, 2, 50, 50, 30, 30, 1)
      
      INSERT [dbo].[Events] ([Id], [Name], [From], [To], [RowNumber], [State], [StartAge], [Stabilization_Range], [Stabilization_WindowSampleCount], [Stabilization_FiltrationAveragingWindow], [StepMode], [SexMode], [CurveId], [CurveFemaleId], [UniformityRange], [WeighMode], [InitialWeight], [InitialWeightFemale], [DetectionRange], [DetectionRangeFemale], [BirdId]) VALUES (6, N'Brown lite #1',  DATEADD(day,-25 ,GETDATE()),  DATEADD(day, 75 ,GETDATE()), 1, 1, 0, 5, 10, 10, 0, 1, NULL, NULL, 10, 2, 50, 50, 30, 30, 2)
      
      INSERT [dbo].[Events] ([Id], [Name], [From], [To], [RowNumber], [State], [StartAge], [Stabilization_Range], [Stabilization_WindowSampleCount], [Stabilization_FiltrationAveragingWindow], [StepMode], [SexMode], [CurveId], [CurveFemaleId], [UniformityRange], [WeighMode], [InitialWeight], [InitialWeightFemale], [DetectionRange], [DetectionRangeFemale], [BirdId]) VALUES (7, N'White classic #1',  DATEADD(day,-191 ,GETDATE()),  DATEADD(day,-138 ,GETDATE()), 2, 1, 1, 5, 10, 10, 0, 0, NULL, NULL, 10, 2, 50, 50, 30, 30, 3)
      
      INSERT [dbo].[Events] ([Id], [Name], [From], [To], [RowNumber], [State], [StartAge], [Stabilization_Range], [Stabilization_WindowSampleCount], [Stabilization_FiltrationAveragingWindow], [StepMode], [SexMode], [CurveId], [CurveFemaleId], [UniformityRange], [WeighMode], [InitialWeight], [InitialWeightFemale], [DetectionRange], [DetectionRangeFemale], [BirdId]) VALUES (8, N'White classic #2',  DATEADD(day,-137 ,GETDATE()),  DATEADD(day,102 ,GETDATE()), 2, 1, 1, 5, 10, 10, 0, 0, NULL, NULL, 10, 2, 50, 50, 30, 30, 3)
      
      INSERT [dbo].[Events] ([Id], [Name], [From], [To], [RowNumber], [State], [StartAge], [Stabilization_Range], [Stabilization_WindowSampleCount], [Stabilization_FiltrationAveragingWindow], [StepMode], [SexMode], [CurveId], [CurveFemaleId], [UniformityRange], [WeighMode], [InitialWeight], [InitialWeightFemale], [DetectionRange], [DetectionRangeFemale], [BirdId]) VALUES (9, N'White classic #3',  DATEADD(day,103 ,GETDATE()),  DATEADD(day,343 ,GETDATE()), 2, 1, 1, 5, 10, 10, 0, 0, NULL, NULL, 10, 2, 50, 50, 30, 30, 3)
      
      INSERT [dbo].[Events] ([Id], [Name], [From], [To], [RowNumber], [State], [StartAge], [Stabilization_Range], [Stabilization_WindowSampleCount], [Stabilization_FiltrationAveragingWindow], [StepMode], [SexMode], [CurveId], [CurveFemaleId], [UniformityRange], [WeighMode], [InitialWeight], [InitialWeightFemale], [DetectionRange], [DetectionRangeFemale], [BirdId]) VALUES (10, N'White lite #1',  DATEADD(day,-65 ,GETDATE()),  DATEADD(day,-19 ,GETDATE()), 3, 1, 2, 5, 10, 10, 0, 2, NULL, NULL, 10, 2, 50, 50, 30, 30, 4)
      
      INSERT [dbo].[Events] ([Id], [Name], [From], [To], [RowNumber], [State], [StartAge], [Stabilization_Range], [Stabilization_WindowSampleCount], [Stabilization_FiltrationAveragingWindow], [StepMode], [SexMode], [CurveId], [CurveFemaleId], [UniformityRange], [WeighMode], [InitialWeight], [InitialWeightFemale], [DetectionRange], [DetectionRangeFemale], [BirdId]) VALUES (11, N'White lite #2',  DATEADD(day,-16 ,GETDATE()),  DATEADD(day, 42 ,GETDATE()), 3, 1, 2, 5, 10, 10, 0, 2, NULL, NULL, 10, 2, 50, 50, 30, 30, 4)
      
      SET IDENTITY_INSERT [dbo].[Events] OFF
      
      INSERT [dbo].[BirdCurve] ([Birds_Id], [Curves_Id]) VALUES (1, 5)
      
      INSERT [dbo].[BirdCurve] ([Birds_Id], [Curves_Id]) VALUES (1, 6)
      
      INSERT [dbo].[BirdCurve] ([Birds_Id], [Curves_Id]) VALUES (1, 11)
      
      INSERT [dbo].[BirdCurve] ([Birds_Id], [Curves_Id]) VALUES (1, 13)
      
      INSERT [dbo].[BirdCurve] ([Birds_Id], [Curves_Id]) VALUES (1, 15)
      
      INSERT [dbo].[BirdCurve] ([Birds_Id], [Curves_Id]) VALUES (2, 5)
      
      INSERT [dbo].[BirdCurve] ([Birds_Id], [Curves_Id]) VALUES (2, 11)
      
      INSERT [dbo].[BirdCurve] ([Birds_Id], [Curves_Id]) VALUES (2, 13)
      
      INSERT [dbo].[BirdCurve] ([Birds_Id], [Curves_Id]) VALUES (2, 16)
      
      INSERT [dbo].[BirdCurve] ([Birds_Id], [Curves_Id]) VALUES (3, 6)
      
      INSERT [dbo].[BirdCurve] ([Birds_Id], [Curves_Id]) VALUES (3, 11)
      
      INSERT [dbo].[BirdCurve] ([Birds_Id], [Curves_Id]) VALUES (3, 13)
      
      INSERT [dbo].[BirdCurve] ([Birds_Id], [Curves_Id]) VALUES (3, 15)
      
      INSERT [dbo].[BirdCurve] ([Birds_Id], [Curves_Id]) VALUES (4, 4)
      
      INSERT [dbo].[BirdCurve] ([Birds_Id], [Curves_Id]) VALUES (4, 11)
      
      INSERT [dbo].[BirdCurve] ([Birds_Id], [Curves_Id]) VALUES (4, 13)
      
      INSERT [dbo].[BirdCurve] ([Birds_Id], [Curves_Id]) VALUES (4, 15)
      
      SET IDENTITY_INSERT [dbo].[CurvePoints] ON 
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (1, 1, 1, 75, 4, 4)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (2, 1, 7, 75, 4, 4)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (3, 1, 14, 130, 3.85, 3.85)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (4, 1, 21, 195, 3.59, 3.59)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (5, 1, 28, 275, 3.64, 3.64)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (6, 1, 35, 367, 3.54, 3.54)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (7, 1, 42, 475, 3.58, 3.58)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (8, 1, 49, 583, 3.43, 3.43)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (9, 1, 56, 685, 3.5, 3.5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (10, 1, 63, 782, 3.45, 3.45)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (11, 1, 70, 874, 3.55, 3.55)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (12, 1, 77, 961, 3.54, 3.54)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (13, 1, 84, 1043, 3.55, 3.55)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (14, 1, 91, 1123, 3.47, 3.47)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (15, 1, 98, 1197, 3.51, 3.51)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (16, 1, 105, 1264, 3.48, 3.48)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (17, 1, 112, 1330, 3.53, 3.53)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (18, 1, 119, 1400, 3.5, 3.5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (19, 1, 126, 1475, 3.53, 3.53)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (20, 1, 133, 1555, 3.47, 3.47)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (21, 1, 140, 1640, 3.48, 3.48)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (22, 1, 147, 1711, 3.51, 3.51)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (23, 1, 154, 1790, 4.53, 4.53)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (24, 1, 161, 1830, 4.48, 5.03)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (25, 1, 168, 1870, 4.97, 5.03)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (26, 1, 175, 1885, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (27, 1, 182, 1900, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (28, 1, 189, 1905, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (29, 1, 196, 1911, 5.02, 5.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (30, 1, 203, 1915, 5.01, 5.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (31, 1, 210, 1920, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (32, 1, 217, 1923, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (33, 1, 224, 1925, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (34, 1, 231, 1928, 4.98, 4.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (35, 1, 238, 1931, 5.02, 5.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (36, 1, 245, 1933, 5.02, 5.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (37, 1, 252, 1935, 5.01, 5.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (38, 1, 259, 1938, 5.01, 5.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (39, 1, 266, 1940, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (40, 1, 273, 1943, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (41, 1, 280, 1945, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (42, 1, 287, 1948, 4.98, 4.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (43, 1, 294, 1951, 5.02, 5.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (44, 1, 301, 1953, 5.02, 5.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (45, 1, 308, 1955, 5.01, 5.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (46, 1, 315, 1958, 5.01, 5.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (47, 1, 322, 1960, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (48, 1, 329, 1963, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (49, 1, 336, 1965, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (50, 1, 343, 1968, 4.98, 4.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (51, 1, 350, 1971, 5.02, 5.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (52, 1, 357, 1973, 5.02, 5.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (53, 1, 364, 1975, 5.01, 5.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (54, 1, 371, 1978, 5.01, 5.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (55, 1, 378, 1980, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (56, 1, 385, 1984, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (57, 1, 392, 1985, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (58, 1, 399, 1989, 4.98, 4.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (59, 1, 406, 1991, 5.02, 5.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (60, 1, 413, 1993, 5.02, 5.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (61, 1, 420, 1995, 5.01, 5.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (62, 1, 427, 1998, 5.01, 5.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (63, 1, 434, 2000, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (64, 1, 441, 2003, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (65, 1, 448, 2005, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (66, 1, 455, 2008, 4.98, 4.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (67, 1, 462, 2011, 5.02, 5.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (68, 1, 469, 2013, 5.02, 5.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (69, 1, 476, 2015, 5.01, 5.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (70, 1, 483, 2018, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (71, 1, 490, 2020, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (72, 1, 497, 2023, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (73, 1, 504, 2025, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (74, 1, 511, 2028, 4.98, 4.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (75, 1, 518, 2031, 5.02, 5.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (76, 1, 525, 2033, 5.02, 5.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (77, 1, 532, 2035, 5.01, 5.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (78, 1, 539, 2038, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (79, 1, 546, 2040, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (80, 1, 553, 2043, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (81, 1, 560, 2045, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (82, 1, 567, 2046, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (83, 1, 574, 2047, 4.98, 4.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (84, 1, 581, 2048, 4.98, 4.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (85, 1, 588, 2049, 4.98, 4.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (86, 1, 595, 2050, 4.98, 5.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (87, 2, 1, 75, 4, 4)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (88, 2, 7, 75, 4, 4)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (89, 2, 14, 125, 4, 4)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (90, 2, 21, 187, 3.74, 3.74)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (91, 2, 28, 257, 3.89, 3.89)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (92, 2, 35, 337, 3.86, 3.86)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (93, 2, 42, 429, 3.96, 3.96)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (94, 2, 49, 529, 3.97, 3.97)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (95, 2, 56, 624, 4.01, 4.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (96, 2, 63, 719, 4.03, 4.03)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (97, 2, 70, 809, 3.96, 3.96)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (98, 2, 77, 887, 3.95, 3.95)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (99, 2, 84, 957, 3.97, 3.97)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (100, 2, 91, 1017, 4.03, 4.03)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (101, 2, 98, 1072, 4.01, 4.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (102, 2, 105, 1122, 4.01, 4.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (103, 2, 112, 1167, 4.03, 4.03)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (104, 2, 119, 1214, 4.04, 4.04)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (105, 2, 126, 1264, 4.03, 4.03)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (106, 2, 133, 1322, 4.01, 4.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (107, 2, 140, 1386, 3.97, 3.97)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (108, 2, 147, 1450, 4, 4)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (109, 2, 154, 1500, 4, 4)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (110, 2, 161, 1540, 4.03, 4.03)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (111, 2, 168, 1580, 3.99, 3.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (112, 2, 175, 1610, 3.98, 3.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (113, 2, 182, 1630, 3.99, 3.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (114, 2, 189, 1650, 4, 4)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (115, 2, 196, 1670, 4.01, 4.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (116, 2, 203, 1690, 4.02, 4.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (117, 2, 210, 1700, 4, 4)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (118, 2, 217, 1705, 3.99, 3.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (119, 2, 224, 1710, 3.98, 3.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (120, 2, 231, 1713, 4.03, 3.97)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (121, 2, 238, 1715, 4.02, 4.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (122, 2, 245, 1718, 4.02, 3.96)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (123, 2, 252, 1720, 4.01, 4.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (124, 2, 259, 1723, 4, 3.95)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (125, 2, 266, 1725, 4, 4)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (126, 2, 273, 1728, 4.05, 3.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (127, 2, 280, 1730, 3.99, 3.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (128, 2, 287, 1733, 4.04, 3.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (129, 2, 294, 1735, 3.98, 3.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (130, 2, 301, 1738, 4.03, 3.97)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (131, 2, 308, 1740, 4.02, 4.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (132, 2, 315, 1743, 4.02, 3.96)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (133, 2, 322, 1745, 4.01, 4.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (134, 2, 329, 1748, 4, 3.95)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (135, 2, 336, 1750, 4, 4)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (136, 2, 343, 1751, 4, 4)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (137, 2, 350, 1753, 4.05, 3.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (138, 2, 357, 1754, 3.99, 3.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (139, 2, 364, 1755, 3.99, 3.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (140, 2, 371, 1756, 3.99, 4.04)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (141, 2, 378, 1758, 4.04, 3.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (142, 2, 385, 1759, 4.04, 3.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (143, 2, 392, 1760, 3.98, 3.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (144, 2, 399, 1761, 3.98, 4.03)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (145, 2, 406, 1763, 4.03, 3.97)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (146, 2, 413, 1764, 4.02, 3.97)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (147, 2, 420, 1765, 4.02, 4.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (148, 2, 427, 1766, 3.96, 4.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (149, 2, 434, 1768, 4.02, 3.96)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (150, 2, 441, 1769, 4.01, 4.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (151, 2, 448, 1770, 4.01, 4.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (152, 2, 455, 1771, 4.01, 4.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (153, 2, 462, 1773, 4, 3.95)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (154, 2, 469, 1774, 4, 4)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (155, 2, 476, 1775, 4, 4)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (156, 2, 483, 1776, 4, 4)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (157, 2, 490, 1778, 4.05, 3.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (158, 2, 497, 1779, 3.99, 3.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (159, 2, 504, 1780, 3.99, 3.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (160, 2, 511, 1781, 3.99, 4.04)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (161, 2, 518, 1783, 4.04, 3.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (162, 2, 525, 1784, 4.04, 3.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (163, 2, 532, 1785, 3.98, 3.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (164, 2, 539, 1786, 3.98, 4.03)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (165, 2, 546, 1788, 4.03, 3.97)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (166, 2, 553, 1789, 4.02, 3.97)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (167, 2, 560, 1790, 4.02, 4.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (168, 2, 567, 1791, 3.96, 4.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (169, 2, 574, 1793, 4.02, 3.96)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (170, 2, 581, 1794, 4.01, 4.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (171, 2, 588, 1795, 4.01, 4.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (172, 2, 595, 1796, 4.01, 4.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (173, 3, 1, 76, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (174, 3, 7, 76, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (175, 3, 14, 132, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (176, 3, 21, 199, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (177, 3, 28, 280, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (178, 3, 35, 374, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (179, 3, 42, 484, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (180, 3, 49, 594, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (181, 3, 56, 698, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (182, 3, 63, 797, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (183, 3, 70, 890, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (184, 3, 77, 979, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (185, 3, 84, 1062, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (186, 3, 91, 1144, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (187, 3, 98, 1219, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (188, 3, 105, 1287, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (189, 3, 112, 1355, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (190, 3, 119, 1426, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (191, 3, 126, 1502, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (192, 3, 133, 1584, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (193, 3, 140, 1670, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (194, 3, 147, 1743, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (195, 3, 154, 1823, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (196, 3, 161, 1868, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (197, 3, 168, 1906, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (198, 3, 175, 1922, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (199, 3, 182, 1935, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (200, 3, 189, 1940, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (201, 3, 196, 1946, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (202, 3, 203, 1951, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (203, 3, 210, 1955, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (204, 3, 217, 1958, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (205, 3, 224, 1962, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (206, 3, 231, 1965, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (207, 3, 238, 1968, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (208, 3, 245, 1971, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (209, 3, 252, 1974, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (210, 3, 259, 1977, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (211, 3, 266, 1980, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (212, 3, 273, 1983, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (213, 3, 280, 1986, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (214, 3, 287, 1989, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (215, 3, 294, 1993, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (216, 3, 301, 1995, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (217, 3, 308, 1997, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (218, 3, 315, 1999, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (219, 3, 322, 2001, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (220, 3, 329, 2003, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (221, 3, 336, 2005, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (222, 3, 343, 2007, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (223, 3, 350, 2009, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (224, 3, 357, 2011, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (225, 3, 364, 2013, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (226, 3, 371, 2015, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (227, 3, 378, 2017, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (228, 3, 385, 2019, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (229, 3, 392, 2021, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (230, 3, 399, 2022, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (231, 3, 406, 2023, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (232, 3, 413, 2024, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (233, 3, 420, 2025, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (234, 3, 427, 2026, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (235, 3, 434, 2027, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (236, 3, 441, 2028, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (237, 3, 448, 2029, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (238, 3, 455, 2030, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (239, 3, 462, 2031, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (240, 3, 469, 2032, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (241, 3, 476, 2033, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (242, 3, 483, 2034, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (243, 3, 490, 2035, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (244, 3, 497, 2036, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (245, 3, 504, 2037, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (246, 3, 511, 2038, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (247, 3, 518, 2039, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (248, 3, 525, 2040, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (249, 3, 532, 2041, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (250, 3, 539, 2042, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (251, 3, 546, 2043, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (252, 3, 553, 2044, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (253, 3, 560, 2045, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (254, 3, 567, 2046, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (255, 3, 574, 2047, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (256, 3, 581, 2048, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (257, 3, 588, 2049, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (258, 3, 595, 2050, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (259, 4, 1, 75, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (260, 4, 7, 75, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (261, 4, 14, 130, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (262, 4, 21, 195, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (263, 4, 28, 275, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (264, 4, 35, 367, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (265, 4, 42, 475, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (266, 4, 49, 583, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (267, 4, 56, 685, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (268, 4, 63, 782, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (269, 4, 70, 874, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (270, 4, 77, 961, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (271, 4, 84, 1043, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (272, 4, 91, 1123, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (273, 4, 98, 1197, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (274, 4, 105, 1264, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (275, 4, 112, 1330, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (276, 4, 119, 1400, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (277, 4, 126, 1475, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (278, 4, 133, 1555, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (279, 4, 140, 1640, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (280, 4, 147, 1711, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (281, 4, 154, 1790, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (282, 4, 161, 1830, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (283, 4, 168, 1870, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (284, 4, 175, 1885, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (285, 4, 182, 1900, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (286, 4, 189, 1905, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (287, 4, 196, 1911, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (288, 4, 203, 1915, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (289, 4, 210, 1920, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (290, 4, 217, 1923, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (291, 4, 224, 1925, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (292, 4, 231, 1928, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (293, 4, 238, 1931, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (294, 4, 245, 1933, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (295, 4, 252, 1935, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (296, 4, 259, 1938, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (297, 4, 266, 1940, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (298, 4, 273, 1943, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (299, 4, 280, 1945, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (300, 4, 287, 1948, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (301, 4, 294, 1951, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (302, 4, 301, 1953, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (303, 4, 308, 1955, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (304, 4, 315, 1958, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (305, 4, 322, 1960, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (306, 4, 329, 1963, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (307, 4, 336, 1965, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (308, 4, 343, 1968, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (309, 4, 350, 1971, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (310, 4, 357, 1973, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (311, 4, 364, 1975, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (312, 4, 371, 1978, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (313, 4, 378, 1980, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (314, 4, 385, 1984, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (315, 4, 392, 1985, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (316, 4, 399, 1989, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (317, 4, 406, 1991, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (318, 4, 413, 1993, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (319, 4, 420, 1995, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (320, 4, 427, 1998, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (321, 4, 434, 2000, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (322, 4, 441, 2003, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (323, 4, 448, 2005, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (324, 4, 455, 2008, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (325, 4, 462, 2011, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (326, 4, 469, 2013, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (327, 4, 476, 2015, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (328, 4, 483, 2018, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (329, 4, 490, 2020, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (330, 4, 497, 2023, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (331, 4, 504, 2025, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (332, 4, 511, 2028, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (333, 4, 518, 2031, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (334, 4, 525, 2033, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (335, 4, 532, 2035, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (336, 4, 539, 2038, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (337, 4, 546, 2040, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (338, 4, 553, 2043, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (339, 4, 560, 2045, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (340, 4, 567, 2046, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (341, 4, 574, 2047, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (342, 4, 581, 2048, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (343, 4, 588, 2049, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (344, 4, 595, 2050, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (345, 5, 1, 74, 4, 4)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (346, 5, 7, 74, 4, 4)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (347, 5, 14, 128, 3.85, 3.85)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (348, 5, 21, 196, 3.59, 3.59)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (349, 5, 28, 274, 3.64, 3.64)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (350, 5, 35, 359, 3.54, 3.54)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (351, 5, 42, 469, 3.58, 3.58)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (352, 5, 49, 580, 3.43, 3.43)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (353, 5, 56, 684, 3.5, 3.5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (354, 5, 63, 782, 3.45, 3.45)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (355, 5, 70, 874, 3.55, 3.55)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (356, 5, 77, 961, 3.54, 3.54)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (357, 5, 84, 1043, 3.55, 3.55)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (358, 5, 91, 1123, 3.47, 3.47)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (359, 5, 98, 1197, 3.51, 3.51)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (360, 5, 105, 1264, 3.48, 3.48)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (361, 5, 112, 1330, 3.53, 3.53)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (362, 5, 119, 1400, 3.5, 3.5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (363, 5, 126, 1475, 3.53, 3.53)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (364, 5, 133, 1555, 3.47, 3.47)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (365, 5, 140, 1640, 3.48, 3.48)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (366, 5, 147, 1711, 3.51, 3.51)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (367, 5, 154, 1790, 4.53, 4.53)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (368, 5, 161, 1830, 4.48, 5.03)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (369, 5, 168, 1870, 4.97, 5.03)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (370, 5, 175, 1885, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (371, 5, 182, 1900, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (372, 5, 189, 1905, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (373, 5, 196, 1911, 5.02, 5.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (374, 5, 203, 1915, 5.01, 5.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (375, 5, 210, 1920, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (376, 5, 217, 1923, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (377, 5, 224, 1925, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (378, 5, 231, 1928, 4.98, 4.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (379, 5, 238, 1931, 5.02, 5.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (380, 5, 245, 1933, 5.02, 5.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (381, 5, 252, 1935, 5.01, 5.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (382, 5, 259, 1938, 5.01, 5.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (383, 5, 266, 1940, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (384, 5, 273, 1943, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (385, 5, 280, 1945, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (386, 5, 287, 1948, 4.98, 4.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (387, 5, 294, 1951, 5.02, 5.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (388, 5, 301, 1953, 5.02, 5.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (389, 5, 308, 1955, 5.01, 5.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (390, 5, 315, 1958, 5.01, 5.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (391, 5, 322, 1960, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (392, 5, 329, 1963, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (393, 5, 336, 1965, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (394, 5, 343, 1968, 4.98, 4.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (395, 5, 350, 1971, 5.02, 5.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (396, 5, 357, 1973, 5.02, 5.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (397, 5, 364, 1975, 5.01, 5.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (398, 5, 371, 1978, 5.01, 5.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (399, 5, 378, 1980, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (400, 5, 385, 1984, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (401, 5, 392, 1985, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (402, 5, 399, 1989, 4.98, 4.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (403, 5, 406, 1991, 5.02, 5.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (404, 5, 413, 1993, 5.02, 5.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (405, 5, 420, 1995, 5.01, 5.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (406, 5, 427, 1998, 5.01, 5.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (407, 5, 434, 2000, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (408, 5, 441, 2003, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (409, 5, 448, 2005, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (410, 5, 455, 2008, 4.98, 4.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (411, 5, 462, 2011, 5.02, 5.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (412, 5, 469, 2013, 5.02, 5.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (413, 5, 476, 2015, 5.01, 5.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (414, 5, 483, 2018, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (415, 5, 490, 2020, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (416, 5, 497, 2023, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (417, 5, 504, 2025, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (418, 5, 511, 2028, 4.98, 4.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (419, 5, 518, 2031, 5.02, 5.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (420, 5, 525, 2033, 5.02, 5.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (421, 5, 532, 2035, 5.01, 5.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (422, 5, 539, 2038, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (423, 5, 546, 2039, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (424, 5, 553, 2041, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (425, 5, 560, 2043, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (426, 5, 567, 2044, 4.99, 4.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (427, 5, 574, 2045, 4.98, 4.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (428, 5, 581, 2046, 4.98, 4.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (429, 5, 588, 2047, 4.98, 4.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (430, 5, 595, 2048, 4.98, 5.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (431, 6, 1, 72, 4, 4)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (432, 6, 7, 72, 4, 4)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (433, 6, 14, 124, 4, 4)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (434, 6, 21, 188, 3.74, 3.74)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (435, 6, 28, 256, 3.89, 3.89)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (436, 6, 35, 335, 3.86, 3.86)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (437, 6, 42, 429, 3.96, 3.96)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (438, 6, 49, 529, 3.97, 3.97)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (439, 6, 56, 624, 4.01, 4.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (440, 6, 63, 719, 4.03, 4.03)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (441, 6, 70, 809, 3.96, 3.96)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (442, 6, 77, 887, 3.95, 3.95)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (443, 6, 84, 957, 3.97, 3.97)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (444, 6, 91, 1017, 4.03, 4.03)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (445, 6, 98, 1072, 4.01, 4.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (446, 6, 105, 1122, 4.01, 4.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (447, 6, 112, 1167, 4.03, 4.03)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (448, 6, 119, 1214, 4.04, 4.04)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (449, 6, 126, 1264, 4.03, 4.03)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (450, 6, 133, 1322, 4.01, 4.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (451, 6, 140, 1386, 3.97, 3.97)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (452, 6, 147, 1450, 4, 4)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (453, 6, 154, 1500, 4, 4)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (454, 6, 161, 1540, 4.03, 4.03)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (455, 6, 168, 1580, 3.99, 3.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (456, 6, 175, 1610, 3.98, 3.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (457, 6, 182, 1630, 3.99, 3.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (458, 6, 189, 1650, 4, 4)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (459, 6, 196, 1670, 4.01, 4.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (460, 6, 203, 1690, 4.02, 4.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (461, 6, 210, 1700, 4, 4)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (462, 6, 217, 1705, 3.99, 3.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (463, 6, 224, 1710, 3.98, 3.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (464, 6, 231, 1713, 4.03, 3.97)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (465, 6, 238, 1715, 4.02, 4.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (466, 6, 245, 1718, 4.02, 3.96)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (467, 6, 252, 1720, 4.01, 4.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (468, 6, 259, 1723, 4, 3.95)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (469, 6, 266, 1725, 4, 4)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (470, 6, 273, 1728, 4.05, 3.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (471, 6, 280, 1730, 3.99, 3.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (472, 6, 287, 1733, 4.04, 3.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (473, 6, 294, 1735, 3.98, 3.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (474, 6, 301, 1738, 4.03, 3.97)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (475, 6, 308, 1740, 4.02, 4.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (476, 6, 315, 1743, 4.02, 3.96)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (477, 6, 322, 1745, 4.01, 4.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (478, 6, 329, 1748, 4, 3.95)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (479, 6, 336, 1750, 4, 4)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (480, 6, 343, 1751, 4, 4)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (481, 6, 350, 1753, 4.05, 3.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (482, 6, 357, 1754, 3.99, 3.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (483, 6, 364, 1755, 3.99, 3.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (484, 6, 371, 1756, 3.99, 4.04)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (485, 6, 378, 1758, 4.04, 3.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (486, 6, 385, 1759, 4.04, 3.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (487, 6, 392, 1760, 3.98, 3.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (488, 6, 399, 1761, 3.98, 4.03)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (489, 6, 406, 1763, 4.03, 3.97)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (490, 6, 413, 1764, 4.02, 3.97)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (491, 6, 420, 1765, 4.02, 4.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (492, 6, 427, 1766, 3.96, 4.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (493, 6, 434, 1768, 4.02, 3.96)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (494, 6, 441, 1769, 4.01, 4.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (495, 6, 448, 1770, 4.01, 4.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (496, 6, 455, 1771, 4.01, 4.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (497, 6, 462, 1773, 4, 3.95)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (498, 6, 469, 1774, 4, 4)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (499, 6, 476, 1775, 4, 4)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (500, 6, 483, 1776, 4, 4)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (501, 6, 490, 1778, 4.05, 3.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (502, 6, 497, 1779, 3.99, 3.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (503, 6, 504, 1780, 3.99, 3.99)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (504, 6, 511, 1781, 3.99, 4.04)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (505, 6, 518, 1783, 4.04, 3.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (506, 6, 525, 1784, 4.04, 3.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (507, 6, 532, 1785, 3.98, 3.98)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (508, 6, 539, 1786, 3.98, 4.03)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (509, 6, 546, 1788, 4.03, 3.97)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (510, 6, 553, 1789, 4.02, 3.97)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (511, 6, 560, 1790, 4.02, 4.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (512, 6, 567, 1792, 3.96, 4.02)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (513, 6, 574, 1793, 4.02, 3.96)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (514, 6, 581, 1795, 4.01, 4.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (515, 6, 588, 1796, 4.01, 4.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (516, 6, 595, 1798, 4.01, 4.01)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (517, 7, 1, 42, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (518, 7, 7, 160, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (519, 7, 14, 285, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (520, 7, 21, 410, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (521, 7, 28, 540, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (522, 7, 35, 645, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (523, 7, 42, 750, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (524, 7, 49, 850, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (525, 7, 56, 950, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (526, 7, 63, 1050, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (527, 7, 70, 1160, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (528, 7, 77, 1250, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (529, 7, 84, 1335, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (530, 7, 91, 1420, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (531, 7, 98, 1505, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (532, 7, 105, 1590, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (533, 7, 112, 1680, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (534, 7, 119, 1790, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (535, 7, 126, 1930, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (536, 7, 133, 2090, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (537, 7, 140, 2250, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (538, 7, 147, 2510, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (539, 7, 154, 2675, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (540, 7, 161, 2845, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (541, 7, 168, 3010, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (542, 7, 175, 3105, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (543, 7, 182, 3200, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (544, 7, 189, 3290, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (545, 7, 196, 3385, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (546, 7, 203, 3480, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (547, 7, 210, 3570, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (548, 7, 217, 3595, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (549, 7, 224, 3615, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (550, 7, 231, 3635, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (551, 7, 238, 3655, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (552, 7, 245, 3675, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (553, 7, 252, 3695, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (554, 7, 259, 3715, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (555, 7, 266, 3735, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (556, 7, 273, 3755, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (557, 7, 280, 3770, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (558, 7, 287, 3785, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (559, 7, 294, 3800, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (560, 7, 301, 3815, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (561, 7, 308, 3830, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (562, 7, 315, 3845, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (563, 7, 322, 3860, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (564, 7, 329, 3875, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (565, 7, 336, 3890, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (566, 7, 343, 3905, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (567, 7, 350, 3915, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (568, 7, 357, 3925, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (569, 7, 364, 3935, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (570, 7, 371, 3945, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (571, 7, 378, 3955, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (572, 7, 385, 3965, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (573, 7, 392, 3975, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (574, 7, 399, 3985, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (575, 7, 406, 3995, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (576, 7, 413, 4005, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (577, 7, 420, 4015, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (578, 7, 427, 4020, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (579, 7, 434, 4025, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (580, 7, 441, 4030, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (581, 7, 448, 4035, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (582, 7, 455, 4040, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (583, 8, 1, 42, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (584, 8, 7, 160, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (585, 8, 14, 285, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (586, 8, 21, 410, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (587, 8, 28, 540, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (588, 8, 35, 645, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (589, 8, 42, 750, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (590, 8, 49, 850, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (591, 8, 56, 950, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (592, 8, 63, 1050, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (593, 8, 70, 1150, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (594, 8, 77, 1245, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (595, 8, 84, 1340, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (596, 8, 91, 1440, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (597, 8, 98, 1540, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (598, 8, 105, 1645, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (599, 8, 112, 1745, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (600, 8, 119, 1895, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (601, 8, 126, 2045, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (602, 8, 133, 2190, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (603, 8, 140, 2340, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (604, 8, 147, 2610, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (605, 8, 154, 2782, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (606, 8, 161, 2959, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (607, 8, 168, 3130, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (608, 8, 175, 3229, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (609, 8, 182, 3328, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (610, 8, 189, 3422, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (611, 8, 196, 3520, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (612, 8, 203, 3619, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (613, 8, 210, 3677, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (614, 8, 217, 3703, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (615, 8, 224, 3723, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (616, 8, 231, 3744, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (617, 8, 238, 3760, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (618, 8, 245, 3770, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (619, 8, 252, 3787, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (620, 8, 259, 3800, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (621, 8, 266, 3821, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (622, 8, 273, 3836, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (623, 8, 280, 3851, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (624, 8, 287, 3866, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (625, 8, 294, 3881, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (626, 8, 301, 3896, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (627, 8, 308, 3906, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (628, 8, 315, 3916, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (629, 8, 322, 3926, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (630, 8, 329, 3936, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (631, 8, 336, 3946, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (632, 8, 343, 3956, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (633, 8, 350, 3966, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (634, 8, 357, 3976, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (635, 8, 364, 3986, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (636, 8, 371, 3996, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (637, 8, 378, 4006, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (638, 8, 385, 4016, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (639, 8, 392, 4026, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (640, 8, 399, 4036, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (641, 8, 406, 4046, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (642, 8, 413, 4056, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (643, 8, 420, 4066, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (644, 8, 427, 4072, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (645, 8, 434, 4078, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (646, 8, 441, 4084, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (647, 8, 448, 4090, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (648, 8, 455, 4096, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (649, 9, 1, 40, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (650, 9, 7, 150, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (651, 9, 14, 330, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (652, 9, 21, 520, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (653, 9, 28, 690, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (654, 9, 35, 850, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (655, 9, 42, 1000, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (656, 9, 49, 1140, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (657, 9, 56, 1270, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (658, 9, 63, 1400, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (659, 9, 70, 1525, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (660, 9, 77, 1650, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (661, 9, 84, 1765, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (662, 9, 91, 1880, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (663, 9, 98, 1995, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (664, 9, 105, 2120, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (665, 9, 112, 2270, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (666, 9, 119, 2430, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (667, 9, 126, 2590, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (668, 9, 133, 2750, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (669, 9, 140, 2910, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (670, 9, 147, 3040, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (671, 9, 154, 3170, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (672, 9, 161, 3400, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (673, 9, 168, 3515, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (674, 9, 175, 3630, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (675, 9, 182, 3730, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (676, 9, 189, 3850, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (677, 9, 196, 3950, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (678, 9, 203, 4050, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (679, 9, 210, 4140, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (680, 9, 217, 4185, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (681, 9, 224, 4230, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (682, 9, 231, 4255, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (683, 9, 238, 4275, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (684, 9, 245, 4300, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (685, 9, 252, 4320, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (686, 9, 259, 4340, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (687, 9, 266, 4365, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (688, 9, 273, 4385, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (689, 9, 280, 4410, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (690, 9, 287, 4430, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (691, 9, 294, 4455, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (692, 9, 301, 4475, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (693, 9, 308, 4500, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (694, 9, 315, 4520, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (695, 9, 322, 4540, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (696, 9, 329, 4565, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (697, 9, 336, 4585, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (698, 9, 343, 4610, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (699, 9, 350, 4630, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (700, 9, 357, 4655, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (701, 9, 364, 4675, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (702, 9, 371, 4700, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (703, 9, 378, 4720, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (704, 9, 385, 4745, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (705, 9, 392, 4765, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (706, 9, 399, 4785, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (707, 9, 406, 4810, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (708, 9, 413, 4830, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (709, 9, 420, 4855, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (710, 9, 427, 4870, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (711, 9, 434, 4885, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (712, 9, 441, 4900, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (713, 9, 448, 4915, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (714, 9, 455, 4930, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (715, 10, 1, 40, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (716, 10, 7, 145, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (717, 10, 14, 340, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (718, 10, 21, 520, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (719, 10, 28, 665, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (720, 10, 35, 800, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (721, 10, 42, 930, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (722, 10, 49, 1060, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (723, 10, 56, 1190, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (724, 10, 63, 1320, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (725, 10, 70, 1455, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (726, 10, 77, 1570, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (727, 10, 84, 1695, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (728, 10, 91, 1810, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (729, 10, 98, 1920, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (730, 10, 105, 2035, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (731, 10, 112, 2160, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (732, 10, 119, 2300, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (733, 10, 126, 2450, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (734, 10, 133, 2600, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (735, 10, 140, 2725, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (736, 10, 147, 2850, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (737, 10, 154, 2970, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (738, 10, 161, 3230, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (739, 10, 168, 3355, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (740, 10, 175, 3485, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (741, 10, 182, 3610, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (742, 10, 189, 3735, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (743, 10, 196, 3865, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (744, 10, 203, 3930, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (745, 10, 210, 3970, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (746, 10, 217, 4015, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (747, 10, 224, 4060, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (748, 10, 231, 4085, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (749, 10, 238, 4105, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (750, 10, 245, 4130, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (751, 10, 252, 4150, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (752, 10, 259, 4170, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (753, 10, 266, 4195, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (754, 10, 273, 4215, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (755, 10, 280, 4240, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (756, 10, 287, 4260, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (757, 10, 294, 4285, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (758, 10, 301, 4305, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (759, 10, 308, 4330, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (760, 10, 315, 4350, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (761, 10, 322, 4370, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (762, 10, 329, 4395, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (763, 10, 336, 4415, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (764, 10, 343, 4440, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (765, 10, 350, 4460, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (766, 10, 357, 4485, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (767, 10, 364, 4505, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (768, 10, 371, 4530, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (769, 10, 378, 4550, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (770, 10, 385, 4575, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (771, 10, 392, 4595, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (772, 10, 399, 4615, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (773, 10, 406, 4640, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (774, 10, 413, 4660, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (775, 10, 420, 4685, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (776, 10, 427, 4700, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (777, 10, 434, 4715, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (778, 10, 441, 4730, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (779, 10, 448, 4745, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (780, 10, 455, 4760, 5, 5)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (781, 11, 1, 1, 60, 70)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (782, 11, 595, 1, 60, 70)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (783, 12, 1, 1, 50, 75)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (784, 12, 49, 1, 55, 70)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (785, 12, 98, 1, 60, 70)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (786, 12, 595, 1, 60, 70)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (787, 13, 1, 1, 35, 38)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (788, 13, 49, 1, 30, 35)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (789, 13, 98, 1, 27, 30)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (790, 13, 595, 1, 25, 29)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (791, 14, 7, 1, 35, 36)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (792, 14, 7, 1, 35, 36)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (793, 14, 14, 1, 35, 36)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (794, 14, 21, 1, 33, 34)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (795, 14, 28, 1, 33, 34)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (796, 14, 35, 1, 31, 32)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (797, 14, 42, 1, 31, 32)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (798, 14, 49, 1, 31, 32)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (799, 14, 56, 1, 28, 29)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (800, 14, 63, 1, 28, 29)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (801, 14, 70, 1, 28, 29)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (802, 14, 77, 1, 28, 29)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (803, 14, 84, 1, 28, 29)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (804, 14, 91, 1, 28, 29)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (805, 14, 98, 1, 28, 29)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (806, 14, 105, 1, 26, 27)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (807, 14, 112, 1, 26, 27)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (808, 14, 119, 1, 26, 27)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (809, 14, 126, 1, 26, 27)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (810, 14, 133, 1, 26, 27)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (811, 14, 140, 1, 26, 27)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (812, 14, 147, 1, 26, 27)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (813, 14, 154, 1, 22, 24)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (814, 14, 161, 1, 22, 24)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (815, 14, 168, 1, 22, 24)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (816, 14, 175, 1, 22, 24)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (817, 14, 182, 1, 22, 24)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (818, 14, 189, 1, 22, 24)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (819, 14, 196, 1, 22, 24)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (820, 14, 203, 1, 18, 20)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (821, 14, 210, 1, 18, 20)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (822, 14, 217, 1, 18, 20)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (823, 14, 224, 1, 18, 20)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (824, 14, 231, 1, 18, 20)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (825, 14, 238, 1, 18, 20)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (826, 14, 245, 1, 18, 20)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (827, 15, 1, NULL, 350, 4500)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (828, 15, 150, NULL, 350, 4500)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (829, 16, 0, 1250, 1200, 1500)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (830, 16, 1, 1250, 1200, 1500)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (831, 16, 2, 1250, 1200, 1500)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (832, 16, 3, 1250, 1200, 1500)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (833, 16, 4, 1250, 1200, 1500)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (834, 16, 5, 1250, 1200, 1500)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (835, 16, 6, 1250, 1200, 1500)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (836, 16, 7, 1250, 1200, 1500)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (837, 16, 8, 1250, 1200, 1500)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (838, 16, 9, 1250, 1200, 1500)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (839, 16, 10, 1250, 1200, 1500)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (840, 16, 11, 1250, 1200, 1500)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (841, 16, 12, 1250, 1200, 1500)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (842, 16, 13, 1250, 1200, 1500)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (843, 16, 14, 1250, 1200, 1500)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (844, 16, 15, 1250, 1200, 1500)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (845, 16, 16, 1250, 1200, 1500)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (846, 16, 17, 1250, 1200, 1500)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (847, 16, 18, 1250, 1200, 1500)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (848, 16, 19, 1250, 1200, 1500)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (849, 16, 20, 1250, 1200, 1500)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (850, 16, 21, 1150, 1000, 1200)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (851, 16, 22, 1100, 1000, 1200)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (852, 16, 23, 1080, 1000, 1200)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (853, 16, 24, 1030, 1000, 1200)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (854, 16, 25, 1030, 1000, 1200)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (855, 16, 26, 1030, 1000, 1200)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (856, 16, 27, 1030, 1000, 1200)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (857, 16, 28, 1030, 1000, 1200)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (858, 16, 29, 1030, 1000, 1200)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (859, 16, 30, 900, 800, 1000)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (860, 16, 31, 900, 800, 1000)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (861, 16, 32, 900, 800, 1000)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (862, 16, 33, 900, 800, 1000)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (863, 16, 34, 900, 800, 1000)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (864, 16, 35, 900, 800, 1000)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (865, 16, 36, 900, 800, 1000)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (866, 16, 37, 900, 800, 1000)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (867, 16, 38, 900, 800, 1000)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (868, 16, 39, 900, 800, 1000)
      
      INSERT [dbo].[CurvePoints] ([Id], [CurveId], [Day], [Value], [Min], [Max]) VALUES (869, 16, 40, 900, 800, 1000)
      
      SET IDENTITY_INSERT [dbo].[CurvePoints] OFF
      
      SET IDENTITY_INSERT [dbo].[Farms] ON 

      
      INSERT [dbo].[Farms] ([Id], [Name], [Location]) VALUES (1, N'Farm#1', NULL)
      
      INSERT [dbo].[Farms] ([Id], [Name], [Location]) VALUES (2, N'Farm#2', NULL)
      
      INSERT [dbo].[Farms] ([Id], [Name], [Location]) VALUES (3, N'Farm#3', NULL)
      
      SET IDENTITY_INSERT [dbo].[Farms] OFF
      
      SET IDENTITY_INSERT [dbo].[Houses] ON 
      
      INSERT [dbo].[Houses] ([Id], [FarmId], [Name], [Location]) VALUES (1, 1, N'House#1', NULL)
      
      INSERT [dbo].[Houses] ([Id], [FarmId], [Name], [Location]) VALUES (2, 1, N'House#2', NULL)
      
      INSERT [dbo].[Houses] ([Id], [FarmId], [Name], [Location]) VALUES (3, 1, N'House#3', NULL)
      
      INSERT [dbo].[Houses] ([Id], [FarmId], [Name], [Location]) VALUES (4, 2, N'House#1', NULL)
      
      INSERT [dbo].[Houses] ([Id], [FarmId], [Name], [Location]) VALUES (5, 3, N'House#1', NULL)
      
      INSERT [dbo].[Houses] ([Id], [FarmId], [Name], [Location]) VALUES (6, 3, N'House#2', NULL)
      
      INSERT [dbo].[Houses] ([Id], [FarmId], [Name], [Location]) VALUES (7, 3, N'Outside', NULL)
      
      SET IDENTITY_INSERT [dbo].[Houses] OFF
      
      SET IDENTITY_INSERT [dbo].[Terminals] ON 
      
      INSERT [dbo].[Terminals] ([Id], [Uid], [Name]) VALUES (1, N'031fc921-40a2-4818-8668-5201a465fb02', N'Terminal#1')
      
      INSERT [dbo].[Terminals] ([Id], [Uid], [Name]) VALUES (2, N'3c1083f4-51bd-42d3-819c-644abcb546c2', N'Terminal#2')
      
      INSERT [dbo].[Terminals] ([Id], [Uid], [Name]) VALUES (3, N'7a0148a8-c631-41a7-9384-be0d10508b94', N'Terminal#3')
      
      INSERT [dbo].[Terminals] ([Id], [Uid], [Name]) VALUES (4, N'46cd73d4-db4b-4b18-9f89-f6875e7ff4b1', N'Terminal#4')
      
      INSERT [dbo].[Terminals] ([Id], [Uid], [Name]) VALUES (5, N'ddc44fae-a4ce-42e1-b6ee-05d932a541f6', N'Terminal#BAT2')
      
      SET IDENTITY_INSERT [dbo].[Terminals] OFF
      
      SET IDENTITY_INSERT [dbo].[Devices] ON 
      
      INSERT [dbo].[Devices] ([Id], [Uid], [Name], [Type], [HouseId], [TerminalId]) VALUES (1, N'bae12058-8991-42b8-8b22-0363d64178ec', N'SP', 1, 1, NULL)
      
      INSERT [dbo].[Devices] ([Id], [Uid], [Name], [Type], [HouseId], [TerminalId]) VALUES (2, N'6ac4a885-cb4a-45e9-99a7-78de5adf3e64', N'SP', 1, 2, 1)
      
      INSERT [dbo].[Devices] ([Id], [Uid], [Name], [Type], [HouseId], [TerminalId]) VALUES (3, N'e339a6c5-7b4c-47c9-840d-65df796fcacd', N'SP3(Ext)', 3, NULL, 2)
      
      INSERT [dbo].[Devices] ([Id], [Uid], [Name], [Type], [HouseId], [TerminalId]) VALUES (4, N'65ea4ef5-a52f-42b8-97fa-f971073fb6ef', N'Bat2-Cable', 4, 3, 5)
      
      INSERT [dbo].[Devices] ([Id], [Uid], [Name], [Type], [HouseId], [TerminalId]) VALUES (5, N'5f58768b-3f7e-4899-8619-a77e88ec4dc3', N'Bat2-GSM', 2, 4, NULL)
      
      INSERT [dbo].[Devices] ([Id], [Uid], [Name], [Type], [HouseId], [TerminalId]) VALUES (6, N'05549aaf-606d-4e6b-853d-95102ccb720a', N'Temperature Outside', 5, 7, 4)
      
      INSERT [dbo].[Devices] ([Id], [Uid], [Name], [Type], [HouseId], [TerminalId]) VALUES (7, N'ad655b1c-0d96-4003-975c-a8722e7663ee', N'Temperature#1', 5, 5, 4)
      
      INSERT [dbo].[Devices] ([Id], [Uid], [Name], [Type], [HouseId], [TerminalId]) VALUES (8, N'469f16b8-bdad-4e39-98f2-9a157c5eb868', N'Temperature#2', 5, 5, 4)
      
      INSERT [dbo].[Devices] ([Id], [Uid], [Name], [Type], [HouseId], [TerminalId]) VALUES (9, N'463a600f-d619-47ca-b74d-1f224c7fff8a', N'Temperature#3', 5, 5, 4)
      
      INSERT [dbo].[Devices] ([Id], [Uid], [Name], [Type], [HouseId], [TerminalId]) VALUES (10, N'9643d01c-c7da-4e32-b25b-40005dc25eb9', N'Temperature#4', 5, 5, 4)
      
      INSERT [dbo].[Devices] ([Id], [Uid], [Name], [Type], [HouseId], [TerminalId]) VALUES (11, N'080a64b0-b02b-46e0-84be-f02a06522fdb', N'Co2', 7, 5, 4)
      
      INSERT [dbo].[Devices] ([Id], [Uid], [Name], [Type], [HouseId], [TerminalId]) VALUES (12, N'd5fe7ff5-91fe-4cbd-95bb-8d1b00a50084', N'Humidity', 6, 5, 4)
      
      INSERT [dbo].[Devices] ([Id], [Uid], [Name], [Type], [HouseId], [TerminalId]) VALUES (13, N'de68d89f-3c21-452e-acd5-c15523d7f9b6', N'Temperatur#1', 5, 6, 4)
      
      INSERT [dbo].[Devices] ([Id], [Uid], [Name], [Type], [HouseId], [TerminalId]) VALUES (14, N'46788d5b-cadd-4217-b2a4-d0dcfbba0d30', N'Temperatur#2', 5, 6, 4)
      
      INSERT [dbo].[Devices] ([Id], [Uid], [Name], [Type], [HouseId], [TerminalId]) VALUES (15, N'75cdd9eb-1a4a-4f82-b099-e49899844ed3', N'Temperatur#3', 5, 6, 4)
      
      INSERT [dbo].[Devices] ([Id], [Uid], [Name], [Type], [HouseId], [TerminalId]) VALUES (16, N'3fa078d2-c42f-441f-ad43-d1f9f913cff9', N'Temperatur#4', 5, 6, 4)
      
      INSERT [dbo].[Devices] ([Id], [Uid], [Name], [Type], [HouseId], [TerminalId]) VALUES (17, N'621f0750-73c3-47b2-8bad-21b8cffb6053', N'Co2', 7, 6, 4)
      
      INSERT [dbo].[Devices] ([Id], [Uid], [Name], [Type], [HouseId], [TerminalId]) VALUES (18, N'1b77f97d-cd7c-48f9-99c2-317f55a26342', N'Humidity', 6, 6, 4)
      
      SET IDENTITY_INSERT [dbo].[Devices] OFF
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (1, 1)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (1, 2)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (2, 1)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (2, 2)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (3, 1)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (3, 2)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (4, 1)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (4, 2)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (5, 1)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (5, 2)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (6, 1)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (6, 2)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (7, 1)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (7, 2)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (8, 1)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (8, 2)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (9, 1)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (9, 2)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (10, 1)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (10, 2)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (11, 1)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (11, 2)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (12, 1)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (12, 2)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (13, 1)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (13, 2)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (14, 1)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (14, 2)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (15, 1)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (15, 2)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (16, 1)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (16, 2)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (17, 1)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (17, 2)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (18, 1)
      
      INSERT [dbo].[DeviceTimeSize] ([DeviceTimeSize_TimeSize_Id], [TimeSizes_Id]) VALUES (18, 2)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (1, 1)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (1, 2)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (2, 1)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (2, 2)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (3, 1)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (3, 2)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (4, 1)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (4, 2)   
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (6, 3)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (7, 4)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (8, 4)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (9, 4)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (10, 5)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (10, 6)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (10, 7)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (10, 8)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (10, 9)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (10, 10)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (10, 11)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (10, 12)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (10, 13)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (10, 14)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (10, 15)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (10, 16)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (10, 17)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (10, 18)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (11, 5)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (11, 6)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (11, 7)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (11, 8)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (11, 9)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (11, 10)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (11, 11)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (11, 12)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (11, 13)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (11, 14)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (11, 15)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (11, 16)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (11, 17)
      
      INSERT [dbo].[EventDevice] ([Events_Id], [Devices_Id]) VALUES (11, 18)
      ";
   }
}
