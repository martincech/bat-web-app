﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataContext.Context;
using DataContext.DTOs;
using DataContext.DTOs.Enums;
using DataContext.Generator;
using DataContext.Mapping;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using CurveType = DataModel.CurveType;
using DeviceType = DataModel.DeviceType;
using Event = DataModel.Event;
using SexMode = DataModel.SexMode;

namespace DataContext.Demo
{
   public class DemoGenerate
   {
      
      public static List<WeightStat> GenerateToInflux(BatContext bc, List<int> eventIds = null, bool tillToday = false)
      {
         var context = bc.Container;
         if (!(bc.Container.Database.GetService<IDatabaseCreator>() as RelationalDatabaseCreator).Exists())
            throw new Exception(string.Format("Database {0} do not exist.", bc.DatabaseName));     

         var events = eventIds == null
            ? context.EventsLazy.ToList()
            : context.EventsLazy.Where(w => eventIds.Contains(w.Id)).ToList();
         var evetsStats = GenerateDeviceData(events, tillToday);

         foreach (var evStats in evetsStats)
         {
            foreach (var stat in evStats.Value)
            {
               Task.Run(async () => { await bc.AddStats(stat.Key.Device.Map(), stat.Value); }).Wait();
            }

            var houseStats = evStats.Value.GroupBy(g => g.Key.Device.House);
            foreach (var houseStat in houseStats)
            {
               var stats = AggregateStats(houseStat.SelectMany(s => s.Value).ToList()).GroupBy(g => g.TimeSize);
               foreach (var stat in stats)
               {
                  Task.Run(async () => { await bc.AddStats(houseStat.Key.Map(), stat); }).Wait();
               }
            }
            
            var eventStats = AggregateStats(evStats.Value.SelectMany(s=>s.Value).ToList()).GroupBy(g=>g.TimeSize);
            foreach (var evStat in eventStats)
            {
               Task.Run(async () => { await bc.AddStats(evStats.Key.Map(), evStat); }).Wait();
            }
         }

         return evetsStats.SelectMany(s => s.Value).SelectMany(s => s.Value).ToList();
      }

      private static List<WeightStat> AggregateStats(List<WeightStat> stats)
      {
         var aggregations = new List<WeightStat>();
         foreach (var stat in stats.GroupBy(g => new { g.Type, g.TimeSize, g.TimeStamp, g.Sex }))
         {
            aggregations.Add(Aggregate(stat.ToList()));
         }
         return aggregations;
      }

      private static WeightStat Aggregate(List<WeightStat> stats)
      {
         if (stats == null || stats.Count == 0) return null;
         if (stats.Count == 1) return stats.First();
         var stat = stats.First();
         var count = stats.Sum(s => s.Count);
         return new WeightStat
         {
            Average = stats.Sum(a => a.Average * a.Count) / count,
            Count = count,
            Cv = stats.Sum(a => a.Cv * a.Count) / count,
            Day = stat.Day,
            Gain = stats.Sum(a => a.Gain * a.Count) / count,
            Max = stats.Max(m=>m.Max),
            Min = stats.Min(m=>m.Min),
            Sex = stat.Sex,
            Sigma = stats.Sum(a => a.Sigma * a.Count) / count,
            Target = stat.Target,
            Type = stat.Type,
            TimeStamp = stat.TimeStamp,
            TimeSize = stat.TimeSize,
            Uniformity = stats.Sum(a => a.Uniformity * a.Count) / count
         };
      }

      private static Dictionary<Event, Dictionary<DeviceKey, List<WeightStat>>> GenerateDeviceData(List<DataModel.Event> events,bool tillToday = false)
      {
         var eventsStats = new Dictionary<Event, Dictionary<DeviceKey, List<WeightStat>>>();
         foreach (var ev in events)
         {
            if(tillToday && ev.From > DateTime.Now) continue;
            var eventStats = GenerateDeviceData(ev, tillToday);
            if (eventStats != null) eventsStats.Add(ev, eventStats);
         }
         return eventsStats;
      }

      private static Dictionary<DeviceKey, List<WeightStat>> GenerateDeviceData(DataModel.Event ev, bool tillToday = false)
      {
         if (ev.Devices == null || ev.Devices.Count() == 0) return null;
         var eventStats = new Dictionary<DeviceKey, List<WeightStat>>();
         if (ev.Bird == null) return null;
         foreach (var curve in ev.Bird.Curves)
         {
            var sex = curve.Type.GetSexMode();
            if (curve.Type.IsWeight() && ((ev.SexMode != SexMode.Mixed && sex != ev.SexMode) || (ev.SexMode == SexMode.Mixed && curve.Type == CurveType.BirdUndefined))) continue;
            foreach (var device in ev.Devices)
            {
               if (device.Type == DeviceType.ExternalCo2 && curve.Type != CurveType.Co2) continue;
               if (device.Type == DeviceType.ExternalTemperature && curve.Type != CurveType.Temperature) continue;
               if (device.Type == DeviceType.ExternalHumidity && curve.Type != CurveType.Humidity) continue;
               if (device.Type == DeviceType.Bat2Cable && !curve.Type.IsWeight()) continue;
               if (device.Type == DeviceType.Bat2Gsm && !curve.Type.IsWeight()) continue;
               var deviceKey = eventStats.Keys.FirstOrDefault(f => f.Device == device && f.TimeSize == AggregationInterval.Day && sex == f.Sex);
               if (deviceKey == null)
               {
                  deviceKey = new DeviceKey() { Device = device, TimeSize = AggregationInterval.Day };
                  eventStats.Add(deviceKey, new List<WeightStat>());
               }
               var to = ev.To;
               if (tillToday && to > DateTime.Now)
               {
                  to = device.Id == 1 ? DateTime.Now.AddDays(-2) : DateTime.Now;
               }
               eventStats[deviceKey].AddRange(DataGenerator.CreateStats(curve, ev.From, to, AggregationInterval.Day, sex.Map()));
               if (!curve.Type.IsWeight() && ev.To > DateTime.Now)
               {
                  deviceKey = eventStats.Keys.FirstOrDefault(f => f.Device == device && f.TimeSize == AggregationInterval.HalfHour);
                  if (deviceKey == null)
                  {
                     deviceKey = new DeviceKey() { Device = device, TimeSize = AggregationInterval.HalfHour };
                     eventStats.Add(deviceKey, new List<WeightStat>());
                  }
                  eventStats[deviceKey].AddRange(DataGenerator.CreateStats(curve, ev.From, to, AggregationInterval.HalfHour, sex.Map()));
               }
            }
         }
         return eventStats;
      }

   }

   public class DeviceKey
   {
      public DataModel.Device Device { get; set; }
      public AggregationInterval TimeSize { get; set; }
      public DataModel.SexMode Sex { get; set; }
   }
}