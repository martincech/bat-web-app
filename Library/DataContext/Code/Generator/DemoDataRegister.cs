﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataContext.Context;
using DataContext.Demo;
using DataContext.Mapping;
using DataModel;
using Microsoft.EntityFrameworkCore;
using UsersDataModel;
using UsersDataModel.DTO;
using Bird = DataContext.DTOs.Bird;
using Event = DataModel.Event;
using Farm = DataContext.DTOs.Farm;
using House = DataContext.DTOs.House;
using SexMode = DataContext.DTOs.Enums.SexMode;
using Stabilization = DataContext.DTOs.Stabilization;
using StepMode = DataContext.DTOs.Enums.StepMode;
using WeighMode = DataContext.DTOs.Enums.WeighMode;

namespace DataContext.Generator
{
   public class DemoDataRegister
   {
      private readonly BatContext context;
      private readonly UserContext userContext;

      public DemoDataRegister(BatContext context, UserContext userContext)
      {

         this.context = context;
         this.userContext = userContext;
      }

      public async Task GenerateData(bool createNewMetadata = false)
      {
         if (createNewMetadata)
         {
            await CreateMetadata();
         }
         else
         {
            SqlMetadata();
            CreateDemoWorker();
         }
         var events = await context.GetAllEvents();
         DemoGenerate.GenerateToInflux(context, events.Select(s => s.Id).ToList(), true);
      }

      private void CreateDemoWorker()
      {
         var guid = Guid.NewGuid().ToString().Replace("-", "");
         var name = "DemoWorker";
         var email = name + "@" + guid + ".demo";
         userContext.CreateUser(new UserDto { Name = name, Email = email, Role = "Worker" });
      }

      private void SqlMetadata()
      {
         using (var container = new BatModelContainer(context.DatabaseName))
         {
            container.Database.ExecuteSqlCommand(Metadata.SCRIPT);
         }
      }

      private async Task CreateMetadata()
      {
         var today = DateTime.Now.Date;
         var curves = new
         {
            //Weight 
            lohman1 = await context.CreateCurve(DemoCurves.LohmannBrownClassic),
            lohman2 = await context.CreateCurve(DemoCurves.LohmannLslClassic),
            brown1 = await context.CreateCurve(DemoCurves.BrownExtra),
            brown2 = await context.CreateCurve(DemoCurves.BrownLite),
            classic1 = await context.CreateCurve(DemoCurves.ClassicFemale),
            classic2 = await context.CreateCurve(DemoCurves.ClassicMale),
            cobbFemale1 = await context.CreateCurve(DemoCurves.CobbFemaleImSeason),
            cobbFemale2 = await context.CreateCurve(DemoCurves.CobbFemaleOutSeason),
            cobbMale1 = await context.CreateCurve(DemoCurves.CobbMale),
            cobbMale2 = await context.CreateCurve(DemoCurves.CobbMaleMx),
            //Humidty
            hum1 = await context.CreateCurve(DemoCurves.HumidityClassic),
            hum2 = await context.CreateCurve(DemoCurves.HumidityExtra),
            //Temperature
            temp1 = await context.CreateCurve(DemoCurves.TemperatureExtra),
            temp2 = await context.CreateCurve(DemoCurves.TemperaturePullets),
            //Co2
            co2 = await context.CreateCurve(DemoCurves.Co2),
            co2Extra = await context.CreateCurve(DemoCurves.Co2Extra)
         };

         var birds = new
         {
            b = await context.CreateBird(new Bird
            {
               Name = "Tradition-brown-mixed",
               Description = "Tradition brown chicks. They grows cca 45 days.",
               SexMode = SexMode.MIXED,
               FemaleCurveId = curves.classic1.Id,
               MaleCurveId = curves.classic2.Id,
               TemperatureCurveId = curves.temp1.Id,
               HumidityCurveId = curves.hum1.Id,
               Co2CurveId = curves.co2.Id
            }),
            b2 = await context.CreateBird(new Bird
            {
               Name = "Lite-brown-female",
               Description = "Brown chicks lite. They grows cca 42 days for 1200g",
               SexMode = SexMode.FEMALE,
               FemaleCurveId = curves.classic1.Id,
               TemperatureCurveId = curves.temp1.Id,
               HumidityCurveId = curves.hum1.Id
               ,
               Co2CurveId = curves.co2Extra.Id
            }),
            b3 = await context.CreateBird(new Bird
            {
               Name = "Tradition-white-male",
               Description = "White chicks classic. They grows cca 52 days for 1350g",
               SexMode = SexMode.MALE,
               MaleCurveId = curves.classic2.Id,
               TemperatureCurveId = curves.temp1.Id,
               HumidityCurveId = curves.hum1.Id,
               Co2CurveId = curves.co2.Id
            }),
            b4 = await context.CreateBird(new Bird
            {
               Name = "Lite-white-undefined",
               Description = "White chicks lite.",
               SexMode = SexMode.UNDEFINED,
               UndefinedCurveId = curves.brown2.Id,
               TemperatureCurveId = curves.temp1.Id,
               HumidityCurveId = curves.hum1.Id,
               Co2CurveId = curves.co2.Id
            })
         };


         var farms = new
         {
            farm1 = await CreateFarm("Farm#1")
         };

         var houses = new
         {
            house1 = await CreateHouse("House#1", farms.farm1.Id),
            house2 = await CreateHouse("House#2", farms.farm1.Id),
            house3 = await CreateHouse("House#3", farms.farm1.Id),
         };

         var terminals = new
         {
            t = await CreateTerminal("Terminal#1"),
         };

         var devices = new
         {

            d1 = await CreateDevice("SP3(Ext)", Veit.Bat.Common.DeviceType.Bat2Gsm, houses.house1.Id, terminals.t.Id),
            d2 = await CreateDevice("Bat2-Cable", Veit.Bat.Common.DeviceType.Bat2Cable, houses.house2.Id, terminals.t.Id),
            d3 = await CreateDevice("Bat2-GSM", Veit.Bat.Common.DeviceType.Bat2Gsm, houses.house3.Id)
         };

         var events = new List<Event>
         {
            await
               CreateEvent("Brown classic #1", new List<int> {devices.d1.Id}, today.AddDays(-198),
                  today.AddDays(-50), 0, 0, birds.b),
            await
               CreateEvent("Brown classic #2", new List<int> {devices.d2.Id }, today.AddDays(-48),
                  today.AddDays(100), 0, 0, birds.b),
            await
               CreateEvent("Brown classic #3", new List<int> {devices.d3.Id }, today.AddDays(102),
                  today.AddDays(250), 0, 0, birds.b)
         };
      }

      private async Task<Farm> CreateFarm(string name)
      {
         return await context.CreateFarm(new Farm { Name = name });
      }

      private async Task<House> CreateHouse(string name, int? farmId = null)
      {
         var house = new DataModel.House { Name = name, FarmId = farmId };
         context.Container.Houses.Add(house);
         await context.Container.SaveChangesAsync();
         return house.Map();
      }

      private async Task<DTOs.Terminal> CreateTerminal(string name)
      {
         return (await context.CreateTerminalAndRegisterInUserCompany(Guid.NewGuid().ToString(), name, userContext)).Item1;
      }

      private async Task<DTOs.Device> CreateDevice(string name, Veit.Bat.Common.DeviceType type, int? houseId = null, int? terminalId = null)
      {
         return await context.CreateDevice(new DataModel.Device { Name = name, HouseId = houseId, Type = type.Map(), TerminalId = terminalId, Uid = Guid.NewGuid().ToString() });
      }

      private async Task<Event> CreateEvent(string name, ICollection<int> devices, DateTime from, DateTime to, int row, int startAge, Bird bird)
      {
         var ev = GetEventBase();
         ev.Name = name;
         ev.Devices = devices;
         ev.From = from;
         ev.To = to;
         ev.BirdId = bird.Id;
         ev.SexMode = bird.SexMode;
         ev.Row = row;
         ev.StartAge = startAge;
         return await context.CreateDbEvent(ev);
      }

      private static DTOs.Event GetEventBase()
      {
         return new DTOs.Event
         {
            Name = "Event",
            Row = 2,
            StartAge = 1,
            WeighMode = WeighMode.Curve,
            SexMode = SexMode.MALE,
            StepMode = StepMode.Enter,
            DetectionRange = 30,
            DetectionRangeFemale = 30,
            InitialWeight = 50,
            InitialWeightFemale = 50,
            Stabilization = new Stabilization
            {
               FiltrationAveragingWindow = 10,
               Range = 5,
               WindowSampleCount = 10
            },
            UniformityRange = 10
         };
      }
   }
}
