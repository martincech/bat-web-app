﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using DataContext.DTOs;
using DataContext.DTOs.Enums;
using DataContext.Extensions;
using DataContext.Mapping;
using Veit.Bat.Common;
using Curve = DataModel.Curve;
using CurvePoint = DataContext.DTOs.CurvePoint;
using CurveType = DataModel.CurveType;

namespace DataContext.Generator
{
   public class DataGenerator
   {
      private const int DEFALT_SAMPLES_RATE = 4;
      private static readonly double TwoPi = 2 * Math.PI;
      private static readonly Random RANDOM = new Random();
      private static double _z0, _z1;
      private static bool _generate;

      private static double GenerateGaussianNoise(double mu, double sigma)
      {
         _generate = !_generate;

         if (!_generate)
         {
            return _z1 * sigma + mu;
         }

         double u1, u2;
         do
         {
            u1 = RANDOM.NextDouble();
            u2 = RANDOM.NextDouble();
         } while (u1 <= double.Epsilon);

         var doubleLogU1 = Math.Sqrt(-2.0 * Math.Log(u1));

         _z0 = doubleLogU1 * Math.Cos(TwoPi * u2);
         _z1 = doubleLogU1 * Math.Sin(TwoPi * u2);
         return _z0 * sigma + mu;
      }

      public static IEnumerable<WeightSample> CreateWeightSamples(WeightStat stat)
      {
         var samples = new List<WeightSample>();
         var lastTs = stat.TimeStamp;
         var msOffset = TimeSpan.FromDays(1).Milliseconds / stat.Count;
         _z0 = _z1 = 0;
         _generate = false;

         for (var i = 0; i <= stat.Count; i++)
         {
            samples.Add(new WeightSample
            {
               Value = GenerateGaussianNoise(stat.Average.Value, stat.Sigma.Value),
               TimeStamp = lastTs,
               Sex = stat.Sex
            });
            lastTs = lastTs.AddMilliseconds(msOffset);
         }
         return samples;
      }

      public static ICollection<WeightSample> CreateWeightSamples(CurvePoint targetPoint, int count, SexMode? sex = null)
      {
         var samples = new List<WeightSample>();
         for (var i = 0; i <= count; i++)
         {
            samples.Add(
               new WeightSample
               {
                  Sex = sex.HasValue ? sex.Value : SexMode.UNDEFINED,
                  TimeStamp = DateTime.Now,
                  Value = CreateSample(
                     targetPoint.Value.Value - new Percent(targetPoint.Min.Value),
                     targetPoint.Value.Value + new Percent(targetPoint.Max.Value), SensorType.WEIGHT)
               });
         }
         return samples;
      }

      public static ICollection<Sample> CreateSamples(Curve curve, DateTime from, DateTime to, int samplesPerHour)
      {
         if (from >= DateTime.Now) return new List<Sample>();
         if (samplesPerHour > 60 || samplesPerHour < 1) samplesPerHour = DEFALT_SAMPLES_RATE;
         var tick = 60 / samplesPerHour;
         var sampleDate = new DateTime(from.Ticks);
         SexMode? sex = null;
         if (curve.Type == CurveType.BirdFemale) sex = SexMode.FEMALE;
         if (curve.Type == CurveType.BirdMale) sex = SexMode.MALE;

         var curvePoints = curve.CurvePoints.ToList();
         var dayTarget = curvePoints.GetApprox(0);
         var samples = new List<Sample>();
         while (sampleDate <= to && sampleDate < DateTime.Now)
         {
            var day = (sampleDate - from).Days;
            if (day != dayTarget.Day) dayTarget = curvePoints.GetApprox(day);
            var min = dayTarget.Min;
            var max = dayTarget.Max;
            if (min == null || max == null) continue;
            if (curve.Type == CurveType.BirdFemale || curve.Type == CurveType.BirdMale ||
                curve.Type == CurveType.BirdUndefined)
            {
               min = dayTarget.Value * ((100 - min) / 100);
               max = dayTarget.Value * ((100 + max) / 100);
            }
            Sample sample;
            if (sex.HasValue)
            {
               sample = new WeightSample { Sex = sex };
            }
            else
            {
               sample = new Sample();
            }
            sample.TimeStamp = sampleDate;
            sample.Type = SampleTypeByCurveType(curve.Type);
            sample.Value = CreateSample(min.Value, max.Value, sample.Type);

            samples.Add(sample);
            sampleDate = sampleDate.AddMinutes(tick);
         }
         return samples;
      }


      public static ICollection<WeightStat> CreateStats(Curve curve, DateTime from, DateTime to, AggregationInterval timeSize, SexMode sexMode)
      {
         var stats = new List<WeightStat>();
         if (curve == null || curve.CurvePoints == null || curve.CurvePoints.Count == 0 || from > to || (to - from) > new TimeSpan(730, 0, 0, 0)) return stats;
         var date = from.Date;
         var count = timeSize.MapToMinutes();
         var isWeight = IsWeightCurve(curve.Type);
         var sensorType = SampleTypeByCurveType(curve.Type);
         while (date.AddMinutes(timeSize.MapToMinutes()) < to)
         {
            var day = (date - from).TotalMinutes / 1440;
            var target = curve.CurvePoints.GetApprox((int)day);
            var min = target.Min;
            var max = target.Max;

            if (isWeight)
            {
               min = target.Value * ((100 - min) / 100);
               max = target.Value * ((100 + max) / 100);
               var nextCount = RANDOM.Next(25, 65);

               if (count > 2000 || nextCount % 2 == 0)
               {
                  nextCount = -nextCount;
               }

               if (count < 450)
               {
                  nextCount = Math.Abs(nextCount);
               }

               count = count + nextCount;
            }

            if (min != null && max != null)
            {
               var average = CreateSample(min.Value, max.Value, sensorType);
               var maxValue = RANDOM.Next((int)average, (int)(max.Value * 1.1));
               var minValue = RANDOM.Next((int)(min.Value * 0.9), (int)average);
               var unif = RANDOM.Next(65, 95);
               var stdv = RANDOM.Next(80, 120);
               var cv = RANDOM.Next(10, 14);
               double? gain = 0;
               if (stats.Count > 0) gain = average - stats[stats.Count - 1].Average;
               stats.Add(new WeightStat
               {
                  Average = average,
                  Count = count,
                  TimeStamp = date.Add(new TimeSpan(0, 0, timeSize.MapToMinutes() - 1, 59)),
                  Cv = cv,
                  Max = maxValue,
                  Min = minValue,
                  TimeSize = timeSize,
                  Type = sensorType,
                  Sigma = stdv,
                  Uniformity = unif,
                  Day = day,
                  Sex = sexMode,
                  Gain = gain,
               });
            }
            date = date.AddMinutes(timeSize.MapToMinutes());
         }
         return stats;
      }


      private static bool IsWeightCurve(CurveType type)
      {
         return type == CurveType.BirdFemale || type == CurveType.BirdMale || type == CurveType.BirdUndefined;
      }


      private static double CreateSample(double min, double max, SensorType type)
      {
         if (type == SensorType.WEIGHT)
         {
            return Math.Truncate((RANDOM.NextDouble() * (max - min) + min));
         }
         return Math.Truncate((RANDOM.NextDouble() * (max - min) + min) * 1000) / 1000;
      }

      private static SensorType SampleTypeByCurveType(CurveType type)
      {
         switch (type)
         {
            default:
               return SensorType.WEIGHT;
            case CurveType.Temperature:
               return SensorType.TEMPERATURE;
            case CurveType.Humidity:
               return SensorType.HUMIDITY;
            case CurveType.Co2:
               return SensorType.CO2;
            //default:
            //return SensorType.RAW_WEIGHT;
         }
      }
   }
}