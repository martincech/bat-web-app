﻿using System.Collections.Generic;
using System.Linq;
using DataModel;

namespace DataContext.Mapping
{
   public static class MapFarm
   {
      #region DTO to DataModel

      public static Farm Map(this DTOs.Farm item)
      {
         if (item == null) return null;
         return new Farm
         {
            Id = item.Id,
            Location = item.Location,
            Name = item.Name
         };
      }

      public static ICollection<Farm> Map(this ICollection<DTOs.Farm> items)
      {
         return items == null ? new List<Farm>() : items.Select(s => s.Map()).OrderBy(o => o.Name).ToList();
      }

      #endregion

      #region DataModel to DTO

      public static DTOs.Farm Map(this Farm item)
      {
         if (item == null) return null;
         return new DTOs.Farm
         {
            Id = item.Id,
            Location = item.Location,
            Name = item.Name,
            Houses = item.Houses?.Select(s => s.Id).ToList()
         };
      }

      public static ICollection<DTOs.Farm> Map(this ICollection<Farm> items)
      {
         return items == null ? null : items.Select(s => s.Map()).OrderBy(o => o.Name).ToList();
      }

      #endregion
   }
}