﻿using System;
using DataModel;

namespace DataContext.Mapping
{
   public static class MapStepMode
   {
      #region DTO to DataModel

      public static StepMode Map(this DTOs.Enums.StepMode item)
      {
         switch (item)
         {
            case DTOs.Enums.StepMode.Enter:
               return StepMode.Enter;
            case DTOs.Enums.StepMode.Leave:
               return StepMode.Leave;
            case DTOs.Enums.StepMode.Both:
               return StepMode.Both;
            default:
               throw new ArgumentOutOfRangeException();
         }
      }

      #endregion

      #region DataModel to DTO

      public static DTOs.Enums.StepMode Map(this StepMode item)
      {
         switch (item)
         {
            case StepMode.Enter:
               return DTOs.Enums.StepMode.Enter;
            case StepMode.Leave:
               return DTOs.Enums.StepMode.Leave;
            case StepMode.Both:
               return DTOs.Enums.StepMode.Both;
            default:
               throw new ArgumentOutOfRangeException();
         }
      }

      #endregion
   }
}
