﻿using System;
using DataModel;

namespace DataContext.Mapping
{
   public static class MapCurveType
   {
      #region DTO to DataModel

      public static CurveType Map(this DTOs.Enums.CurveType item)
      {
         switch (item)
         {
            case DTOs.Enums.CurveType.BirdMale:
               return CurveType.BirdMale;
            case DTOs.Enums.CurveType.BirdFemale:
               return CurveType.BirdFemale;
            case DTOs.Enums.CurveType.BirdUndefined:
               return CurveType.BirdUndefined;
            case DTOs.Enums.CurveType.Humidity:
               return CurveType.Humidity;
            case DTOs.Enums.CurveType.Temperature:
               return CurveType.Temperature;
            case DTOs.Enums.CurveType.Detection:
               return CurveType.Detection;
            case DTOs.Enums.CurveType.Co2:
               return CurveType.Co2;
            default:
               throw new ArgumentOutOfRangeException();
         }
      }

      #endregion

      #region DataModel to DTO

      public static DTOs.Enums.CurveType Map(this CurveType item)
      {
         switch (item)
         {
            case CurveType.BirdMale:
               return DTOs.Enums.CurveType.BirdMale;
            case CurveType.BirdFemale:
               return DTOs.Enums.CurveType.BirdFemale;
            case CurveType.BirdUndefined:
               return DTOs.Enums.CurveType.BirdUndefined;
            case CurveType.Humidity:
               return DTOs.Enums.CurveType.Humidity;
            case CurveType.Temperature:
               return DTOs.Enums.CurveType.Temperature;
            case CurveType.Detection:
               return DTOs.Enums.CurveType.Detection;
            case CurveType.Co2:
               return DTOs.Enums.CurveType.Co2;
            default:
               throw new ArgumentOutOfRangeException();
         }
      }

      #endregion

      public static bool IsWeight(this CurveType item)
      {
         return item == CurveType.BirdFemale || item == CurveType.BirdMale || item == CurveType.BirdUndefined;
      }

      public static bool IsWeight(this DTOs.Enums.CurveType item)
      {
         return item == DTOs.Enums.CurveType.BirdFemale || item == DTOs.Enums.CurveType.BirdMale || item == DTOs.Enums.CurveType.BirdUndefined;
      }


      public static DTOs.Enums.SexMode GetSexMode(this DTOs.Enums.CurveType item)
      {
         switch (item)
         {
            case DTOs.Enums.CurveType.BirdFemale:
               return DTOs.Enums.SexMode.FEMALE;
            case DTOs.Enums.CurveType.BirdMale:
               return DTOs.Enums.SexMode.MALE;
            case DTOs.Enums.CurveType.BirdUndefined:
               return DTOs.Enums.SexMode.UNDEFINED;
            default:
               return DTOs.Enums.SexMode.UNDEFINED;
         }
      }

      public static SexMode GetSexMode(this CurveType item)
      {
         switch (item)
         {
            case CurveType.BirdFemale:
               return SexMode.Female;
            case CurveType.BirdMale:
               return SexMode.Male;
            case CurveType.BirdUndefined:
               return SexMode.Undefined;
            default:
               return SexMode.Undefined;
         }
      }

   }
}
