using System.Collections.Generic;
using System.Linq;
using DataModel;

namespace DataContext.Mapping
{
   public static class MapHouse
   {
      #region DTO to DataModel

      public static House Map(this DTOs.House item)
      {
         if (item == null) return null;
         return new House
         {
            Id = item.Id,
            FarmId = item.FarmId,
            Location = item.Location,
            Name = item.Name
         };
      }

      public static ICollection<House> Map(this ICollection<DTOs.House> items)
      {
         return items == null ? new List<House>() : items.Select(s => s.Map()).OrderBy(o => o.Name).ToList();
      }

      #endregion

      #region DataModel to DTO

      public static DTOs.House Map(this House item)
      {
         if (item == null) return null;
         return new DTOs.House
         {
            Id = item.Id,
            FarmId = item.FarmId,
            Location = item.Location,
            Name = item.Name,
            Devices = item.Devices?.Select(s => s.Id).ToList()
         };
      }

      public static ICollection<DTOs.House> Map(this ICollection<House> items)
      {
         return items == null ? null : items.Select(s => s.Map()).OrderBy(o => o.Name).ToList();
      }

      #endregion
   }
}