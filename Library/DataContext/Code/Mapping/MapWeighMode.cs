﻿using System;
using DataModel;

namespace DataContext.Mapping
{
   public static class MapWeighMode
   {
      #region DTO to DataModel

      public static WeighMode Map(this DTOs.Enums.WeighMode item)
      {
         switch (item)
         {
            case DTOs.Enums.WeighMode.AutomaticSlow:
               return WeighMode.AutomaticSlow;
            case DTOs.Enums.WeighMode.AutomaticFast:
               return WeighMode.AutomaticFast;
            case DTOs.Enums.WeighMode.Curve:
               return WeighMode.Curve;
            default:
               throw new ArgumentOutOfRangeException();
         }
      }

      #endregion

      #region DataModel to DTO

      public static DTOs.Enums.WeighMode Map(this WeighMode item)
      {
         switch (item)
         {
            case WeighMode.AutomaticSlow:
               return DTOs.Enums.WeighMode.AutomaticSlow;
            case WeighMode.AutomaticFast:
               return DTOs.Enums.WeighMode.AutomaticFast;
            case WeighMode.Curve:
               return DTOs.Enums.WeighMode.Curve;
            default:
               throw new ArgumentOutOfRangeException();
         }
      }

      #endregion
   }
}
