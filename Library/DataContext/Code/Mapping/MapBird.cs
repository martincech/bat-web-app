﻿using System.Collections.Generic;
using System.Linq;
using DataModel;
using Curve = DataModel.Curve;

namespace DataContext.Mapping
{
   public static class MapBird
   {
      #region DTO to DataModel

      public static Bird Map(this DTOs.Bird item, IEnumerable<DTOs.Curve> curves = null)
      {
         if (item == null) return null;
         var modelCurves = new List<Curve>();
         if (curves != null)
         {
            modelCurves.AddRange(curves.Select(curve => curve.Map()));
         }
         return new Bird
         {
            Id = item.Id,
            Name = item.Name ?? "",
            Description = item.Description ?? "Description",
            SexMode = item.SexMode.Map(),
            BirdCurves = modelCurves.Select(s => new BirdCurve() { Curve = s }).ToList()
         };
      }

      public static ICollection<Bird> Map(this ICollection<DTOs.Bird> items)
      {
         return items == null ? null : items.Select(s => s.Map()).ToList();
      }

      #endregion

      #region DataModel to DTO

      public static DTOs.Bird Map(this Bird item)
      {
         if (item == null) return null;

         var conf = new DTOs.Bird
         {
            Id = item.Id,
            Name = item.Name,
            Description = item.Description,
            SexMode = item.SexMode.Map(),
            TemperatureCurveId = item.Curves?.FirstOrDefault(f => f.Type == CurveType.Temperature).MapId(),
            Co2CurveId = item.Curves?.FirstOrDefault(f => f.Type == CurveType.Co2).MapId(),
            HumidityCurveId = item.Curves?.FirstOrDefault(f => f.Type == CurveType.Humidity).MapId(),
            MaleCurveId = item.Curves?.FirstOrDefault(f => f.Type == CurveType.BirdMale).MapId(),
            FemaleCurveId = item.Curves?.FirstOrDefault(f => f.Type == CurveType.BirdFemale).MapId(),
            UndefinedCurveId = item.Curves?.FirstOrDefault(f => f.Type == CurveType.BirdUndefined).MapId()
         };
         return conf;
      }

      public static ICollection<DTOs.Bird> Map(this ICollection<Bird> items)
      {
         return items == null ? null : items.Select(s => s.Map()).ToList();
      }

      #endregion
   }
}
