﻿using Veit.Bat.Common.Units;

namespace DataContext.Mapping
{
   public class MapUnit
   {
      #region Private helpers

      public static TemperatureUnits Temperature(string str)
      {
         switch (str)
         {
            case "c":
               return TemperatureUnits.Celsius;
            case "f":
               return TemperatureUnits.Fahrenheit;
            case "k":
               return TemperatureUnits.Kelvin;
            default:
               return TemperatureUnits.Celsius;
         }
      }

      public static WeightUnits Weight(string str)
      {
         switch (str)
         {
            case "g":
               return WeightUnits.G;
            case "kg":
               return WeightUnits.KG;
            case "lb":
               return WeightUnits.LB;
            default:
               return WeightUnits.G;
         }
      }

      #endregion
   }
}
