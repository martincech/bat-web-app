﻿using System.Collections.Generic;
using System.Linq;
using DataContext.Extensions;
using DataModel;

namespace DataContext.Mapping
{
   public static class MapCurvePoint
   {
      #region DTO to DataModel

      public static CurvePoint Map(this DTOs.CurvePoint item)
      {
         if (item == null) return null;
         return new CurvePoint
         {
            Day = item.Day,
            Value = item.Value,
            Min = item.Min,
            Max = item.Max
         };
      }

      public static ICollection<CurvePoint> Map(this ICollection<DTOs.CurvePoint> items)
      {
         return items == null ? new List<CurvePoint>() : items.Select(s => s.Map()).OrderBy(o => o.Day).ToList();
      }

      #endregion

      #region DataModel to DTO

      public static DTOs.CurvePoint Map(this CurvePoint item)
      {
         if (item == null) return null;
         return new DTOs.CurvePoint
         {
            Day = item.Day,
            Value = item.Value,
            Min = item.Min,
            Max = item.Max
         };
      }

      public static ICollection<DTOs.CurvePoint> Map(this ICollection<CurvePoint> items, bool approx = true)
      {
         if (items == null) return null;
         //aproximace
         return (approx ? items.GetApprox() : items).Select(s => s.Map()).OrderBy(o => o.Day).ToList();
      }

      #endregion
   }
}
