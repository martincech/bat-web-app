﻿using System.Collections.Generic;
using System.Linq;
using DataModel;

namespace DataContext.Mapping
{
   public static class MapTerminal
   {
      #region DTO to DataModel

      public static Terminal Map(this DTOs.Terminal item)
      {
         if (item == null) return null;

         return new Terminal
         {
            Id = item.Id,
            Uid = item.Uid,
            Name = item.Name
         };
      }

      public static ICollection<Terminal> Map(this ICollection<DTOs.Terminal> items)
      {
         return items == null ? null : items.Select(s => s.Map()).ToList();
      }

      #endregion

      #region DataModel to DTO

      public static DTOs.Terminal Map(this Terminal item)
      {
         if (item == null) return null;
         return new DTOs.Terminal
         {
            Id = item.Id,
            Uid = item.Uid,
            Devices = item.Devices?.Select(s => s.Id).ToList(),
            Name = item.Name
         };
      }


      public static ICollection<DTOs.Terminal> Map(this ICollection<Terminal> items)
      {
         return items == null ? null : items.Select(s => s.Map()).ToList();
      }

      #endregion
   }
}
