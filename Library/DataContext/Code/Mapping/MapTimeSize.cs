using System.Collections.Generic;
using System.Linq;
using DataModel;

namespace DataContext.Mapping
{
   public static class MapTimeSize
   {
      #region DTO to DataModel

      public static TimeSize Map(this DTOs.TimeSize item)
      {
         if (item == null) return null;
         return new TimeSize
         {
            Id = item.Id,
            Minutes = item.Minutes
         };
      }

      public static ICollection<TimeSize> Map(this ICollection<DTOs.TimeSize> items)
      {
         return items == null ? new List<TimeSize>() : items.Select(s => s.Map()).ToList();
      }

      #endregion

      #region DataModel to DTO

      public static DTOs.TimeSize Map(this TimeSize item)
      {
         if (item == null) return null;
         return new DTOs.TimeSize
         {
            Id = item.Id,
            Minutes = item.Minutes
         };
      }

      public static ICollection<DTOs.TimeSize> Map(this ICollection<TimeSize> items)
      {
         return items == null ? null : items.Select(s => s.Map()).ToList();
      }

      #endregion
   }
}