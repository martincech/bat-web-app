﻿using System;
using DataModel;

namespace DataContext.Mapping
{
   public static class MapSexMode
   {
      #region DTO to DataModel

      public static SexMode Map(this DTOs.Enums.SexMode item)
      {
         switch (item)
         {
            case DTOs.Enums.SexMode.MALE:
               return SexMode.Male;
            case DTOs.Enums.SexMode.FEMALE:
               return SexMode.Female;
            case DTOs.Enums.SexMode.UNDEFINED:
               return SexMode.Undefined;
            case DTOs.Enums.SexMode.MIXED:
               return SexMode.Mixed;
            default:
               throw new ArgumentOutOfRangeException();
         }
      }

      #endregion

      #region DataModel to DTO

      public static DTOs.Enums.SexMode Map(this SexMode item)
      {
         switch (item)
         {
            case SexMode.Male:
               return DTOs.Enums.SexMode.MALE;
            case SexMode.Female:
               return DTOs.Enums.SexMode.FEMALE;
            case SexMode.Undefined:
               return DTOs.Enums.SexMode.UNDEFINED;
            case SexMode.Mixed:
               return DTOs.Enums.SexMode.MIXED;
            default:
               throw new ArgumentOutOfRangeException();
         }
      }

      #endregion
   }
}
