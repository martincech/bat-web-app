using System;
using DataModel;

namespace DataContext.Mapping
{
   public static class MapDeviceType
   {
      #region DTO to DataModel

      public static DeviceType Map(this Veit.Bat.Common.DeviceType item)
      {
         switch (item)
         {
            default:
               return DeviceType.None;
            case Veit.Bat.Common.DeviceType.SensorPack:
               return DeviceType.SensorPack;
            case Veit.Bat.Common.DeviceType.Bat2Gsm:
               return DeviceType.Bat2Gsm;
            case Veit.Bat.Common.DeviceType.ExternalSensorPack:
               return DeviceType.ExternalSensorPack;
            case Veit.Bat.Common.DeviceType.Bat2Cable:
               return DeviceType.Bat2Cable;
            case Veit.Bat.Common.DeviceType.ExternalTemperature:
               return DeviceType.ExternalTemperature;
            case Veit.Bat.Common.DeviceType.ExternalHumidity:
               return DeviceType.ExternalHumidity;
            case Veit.Bat.Common.DeviceType.ExternalCo2:
               return DeviceType.ExternalCo2;
            case Veit.Bat.Common.DeviceType.ExternalWeight:
               return DeviceType.ExternalWeight;
            case Veit.Bat.Common.DeviceType.Bat3:
               return DeviceType.Bat3;
         }
      }

      public static bool HasWeight(this Veit.Bat.Common.DeviceType item)
      {
         return item == Veit.Bat.Common.DeviceType.Bat2Cable
               || item == Veit.Bat.Common.DeviceType.Bat2Gsm
               || item == Veit.Bat.Common.DeviceType.ExternalWeight
               || item == Veit.Bat.Common.DeviceType.ExternalSensorPack
               || item == Veit.Bat.Common.DeviceType.SensorPack
               || item == Veit.Bat.Common.DeviceType.Bat3
               || item == Veit.Bat.Common.DeviceType.None;
      }

      public static bool HasOtherSensors(this Veit.Bat.Common.DeviceType item)
      {
         return item == Veit.Bat.Common.DeviceType.ExternalTemperature
               || item == Veit.Bat.Common.DeviceType.ExternalCo2
               || item == Veit.Bat.Common.DeviceType.ExternalHumidity
               || item == Veit.Bat.Common.DeviceType.ExternalSensorPack
               || item == Veit.Bat.Common.DeviceType.SensorPack
               || item == Veit.Bat.Common.DeviceType.None;
      }

      #endregion

      #region DataModel to DTO

      public static Veit.Bat.Common.DeviceType Map(this DeviceType item)
      {
         switch (item)
         {
            default:
               return Veit.Bat.Common.DeviceType.None;
            case DeviceType.SensorPack:
               return Veit.Bat.Common.DeviceType.SensorPack;
            case DeviceType.Bat2Gsm:
               return Veit.Bat.Common.DeviceType.Bat2Gsm;
            case DeviceType.ExternalSensorPack:
               return Veit.Bat.Common.DeviceType.ExternalSensorPack;
            case DeviceType.Bat2Cable:
               return Veit.Bat.Common.DeviceType.Bat2Cable;
            case DeviceType.ExternalTemperature:
               return Veit.Bat.Common.DeviceType.ExternalTemperature;
            case DeviceType.ExternalHumidity:
               return Veit.Bat.Common.DeviceType.ExternalHumidity;
            case DeviceType.ExternalCo2:
               return Veit.Bat.Common.DeviceType.ExternalCo2;
            case DeviceType.ExternalWeight:
               return Veit.Bat.Common.DeviceType.ExternalWeight;
            case DeviceType.Bat3:
               return Veit.Bat.Common.DeviceType.Bat3;
         }
      }

      #endregion
   }
}