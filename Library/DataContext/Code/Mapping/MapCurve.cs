﻿using System.Collections.Generic;
using System.Linq;
using DataModel;

namespace DataContext.Mapping
{
   public static class MapCurve
   {

      private static readonly Dictionary<DTOs.Enums.CurveType, CurveType> CURVE_TYPES = new Dictionary<DTOs.Enums.CurveType, CurveType>()
      {
         {DTOs.Enums.CurveType.Temperature,CurveType.Temperature},
         {DTOs.Enums.CurveType.Humidity,CurveType.Humidity},
         {DTOs.Enums.CurveType.BirdFemale,CurveType.BirdFemale},
         {DTOs.Enums.CurveType.BirdMale,CurveType.BirdMale},
         {DTOs.Enums.CurveType.BirdUndefined,CurveType.BirdUndefined},
         {DTOs.Enums.CurveType.Detection,CurveType.Detection},
         {DTOs.Enums.CurveType.Co2,CurveType.Co2},
      };

      #region DTO to DataModel

      public static Curve Map(this DTOs.Curve item, bool approx = true)
      {
         if (item == null) return null;
         return new Curve
         {
            Id = item.Id,
            Name = item.Name,
            Type = CURVE_TYPES[item.Type],
            CurvePoints = item.CurvePoints == null ? new List<CurvePoint>() : item.CurvePoints.Map()
         };
      }

      public static ICollection<Curve> Map(this ICollection<DTOs.Curve> items)
      {
         return items == null ? null : items.Select(s => s.Map()).ToList();
      }

      #endregion

      #region DataModel to DTO

      public static int? MapId(this Curve item)
      {
         if (item == null) return null;
         return item.Id;
      }

      public static DTOs.Curve Map(this Curve item, bool approx = true)
      {
         if (item == null) return null;
         return new DTOs.Curve
         {
            Id = item.Id,
            Name = item.Name,
            Type = CURVE_TYPES.FirstOrDefault(f => f.Value == item.Type).Key,
            CurvePoints = item.CurvePoints == null ? new List<DTOs.CurvePoint>() : item.CurvePoints.Map(approx)
         };
      }

      public static ICollection<DTOs.Curve> Map(this ICollection<Curve> items, bool approx = true)
      {
         return items == null ? null : items.Select(s => s.Map(approx)).ToList();
      }

      #endregion
   }
}
