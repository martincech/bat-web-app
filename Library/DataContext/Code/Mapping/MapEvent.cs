﻿using System.Collections.Generic;
using System.Linq;
using DataModel;

namespace DataContext.Mapping
{
   public static class MapEvent
   {
      #region DTO to DataModel

      public static Event Map(this DTOs.Event @event, IEnumerable<DTOs.Curve> curves = null)
      {
         if (@event == null) return null;
         var result = new Event
         {
            Id = @event.Id,
            BirdId = @event.BirdId,
            CurveId = @event.CurveId,
            CurveFemaleId = @event.CurveFemaleId,
            Name = @event.Name,
            From = @event.From,
            To = @event.To,
            StepMode = @event.StepMode.Map(),
            SexMode = @event.SexMode.Map(),
            StartAge = @event.StartAge,
            RowNumber = @event.Row,
            UniformityRange = @event.UniformityRange,
            State = @event.State.Map(),
            WeighMode = @event.WeighMode.Map(),
            InitialWeight = @event.InitialWeight,
            InitialWeightFemale = @event.InitialWeightFemale,
            DetectionRange = @event.DetectionRange,
            DetectionRangeFemale = @event.DetectionRangeFemale
            //Configuration = @event.Configuration.Map(curves),
         };
         return result;
      }

      public static ICollection<Event> Map(this ICollection<DTOs.Event> items)
      {
         return items == null ? null : items.Select(s => s.Map()).ToList();
      }

      #endregion

      #region DataModel to DTO

      public static DTOs.Event Map(this Event @event)
      {
         if (@event == null) return null;
         var result = new DTOs.Event
         {
            Id = @event.Id,
            CurveId = @event.CurveId,
            CurveFemaleId = @event.CurveFemaleId,
            BirdId = @event.BirdId,
            Name = @event.Name,
            From = @event.From,
            To = @event.To,
            StepMode = @event.StepMode.Map(),
            SexMode = @event.SexMode.Map(),
            StartAge = @event.StartAge,
            Row = @event.RowNumber,
            UniformityRange = @event.UniformityRange,
            State = @event.State.Map(@event.From, @event.To),
             WeighMode = @event.WeighMode.Map(),
            InitialWeight = @event.InitialWeight,
            InitialWeightFemale = @event.InitialWeightFemale,
            DetectionRange = @event.DetectionRange,
            DetectionRangeFemale = @event.DetectionRangeFemale,
            Bird = @event.Bird.Map(),
            Devices = @event.Devices.Select(s => s.Id).ToList()
         };
         return result;
      }

      public static ICollection<DTOs.Event> Map(this ICollection<Event> items)
      {
         return items == null ? null : items.Select(s => s.Map()).ToList();
      }

      #endregion
   }
}
