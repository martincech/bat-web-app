﻿using System;
using DataModel;
using Event = DataContext.DTOs.Event;

namespace DataContext.Mapping
{
   public static class MapEventState
   {
      #region DTO to DataModel

      public static EventState Map(this DTOs.Enums.EventState state)
      {
         return state == DTOs.Enums.EventState.Paused ? EventState.Pause : EventState.Unpause;
      }

      #endregion

      #region DataModel to DTO

      public static DTOs.Enums.EventState Map(this EventState state, DateTime from, DateTime to)
      {
         var checkedState = Event.CheckState(@from, to);
         if (state == EventState.Pause && checkedState == DTOs.Enums.EventState.Active)
         {
            return DTOs.Enums.EventState.Paused;
         }
         return checkedState;
      }

      #endregion
   }
}
