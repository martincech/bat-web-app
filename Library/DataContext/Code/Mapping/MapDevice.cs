﻿using System.Collections.Generic;
using System.Linq;
using DataModel;

namespace DataContext.Mapping
{
   public static class MapDevice
   {
      #region DTO to DataModel

      public static Device Map(this DTOs.Device item)
      {
         return new Device
         {
            Id = item.Id,
            Uid = item.Uid,
            Name = item.Name,
            HouseId = item.HouseId,
            TerminalId = item.TerminalId,
            Type = item.Type.Map()
         };
      }

      public static ICollection<Device> Map(this ICollection<DTOs.Device> items)
      {
         return items == null ? null : items.Select(s => s.Map()).ToList();
      }

      #endregion

      #region DataModel to DTO

      public static DTOs.Device Map(this Device item)
      {
         if (item == null) return null;
       
         return new DTOs.Device
         {
            Id = item.Id,
            Uid = item.Uid,
            Name = item.Name,
            HouseId = item.HouseId,
            TerminalId = item.TerminalId,
            Type = item.Type.Map()
         };
      }

      public static ICollection<DTOs.Device> Map(this ICollection<Device> items)
      {
         return items == null ? null : items.Select(s => s.Map()).ToList();
      }
      #endregion
   }
}
