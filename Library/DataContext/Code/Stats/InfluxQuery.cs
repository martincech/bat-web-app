using System;
using System.Collections.Generic;
using System.Linq;

namespace DataContext.Stats
{
   public class InfluxQuery
   {
      private static string[] Select
      {
         get
         {
            return new[]
            {
               "SELECT",
               "count,",
               "sex,",
               "day,",
               AColumn("average"),
               AColumn("cv"),
               AColumn("gain"),
               AColumn("sigma"),
               AColumn("uniformity", false)
            };
         }
      }

      public static string GetStatsQuery(string db, int timeSize, IEnumerable<string> deviceUids, DateTime[] dates)
      {
         if (dates == null || dates.Count() == 0) return null;
         var q = Select.ToList();
         q.Add(From(deviceUids, timeSize, db));
         q.Add(dates.Count() == 1 ? WhereDateIs(dates[0]) : WhereDateIsBetween(dates[0], dates[1]));
         q.Add("GROUP BY sex");
         return string.Join(" ", q);
      }

      private static string From(IEnumerable<string> deviceUids, int size, string db)
      {
         return "FROM " + ADevices(deviceUids, size, db);
      }

      private static string WhereDateIs(DateTime date)
      {
         return string.Format(@"WHERE time='{0}Z'", date.ToString("yyyy-MM-ddTHH:mm:ss"));
      }

      private static string WhereDateIsBetween(DateTime from, DateTime to)
      {
         return string.Format(@"WHERE time > '{0}Z' AND time < '{1}Z'", from.ToString("yyyy-MM-ddTHH:mm:ss"), to.ToString("yyyy-MM-ddTHH:mm:ss"));
      }

      private static string ADevices(IEnumerable<string> deviceIds, int size, string db)
      {
         return string.Join(", ", deviceIds.Select(s => string.Format("\"{0}\".\"{1}_m\".\"device_{2}_weight_{1}m\"", db, size, s)));
      }

      private static string AColumn(string columnt, bool delimiter = true)
      {
         return string.Format("{0}*count as {0}{1}", columnt, delimiter ? "," : "");
      }
   }
}