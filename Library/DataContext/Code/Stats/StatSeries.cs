﻿using System;
using System.Collections.Generic;
using System.Linq;
using InfluxData.Net.InfluxDb.Models;
using InfluxData.Net.InfluxDb.Models.Responses;

namespace DataContext.Stats
{
   public class StatSeries
   {
      private readonly List<string> columns;
      private readonly List<object> values;

      public DateTime? Time;
      public double? Average;
      public int? Count;
      public string Sex;
      public double? Day;
      public double? Gain;
      public double? Cv;
      public double? Sigma;
      public double? Uniformity;

      public const string F_TIME = "time";
      public const string F_DAY = "day";
      public const string F_COUNT = "count";
      public const string F_AVERAGE = "average";
      public const string F_CV = "cv";
      public const string F_GAIN = "gain";
      public const string F_SIGMA = "sigma";
      public const string F_UNIFORMITY = "uniformity";
      public const string T_SEX = "sex";

      //Stats from sensors must have day set to range from expexted day +- 30 days. If expexted day is 45 then stats with day less than 15 and greater than 75 are ignored.
      //This will prevent integrating statistics from incorretly started scales. 
      public const int STAT_DAY_RANGE = 30;

      public StatSeries(List<string> columns, List<object> values)
      {
         this.columns = columns ?? new List<string>();
         this.values = values ?? new List<object>();
         Process();
      }

      private void Process()
      {
         Set(out Time, F_TIME);
         Set(out Count, F_COUNT);
         Set(out Sex, T_SEX);
         Set(out Average, F_AVERAGE);
         Set(out Cv, F_CV);
         Set(out Day, F_DAY);
         Set(out Gain, F_GAIN);
         Set(out Sigma, F_SIGMA);
         Set(out Uniformity, F_UNIFORMITY);
         if (Sex == null) Sex = "undefined";
      }

      private void Set<T>(out T value, string column)
      {
         var index = columns.IndexOf(column);
         if (index == -1 || values.Count <= index)
         {
            value = default(T);
            return;
         }
         Convert(out value, values[index]);
      }

      private void Convert<T>(out T value, object o)
      {
         if (o == null)
         {
            value = default(T);
            return;
         }
         try
         {
            var type = Nullable.GetUnderlyingType(typeof(T)) ?? typeof(T);
            value = (T)System.Convert.ChangeType(o, type);
         }
         catch (Exception)
         {
            value = default(T);
         }
      }

      public static List<StatSeries> Parse(Serie serie)
      {
         return serie.Values.Select(s => new StatSeries(serie.Columns.ToList(), s.ToList())).ToList();
      }

      public static List<StatSeries> Parse(List<Serie> series)
      {
         return series.SelectMany(Parse).ToList();
      }

      public static IEnumerable<Point> GroupAndAggregate(List<StatSeries> series, DateTime date, int expectedDay, string name = null)
      {
         var netSeries = new List<StatSeries>();
         foreach (var serie in series)
         {
            if (!serie.Time.HasValue || !serie.Count.HasValue) continue;
            var day = (serie.Time.Value - date).TotalDays + expectedDay;
            if (serie.Day.HasValue)
            {
               if (serie.Day.Value < day - STAT_DAY_RANGE || serie.Day.Value > day + STAT_DAY_RANGE) continue;
            }
            else
            {
               serie.Day = day;
            }
            netSeries.Add(serie);
         }

         var groups = netSeries.GroupBy(g => new { g.Sex, g.Time }).ToDictionary(k => k.Key, k => k.ToList());
         return groups.Select(s => Agregate(s.Value, date, expectedDay, name)).Where(w => w != null);
      }

      public static Point Agregate(List<StatSeries> series, DateTime date, int expectedDay, string name = null)
      {
         if (series == null || !series.Any()) return null;
         var count = series.Sum(s => s.Count);
         if (count == null || count == 0) return null;

         return new Point
         {
            Name = name,
            Timestamp = series.First().Time,
            Fields = new Dictionary<string, object>
            {
               {F_TIME,series.First().Time},
               {F_AVERAGE,series.Sum(s => s.Average) / count},
               {F_COUNT,count},
               {F_DAY,series.First().Day },
               {F_CV,series.Sum(s => s.Cv) / count},
               {F_GAIN,series.Sum(s => s.Gain) / count},
               {F_SIGMA,series.Sum(s => s.Sigma) / count},
               {F_UNIFORMITY,series.Sum(s => s.Uniformity) / count}
            },
            Tags = new Dictionary<string, object>
            {
                  {T_SEX,series.First().Sex}
            }
         };
      }
   }
}
