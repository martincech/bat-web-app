﻿using System.Collections.Generic;
using DataContext.DTOs.Enums;

namespace DataContext.DTOs
{
   public class Curve
   {
      public int Id { get; set; }
      public string Name { get; set; }
      public CurveType Type { get; set; }
      public ICollection<CurvePoint> CurvePoints { get; set; }
   }
}
