﻿using System.Collections.Generic;
using Veit.Bat.Common;

namespace DataContext.DTOs
{
   public class TerminalInfo
   {
      public TerminalInfo()
      {
         Devices = new List<DeviceInfo>();
      }

      public string TerminalSn { get; set; }
      public string Name { get; set; }
      public bool IsActive { get; set; }
      public List<DeviceInfo> Devices { get; set; } 
   }

   public class DeviceInfo
   {
      public string Uid { get; set; }
      public string Name { get; set; }
      public DeviceType Type { get; set; }
   }
}
