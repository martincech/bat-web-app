﻿using System;
using DataContext.DTOs.Enums;

namespace DataContext.DTOs
{
   public class Stat
   {
      public DateTime TimeStamp { get; set; }
      public AggregationInterval TimeSize { get; set; }
      public double? Average { get; set; }
      public double? Min { get; set; }
      public double? Max { get; set; }
      public int Count { get; set; }
      public double? Cv { get; set; }
      public double? Sigma { get; set; }
      public double? Uniformity { get; set; }
      public SensorType Type { get; set; }
   }
}
