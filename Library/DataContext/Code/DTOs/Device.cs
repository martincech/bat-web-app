﻿using System.Collections.Generic;
using Veit.Bat.Common;

namespace DataContext.DTOs
{
   public class Device
   {
      public Device()
      {
         TimeSizes = new List<int>();
      }

      public int Id { get; set; }
      public string Uid { get; set; }
      public string Name { get; set; }
      public DeviceType Type { get; set; }
      public int? HouseId { get; set; }
      public int? TerminalId { get; set; }

      public ICollection<int> TimeSizes { get; set; } 
   }
}
