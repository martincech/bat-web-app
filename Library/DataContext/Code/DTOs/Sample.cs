﻿using System;
using DataContext.DTOs.Enums;

namespace DataContext.DTOs
{
   public class Sample
   {
      public DateTime TimeStamp { get; set; }
      public double Value { get; set; }
      public SensorType Type { get; set; }
   }
}
