﻿using System.Collections.Generic;

namespace DataContext.DTOs
{
   public class GroupData : GroupBase
   {
      public GroupData()
      {
         Values = new List<List<List<object>>>();
      }
      public string GroupId { get; set; }
      public int TimeSize { get; set; }
      public List<List<List<object>>> Values { get; set; }
   }
}