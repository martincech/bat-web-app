﻿using DataContext.DTOs.Enums;

namespace DataContext.DTOs
{
   public class WeightStat : Stat
   {
      public WeightStat()
      {
         Type = SensorType.WEIGHT;
      }

      public double? Day { get; set; }
      public double? Target { get; set; }
      public double? Gain { get; set; }
      public SexMode Sex { get; set; }
   }
}
