﻿using DataContext.DTOs.Enums;

namespace DataContext.DTOs
{
   public class WeightSample : Sample
   {
      public WeightSample()
      {
         Type = SensorType.WEIGHT;
      }

      public SexMode? Sex { get; set; }
   }
}