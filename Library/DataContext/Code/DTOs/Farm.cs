﻿using System.Collections.Generic;

namespace DataContext.DTOs
{
   public class Farm : LocationBase
   {
      public ICollection<int> Houses { get; set; } 
   }
}
