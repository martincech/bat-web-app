﻿using DataContext.DTOs.Enums;

namespace DataContext.DTOs
{
   public class Bird
   {
      public int Id { get; set; }
      public string Name { get; set; }
      public string Description { get; set; }
      public SexMode SexMode { get; set; }

      public int? TemperatureCurveId { get; set; }
      public int? HumidityCurveId { get; set; }
      public int? Co2CurveId { get; set; }

      public int? MaleCurveId { get; set; }
      public int? FemaleCurveId { get; set; }
      public int? UndefinedCurveId { get; set; }
   }
}
