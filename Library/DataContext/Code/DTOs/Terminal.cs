﻿using System.Collections.Generic;

namespace DataContext.DTOs
{
   public class Terminal
   {
      public Terminal()
      {
         Devices = new List<int>();
      }

      public int Id { get; set; }
      public string Uid { get; set; }
      public string Name { get; set; }
      public ICollection<int> Devices { get; set; }
   }
}
