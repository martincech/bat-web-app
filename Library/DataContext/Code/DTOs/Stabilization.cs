﻿namespace DataContext.DTOs
{
   public class Stabilization
   {
      public double Range { get; set; }
      public int WindowSampleCount { get; set; }
      public int FiltrationAveragingWindow { get; set; }
   }
}
