namespace DataContext.DTOs
{
   public class TimeSize
   {
      public int Id { get; set; }
      public int Minutes { get; set; }
   }
}