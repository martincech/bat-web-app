using System.Collections.Generic;

namespace DataContext.DTOs
{
   public class GroupFilter : GroupBase
   {
      public List<string> GroupIds { get; set; }

      public List<int> TimeSizes { get; set; }
   }
}