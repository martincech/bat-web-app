﻿namespace DataContext.DTOs
{
   public class DeviceData<T>
   {
      public long TerminalSn { get; set; }
      public long SensorPackSn { get; set; }
      public T Data { get; set; }
   }
}
