﻿using System.Collections.Generic;

namespace DataContext.DTOs
{
   public class House : LocationBase
   {
      public int? FarmId { get; set; }
      public ICollection<int> Devices { get; set; } 
   }
}