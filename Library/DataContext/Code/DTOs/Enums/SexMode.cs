﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DataContext.DTOs.Enums
{
   public enum SexMode : byte
   {
      UNDEFINED,
      MALE,
      FEMALE,
      MIXED
   }

   public static class SexModeExtensions
   {
      public static SexMode? MapToSexMode(this string name)
      {
         var upperName = name.ToUpper();
         var sexModes = SexModes();
         foreach (var sexMode in sexModes)
         {
            if (sexMode.ToUpperName() == upperName)
            {
               return sexMode;
            }
         }
         return null;
      }
      public static IEnumerable<SexMode> SexModes()
      {
         return Enum.GetValues(typeof(SexMode)).Cast<SexMode>();
      }
      public static IEnumerable<string> SensorLowerNames()
      {
         return SexModes().Select(s => s.ToLowerName());
      }
      public static IEnumerable<string> SensorUpperNames()
      {
         return SexModes().Select(s => s.ToUpperName());
      }
      public static IEnumerable<SexMode?> MapToSexMode(this IEnumerable<string> names)
      {
         return names.Select(name => name.MapToSexMode()).ToList();
      }

      public static string ToLowerName(this SexMode type)
      {
         return type.ToString().ToLower();
      }
      public static string ToUpperName(this SexMode type)
      {
         return type.ToString().ToUpper();
      }

      public static string ToLowerName(this SexMode? type)
      {
         if (type.HasValue)
         {
            return type.Value.ToLowerName();
         }
         return "";
      }
      public static string ToUpperName(this SexMode? type)
      {
         if (type.HasValue)
         {
            return type.Value.ToUpperName();
         }
         return "";
      }
   }
}
