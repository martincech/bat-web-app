﻿namespace DataContext.DTOs.Enums
{
   public enum CurveType
   {
      BirdFemale,
      BirdMale,
      BirdUndefined,
      Humidity,
      Temperature,
      Detection,
      Co2
   }
}
