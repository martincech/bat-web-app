﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DataContext.DTOs.Enums
{
   public enum SensorType : byte
   {
      TEMPERATURE=0,
      HUMIDITY=1,
      CO2=2,
      //AMMONIA=3,
      //RAW_WEIGHT=4,
      WEIGHT=5
   }

   public static class SensorTypeExtension
   {
      public static SensorType? MapToSensorType(this string name)
      {
         var upperName = name.ToUpper();
         var sensorTypes = SensorTypes();
         foreach (var sensorType in sensorTypes)
         {
            if (sensorType.ToUpperName() == upperName)
            {
               return sensorType;
            }
         }
         return null;
      }

      public static IEnumerable<SensorType> SensorTypesExceptWeight()
      {
         return Enum.GetValues(typeof(SensorType)).Cast<SensorType>().Except(new[] { SensorType.WEIGHT });
      }
      public static IEnumerable<string> SensorLowerNamesExceptWeight()
      {
         return SensorTypesExceptWeight().Select(s => s.ToLowerName());
      }
      public static IEnumerable<string> SensorUpperNamesExceptWeight()
      {
         return SensorTypesExceptWeight().Select(s => s.ToUpperName());
      }
      
      public static IEnumerable<SensorType> SensorTypes()
      {
         return Enum.GetValues(typeof (SensorType)).Cast<SensorType>();
      }
      public static IEnumerable<string> SensorLowerNames()
      {
         return SensorTypes().Select(s => s.ToLowerName());
      }
      public static IEnumerable<string> SensorUpperNames()
      {
         return SensorTypes().Select(s => s.ToUpperName());
      }

      public static IEnumerable<SensorType?> MapToSensorType(this IEnumerable<string> names)
      {
         return names.Select(name => name.MapToSensorType()).ToList();
      }

      public static string ToLowerName(this SensorType type)
      {
         return type.ToString().ToLower();
      }
      public static string ToUpperName(this SensorType type)
      {
         return type.ToString().ToUpper();
      }

      public static bool IsWeightSensorString(this string sensorname)
      {
         return sensorname.ToLower().StartsWith(SensorType.WEIGHT.ToLowerName());
      }

      public static string ConcatWeightWithSex(this string sensorName, SexMode sex)
      {
         return string.Format("{0}_{1}", sensorName, sex.ToLowerName());
      }
      public static string ConcatWeightWithSex(this string sensorName, string sex)
      {
         return string.Format("{0}_{1}", sensorName, sex.ToLower());
      }

      public static SexMode? GetSexFromSensorName(this string sensorName)
      {
         var names = sensorName.Split('_');
         if (names.Length == 2)
         {
            return names[1].MapToSexMode();
         }
         return null;
      }
   }
}
