﻿namespace DataContext.DTOs.Enums
{
   public enum EventState : byte
   {
      Active,
      Paused,
      Stopped,
      Closed
   }
}
