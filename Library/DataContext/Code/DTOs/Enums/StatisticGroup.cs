﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DataContext.DTOs.Enums
{
   public enum StatisticGroup
   {
      DEVICE,
      EVENT,
      HOUSE
   }

   public static class StatisticGroupExtensions
   {
      public static IEnumerable<StatisticGroup?> MapToStatisticGroups(this IEnumerable<string> statisticGroupName)
      {
         return statisticGroupName.Select(MapToStatisticGroup).ToList();
      }

      public static StatisticGroup? MapToStatisticGroup(this string statisticGroupName)
      {
         var types = Enum.GetValues(typeof(StatisticGroup)).Cast<StatisticGroup>();
         var upperName = statisticGroupName.ToUpper();
         foreach (var type in types)
         {
            if (type.ToUpperName() == upperName)
            {
               return type;
            }
         }
         return null;
      }

      public static IEnumerable<StatisticGroup> StatisticGroups()
      {
         return Enum.GetValues(typeof(StatisticGroup)).Cast<StatisticGroup>();
      }
      public static IEnumerable<string> StatisticGroupLowerNames()
      {
         return StatisticGroups().Select(s => s.ToLowerName());
      }
      public static IEnumerable<string> StatisticGroupUpperNames()
      {
         return StatisticGroups().Select(s => s.ToUpperName());
      }

      public static string ToLowerName(this StatisticGroup type)
      {
         return type.ToString().ToLower();
      }
      public static string ToUpperName(this StatisticGroup type)
      {
         return type.ToString().ToUpper();
      }
   }
}