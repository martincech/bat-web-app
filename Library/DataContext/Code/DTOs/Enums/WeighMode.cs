﻿namespace DataContext.DTOs.Enums
{
   public enum WeighMode : byte
   {
      AutomaticSlow = 0,
      AutomaticFast = 1,
      Curve = 2
   }
}
