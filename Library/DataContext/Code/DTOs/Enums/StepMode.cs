﻿namespace DataContext.DTOs.Enums
{
   public enum StepMode : byte
   {
      Enter,
      Leave,
      Both
   }
}
