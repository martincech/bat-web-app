﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DataContext.DTOs.Enums
{
   public enum AggregationType
   {
      AVERAGE,
      MIN,
      MAX,
      COUNT,
      SIGMA,
      CV,
      UNIFORMITY,
      GAIN,
      DAY,
      TIMESTAMP,
      TARGET
   }

   public static class AggregationTypeExtensions
   {
      public static IEnumerable<AggregationType?> MapToAggregationTypes(this IEnumerable<string> aggregationName)
      {
         return aggregationName.Select(MapToAggregationType).ToList();
      }

      public static AggregationType? MapToAggregationType(this string aggregationName)
      {
         var types = Enum.GetValues(typeof(AggregationType)).Cast<AggregationType>();
         var upperName = aggregationName.ToUpper();
         foreach (var type in types)
         {
            if (type == AggregationType.TIMESTAMP && upperName == AggregationType.TIMESTAMP.ToString().ToUpper())
            {
               return type;
            }
            if (type.ToUpperName() == upperName)
            {
               return type;
            }
         }
         return null;
      }

      public static IEnumerable<AggregationType> AggregationTypes()
      {
         return Enum.GetValues(typeof(AggregationType)).Cast<AggregationType>();
      }
      public static IEnumerable<string> AggregationLowerNames()
      {
         return AggregationTypes().Select(s => s.ToLowerName());
      }
      public static IEnumerable<string> AggregationUpperNames()
      {
         return AggregationTypes().Select(s => s.ToUpperName());
      }

      public static string ToLowerName(this AggregationType type)
      {
         if (type == AggregationType.TIMESTAMP)
         {
            return "time";
         }
         return type.ToString().ToLower();
      }
      public static string ToUpperName(this AggregationType type)
      {
         if (type == AggregationType.TIMESTAMP)
         {
            return "TIME";
         }
         return type.ToString().ToUpper();
      }
   }
}