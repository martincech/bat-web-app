﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DataContext.DTOs.Enums
{
   public enum AggregationInterval
   {
      HalfHour,
      Hour,
      Day,
      Week
   }
   public static class AggregationIntervalExtensions
   {

      public static TimeSpan MapToTimeSpan(this AggregationInterval item)
      {
         switch (item)
         {
            case AggregationInterval.Week:
               return new TimeSpan(7, 0, 0, 0);
            case AggregationInterval.Day:
               return new TimeSpan(1, 0, 0, 0);
            case AggregationInterval.Hour:
               return new TimeSpan(1, 0, 0);
            case AggregationInterval.HalfHour:
               return new TimeSpan(0, 30, 0);
            default:
               return new TimeSpan(0);
         }
      }

      public static int MapToMinutes(this AggregationInterval item)
      {
         return (int)item.MapToTimeSpan().TotalMinutes;
      }

      public static TimeSize MapToTimeSize(this AggregationInterval item)
      {
         return new TimeSize { Minutes = item.MapToMinutes() };
      }
      public static TimeSize MapToTimeSize(this int minutes)
      {
         return new TimeSize { Minutes = minutes };
      }

      public static AggregationInterval? MapToAggregationInterval(this int minutes)
      {
         var sampleSizes = Enum.GetValues(typeof(AggregationInterval)).Cast<AggregationInterval>();
         foreach (var sampleSize in sampleSizes)
         {
            if (sampleSize.MapToMinutes() == minutes)
            {
               return sampleSize;
            }
         }
         return null;
      }
      public static IEnumerable<AggregationInterval> AggregationIntervals()
      {
         return Enum.GetValues(typeof(AggregationInterval)).Cast<AggregationInterval>();
      }
      public static IEnumerable<TimeSize> AggregationIntervalsTimeSizes()
      {
         return AggregationIntervals().Select(MapToTimeSize);
      }
      public static IEnumerable<int> AggregationIntervalsMinutes()
      {
         return AggregationIntervals().Select(MapToMinutes);
      }
      public static IEnumerable<TimeSpan> AggregationIntervalsTimeSpans()
      {
         return AggregationIntervals().Select(MapToTimeSpan);
      }
   }
}