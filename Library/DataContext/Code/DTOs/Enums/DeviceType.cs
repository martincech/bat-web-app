﻿using System.Collections.Generic;
using Veit.Bat.Common;

namespace DataContext.DTOs.Enums
{
   public static class DeviceTypeExtension
   {
      public static IEnumerable<SensorType> MapToSensorType(this DeviceType deviceType)
      {
         var ret = new List<SensorType>();
         switch (deviceType)
         {
            case DeviceType.SensorPack:
            case DeviceType.ExternalSensorPack:
               ret.Add(SensorType.CO2);
               ret.Add(SensorType.HUMIDITY);
               ret.Add(SensorType.TEMPERATURE);
               break;
            case DeviceType.Bat2Gsm:
            case DeviceType.Bat3:
            case DeviceType.Bat2Cable:
            case DeviceType.ExternalWeight:
               ret.Add(SensorType.WEIGHT);
               break;
            case DeviceType.ExternalTemperature:
               ret.Add(SensorType.TEMPERATURE);
               break;
            case DeviceType.ExternalHumidity:
               ret.Add(SensorType.HUMIDITY);
               break;
            case DeviceType.ExternalCo2:
               ret.Add(SensorType.CO2);
               break;
         }
         return ret;
      }
   }
}
