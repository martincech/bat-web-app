﻿namespace DataContext.DTOs.Enums
{
   public enum CalibrationState : byte
   {
      Stop,
      Start,
      FirstStableSample,
      Calibrated
   }
}
