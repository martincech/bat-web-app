﻿namespace DataContext.DTOs
{
   public class CurvePoint
   {
      public int Day { get; set; }
      public double? Value { get; set; }
      public double? Min { get; set; }
      public double? Max { get; set; }
   }
}
