﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataContext.DTOs.Enums;
using DataContext.Mapping;

namespace DataContext.DTOs
{
   public class Event
   {
      #region Private fields

      private EventState state;

      #endregion

      public int Id { get; set; }
      public int? CurveId { get; set; }
      public int? CurveFemaleId { get; set; }
      public int? BirdId { get; set; }
      public string Name { get; set; }
      public DateTime From { get; set; }
      public DateTime To { get; set; }
      public int Row { get; set; }
      public int StartAge { get; set; }
      public StepMode StepMode { get; set; }
      public SexMode SexMode { get; set; }
      public WeighMode WeighMode { get; set; }
      public double InitialWeight { get; set; }
      public double InitialWeightFemale { get; set; }
      public double DetectionRange { get; set; }
      public double DetectionRangeFemale { get; set; }
      public Stabilization Stabilization { get; set; }
      public double UniformityRange { get; set; }
      public Bird Bird { get; set; }
      public IEnumerable<int> Devices { get; set; } 

      public EventState State
      {
         get { return CheckState(); }
         set { state = value; } 
      }

      #region Helpers

      public int DayOfLife()
      {
         var actual = DateTime.Now;
         return actual.Subtract(From).Days + StartAge;
      }

      private EventState CheckState()
      {
         return state == EventState.Paused ? state : CheckState(From, To);
      }

      public static EventState CheckState(DateTime from, DateTime to)
      {
         var actual = DateTime.Now;
         if (DateTime.Compare(to, actual) < 0)
         {  // To is in past
            return EventState.Closed;
         }
         if (DateTime.Compare(from, actual) > 0)
         {  // From is in future
            return EventState.Stopped;
         }
         return EventState.Active;
      }

    
      public IEnumerable<DateTime> GetEventDates(TimeSpan timeIncrement)
      {
         if (timeIncrement.Equals(TimeSpan.Zero)) throw new ArgumentOutOfRangeException("timeIncrement");
         var date = From.Date;
         var dates = new List<DateTime>();
         while (date <= To.Date)
         {
            date = date.Add(timeIncrement);
            dates.Add(date);
         }
         return dates;
      }

      public IEnumerable<DateTime> GetEventDates(AggregationInterval aggregationInterval)
      {
         var timeSpan = aggregationInterval.MapToTimeSpan();
         return GetEventDates(timeSpan);
      }

      #endregion
   }
}
