﻿using System;
using System.Collections.Generic;

namespace DataContext.DTOs
{
   public abstract class GroupBase
   {
      protected GroupBase()
      {
         Sensors = new List<string>();
         Types = new List<string>();
      }

      public string Group { get; set; }

      public DateTime? From { get; set; }
      public DateTime? To { get; set; }

      public List<string> Sensors { get; set; }
      public List<string> Types { get; set; }
   }
}