﻿using DataModel;
using System.Collections.Generic;
using System.Linq;

namespace DataContext.Extensions
{
   public static class CurveExtension
   {
      public static ICollection<CurvePoint> GetApprox(this ICollection<CurvePoint> points)
      {
         var retPoinst = new List<CurvePoint>();
         if (points.Count == 0) return retPoinst;

         var orderedPoints = points.OrderBy(p => p.Day).ToList();
         var lastDay = orderedPoints.Last().Day;
         var firstDay = orderedPoints.First().Day;
         for (var day = firstDay; day <= lastDay; day++)
         {
            retPoinst.Add(orderedPoints.GetApprox(day));
         }
         return retPoinst;
      }

      public static CurvePoint GetApprox(this ICollection<CurvePoint> points, int day)
      {
         var point = points.FirstOrDefault(f => f.Day == day);
         if (point != null) return point;

         var lower = points.TakeWhile(p => p.Day < day).LastOrDefault();
         var heigher = points.SkipWhile(p => p.Day <= day).FirstOrDefault();

         if (lower == null) return heigher;
         if (heigher == null) return lower;
         return new CurvePoint
         {
            Day = day,
            Value = Interpolate(lower.Day, lower.Value ?? 1, heigher.Day, heigher.Value ?? 1, day),
            Min = Interpolate(lower.Day, lower.Min ?? 1, heigher.Day, heigher.Min ?? 1, day),
            Max = Interpolate(lower.Day, lower.Max ?? 1, heigher.Day, heigher.Max ?? 1, day)
         };
      }

      private static double Interpolate(double x0, double y0, double x1, double y1, double x)
      {
         return y0 + ((y1 - y0) / (x1 - x0)) * (x - x0);
      }
   }
}
