using System;

namespace DataContext.Extensions
{
   public static class DateHelper
   {
      public static double DateTimeToUnixTimestamp(this DateTime dateTime)
      {
         return (TimeZoneInfo.ConvertTimeToUtc(dateTime) -
                 new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc)).TotalSeconds;
      }
   }
}