﻿using Microsoft.EntityFrameworkCore.Storage;
using NLog;
using System;

namespace DataContext.Extensions
{
   public static class TransactionExtension
   {
      private static readonly Logger LOG = LogManager.GetCurrentClassLogger();
      public static bool Resolve(this IDbContextTransaction transaction, bool save)
      {
         if (transaction == null) return false;
         try
         {
            if (save)
            {
               transaction.Commit();
            }
            else
            {
               transaction.Rollback();
            }
            return save;
         }
         catch (Exception e)
         {
            LOG.Error(e, $"Failed when trying {(save ? "commit" : "rollback")} sql transaction.");
            return false;
         }
      }
   }
}
