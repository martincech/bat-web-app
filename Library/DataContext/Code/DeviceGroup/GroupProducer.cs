﻿using System;
using System.Collections.Generic;
using System.Linq;
using KafkaBatLibrary;

namespace DataContext.DeviceGroup
{
   /// <summary>
   ///  Middleware to provide transformation of database relations between Devices and statistical groups(Events,Houses) to Kafka producer. 
   /// </summary>
   public class GroupProducer
   {
      private readonly string database;
      private readonly IKafkaSender producer;
      private readonly DataModel.BatModelContainer container;

      private const string EVENT_GROUP_FORMAT = "event_{0}";
      private const string HOUSE_GROUP_FORMAT = "house_{0}";
      private const string DEVICE_GROUP_FORMAT = "device_{0}";

      public GroupProducer(string database, IKafkaSender producer, DataModel.BatModelContainer container)
      {
         this.database = database;
         this.producer = producer;
         this.container = container;
      }

      /// <summary>
      /// Update device statistical groups. 
      /// </summary>
      /// <param name="deviceId">Device ID used in database.</param>
      /// <returns>Indicates if device statistical groups has been updated succesfully.</returns>
      public bool UpdateGroup(int deviceId)
      {
         return UpdateGroup(new[] { deviceId }.ToList());
      }

      /// <summary>
      /// Update all devices statistical groups. 
      /// </summary>
      /// <param name="deviceIds">List of device IDs used in database.</param>
      /// <returns>Indicates if all devices statistical groups has been updated succesfully.</returns>
      public bool UpdateGroup(List<int> deviceIds)
      {
         return UpdateGroups(container.DevicesLazy.Where(s => deviceIds.Contains(s.Id)).ToList());
      }

      /// <summary>
      /// Update device statistical groups. 
      /// </summary>
      /// <param name="deviceUid">Device unique ID.</param>
      /// <param name="groups">List of statistical groups in which device is listed.</param>
      /// <returns>Indicates if device statistical groups has been updated succesfully.</returns>
      public bool UpdateGroup(string deviceUid, List<StatGroup> groups)
      {
         if (string.IsNullOrEmpty(deviceUid) || string.IsNullOrEmpty(database)) return true;

         var group = new KafkaBatLibrary.DeviceGroup
         {
            Uid = deviceUid,
            Database = database,
            Groups = groups ?? new List<StatGroup>()
         };
         return UpdateGroups(new[] { group }.ToList());
      }

      private bool UpdateGroups(List<DataModel.Device> devices)
      {
         if (devices == null) return true;
         return UpdateGroups(devices.Select(CreateGroup).ToList());
      }

      private KafkaBatLibrary.DeviceGroup CreateGroup(DataModel.Device device)
      {
         if (device == null) return null;
         var events = device.Events.Where(w => w.To >= DateTime.Now.Date);
         var house = device.House;

         var statGroups = new List<StatGroup>();
         foreach (var ev in events)
         {
            var from = ev.From;
            statGroups.Add(CreateStatGroup(device.Uid, from, ev.To, DEVICE_GROUP_FORMAT));
            statGroups.Add(CreateStatGroup(ev.Id.ToString(), from, ev.To, EVENT_GROUP_FORMAT));
            if (house != null) statGroups.Add(CreateStatGroup(house.Id.ToString(), from, ev.To, HOUSE_GROUP_FORMAT));
         }
         return new KafkaBatLibrary.DeviceGroup { Database = database, Groups = statGroups, Uid = device.Uid };
      }

      private static StatGroup CreateStatGroup(string group, DateTime from, DateTime to, string format = null)
      {
         return new StatGroup { Group = format == null ? group : string.Format(format, group), From = from, To = to };
      }

      private bool UpdateGroups(List<KafkaBatLibrary.DeviceGroup> groups)
      {
         if (groups == null) return true;
         return producer.Send(groups);
      }

      public static Dictionary<string, string> UpdateDatabase(List<string> databases, IKafkaSender sender, Func<string, DataModel.BatModelContainer> dbFactory = null)
      {
         if (dbFactory == null) dbFactory = (databse) => { return new DataModel.BatModelContainer(databse); };
         var results = new Dictionary<string, string>();
         foreach (var dbName in databases)
         {
            if (results.ContainsKey(dbName)) continue;
            var result = "";
            try
            {
               using (var db = dbFactory?.Invoke(dbName))
               {
                  var producer = new GroupProducer(dbName, sender, db);
                  result = producer.UpdateGroup(db.Devices.Select(s => s.Id).ToList()) ? "success" : "fail";
               }
            }
            catch (Exception e)
            {
               result = "Exception : " + e.Message;
            }
            finally
            {
               results.Add(dbName, result);
            }
         }
         return results;
      }
   }
}
