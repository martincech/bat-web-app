﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DataContext.DTOs;
using DataContext.DTOs.Enums;
using InfluxData.Net.InfluxDb.Models.Responses;
using Event = DataModel.Event;

namespace DataContext.Context
{
   public partial class BatContext
   {
      #region Read stats method helper objects

      private SensorType?[] requestedSensors;
      private AggregationType?[] requestedAggregations;
      private AggregationInterval?[] aggregationIntervals;

      private IEnumerable<AggregationInterval> ValidAggregationIntervals
      {
         get { return aggregationIntervals.Where(t => t.HasValue).Select(t => t.Value); }
      }

      private IEnumerable<AggregationType> ValidRequestedAggregations
      {
         get { return requestedAggregations.Where(t => t.HasValue).Select(t => t.Value); }
      }

      private IEnumerable<SensorType> ValidRequestedSensors
      {
         get { return requestedSensors.Where(t => t.HasValue).Select(t => t.Value); }
      }

      private readonly SemaphoreSlim readStatsLock = new SemaphoreSlim(1, 1);
      private Dictionary<string, Tuple<GroupData, SensorType>> influxSelectQueries;
      private Dictionary<GroupData, Dictionary<SensorType, List<Serie>>> influxQueryResults;
      private Dictionary<GroupData, object> metadataForGroups;
      private Dictionary<GroupData, Event> eventForGroups;

      private GroupData[] ReadStatsResponse
      {
         get { return metadataForGroups.Keys.ToArray(); }
      }

      #endregion

      public Task<IEnumerable<GroupData>> ReadLastStats(GroupFilter filter)
      {
         return ReadStats(filter, true);
      }

      public async Task<IEnumerable<GroupData>> ReadStats(GroupFilter filter, bool lastOnly = false)
      {
         await readStatsLock.WaitAsync();
         try
         {
            if (filter.GroupIds == null || filter.TimeSizes == null)
            {
               return new List<GroupData>();
            }
            InitResponseObjects(filter);
            LoadMetadata(filter);
            CreateInfluxQueries();

            if (!influxSelectQueries.Any())
            {
               return ReadStatsResponse;
            }
            var influxData = await MultiQueryInflux(influxSelectQueries.Select(q => q.Key), lastOnly);
            FillInfluxResultsDictionary(influxData);
            ParseInfluxDataToResult();
         }
         catch (Exception)
         {
            return new List<GroupData>();
         }
         finally
         {
            readStatsLock.Release();
         }
         return ReadStatsResponse;
      }

      private void InitResponseObjects(GroupFilter filter)
      {
         var respGroups = new GroupData[filter.GroupIds.Count * filter.TimeSizes.Count];
         var resultIndex = 0;
         foreach (var groupId in filter.GroupIds)
         {
            foreach (var timeSize in filter.TimeSizes)
            {
               var groupData = new GroupData
               {
                  Group = filter.Group,
                  GroupId = groupId,
                  TimeSize = timeSize,
                  Sensors = filter.Sensors,
                  From = filter.From,
                  To = filter.To,
                  Types = filter.Types
               };
               respGroups[resultIndex] = groupData;
               resultIndex++;
            }
         }

         influxSelectQueries = new Dictionary<string, Tuple<GroupData, SensorType>>();
         influxQueryResults = new Dictionary<GroupData, Dictionary<SensorType, List<Serie>>>();
         metadataForGroups = new Dictionary<GroupData, object>();
         eventForGroups = new Dictionary<GroupData, Event>();
         foreach (var groupData in respGroups)
         {
            metadataForGroups.Add(groupData, null);
            eventForGroups.Add(groupData, null);
         }
         requestedSensors = filter.Sensors.MapToSensorType().ToArray();
         requestedAggregations = filter.Types.MapToAggregationTypes().ToArray();
         aggregationIntervals = filter.TimeSizes.Select(i => i.MapToAggregationInterval()).ToArray();
      }

      private void LoadMetadata(GroupFilter filter)
      {
         var now = DateTime.Now;
         var statGroup = filter.Group.MapToStatisticGroup();
         switch (statGroup)
         {
            case StatisticGroup.DEVICE:
               FillMetadataForGroups(filter,
                  id => Devices.FirstOrDefault(d => d.Uid.ToString() == id),
                  dev => dev.Events.FirstOrDefault(e => now >= e.From && now <= e.To));
               break;
            case StatisticGroup.EVENT:
               FillMetadataForGroups(filter, id => Events.FirstOrDefault(d => d.Id.ToString() == id),
                  ev => ev);
               break;
            case StatisticGroup.HOUSE:
               FillMetadataForGroups(filter, id => Houses.FirstOrDefault(d => d.Id.ToString() == id),
                house => Events.ToList().FirstOrDefault(e => now >= e.From && now <= e.To && e.Devices.Intersect(house.Devices).Any()));
               break;
            default:
               FillMetadataForGroups<object>(filter, id => null, t => null);
               break;
         }
      }

      private void FillMetadataForGroups<T>(GroupFilter filter,
         Func<string, T> getMetadataObject,
         Func<T, Event> getEventForMetadataObject)
      {
         var timeSizesCount = filter.TimeSizes.Count;
         for (var i = 0; i < filter.GroupIds.Count; i++)
         {
            var id = filter.GroupIds[i];
            var metadataObject = getMetadataObject(id);
            var eventForMetadataObject = metadataObject == null ? null : getEventForMetadataObject(metadataObject);
            for (var j = 0; j < timeSizesCount; j++)
            {
               var groupData = ReadStatsResponse[i + j];
               metadataForGroups[groupData] = metadataObject;
               eventForGroups[groupData] = eventForMetadataObject;
            }
         }
      }

      private void CreateInfluxQueries()
      {
         foreach (var groupData in ReadStatsResponse)
         {
            if (!AnyValidGroup(groupData))
            {
               groupData.Values.Add(requestedSensors.Select(sensorType => new List<object>()).ToList());
               continue;
            }
            if (!AnyValidAggregationType)
            {
               groupData.Values.Add(requestedSensors.Select(sensorType =>
                  ValidRequestedAggregations.Select(requestedColumn => (object)null).ToList()).ToList());
               continue;
            }
            CreateInfluxQueriesForValidRequests(groupData);
         }
      }

      private void FillInfluxResultsDictionary(Dictionary<string, IEnumerable<Serie>> influxResponse)
      {
         foreach (var series in influxResponse)
         {
            var query = series.Key;
            var responses = series.Value;
            var groupData = influxSelectQueries[query].Item1;
            var sensorType = influxSelectQueries[query].Item2;
            if (!influxQueryResults.ContainsKey(groupData))
            {
               influxQueryResults.Add(groupData, new Dictionary<SensorType, List<Serie>>());
            }
            if (!influxQueryResults[groupData].ContainsKey(sensorType))
            {
               influxQueryResults[groupData].Add(sensorType, responses.ToList());
            }
         }
      }

      private void ParseInfluxDataToResult()
      {
         foreach (var groupResult in influxQueryResults)
         {
            var groupData = groupResult.Key;
            var sensorSeries = groupResult.Value;
            var serieResponseCount = sensorSeries.Values.Sum(value => value.Count == 0 ? 1 : value.Count);
            var sensorResponseCount = requestedSensors.Where(s => !s.HasValue).Count() + serieResponseCount;
            var vals = new List<List<object>>[sensorResponseCount];
            var sens = new string[sensorResponseCount];
            for (int i = groupData.Sensors.Count - 1, j = sensorResponseCount - 1; i >= 0; i--, j--)
            {
               var sensorName = groupData.Sensors[i];
               var sensorType = sensorName.MapToSensorType();
               if (!sensorType.HasValue || sensorSeries[sensorType.Value].Count == 0)
               {
                  sens[j] = sensorName;
                  vals[j] = new List<List<object>>();
                  continue;
               }
               var series = sensorSeries[sensorType.Value];
               for (var serieIndex = series.Count - 1; serieIndex >= 0; serieIndex--)
               {
                  var serie = series[serieIndex];
                  // ReSharper disable once PossibleInvalidOperationException                  
                  sens[j] = sensorName.IsWeightSensorString()
                     ? SensorNameBasedOnSerieTag(serie, sensorName)
                     : sensorName;
                  vals[j] = ParseDataFromSerieToValues(groupData, sens[j], serie);
                  if (serieIndex != 0)
                  {
                     j--;
                  }
               }
            }
            groupData.Values = vals.ToList();
            groupData.Sensors = sens.ToList();
         }
      }

      private static string SensorNameBasedOnSerieTag(Serie serie, string sensorName)
      {
         if (!serie.Tags.ContainsKey("sex") || string.IsNullOrEmpty(serie.Tags["sex"]))
         {
            sensorName = sensorName.ConcatWeightWithSex(SexMode.UNDEFINED);
         }
         else
         {
            sensorName = sensorName.ConcatWeightWithSex(serie.Tags["sex"]);
         }
         return sensorName;
      }

      private List<List<object>> ParseDataFromSerieToValues(GroupData groupData, string sensorName, Serie serie)
      {
         var columnIndices = GetIndicesOfQueryResponseColumns(serie.Columns);
         var evForGroup = eventForGroups[groupData];
         var timeSizeMinutes = TimeSpan.FromMinutes(groupData.TimeSize);
         var dataPoints = new List<List<object>>();
         // go through all the data points
         for (var j = 0; j < serie.Values.Count; j++)
         {
            var currentValue = serie.Values[j];
            var dataPointColumns = new List<object>();
            // go through all the requested columns
            foreach (var requestedColumn in requestedAggregations)
            {
               if (!requestedColumn.HasValue || !columnIndices.ContainsKey(requestedColumn.Value.ToLowerName()) ||
                   evForGroup == null)
               {
                  dataPointColumns.Add(null);
                  continue;
               }
               var columnIndex = columnIndices[requestedColumn.Value.ToLowerName()];
               var curColumn = currentValue[columnIndex];
               DateTime timeStamp;
               switch (requestedColumn.Value)
               {
                  case AggregationType.DAY:
                     var day = curColumn;

                     if (day != null)
                     {
                        dataPointColumns.Add(day);
                     }
                     else
                     {
                        timeStamp = ConvertTime(ref currentValue, columnIndices);
                        dataPointColumns.Add((timeStamp - evForGroup.From).TotalDays + evForGroup.StartAge);
                     }
                     break;
                  case AggregationType.TIMESTAMP:
                     timeStamp = ConvertTime(ref currentValue, columnIndices);
                     var offset = timeSizeMinutes - TimeSpan.FromSeconds(1);
                     timeStamp += offset;
                     //if (timeStamp > now)
                     //{
                     //   timeStamp = now;
                     //}
                     dataPointColumns.Add(timeStamp);
                     break;
                  case AggregationType.TARGET:
                     //  time instead of target on the place
                     //timeStamp = ConvertTime(ref currentValue, columnIndices);
                     //if (!sensorName.IsWeightSensorString())
                     //{

                     //}
                     //else
                     //{
                     //    var target = GetTargetForDay(evForGroup, (timeStamp - evForGroup.From).Days);
                     //    var sex = sensorName.GetSexFromSensorName();
                     //    if (sex.HasValue && target.ContainsKey(sex.Value))
                     //    {
                     //        dataPointColumns.Add(target[sex.Value]);
                     //    }
                     //    else
                     //    {
                     //        dataPointColumns.Add(null);
                     //    }
                     //}
                     dataPointColumns.Add(null);
                     break;
                  default:
                     dataPointColumns.Add(curColumn);
                     break;
               }
            }
            dataPoints.Add(dataPointColumns);
         }

         return dataPoints;
      }

      private bool AnyValidAggregationType
      {
         get { return ValidRequestedAggregations.Any(); }
      }

      private bool AnyValidGroup(GroupData groupData)
      {
         return ValidAggregationIntervals.Any() && metadataForGroups.Any(g => g.Value != null) &&
                ValidRequestedSensors.Any() &&
                groupData.TimeSize.MapToAggregationInterval().HasValue;
      }


      private void CreateInfluxQueriesForValidRequests(GroupData groupData)
      {
         var requestedColumns = ValidRequestedAggregations.ToArray();
         var evForGroup = eventForGroups[groupData];
         if ((requestedColumns.Contains(AggregationType.TARGET) || requestedColumns.Contains(AggregationType.DAY))
             && !requestedColumns.Contains(AggregationType.TIMESTAMP)
            )
         {
            //for target and day allways request timestamp
            requestedColumns = requestedColumns.Concat(new[] { AggregationType.TIMESTAMP }).ToArray();
         }
         if (RequestOnlyOnTimeStamp)
         {
            // influx can't return just timestamp, so if only timestamp request query at least average together with it 
            requestedColumns = requestedColumns.Concat(new[] { AggregationType.AVERAGE }).ToArray();
         }
         foreach (var sensor in ValidRequestedSensors)
         {
            var s = CreateSelect(
               MeasurementDescriptor(groupData.Group, groupData.GroupId),
               sensor,
               groupData.TimeSize.MapToTimeSize(),
               requestedColumns,
               !groupData.From.HasValue && evForGroup != null ? evForGroup.From : groupData.From,
               !groupData.To.HasValue && evForGroup != null ? evForGroup.To : groupData.To);
            influxSelectQueries.Add(s, new Tuple<GroupData, SensorType>(groupData, sensor));
         }
      }

      private bool RequestOnlyOnTimeStamp
      {
         get
         {
            return
               ValidRequestedAggregations.All(
                  t => t == AggregationType.TIMESTAMP || t == AggregationType.DAY || t == AggregationType.TARGET);
         }
      }

      /// <summary>
      ///    Read statistics from InfluxDb.
      /// </summary>
      /// <param name="groupName">Identification of the statistical group</param>
      /// <param name="from">Time from</param>
      /// <param name="to">Time to</param>
      /// <param name="sensorTypes">Types of sensor to query (which measurement)</param>
      /// <param name="aggrIntervals">Intervals to query (which measurement)</param>
      /// <param name="aggregationTypes">Type of aggregation to read (columns)</param>
      /// <param name="onlyLast">returns only last available results</param>
      /// <returns>Returns series for each sensorType and for each interval</returns>
      private async Task<IEnumerable<IEnumerable<Serie>>> ReadStats(
         string groupName,
         DateTime? from = null, DateTime? to = null,
         IEnumerable<SensorType> sensorTypes = null,
         IEnumerable<TimeSize> aggrIntervals = null,
         IEnumerable<AggregationType> aggregationTypes = null,
         bool onlyLast = false)
      {
         var measurements = new List<string>();
         IList<SensorType> sensors;
         IList<TimeSize> aggregationTimes;
         IList<AggregationType> aggregations;
         IList<string> devices;
         EnumerateParams(null, sensorTypes, aggrIntervals, aggregationTypes, out devices, out sensors,
            out aggregationTimes, out aggregations);

         var columns = aggregationTypes == null || !aggregations.Any()
            ? new List<string>()
            : aggregations.Select(t => t.ToLowerName()).ToList();
         foreach (var sensorType in sensors)
         {
            foreach (var aggregationInterval in aggregationTimes)
            {
               measurements.Add(
                  AggregationMeasurementFqName(groupName, sensorType, aggregationInterval.Minutes));
            }
         }

         var cols = columns.Any() && !onlyLast
            ? string.Join(",", columns.Select(m => string.Format("\"{0}\"", m)))
            : onlyLast ? "last(*)" : "*";
         var where = WhereClause(null, from, to);
         var groupBy = GroupByClause(sensors);
         var selects = new List<string>();
         foreach (var measurement in measurements)
         {
            selects.Add(string.Format("SELECT {0} FROM {1}{2}{3}",
               cols, measurement, where, groupBy));
         }
         var queryResult = await influx.Client.MultiQueryAsync(selects, DatabaseName);
         return queryResult;
      }

      private static void EnumerateParams(IEnumerable<string> deviceIds, IEnumerable<SensorType> sensorTypes,
         IEnumerable<TimeSize> aggregationIntervals, IEnumerable<AggregationType> aggregationTypes,
         out IList<string> devices, out IList<SensorType> sensors, out IList<TimeSize> aggregationTimes,
         out IList<AggregationType> aggregations)
      {
         devices = deviceIds as IList<string> ?? (deviceIds == null ? null : deviceIds.ToList());
         sensors = sensorTypes as IList<SensorType> ??
                   (sensorTypes == null
                      ? SensorTypeExtension.SensorTypes().ToList()
                      : sensorTypes.ToList());
         aggregationTimes = aggregationIntervals as IList<TimeSize> ??
                            (aggregationIntervals == null
                               ? AggregationIntervalExtensions.AggregationIntervalsTimeSizes()
                                  .ToList()
                               : aggregationIntervals.ToList());
         aggregations = aggregationTypes as IList<AggregationType> ??
                        (aggregationTypes == null
                           ? AggregationTypeExtensions.AggregationTypes().ToList()
                           : aggregationTypes.ToList());
      }

      private static IEnumerable<Stat> MapStatSerieValues(SensorType type,
         AggregationInterval interval, IEnumerable<IList<object>> values, IList<string> columns)
      {
         var columnIndices = GetIndicesOfQueryResponseColumns(columns);
         return values.Select(v => new Stat
         {
            TimeStamp = ConvertTime(ref v, columnIndices),
            TimeSize = interval,
            Type = type,
            Average = ConvertDouble(ref v, AggregationType.AVERAGE, columnIndices),
            Min = ConvertDouble(ref v, AggregationType.MIN, columnIndices),
            Max = ConvertDouble(ref v, AggregationType.MAX, columnIndices),
            Count = ConvertInt(ref v, AggregationType.COUNT, columnIndices),
            Cv = ConvertDouble(ref v, AggregationType.CV, columnIndices),
            Sigma = ConvertDouble(ref v, AggregationType.SIGMA, columnIndices),
            Uniformity = ConvertDouble(ref v, AggregationType.UNIFORMITY, columnIndices)
         });
      }

      #region Data conversion from read data

      private static Dictionary<string, int> GetIndicesOfQueryResponseColumns(IList<string> columns)
      {
         var columnIndices = new Dictionary<string, int>();
         for (var i = 0; i < columns.Count; i++)
         {
            columnIndices.Add(columns[i], i);
         }
         return columnIndices;
      }

      private static DateTime ConvertTime(ref IList<object> data, IReadOnlyDictionary<string, int> columnIndices)
      {
         return columnIndices.ContainsKey(AggregationType.TIMESTAMP.ToLowerName()) && data != null
            ? System.Convert.ToDateTime(data[columnIndices[AggregationType.TIMESTAMP.ToLowerName()]])
            : DateTime.Now;
      }

      private static double? ConvertDouble(ref IList<object> data, AggregationType type,
         IReadOnlyDictionary<string, int> columnIndices)
      {
         return columnIndices.ContainsKey(type.ToLowerName())
            ? System.Convert.ToDouble(data[columnIndices[type.ToLowerName()]])
            : default(double?);
      }

      private static int ConvertInt(ref IList<object> data, AggregationType type,
         IReadOnlyDictionary<string, int> columnIndices)
      {
         return columnIndices.ContainsKey(type.ToLowerName())
            ? System.Convert.ToInt32(data[columnIndices[type.ToLowerName()]])
            : 0;
      }

      #endregion
   }
}
