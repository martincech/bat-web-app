﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataContext.DTOs;
using DataContext.DTOs.Enums;
using InfluxData.Net.Common.Helpers;
using Veit.Bat.Common;

namespace DataContext.Context
{
   partial class BatContext
   {
      private static readonly string[] SESNOR_COLUMNS =
      {
         AggregationType.TIMESTAMP.ToLowerName(),
         AggregationType.DAY.ToLowerName(),
         AggregationType.AVERAGE.ToLowerName()
      };

      private static readonly string[] WEIGHT_COLUMNS =
      {
         AggregationType.TIMESTAMP.ToLowerName(),
         AggregationType.DAY.ToLowerName(),
         AggregationType.GAIN.ToLowerName(),
         AggregationType.UNIFORMITY.ToLowerName(),
         AggregationType.COUNT.ToLowerName(),
         AggregationType.AVERAGE.ToLowerName()
      };

      public async Task<List<GroupData>> GetHousesLast(List<Event> events, List<House> allHouses)
      {
         var result = new List<GroupData>();
         if (events == null || !events.Any() || allHouses == null || !allHouses.Any()) return result;
         var houses = allHouses.Where(w => w.Devices.Intersect(events.SelectMany(s => s.Devices)).Any()).ToList();
         if (!houses.Any()) return result;


         var weightDevices = new List<string>();
         var sensorDevices = new List<string>();
         var uidEvent = new Dictionary<string, DateTime[]>();

         foreach (var house in houses)
         {
            var ev = events.FirstOrDefault(f => f.Devices.Intersect(house.Devices).Any());
            if (ev != null) uidEvent.Add(house.Id.ToString(), new[] { ev.From, ev.To });

            sensorDevices.Add(BuildTable("house", house.Id.ToString(), "humidity", 30));
            sensorDevices.Add(BuildTable("house", house.Id.ToString(), "temperature", 30));
            sensorDevices.Add(BuildTable("house", house.Id.ToString(), "co2", 30));
            weightDevices.Add(BuildTable("house", house.Id.ToString(), "weight", 1440));
         }

         var eventFrom = events.Min(m => m.From);
         var eventTo = events.Max(m => m.To);

         var weightQuery = BuildQuery(WEIGHT_COLUMNS, weightDevices, eventFrom, eventTo);
         var sensorsQuery = BuildQuery(SESNOR_COLUMNS, sensorDevices, eventFrom, eventTo);

         if (weightDevices.Count != 0) result.AddRange(await ReadLast(weightQuery, uidEvent));
         if (sensorDevices.Count != 0) result.AddRange(await ReadLast(sensorsQuery, uidEvent));
         return result;
      }

      public async Task<List<GroupData>> GetDevicesLast(List<Event> events, List<Device> allDevices)
      {
         var result = new List<GroupData>();
         if (events == null || !events.Any() || allDevices == null || !allDevices.Any()) return result;

         var ids = events.Where(w => w.From < DateTime.Now).SelectMany(s => s.Devices);
         var devices = allDevices.Where(w => ids.Contains(w.Id)).ToList();
         if (!devices.Any()) return result;

         var weightDevices = new List<string>();
         var sensorDevices = new List<string>();
         var uidEvent = new Dictionary<string, DateTime[]>();
         foreach (var device in devices)
         {
            var ev = events.FirstOrDefault(f => f.Devices.Contains(device.Id));
            if (ev != null) uidEvent.Add(device.Uid, new[] { ev.From, ev.To });

            switch (device.Type)
            {
               case DeviceType.Bat2Cable:
               case DeviceType.Bat2Gsm:
               case DeviceType.ExternalWeight:
                  weightDevices.Add(BuildTable("device", device.Uid, "weight", 1440));
                  break;
               case DeviceType.ExternalCo2:
                  sensorDevices.Add(BuildTable("device", device.Uid, "co2", 30));
                  break;
               case DeviceType.ExternalTemperature:
                  sensorDevices.Add(BuildTable("device", device.Uid, "temperature", 30));
                  break;
               case DeviceType.ExternalHumidity:
                  sensorDevices.Add(BuildTable("device", device.Uid, "humidity", 30));
                  break;
               default:
                  sensorDevices.Add(BuildTable("device", device.Uid, "humidity", 30));
                  sensorDevices.Add(BuildTable("device", device.Uid, "temperature", 30));
                  sensorDevices.Add(BuildTable("device", device.Uid, "co2", 30));
                  weightDevices.Add(BuildTable("device", device.Uid, "weight", 1440));
                  break;
            }
         }

         var eventFrom = events.Min(m => m.From);
         var eventTo = events.Max(m => m.To);

         var weightQuery = BuildQuery(WEIGHT_COLUMNS, weightDevices, eventFrom, eventTo);
         var sensorsQuery = BuildQuery(SESNOR_COLUMNS, sensorDevices, eventFrom, eventTo);

         if (weightDevices.Count != 0) result.AddRange(await ReadLast(weightQuery, uidEvent));
         if (sensorDevices.Count != 0) result.AddRange(await ReadLast(sensorsQuery, uidEvent));
         return result;
      }

      public async Task<IEnumerable<GroupData>> ReadLast(string q, Dictionary<string, DateTime[]> limits)
      {
         await readStatsLock.WaitAsync();
         var res = new List<GroupData>();
         try
         {
            var result = await influx.Client.QueryAsync(q, DatabaseName);
            var ids = limits.Select(s => s.Key).ToList();
            foreach (var series in result)
            {
               if (!series.Values.Any() || !series.Values.First().Any()) continue;
               var time = series.Values.First().FirstOrDefault() as DateTime?;
               if (time == null) continue;
               var g = new GroupData();
               var parts = series.Name.Split(new[] { "_" }, StringSplitOptions.None);
               var id = ids.FirstOrDefault(w => series.Name.Contains("_" + w + "_")) ?? parts[1];
               g.GroupId = id;
               var limit = limits.ContainsKey(id) ? limits[id] : null;
               if (limit != null && limit.Length > 1 && (limit[0] > time || time > limit[1])) continue;
               if (series.Name.Contains("_30m")) g.TimeSize = 30;
               if (series.Name.Contains("_1440m")) g.TimeSize = 1440;
               g.Values.Add(series.Values.Select(s => s.ToList()).ToList());
               g.Group = parts[0];
               if (series.Name.Contains("_temperature_")) g.Sensors.Add("temperature");
               if (series.Name.Contains("_humidity_")) g.Sensors.Add("humidity");
               if (series.Name.Contains("_co2_")) g.Sensors.Add("co2");
               if (series.Name.Contains("_weight_")) g.Sensors.Add("weight_" + (string.IsNullOrEmpty(series.Tags["sex"]) ? "undefined" : series.Tags["sex"]));
               g.Types = series.Columns.ToList();
               if (g.TimeSize == 1440) UpdateDate(g);
               res.Add(g);
            }
         }
         catch (Exception)
         {
            return new List<GroupData>();
         }
         finally
         {
            readStatsLock.Release();
         }
         return res;
      }

      private void UpdateDate(GroupData data)
      {
         if (data == null || !data.Values.Any() || !data.Types.Any()) return;

         foreach (var values in data.Values)
         {
            if (values == null || values.Count == 0) continue;
            foreach (var value in values)
            {
               if (value == null || value.Count == 0) continue;
               var date = value[0] as DateTime?;
               if (!date.HasValue) continue;
               value[0] = date.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            }
         }
      }

      private string BuildQuery(string[] columns, List<string> tables, DateTime from, DateTime to)
      {
         return string.Format("SELECT {0} FROM {1} WHERE time > {2}000000 AND time < {3}000000 GROUP BY sex ORDER BY time DESC LIMIT 1",
               string.Join(", ", columns),
               string.Join(", ", tables),
               from.ToUnixTime(),
               to.ToUnixTime());
      }

      private string BuildTable(string group, string groupUid, string sensor, int timeSize)
      {
         return string.Format("\"{0}_m\".\"{1}_{2}_{3}_{0}m\"", timeSize, group, groupUid, sensor);
      }
   }
}
