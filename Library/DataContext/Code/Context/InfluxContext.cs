﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataContext.DTOs;
using DataContext.DTOs.Enums;
using InfluxData.Net.Common.Helpers;
using InfluxData.Net.InfluxDb.Models.Responses;

namespace DataContext.Context
{
   public partial class BatContext
   {
      private const int INFLUX_BATCH_SIZE = 10000;

      private async Task<Dictionary<string, IEnumerable<Serie>>> MultiQueryInflux(IEnumerable<string> selects, bool lastOnly = false)
      {
         var res = new Dictionary<string, IEnumerable<Serie>>();
         foreach (var @select in selects)
         {

            var queryResult = await QueryInflux(string.Format("{0}{1}", @select, lastOnly ? " ORDER BY time DESC LIMIT 1" : ""));
            res.Add(@select, queryResult);
         }
         return res;
      }

      private async Task<IEnumerable<Serie>> QueryInflux(string select, int offset = 0)
      {
         var queryResult =
            (await
               influx.Client.QueryAsync(string.Format("{0} OFFSET {1}", select, offset * INFLUX_BATCH_SIZE), DatabaseName));
         foreach (var series in queryResult)
         {
            if (series != null && series.Values.Count == INFLUX_BATCH_SIZE)
            {
               var additionalSerie = (await QueryInfluxSingleSerie(select, offset + 1));
               var list = series.Values.ToList();
               list.AddRange(additionalSerie.Values);
               series.Values = list;
            }
         }

         return queryResult;
      }

      private async Task<Serie> QueryInfluxSingleSerie(string select, int offset = 0)
      {
         var queryResult =
            (await
               influx.Client.QueryAsync(string.Format("{0} OFFSET {1}", select, offset * INFLUX_BATCH_SIZE), DatabaseName)).FirstOrDefault();
         if (queryResult != null && queryResult.Values.Count == INFLUX_BATCH_SIZE)
         {
            var additionalSerie = (await QueryInfluxSingleSerie(select, offset + 1));
            var list = queryResult.Values.ToList();
            list.AddRange(additionalSerie.Values);
            queryResult.Values = list;
         }
         return queryResult;
      }


      private static string WhereClause(IEnumerable<string> deviceIds, DateTime? @from, DateTime? to)
      {
         var devices = deviceIds == null ? new List<string>() : (deviceIds as IList<string> ?? deviceIds.ToList());
         var deviceIdTags = devices.Select(did => string.Format("deviceid = '{0}'", did.ToString())).ToList();
         var whereFrom = @from.HasValue ? string.Format("time>={0}000000", @from.Value.ToUnixTime()) : "";
         var whereTo = to.HasValue ? string.Format("time<={0}000000", to.Value.ToUnixTime()) : "";
         var whereDevices = !deviceIdTags.Any() ? "" : string.Format("({0})", string.Join(" OR ", deviceIdTags));
         return string.Format("{0}{1}{2}{3}{4}{5}",
            string.IsNullOrEmpty(whereDevices) && string.IsNullOrEmpty(whereFrom) && string.IsNullOrEmpty(whereTo)
               ? ""
               : " WHERE ",
            whereDevices,
            !string.IsNullOrEmpty(whereDevices) &&
            (!string.IsNullOrEmpty(whereFrom) || !string.IsNullOrEmpty(whereTo))
               ? " AND "
               : "",
            whereFrom,
            !string.IsNullOrEmpty(whereFrom) && !string.IsNullOrEmpty(whereTo)
               ? " AND "
               : "",
            whereTo);
      }

      private static string GroupByClause(SensorType sensor)
      {
         return GroupByClause(new[] { sensor });
      }
      private static string GroupByClause(ICollection<SensorType> sensors)
      {
         return sensors.Contains(SensorType.WEIGHT) ? " GROUP BY sex" : "";
      }

      private static string CreateSelect(
         string descriptor,
         SensorType type,
         TimeSize size,
         AggregationType[] aggregationTypes = null,
         DateTime? from = null, DateTime? to = null)
      {
         var columns = aggregationTypes == null || !aggregationTypes.Any()
            ? new List<string>()
            : aggregationTypes.Select(t => t.ToLowerName()).ToList();
         var columnsString = columns.Any()
            ? string.Join(",", columns.Select(m => string.Format("\"{0}\"", m)))
            : "*";
         return string.Format(
            "SELECT" +
            " {0}" +
            " FROM {1}{2}{3}",
               columnsString,
               AggregationMeasurementFqName(descriptor, type, size.Minutes),
               WhereClause(null, from, to),
               GroupByClause(type)
            );
      }

      #region Naming conventions for Influx objects

      private static string AggregationMeasurementFqName(string descriptor, SensorType sensorType, int minutes,
         bool withQuotes = true)
      {
         return string.Format("{0}.{1}",
            RetentionPolicyName(minutes, withQuotes),
            AggregationMeasurementName(descriptor, sensorType, minutes, withQuotes));
      }

      private static string RetentionPolicyName(int minutes, bool withQuotes = true)
      {
         return string.Format("{1}{0}_m{1}",
            minutes,
            withQuotes ? "\"" : "");
      }

      private static string AggregationMeasurementName(string descriptor, SensorType sensorType, int minutes,
         bool withQuotes = true)
      {
         return string.Format("{3}{0}{1}_{2}m{3}",
            UnderscoreDescriptor(descriptor),
            sensorType.ToLowerName(),
            minutes,
            withQuotes ? "\"" : "");
      }

      #endregion

      #region Downsampling of data

      private Task<bool> CreateRetentionPolicies()
      {
         return CreateRetentionPolicies(RETENTION_POLICIES, DatabaseName);
      }
      private async Task<bool> CreateRetentionPolicies(Dictionary<string, string> rp, string dbName)
      {
         var policies = new List<string>();
         foreach (var policy in rp)
         {
            policies.Add(string.Format("CREATE RETENTION POLICY \"{1}\" ON \"{0}\" DURATION {2} REPLICATION 1{3}",
               dbName, policy.Key, policy.Value, policies.Any() ? "" : " DEFAULT"));
         }
         foreach (var policy in policies)
         {
            await influx.RequestClient.PostQueryAsync(policy, dbName);
         }
         return true;
      }

      #endregion

      #region String helpers

      private static string UnderscoreDescriptor(string descriptor)
      {
         if (!string.IsNullOrEmpty(descriptor))
         {
            descriptor = descriptor + "_";
         }
         return descriptor;
      }


      private static string MeasurementDescriptor(string name, string id)
      {
         return string.Format("{0}_{1}", name, id);
      }
      #endregion

   }
}
