﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataContext.DTOs;
using DataContext.DTOs.Enums;

namespace DataContext.Context
{
   public partial class BatContext
   {
      public static class StatisticsCalculator
      {
         private const double HUNDRED_PERCENT = 100;
         private const double DEFAULT_UNIFORMITY_RANGE = 10;

         private static readonly Func<IEnumerable<Sample>, double?, double> UNIFORMITY = (samples, uniformityRange) =>
         {
            var sampleList = samples.ToList();
            const int decimalPoints = 2;
            if (sampleList.Count == 0) return HUNDRED_PERCENT;
            uniformityRange = uniformityRange ?? DEFAULT_UNIFORMITY_RANGE;
            var avg = sampleList.Average(s => s.Value);
            var min = avg*(HUNDRED_PERCENT - uniformityRange)/HUNDRED_PERCENT;
            var max = avg*(HUNDRED_PERCENT + uniformityRange)/HUNDRED_PERCENT;
            var inside = sampleList.Count(v => v.Value >= min && v.Value <= max);
            return Math.Round(HUNDRED_PERCENT*inside/(sampleList.Count), decimalPoints);
         };

         public static ICollection<Stat> CalcStatistics(IEnumerable<Sample> samples, AggregationInterval timeSize)
         {
            var stats = new List<Stat>();
            Stat lastStat = null;
            var sampleFrom = 0;
            var samps = samples as List<Sample> ?? samples.ToList();
            for (var i = 0; i < samps.Count; i++)
            {
               if (samps[sampleFrom].TimeStamp.AddMinutes(timeSize.MapToMinutes()) > samps[i].TimeStamp &&
                   i != samps.Count - 1)
               {
                  continue;
               }

               lastStat = CalcOne(samps.GetRange(sampleFrom, i - sampleFrom), samps[sampleFrom].TimeStamp, lastStat,
                  timeSize);
               stats.Add(lastStat);
               sampleFrom = i;
            }
            return stats;
         }

         private static Stat CalcOne(IReadOnlyCollection<Sample> samples, DateTime @from, Stat lastStat,
            AggregationInterval timeSize)
         {
            if (samples == null || !samples.Any()) return null;
            var count = samples.Count;
            var average = count > 0? samples.Sum(s => s.Value)/ count : 0;
            var uniformity = UNIFORMITY(samples, null);
            var sum = samples.Sum(val => Math.Pow(val.Value - average, 2));
            var stddev = count >0?Math.Sqrt(sum / (count - 1)) : 0;
            if (samples.First() is WeightSample)
            {
               var samps = samples.Cast<WeightSample>();
               return new WeightStat
               {
                  TimeStamp = @from,
                  TimeSize = timeSize,
                  Average = Round(average),
                  Max = Round(samples.Max(s => s.Value)),
                  Min = Round(samples.Min(s => s.Value)),
                  Count = count,
                  Uniformity = Round(uniformity),
                  Cv = stddev / average * 100,
                  Sigma = Round(stddev),
                  Type = samples.First().Type,
                  Gain = lastStat == null ? null : Round(average - lastStat.Average),
                  Sex = samps.First().Sex ?? SexMode.UNDEFINED
               };
            }

            return new Stat
            {
               TimeStamp = @from,
               TimeSize = timeSize,
               Average = Round(average),
               Max = Round(samples.Max(s => s.Value)),
               Min = Round(samples.Min(s => s.Value)),
               Count = count,
               Uniformity = Round(uniformity),
               Cv = Round(uniformity),
               Sigma = Round(uniformity),
               Type = samples.First().Type
            };
         }

         private static double? Round(double? value)
         {
            if (value == null) return null;
            return Math.Truncate(value.Value*1000)/1000;
         }
      }
   }
}
