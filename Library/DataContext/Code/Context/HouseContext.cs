﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using DataContext.DTOs.Enums;
using DataContext.Extensions;
using DataContext.Mapping;
using DataModel;

namespace DataContext.Context
{
   public partial class BatContext
   {
      public async Task<IEnumerable<DTOs.House>> GetHouses()
      {
         return (await Houses.ToListAsync()).Map();
      }

      public async Task<DTOs.House> GetHouse(int id)
      {
         return (await Houses.FirstOrDefaultAsync(f => f.Id == id)).Map();
      }

      public async Task<DTOs.House> CreateHouse(DTOs.House house)
      {
         if (house == null) return null;
         using (var tr = container.Database.BeginTransaction())
         {

            var h = house.Map();
            if (house.Devices != null)
            {
               var devices = await container.DevicesLazy.Where(w => house.Devices.Contains(w.Id)).ToListAsync();
               h.Devices = devices;
            }
            container.Houses.Add(h);
            await SaveChangesAsync();
            house = h.Map();
            LOG.Debug("Created house : {0}", Json(house));
            container.Entry(h).Collection(c => c.Devices).Load();
            return tr.Resolve(GroupProducer.UpdateGroup(h.Devices.Select(s => s.Id).ToList())) ? house : null;
         }
      }

      public async Task<DTOs.House> UpdateHouse(DTOs.House house)
      {
         var h = await container.HousesLazy.FirstOrDefaultAsync(f => f.Id == house.Id);
         if (h == null) return null;
         h.Location = house.Location;
         h.Name = house.Name;
         await SaveChangesAsync();
         return h.Map();
      }

      public async Task<bool> UpdateDevicesInHouse(int houseId, int deviceId, bool add)
      {
         using (var tr = container.Database.BeginTransaction())
         {
            var house = container.HousesLazy.FirstOrDefault(f => f.Id == houseId);
            var device = container.DevicesLazy.FirstOrDefault(f => f.Id == deviceId);

            if (house == null || device == null || (add && device.HouseId != null) || (!add && device.HouseId != house.Id)) return false;
            var houseEvents = house.Devices.SelectMany(s => s.Events.Where(w => w.To > DateTime.Now))
                  .Distinct()
                  .OrderBy(o => o.From).ToList();
            var deviceEvets = device.Events.Where(w => w.To > DateTime.Now).ToList();

            if (add)
            {
               await AddDeviceToHouse(device, house, deviceEvets, houseEvents);
            }
            else
            {
               await RemoveDeviceFromHouse(device, house, deviceEvets);
            }
            await SaveChangesAsync();
            var devices = house.Devices.Select(s => s.Id).ToList();
            if (!devices.Contains(deviceId)) devices.Add(deviceId);
            return tr.Resolve(GroupProducer.UpdateGroup(devices));
         }
      }

      private async Task<bool> AddDeviceToHouse(Device device, House house, List<Event> deviceEvents, List<Event> houseEvents)
      {
         if (houseEvents.Any())
         {
            if (deviceEvents.Any())
            {
               /* REMOVE DEVICE FROM OLD DEVICE EVENTS */
               foreach (var de in deviceEvents)
               {
                  var eventDevice = device.EventDevices.FirstOrDefault(f => f.EventId == de.Id);
                  if (eventDevice != null) device.EventDevices.Remove(eventDevice);
               }
               await SaveChangesAsync();
            }
            /* ADD NEW DEVICE TO ALL HOUSE EVENTS */
            houseEvents.Where(w => !device.Events.Contains(w)).ToList().ForEach(d => device.EventDevices.Add(new EventDevice { Event = d, Device = device }));
            await SaveChangesAsync();
         }
         else
         {
            if (deviceEvents.Any())
            {
               /* ADD ALL HOUSE DEVICES TO NEW DEVICE EVENTS */
               house.Devices.ToList().ForEach(f => deviceEvents.Where(w => !f.Events.Contains(w)).ToList().ForEach(df => f.EventDevices.Add(new EventDevice { Event = df, Device = device })));
               await SaveChangesAsync();
            }
         }
         /* ADD NEW DEVICE TO HOUSE*/
         house.Devices.Add(device);
         return true;
      }

      private async Task<bool> RemoveDeviceFromHouse(Device device, House house, List<Event> deviceEvents)
      {

         var moreThanOneDeviceEvents = deviceEvents.Where(w => w.Devices.Count() > 1).ToList();

         if (moreThanOneDeviceEvents.Any())
         {
            /* REMOVE DEVICE FROM OLD DEVICE EVENTS */
            foreach (var ev in moreThanOneDeviceEvents)
            {
               var eventDeivce = device.EventDevices.FirstOrDefault(f => f.EventId == ev.Id);
               if (eventDeivce != null) device.EventDevices.Remove(eventDeivce);
            }
            await SaveChangesAsync();
         }
         /* REMOVE DEVICE FROM HOUSE*/
         house.Devices.Remove(device);
         return true;
      }

      public async Task DeleteHouses(List<int> houseIds)
      {
         if (houseIds == null || houseIds.Count == 0) return;
         using (var tr = container.Database.BeginTransaction())
         {
            var devices = Devices.Where(w => w.HouseId != null && houseIds.Contains(w.HouseId.Value)).ToList();
            devices.ForEach(f => f.HouseId = null);
            container.Houses.RemoveRange(container.Houses.Where(w => houseIds.Contains(w.Id)));
            await SaveChangesAsync();
            tr.Resolve(GroupProducer.UpdateGroup(devices.Select(s => s.Id).ToList()));
         }
      }

      public async Task ChangeName(int houseId, string name)
      {
         var house = await container.HousesLazy.FirstOrDefaultAsync(f => f.Id == houseId);
         if (house == null) return;
         house.Name = name;
         await SaveChangesAsync();
      }

      private static string HouseMeasurementDescriptor(int houseId)
      {
         return MeasurementDescriptor(StatisticGroup.HOUSE.ToLowerName(), houseId.ToString());
      }
   }
}
