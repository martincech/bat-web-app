﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using DataContext.DTOs;
using DataContext.Mapping;

namespace DataContext.Context
{
   public partial class BatContext
   {
      public async Task<IEnumerable<Farm>> GetFarms()
      {
         return (await Farms.ToListAsync()).Map();
      }

      public async Task<Farm> GetFarm(int id)
      {
         return (await Farms.FirstOrDefaultAsync(f => f.Id == id)).Map();
      }

      public async Task<Farm> CreateFarm(Farm farm)
      {
         if (farm == null) return null;
         var f = farm.Map();
         container.Farms.Add(f);
         await SaveChangesAsync();
         var farmDto = f.Map();         
         LOG.Debug("Create farm : {0}", Json(farmDto));
         return farmDto;
      }

      public async Task<Farm> UpdateFarm(Farm farm)
      {
         var fa = await container.FarmsLazy.FirstOrDefaultAsync(f => f.Id == farm.Id);
         if (fa == null) return null;
         fa.Houses.Clear();
         if (farm.Houses == null) farm.Houses = new List<int>();
         var houses = await container.HousesLazy.Where(w => farm.Houses.Contains(w.Id)).ToListAsync();
         foreach (var house in houses)
         {
            fa.Houses.Add(house);
         }
         fa.Location = farm.Location;
         fa.Name = farm.Name;
         await SaveChangesAsync();
         return fa.Map();
      }

      public async Task DeleteFarm(List<int> farmIds)
      {
         if (farmIds == null || !farmIds.Any()) return;
         LOG.Debug("Delete farms : {0}", Json(farmIds));
         var farms = container.FarmsLazy.Where(w => farmIds.Contains(w.Id));
         foreach (var farm in Farms)
         {
            farm.Houses.Clear();
         }
         container.Farms.RemoveRange(farms);
         await SaveChangesAsync();
      }
   }
}
