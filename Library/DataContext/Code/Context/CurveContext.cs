﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using DataContext.DTOs;
using DataContext.Mapping;

namespace DataContext.Context
{
   public partial class BatContext
   {
      #region Curves

      public async Task<IEnumerable<Curve>> GetCurves(bool approx = true)
      {
         var result = await container.CurvesLazy.ToListAsync();
         return result.Select(s => s.Map(approx));
      }

      public async Task<IEnumerable<Curve>> GetCurvesName()
      {
         var result = await container.Curves.ToListAsync();
         return result.Select(s => new Curve { Id = s.Id, Name = s.Name, Type = s.Type.Map() });
      }

      public async Task<Curve> GetCurve(int id)
      {
         var result = await container.CurvesLazy.Where(w => w.Id == id).FirstOrDefaultAsync();
         return result.Map();
      }

      public async Task<Curve> CreateCurve(Curve curve)
      {

         var newCurve = curve.Map();
         newCurve.Id = 0;
         container.Curves.Add(newCurve);
         await SaveChangesAsync();
         var curveDto = newCurve.Map(false);
         LOG.Debug("Created curve: {0}", Json(curveDto));
         return curveDto;
      }

      public async Task UpdateCurve(int id, Curve curve)
      {
         var lastCurve = await container.CurvesLazy.FirstOrDefaultAsync(f => f.Id == id);
         if (lastCurve == null) return;
         lastCurve.Name = curve.Name;
         lastCurve.Type = curve.Type.Map();
         try
         {
            container.CurvePoints.RemoveRange(lastCurve.CurvePoints);
            lastCurve.CurvePoints.Clear();
            if (curve.CurvePoints != null)
               curve.CurvePoints.ToList().ForEach(s =>
               {
                  lastCurve.CurvePoints.Add(s.Map());
               });
            await SaveChangesAsync();
         }
         catch (Exception)
         {
         }
      }

      public async Task DeleteCurves(ICollection<int> ids)
      {
         foreach (var id in ids)
         {
            await DeleteCurve(id);
         }
      }

      public async Task DeleteCurve(int id)
      {
         var curve = await container.CurvesLazy.FirstOrDefaultAsync(f => f.Id == id);
         if (curve == null) return;
         LOG.Debug("Delete curve: {0}", Json(curve.Map()));
         curve.BirdCurves.Clear();
         await container.EventsLazy.Where(s => s.CurveFemaleId == id).ForEachAsync(f => f.CurveFemaleId = null);
         await container.EventsLazy.Where(s => s.CurveId == id).ForEachAsync(f => f.CurveId = null);
         container.Curves.Remove(curve);
         await SaveChangesAsync();
      }

      #region Curve points

      public async Task CreateCurvePoint(int curveId, CurvePoint curvePoint)
      {
         var curve = await container.CurvesLazy.FirstOrDefaultAsync(f => f.Id == curveId);
         if (curve == null) return;
         var point = curve.CurvePoints.FirstOrDefault(f => f.Day == curvePoint.Day);
         if (point == null)
         {
            curve.CurvePoints.Add(curvePoint.Map());
         }
         else
         {
            point.Max = curvePoint.Max;
            point.Min = curvePoint.Min;
            point.Value = curvePoint.Value;
         }
         await SaveChangesAsync();
      }

      public async Task<CurvePoint> UpdateCurvePoint(int curveId, CurvePoint curvePoint)
      {
         var oldValue = await container.CurvePoints.FirstOrDefaultAsync(f => f.CurveId == curveId && f.Day == curvePoint.Day);
         if (oldValue == null) return null;
         oldValue.Value = curvePoint.Value;
         await SaveChangesAsync();
         return oldValue.Map();
      }

      public async Task DeleteCurvePoint(int curveId, int day)
      {
         var oldValue = await container.CurvePoints.FirstOrDefaultAsync(f => f.CurveId == curveId && f.Day == day);
         if (oldValue == null) return;
         container.CurvePoints.Remove(oldValue);
         await SaveChangesAsync();
      }

      #endregion

      #endregion

      public async Task<IEnumerable<Curve>> GetCurvesByEvent(int eventId)
      {
         var ev = Events.FirstOrDefault(f => f.Id == eventId);
         if (ev == null) return new List<Curve>();
         var ids = ev.Bird == null ? new List<int>() : ev.Bird.Curves.Select(s => s.Id).ToList();
         if (ev.CurveId != null) ids.Add(ev.CurveId.Value);
         if (ev.CurveFemaleId != null) ids.Add(ev.CurveFemaleId.Value);
         return container.CurvesLazy.Where(w => ids.Contains(w.Id)).ToList().Map();
      }
   }
}
