﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Threading.Tasks;
using DataContext.DeviceGroup;
using DataContext.DTOs.Enums;
using DataContext.Mapping;
using DataModel;
using InfluxData.Net.InfluxDb;
using KafkaBatLibrary;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using NLog;
using UsersDataModel;
using UsersDataModel.DTO;
using Device = DataModel.Device;
using Terminal = DataModel.Terminal;
using TimeSize = DataContext.DTOs.TimeSize;

namespace DataContext.Context
{
   public partial class BatContext : IDisposable
   {
      private static readonly Logger LOG = LogManager.GetCurrentClassLogger();
      private static readonly Dictionary<string, string> RETENTION_POLICIES;

      private readonly UserContext userContext;
      private readonly BatModelContainer container;
      private readonly IInfluxDbClient influx;
      private readonly IKafkaSender kafkaSender;
      private readonly IPrincipal principal;

      private GroupProducer groupProducer;
      private GroupProducer GroupProducer
         => groupProducer ?? (groupProducer = new GroupProducer(DatabaseName, kafkaSender, container));


      /// <summary>
      ///    Initializes a new instance of the <see cref="T:System.Object" /> class.
      /// </summary>
      static BatContext()
      {
         RETENTION_POLICIES = new Dictionary<string, string>
         {
            //2 roky
            {"30_m", "104w"},
            //5 roků
            {"1440_m", "260w"},
         };
      }

      private string databaseName;
      public string DatabaseName => databaseName ?? (databaseName = container.Database.GetDbConnection().Database);

      public BatModelContainer Container => container;

      public List<string> DeviceList { get; private set; }

      public BatContext(BatModelContainer container, UserContext userContext, IInfluxDbClient influx, IKafkaSender kafkaSender)
      {
         this.container = container;
         this.userContext = userContext;
         this.influx = influx;
         this.kafkaSender = kafkaSender;

         principal = userContext.Principal;
         DeviceList = userContext.GetCurrentUserDevices() ?? new List<string>();
      }

      public bool IsInRole(RoleE role) => principal.IsInRole(role.ToString());

      public async Task<bool> CreateDatabaseAsync()
      {
         container.Database.Migrate();
         if (!container.TimeSizes.Any())
         {
            await AddTimeSizeToMetadata(AggregationInterval.HalfHour);
            await AddTimeSizeToMetadata(AggregationInterval.Day);
            container.SaveChanges();
         }
         if (!(await InfluxDbExists()))
         {
            if (!await InfluxDbCreate())
            {
               return false;
            }
            if (!await CreateRetentionPolicies())
            {
               // await InfluxDbDrop();
               LOG.Error("Faild to create retention policy policy INFLUX DB: {0}", DatabaseName);
               return false;
            }
         }
         return true;
      }

      public async Task DropDatabaseAsync()
      {

         try
         {
            LOG.Info("Delete database : {0}", DatabaseName);
            container.Database.EnsureDeleted();
         }
         catch (Exception e)
         {
            LOG.Error(e, "Error deleting database : {0}", DatabaseName);
         }
         await container.SaveChangesAsync();

         if (await InfluxDbExists())
         {
            await InfluxDbDrop();
         }
      }

      #region Influx DB

      private Task<bool> InfluxDbExists()
      {
         return InfluxDbExists(DatabaseName);
      }

      private async Task<bool> InfluxDbExists(string dbName)
      {
         try
         {
            var databases = await influx.Database.GetDatabasesAsync();
            return databases.Any(d => d.Name == dbName);
         }
         catch (Exception e)
         {
            LOG.Error(e, "Error occured when checking if influx db exist : {0}", dbName);
            return false;
         }
      }

      private Task<bool> InfluxDbCreate()
      {
         return InfluxDbCreate(DatabaseName);
      }

      private async Task<bool> InfluxDbCreate(string dbName)
      {
         LOG.Info("Create influx database : {0}", dbName);
         try
         {
            var response = await influx.Database.CreateDatabaseAsync(dbName);
            return response.StatusCode == HttpStatusCode.OK;
         }
         catch (Exception e)
         {
            LOG.Fatal(e, "Failed to careate influx database {0}", dbName);
         }
         return false;
      }

      private Task<bool> InfluxDbDrop()
      {
         return InfluxDbDrop(DatabaseName);
      }

      private async Task<bool> InfluxDbDrop(string dbName)
      {
         try
         {
            var response = await influx.Database.DropDatabaseAsync(dbName);
            return response.StatusCode == HttpStatusCode.OK;
         }
         catch (Exception e)
         {
            LOG.Error(e, "Exception occured when deleting influx database : {0}", dbName);
            return false;
         }
      }

      #endregion

      #region Filters

      public IQueryable<House> Houses
      {
         get
         {
            if (!IsInRole(RoleE.Worker)) return container.HousesLazy.AsQueryable();
            return container.HousesLazy.Where(w => w.Devices.Select(s => s.Uid).Intersect(DeviceList).Any());
         }
      }

      public IQueryable<Farm> Farms
      {
         get
         {
            if (!IsInRole(RoleE.Worker)) return container.FarmsLazy.AsQueryable();
            return container.FarmsLazy.Where(w => w.Houses.Any(a => a.Devices.Select(s => s.Uid).Intersect(DeviceList).Any()));
         }
      }

      public IQueryable<Terminal> Terminals
      {
         get
         {
            if (!IsInRole(RoleE.Worker)) return container.TerminalsLazy.AsQueryable();
            return container.TerminalsLazy.Where(w => w.Devices.Select(s => s.Uid).Intersect(DeviceList).Any());
         }
      }

      public IQueryable<Event> Events
      {
         get
         {
            if (!IsInRole(RoleE.Worker)) return container.EventsLazy.AsQueryable();
            return container.EventsLazy.ToList().Where(w => w.EventDevices.Any(a => UserDeviceList.Contains(a.DeviceId))).AsQueryable();
         }
      }

      public IQueryable<Device> Devices
      {
         get
         {
            if (!IsInRole(RoleE.Worker)) return container.DevicesLazy.AsQueryable();
            return container.DevicesLazy.Where(w => DeviceList.Contains(w.Uid));
         }
      }

      private IEnumerable<int> userDeviceList;
      public IEnumerable<int> UserDeviceList => userDeviceList ??
          (userDeviceList = container.Devices.Where(w => DeviceList.Contains(w.Uid)).Select(s => s.Id));

      #endregion

      #region TimeSizes

      public async Task<IEnumerable<TimeSize>> GetTimeSizes()
      {
         return (await container.TimeSizes.ToArrayAsync()).GroupBy(g => g.Minutes).Select(s => s.First()).ToList().Map();
      }

      private async Task<DataModel.TimeSize> AddTimeSizeToMetadata(AggregationInterval aggregationInterval)
      {
         return await AddTimeSizeToMetadata(aggregationInterval.MapToMinutes());
      }

      private async Task<DataModel.TimeSize> AddTimeSizeToMetadata(int minutes)
      {
         var ts = container.TimeSizes.Add(new DataModel.TimeSize { Minutes = minutes });
         await SaveChangesAsync();
         return ts?.Entity;
      }

      #endregion

      private async Task SaveChangesAsync()
      {
         if (!IsInRole(RoleE.Worker))
            await container.SaveChangesAsync();
      }

      private bool IsWorker()
      {
         return IsInRole(RoleE.Worker);
      }

      private static string Json(object o)
      {
         if (o == null) return "";
         try
         {
            return JsonConvert.SerializeObject(o);
         }
         catch (Exception)
         {
            LOG.Warn("Failed to serialize object to log.");
            return "";
         }
      }

      public void Dispose()
      {
         container.Dispose();
      }
   }
}
