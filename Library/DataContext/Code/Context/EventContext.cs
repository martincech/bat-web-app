﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using DataContext.DTOs.Enums;
using DataContext.Extensions;
using DataContext.Mapping;
using Curve = DataModel.Curve;
using CurveType = DataModel.CurveType;
using Device = DataModel.Device;
using Event = DataContext.DTOs.Event;
using EventState = DataModel.EventState;
using SexMode = DataContext.DTOs.Enums.SexMode;
using DataModel;

namespace DataContext.Context
{
   public partial class BatContext
   {
      #region Event

      public async Task<IEnumerable<Event>> GetAllEvents()
      {
         return Events.ToList().Map();
      }

      public async Task<IEnumerable<Event>> GetDevicesEvent()
      {
         var devices = await Devices.ToListAsync();
         var list = new List<DataModel.Event>();
         foreach (var device in devices)
         {
            var ev = device.Events?.Where(w => w.To >= DateTime.Now.Date).OrderBy(o => o.From).FirstOrDefault();
            if (ev != null && !list.Contains(ev)) list.Add(ev);
         }
         return list.Map();
      }

      public async Task<Event> CreateEvent(Event item)
      {
         return (await CreateDbEvent(item)).Map();
      }

      public async Task<Event> StartDeviceEvent(int deviceId, string name)
      {
         var device = container.DevicesLazy.FirstOrDefault(f => f.Id == deviceId);
         if (device == null) return null;

         var lastEvent = device.Events.OrderByDescending(o => o.From).FirstOrDefault();
         var row = 0;
         if (container.EventsLazy.Any()) row = container.EventsLazy.Max(m => m.RowNumber) + 1;
         var houseDevices = device.HouseId == null ? new List<Device> { device } : device.House.Devices.ToList();
         var subName = "";
         if (device.House != null)
         {
            subName = device.House.Name;
            if (device.House.Farm != null) subName = device.House.Farm.Name + " " + subName;
         }
         var eventLenght = userContext.User.Company.EventLength ?? 150;
         var ev = new DataModel.Event
         {
            Name = name + " - " + subName,
            EventDevices = houseDevices.Where(w => !w.Events.Any(a => a.From > DateTime.Now)).Select(s => new EventDevice { Device = s }).ToList(),
            From = DateTime.Now.Date,
            To = DateTime.Now.Date.AddDays(eventLenght),
            SexMode = DataModel.SexMode.Undefined,
            RowNumber = row,
            State = EventState.Unpause,
         };

         if (lastEvent != null)
         {
            var evDevices = lastEvent.Devices.Where(w => !w.Events.Any(a => a.From > DateTime.Now)).ToList();
            evDevices.AddRange(ev.Devices);
            ev.Name = "COPY - " + lastEvent.Name;
            ev.BirdId = lastEvent.BirdId;
            ev.RowNumber = lastEvent.RowNumber;
            ev.SexMode = lastEvent.SexMode;
            ev.RowNumber = lastEvent.RowNumber;
            ev.EventDevices = evDevices.Distinct().Select(s => new EventDevice { Device = s }).ToList();
         }
         return (await CreateDbEvent(ev.Map())).Map();
      }

      public async Task<Event> StartHouseEvent(int houseId, string name)
      {
         var house = container.HousesLazy.FirstOrDefault(f => f.Id == houseId);
         if (house == null || !house.Devices.Any()) return null;
         return await StartDeviceEvent(house.Devices.First().Id, name);
      }

      internal async Task<DataModel.Event> CreateDbEvent(Event item)
      {
         using (var tr = container.Database.BeginTransaction())
         {
            try
            {
               var curves = GetCurves(item.Bird).Map();
               var savedItem = item.Map(curves);
               savedItem.Id = 0;
               savedItem.EventDevices = GetExistingDevicesFromEvent(item.Devices).ToList();
               container.Events.Add(savedItem);
               if (item.Bird != null)
               {
                  var configuration = container.BirdsLazy.FirstOrDefault(f => f.Id == item.Bird.Id);
                  savedItem.Bird = configuration;
               }
               await SaveChangesAsync();
               LOG.Debug("Created event: {0}", Json(savedItem.Map()));
               return tr.Resolve(GroupProducer.UpdateGroup(savedItem.Devices?.Select(s => s.Id).ToList())) ? savedItem : null;
            }
            catch (Exception e)
            {
               LOG.Error(e, "Exception thrown when creating event : {0}", Json(item));
               tr.Rollback();
               return null;
            }
         }
      }

      private static string EventMeasurementDescriptor(int eventId)
      {
         return MeasurementDescriptor(StatisticGroup.EVENT.ToLowerName(), eventId.ToString());
      }

      public async Task<Event> UpdateEvent(int id, Event item, bool basicOnly = false)
      {

         using (var tr = container.Database.BeginTransaction())
         {
            try
            {
               var ev = Events.FirstOrDefault(f => f.Id == id);
               if (ev == null) return null;

               if (item.BirdId != null)
               {
                  ev.Bird = container.BirdsLazy.FirstOrDefault(f => f.Id == item.BirdId);
               }
               else
               {
                  if (ev.Bird != null) ev.Bird.Events.Remove(ev);
                  ev.Bird = null;
               }

               ev.Name = item.Name;
               ev.RowNumber = item.Row;
               ev.SexMode = item.SexMode.Map();
               ev.StartAge = item.StartAge;

               if (basicOnly)
               {
                  await SaveChangesAsync();
                  tr.Commit();
                  return ev.Map();
               }

               var devices = GetUnique(ev.Devices.Select(s => s.Id), item.Devices);
               if (ev.From.Date != item.From.Date || ev.To.Date != item.To.Date)
               {
                  devices.AddRange(item.Devices);
                  devices.AddRange(ev.Devices.Select(s => s.Id));
                  devices = devices.Distinct().ToList();
               }

               ev.From = item.From;
               ev.To = item.To;
               ev.State = item.State.Map();
               ev.CurveId = item.CurveId;
               ev.CurveFemaleId = item.CurveFemaleId;
               ev.StepMode = item.StepMode.Map();

               ev.UniformityRange = item.UniformityRange;
               ev.WeighMode = item.WeighMode.Map();
               ev.InitialWeight = item.InitialWeight;
               ev.InitialWeightFemale = item.InitialWeightFemale;
               ev.DetectionRange = item.DetectionRange;
               ev.DetectionRangeFemale = item.DetectionRangeFemale;
               ev.EventDevices.Clear();
               ev.EventDevices = GetExistingDevicesFromEvent(item.Devices);
               await SaveChangesAsync();
               return tr.Resolve(GroupProducer.UpdateGroup(devices.Distinct().ToList())) ? ev.Map() : null;
            }
            catch (Exception e)
            {
               LOG.Error(e, "Failed to update events {0} values : {1}", basicOnly ? "basic" : "", Json(item));
               tr.Rollback();
               return null;
            }
         }
      }

      public async Task DeleteEvents(ICollection<int> ids)
      {
         foreach (var id in ids)
         {
            await DeleteEvent(id);
         }
      }

      public async Task<bool> DeleteEvent(int id)
      {
         using (var tr = container.Database.BeginTransaction())
         {
            try
            {
               var ev = Events.FirstOrDefault(f => f.Id == id);
               if (ev == null) return false;
               var devices = ev.Devices.Select(s => s.Id).ToList();
               LOG.Debug("Delete event: {0}", Json(ev.Map()));
               ev.EventDevices.Clear();
               container.Events.Remove(ev);
               await SaveChangesAsync();
               return tr.Resolve(GroupProducer.UpdateGroup(devices));
            }
            catch (Exception e)
            {
               LOG.Error(e, "Exception thrown when deleting event: {0}", id);
               tr.Rollback();
               return false;
            }
         }
      }

      private ICollection<EventDevice> GetExistingDevicesFromEvent(IEnumerable<int> devices)
      {
         if (devices == null) return new List<EventDevice>();
         return container.DevicesLazy.Where(w => devices.Contains(w.Id)).Select(s => new EventDevice { Device = s }).ToList();
      }

      public Dictionary<SexMode, double> GetTargetForDay(DataModel.Event ev, int day)
      {
         var targets = new Dictionary<SexMode, double>();
         var requestedDay = ev.From.AddDays(day);
         if (ev.WeighMode != DataModel.WeighMode.Curve || !(ev.From < requestedDay && ev.To > requestedDay)) return targets;
         var curves = new List<Curve>();
         if (ev.Bird != null)
         {

            curves.AddRange(
               ev.Bird.Curves.Where(
                  w =>
                     w.Type == CurveType.BirdFemale || w.Type == CurveType.BirdMale || w.Type == CurveType.BirdUndefined));
         }
         else
         {
            if (ev.Curve != null) curves.Add(ev.Curve);
            if (ev.SexMode == SexMode.MIXED.Map())
            {
               if (ev.CurveFemale != null) curves.Add(ev.CurveFemale);
            }
         }
         foreach (var curve in curves)
         {
            container.Entry(curve).Collection(r => r.CurvePoints).Load();
            var target = curve.CurvePoints.GetApprox(day);
            if (target == null || target.Value == null) continue;
            SexMode? curveType = null;
            switch (curve.Type)
            {
               case CurveType.BirdFemale:
                  curveType = SexMode.FEMALE;
                  break;
               case CurveType.BirdMale:
                  curveType = SexMode.MALE;
                  break;
               case CurveType.BirdUndefined:
                  curveType = SexMode.UNDEFINED;
                  break;
               default:
                  throw new Exception("Unexpected Case");
            }
            if (curveType != null) targets.Add(curveType.Value, target.Value.Value);
         }
         return targets;
      }

      #endregion

      public async Task<Event> GetActiveDeviceEvent(int deviceId)
      {
         var device = await Devices.FirstOrDefaultAsync(f => f.Id == deviceId);
         if (device == null) return null;
         var ev = device.Events.Where(f => f.To > DateTime.Now).OrderBy(o => o.From).FirstOrDefault();
         return ev.Map();
      }

      private static List<int> GetUnique(IEnumerable<int> list1, IEnumerable<int> list2)
      {
         var unique = new List<int>();
         if (list1 == null && list2 == null) return unique;
         if (list1 != null) unique.AddRange(list1.Except(list2 ?? new List<int>()));
         if (list2 != null) unique.AddRange(list2.Except(list1 ?? new List<int>()));
         return unique;
      }
   }
}
