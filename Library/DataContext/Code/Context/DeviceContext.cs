﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using DataContext.DTOs.Enums;
using DataContext.Extensions;
using DataContext.Mapping;
using Device = DataContext.DTOs.Device;
using Veit.Bat.Common;

namespace DataContext.Context
{
   public partial class BatContext
   {
      public async Task<IEnumerable<Device>> GetDevices()
      {
         return (await Devices.ToListAsync()).Map();
      }

      public async Task<Device> CreateDeviceIfNotExist(string terminalUid, string deviceUid, DeviceType type, string deviceName = null)
      {
         var terminal = await CreateTerminalIfNotExist(terminalUid);
         var device = container.DevicesLazy.FirstOrDefault(f => f.Uid == deviceUid);
         if (device != null)
         {
            if (device.Type != type.Map() || !string.IsNullOrEmpty(deviceName) || terminal?.Id != device.TerminalId)
            {
               device.Type = type.Map();
               if (terminal != null && device.TerminalId != terminal?.Id) device.TerminalId = terminal.Id;
               if (!string.IsNullOrEmpty(deviceName)) device.Name = deviceName;
               await SaveChangesAsync();
            }
         }
         else
         {
            device = new DataModel.Device { Type = type.Map(), Uid = deviceUid, TerminalId = terminal?.Id, Name = deviceName ?? deviceUid };
            container.Devices.Add(device);
            await SaveChangesAsync();
         }
         return device.Map();
      }

      public async Task<Device> CreateDevice(DataModel.Device device)
      {
         container.Devices.Add(device);
         await SaveChangesAsync();
         return device.Map();
      }

      public async Task<Device> GetDeviceById(int deviceId)
      {
         return (await Devices.FirstOrDefaultAsync(f => f.Id == deviceId)).Map();
      }

      public async Task<bool> UpdateDeviceName(int deviceId, string name)
      {
         var device = Devices.FirstOrDefault(f => f.Id == deviceId);
         if (device == null) return false;
         device.Name = name;
         await container.SaveChangesAsync();
         return true;
      }

      public async Task DeleteDeviceByUid(IEnumerable<string> uids)
      {
         foreach (var uid in uids.ToList())
         {
            await DeleteDeviceByUid(uid);
         }
      }
      public async Task<bool> DeleteDeviceByUid(string uid)
      {
         var resolved = false;
         using (var transaction = container.Database.BeginTransaction())
         {
            var device = Devices.FirstOrDefault(f => f.Uid == uid);
            if (device == null) return true;
            LOG.Debug("Delete device: {0}", Json(device.Map()));
            device.DeviceTimeSizes.Clear();
            device.EventDevices.Clear();
            container.Devices.Remove(device);
            await SaveChangesAsync();
            DeviceList.Remove(uid);
            resolved = transaction.Resolve(GroupProducer.UpdateGroup(uid, null));
         }
         if (resolved)
         {
            userContext.DeleteDeviceByUid(uid);
         }
         return resolved;
      }

      private static string DeviceMeasurementDescriptor(string deviceId)
      {
         return MeasurementDescriptor(StatisticGroup.DEVICE.ToLowerName(), deviceId);
      }
   }
}
