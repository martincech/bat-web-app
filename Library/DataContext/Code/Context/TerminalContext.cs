﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using DataContext.Mapping;
using Terminal = DataContext.DTOs.Terminal;

namespace DataContext.Context
{
   public partial class BatContext
   {
      public async Task<ICollection<Terminal>> GetTerminals()
      {
         var result = await Terminals.ToListAsync();
         return result.Select(s => s.Map()).ToList();
      }

      public async Task<Tuple<Terminal, string>> CreateTerminalAndRegisterInUserCompany(string terminalUid, string terminalName)
      {
         return await CreateTerminalAndRegisterInUserCompany(terminalUid, terminalName, userContext);
      }

      public async Task<Tuple<Terminal, string>> CreateTerminalAndRegisterInUserCompany(string terminalUid, string terminalName, UsersDataModel.UserContext userDatabase)
      {
         if (string.IsNullOrEmpty(terminalUid)) return new Tuple<Terminal, string>(null, "terminalUid cannot be null");
         //Check if terminal is assigned to different company than logged user
         if (!userDatabase.IsTerminalAvailable(terminalUid)) return new Tuple<Terminal, string>(null, string.Format("Terminal {0} is registrated in different company.", terminalUid));

         //Create terminal or assign existing terminal to comapny 
         var terminal = await userDatabase.CreateOrUpdateTerminal(terminalUid, userContext.User.CompanyId);
         if (terminal == null) return new Tuple<Terminal, string>(null, string.Format("Terminal {0} is registrated in different company.", terminalUid));
         //Create terminal if not exist
         var companyTerminal = await RegisterTerminalInUserCompany(terminalUid, terminalName);
         return new Tuple<Terminal, string>(companyTerminal.Map(), null);
      }

      private async Task<DataModel.Terminal> RegisterTerminalInUserCompany(string uid, string name)
      {
         if (string.IsNullOrEmpty(uid)) return null;

         var terminal = container.TerminalsLazy.FirstOrDefault(f => f.Uid == uid);
         if (terminal != null)
         {
            if (terminal.Name != name && !string.IsNullOrEmpty(name))
            {
               terminal.Name = name;
               await container.SaveChangesAsync();
            }
            return terminal;
         }
         terminal = new DataModel.Terminal { Uid = uid, Name = name ?? uid };
         container.Terminals.Add(terminal);
         await container.SaveChangesAsync();
         LOG.Debug("Registrated terminal {0} for company: {1}", Json(terminal.Map()), userContext.Companies.FirstOrDefault(c => c.Database == DatabaseName).Name);
         return terminal;
      }

      public async Task<Terminal> CreateTerminalIfNotExist(string terminalUid, string terminalName = null)
      {
         if (string.IsNullOrEmpty(terminalUid)) return null;
         var terminal = container.TerminalsLazy.FirstOrDefault(f => f.Uid == terminalUid);
         if (terminal != null) return terminal.Map();
         terminal = new DataModel.Terminal { Uid = terminalUid, Name = terminalName ?? terminalUid };
         container.Terminals.Add(terminal);
         await SaveChangesAsync();
         return terminal.Map();
      }

      public async Task DeleteTerminal(string uid)
      {
         var terminal = await container.TerminalsLazy.FirstOrDefaultAsync(f => f.Uid == uid);
         if (terminal == null) return;
         await userContext.FreeTerminal(uid);
         container.Terminals.Remove(terminal);
         await SaveChangesAsync();
         LOG.Debug("Deleted terminal : {0}", Json(terminal.Map()));
      }

      public async Task<Terminal> GetTerminalByDevice(int deviceId)
      {
         var device = await Devices.FirstOrDefaultAsync(f => f.Id == deviceId);
         if (device == null) return null;
         return device.Terminal.Map();
      }
   }
}
