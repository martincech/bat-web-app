﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataContext.DTOs;
using DataContext.Mapping;
using Microsoft.EntityFrameworkCore;
using Curve = DataModel.Curve;

namespace DataContext.Context
{
   public partial class BatContext
   {
      #region Bird

      public async Task<IEnumerable<Bird>> GetBirds()
      {
         var result = await container.BirdsLazy.ToListAsync();
         return result.Select(s => s.Map());
      }

      private ICollection<Curve> GetCurves(Bird config)
      {
         if (config == null) return new List<Curve>();
         var curvesIds = new List<int>();

         Action<int?> add = (val =>
         {
            if (val != null) curvesIds.Add(val.Value);
         });

         add(config.FemaleCurveId);
         add(config.MaleCurveId);
         add(config.UndefinedCurveId);
         add(config.TemperatureCurveId);
         add(config.HumidityCurveId);
         add(config.Co2CurveId);
         return container.CurvesLazy.Where(w => curvesIds.Contains(w.Id)).ToList();
      }

      public async Task DeleteBirds(ICollection<int> ids)
      {
         foreach (var id in ids)
         {
            await DeleteBird(id);
         }
      }

      public async Task DeleteBird(int id)
      {
         var bird = await container.BirdsLazy.FirstOrDefaultAsync(f => f.Id == id);
         if (bird == null) return;
         LOG.Debug("Delete bird: {0}", Json(bird.Map()));
         bird.BirdCurves.Clear();
         bird.Events.Clear();
         container.Birds.Remove(bird);
         await SaveChangesAsync();
      }

      public async Task<Bird> CreateBird(Bird value)
      {
         var newConf = value.Map();
         var curves = GetCurves(value);
         foreach (var curve in curves)
         {
            newConf.BirdCurves.Add(new DataModel.BirdCurve { Curve = curve, Bird = newConf });
         }
         container.Birds.Add(newConf);
         await SaveChangesAsync();
         var birdDto = newConf.Map();
         LOG.Debug("Created bird: {0}", Json(birdDto));
         return birdDto;
      }


      public async Task<Bird> UpdateBird(Bird value)
      {
         if (value == null) return null;
         var config = await container.BirdsLazy.FirstOrDefaultAsync(f => f.Id == value.Id);
         if (config == null)
         {
            return await CreateBird(value);
         }
         return await CopyDtoConfigurationToModel(config, value);
      }

      #region Private helpers


      private async Task<Bird> CopyDtoConfigurationToModel(DataModel.Bird model, Bird value)
      {
         var updatedConf = value.Map();
         model.Name = updatedConf.Name;
         model.SexMode = updatedConf.SexMode;
         model.Description = updatedConf.Description;
         var curves = GetCurves(value);
         model.BirdCurves.Clear();
         foreach (var curve in curves)
         {
            model.BirdCurves.Add(new DataModel.BirdCurve() { Curve = curve, Bird = model });
         }
         await SaveChangesAsync();
         return model.Map();
      }

      #endregion

      #endregion
   }
}
