﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using DataContext.DTOs;
using DataContext.DTOs.Enums;
using InfluxData.Net.InfluxDb.Models;

namespace DataContext.Context
{
   public partial class BatContext
   {
      public async Task<bool> WriteStatsForGroup(
         string groupName,
         IEnumerable<Stat> stats)
      {
         var points = new List<Point>();
         if (stats == null || !stats.Any())
         {
            return true;
         }
         var timeSize = stats.First().TimeSize;
         foreach (var stat in stats)
         {
            var point = new Point
            {
               Fields = new Dictionary<string, object>(),
               Tags = new Dictionary<string, object>(),
               Timestamp = stat.TimeStamp,
               Name = AggregationMeasurementName(
                  groupName,
                  stat.Type,
                  stat.TimeSize.MapToMinutes(), false)
            };
            point.Fields.Add(AggregationType.COUNT.ToLowerName(), stat.Count);
            if (stat.Average.HasValue)
            {
               point.Fields.Add(AggregationType.AVERAGE.ToLowerName(), stat.Average);
            }
            if (stat.Min.HasValue)
            {
               point.Fields.Add(AggregationType.MIN.ToLowerName(), stat.Min);
            }
            if (stat.Max.HasValue)
            {
               point.Fields.Add(AggregationType.MAX.ToLowerName(), stat.Max);
            }
            if (stat.Sigma.HasValue)
            {
               point.Fields.Add(AggregationType.SIGMA.ToLowerName(), stat.Sigma);
            }
            if (stat.Cv.HasValue)
            {
               point.Fields.Add(AggregationType.CV.ToLowerName(), stat.Cv);
            }
            if (stat.Uniformity.HasValue)
            {
               point.Fields.Add(AggregationType.UNIFORMITY.ToLowerName(), stat.Uniformity);
            }
            if (stat is WeightStat)
            {
               var wStat = stat as WeightStat;
               if (wStat.Day.HasValue)
               {
                  point.Fields.Add(AggregationType.DAY.ToLowerName(), wStat.Day);
               }
               if (wStat.Gain.HasValue)
               {
                  point.Fields.Add(AggregationType.GAIN.ToLowerName(), wStat.Gain);
               }
               point.Tags.Add("sex", (wStat.Sex.ToLowerName()));
            }
            points.Add(point);
         }
         var response = await influx.Client.WriteAsync(points, DatabaseName, RetentionPolicyName(timeSize.MapToMinutes(), false));
         return response.StatusCode == HttpStatusCode.OK;
      }

      public async Task<bool> AddStats(Device scale, IEnumerable<Stat> stat)
      {
         if (scale == null)
         {
            return false;
         }
         return await WriteStatsForGroup(DeviceMeasurementDescriptor(scale.Uid), stat);
      }

      public async Task<bool> AddStats(Event ev, IEnumerable<Stat> stat)
      {
         if (ev == null)
         {
            return false;
         }
         return await WriteStatsForGroup(EventMeasurementDescriptor(ev.Id), stat);
      }

      public async Task<bool> AddStats(House house, IEnumerable<Stat> stat)
      {
         if (house == null)
         {
            return false;
         }
         return await WriteStatsForGroup(HouseMeasurementDescriptor(house.Id), stat);
      }
   }
}
