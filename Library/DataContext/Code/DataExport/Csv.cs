﻿
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using DataContext.DTOs;
using DataContext.DTOs.Enums;

namespace DataContext.DataExport
{
   public class Csv
   {
      #region Private fields

      private const string DEFAULT_SEPARATOR = ";";
      private static string _separator;
      private const string N_LINE = "\r\n";
      private const string END_DATE_TIME = "23:59";

      #endregion

      #region Constructors

      public Csv(string decimals)
         : this(DEFAULT_SEPARATOR, decimals)
      {
      }

      public Csv(string separator, string decimals)
      {
         decimalFormat = "N" + decimals;
         _separator = separator;

         TemperatureUnit = "c";
         WeightUnit = "g";
      }

      #endregion

      public string TemperatureUnit { get; set; }
      public string WeightUnit { get; set; }

      private readonly string decimalFormat;
      #region Event data

      public string CreateEventStatsContent(
         Event evData,
         IEnumerable<Stat> stats)
      {

         var csv = CreateEventHeader(evData);
         var from = evData.From.Date;
         var samples = stats.ToList();

         //var dateList = CalcDateList(evData, stats);
         //var ts = CreateTimeStamps(dateList, from);
         //csv += addRow(Properties.Resources.Day, ValuesToCsvRow(ts.Days, _separator));
         //csv += addRow(Properties.Resources.Time, ValuesToCsvRow(ts.Times, _separator));

         //foreach (var sample in stats)
         //{
         //   csv += CreateEventGroup(sample, dateList);
         //}
         return csv;
      }

      public string CreateEventSamplesFileContent(
         Event evData, 
         IEnumerable<Sample> samples)
      {
         var csv = CreateEventHeader(evData);
         var from = evData.From.Date;
         //var samples = samples.ToList();
         
         //var dateList = CalcDateList(evData, samples);
         //var ts = CreateTimeStamps(dateList, from);
         //csv += addRow(Properties.Resources.Day, ValuesToCsvRow(ts.Days, _separator));
         //csv += addRow(Properties.Resources.Time, ValuesToCsvRow(ts.Times, _separator));

         //foreach (var sample in samples)
         //{
         //   csv += CreateEventGroup(group, dateList);
         //}
         return csv;
      }

      private string CreateEventHeader(Event evData)
      {
         var str = "";
         str += addRow(Properties.Resources.EventName, evData.Name);
         str += addRow(Properties.Resources.Exported, DateTime.Now.ToString(CultureInfo.CurrentCulture));
         str += addRow(Properties.Resources.StartDate, evData.From.ToString(CultureInfo.CurrentCulture));
         str += addRow(Properties.Resources.EndDate, evData.To.ToString(CultureInfo.CurrentCulture));
         return str;
      }

      private ICollection<DateTime> CalcDateList(Event evData,
         ICollection<Sample> samplesGroup, 
         AggregationInterval aggregationInterval)
      {
         return evData.GetEventDates(aggregationInterval).ToList(); // filtered samples
      }

      private TimeStamp CreateTimeStamps(IEnumerable<DateTime> dateList, DateTime from, AggregationInterval aggregationInterval)
      {
         var timeStamp = new TimeStamp();
         foreach (var date in dateList)
         {
            timeStamp.Days.Add((int)Math.Ceiling((date - from).TotalDays));
            timeStamp.Times.Add(GetSampleTime(aggregationInterval, date));
         }
         return timeStamp;
      }

      private string GetSampleTime(AggregationInterval aggregationInterval, DateTime date)
      {
         if (aggregationInterval == AggregationInterval.Day)
         {
            return END_DATE_TIME;
         }
         return date.Hour + ":" + date.Minute;
      }

      private string CreateEventGroup(Sample group, IEnumerable<DateTime> dateList)
      {
         string format;
         var name = group.Type.ToLowerName();
         if (name == null) throw new ArgumentNullException();
         var translateEnum = Properties.Resources.ResourceManager.GetString(name);
         var nameAndUnit = translateEnum + " " + SurroundByBrackets(GetSampleUnit(group.Type, out format));

         var values = "";
         foreach (var dateTime in dateList)
         {
            //var sample = group.Samples.FirstOrDefault(f => f.TimeStamp.Equals(dateTime));
            //if (sample != null)
            //{
            //   values += string.Format("{0}", sample.Value.ToString(format));
            //}
            values += _separator;
         }
         return addRow(nameAndUnit, values);
      }

      #endregion

      public string NewLine { get { return N_LINE; } }

      public HttpResponseMessage ToHttpResponse(string fileName, string csv)
      {
         var result = new HttpResponseMessage(HttpStatusCode.OK)
         {
            Content = new ByteArrayContent(System.Text.Encoding.Unicode.GetBytes(csv))
         };
         result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
         {
            FileName = fileName
         };
         result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/csv");
         return result;
      }


      #region Private helpers

      private readonly Func<string, string, string> addRow = (text, value) => text + _separator + value + N_LINE;

      private string ValuesToCsvRow<T>(IEnumerable<T> list, string delimeter)
      {
         return list == null ? "" : list.Aggregate("", (current, item) => current + (item + delimeter));
      }

      private string GetSampleUnit(SensorType type, out string decimals)
      {
         //TODO: future improvement - take units from some settings
         var unit = Properties.Resources.G;
         var format = "N0";
         switch (type)
         {
            case SensorType.WEIGHT:
               //case SensorType.RAW_WEIGHT:
               //   unit = Properties.Resources.ResourceManager.GetString(WeightUnit);//Properties.Resources.G;
               //   format = decimalFormat;
               //   break;
               //case StatType.Uniformity:
               //case StatType.Humidity:
               //   unit = Properties.Resources.Percent;
               break;
            case SensorType.TEMPERATURE:
               unit = Properties.Resources.ResourceManager.GetString(TemperatureUnit);//Properties.Resources.Celsius;
               break;
            case SensorType.CO2:
               unit = Properties.Resources.Ppm;
               break;
            default:
               throw new Exception("Unexpected Case");
         }
         decimals = format;
         return unit;
      }

      private static string SurroundByBrackets(string str)
      {
         return "[" + str + "]";
      }

      #endregion

      private class TimeStamp
      {
         public TimeStamp()
         {
            Days = new List<int>();
            Times = new List<string>();
         }

         public ICollection<int> Days { get; private set; }
         public ICollection<string> Times { get; private set; }
      }
   }
}