﻿using System.Collections.Generic;
using System.Linq;
using DataContext.DTOs;
using DataContext.DTOs.Enums;
using Veit.Bat.Common.Units.Conversion;

namespace DataContext.Convert
{
   public partial class Converter
   {
      public IEnumerable<Sample> Convert(IEnumerable<Sample> groups)
      {
         return groups.Select(Convert).ToList();
      }

      public Sample Convert(Sample sample)
      {
         if (!IsTemperatureUnitSame && sample.Type == SensorType.TEMPERATURE)
         {
            sample.Value = ConvertTemperature.Convert(sample.Value, TemperatureFrom, TemperatureTo);
         }
         if (!IsWeightUnitSame && sample.Type == SensorType.WEIGHT)
         {
            sample.Value = ConvertWeight.Convert(sample.Value, WeightFrom, WeightTo);
         }

         return sample;
      }
   }
}
