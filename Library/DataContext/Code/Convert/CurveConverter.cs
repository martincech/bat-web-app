﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataContext.DTOs.Enums;
using Veit.Bat.Common.Units;
using Veit.Bat.Common.Units.Conversion;
using Curve = DataContext.DTOs.Curve;

namespace DataContext.Convert
{
   public partial class Converter
   {
      public IEnumerable<Curve> Convert(IEnumerable<Curve> curves)
      {
         return curves.Select(Convert).ToList();
      }

      public Curve Convert(Curve curve)
      {
         if (curve.CurvePoints == null) return curve;
         if (!IsTemperatureUnitSame && curve.Type == CurveType.Temperature)
         {
            foreach (var sample in curve.CurvePoints)
            {
               sample.Min = ConvertTemperature.Convert(sample.Min, TemperatureFrom, TemperatureTo);
               sample.Max = ConvertTemperature.Convert(sample.Max, TemperatureFrom, TemperatureTo);
            }
         }
         if (!IsWeightUnitSame &&
             (curve.Type == CurveType.BirdFemale || curve.Type == CurveType.BirdMale ||
              curve.Type == CurveType.BirdUndefined || curve.Type == CurveType.Detection))
         {
            foreach (var sample in curve.CurvePoints)
            {
               sample.Value = ConvertWeight.Convert(sample.Value, WeightFrom, WeightTo);
               if (sample.Value.HasValue)
               {
                  if (WeightTo == WeightUnits.G) sample.Value = Math.Round(sample.Value.Value, 2);
                  if (WeightTo != WeightUnits.G) sample.Value = Math.Round(sample.Value.Value, 5);
               }
            }
         }

         return curve;
      }
   }
}
