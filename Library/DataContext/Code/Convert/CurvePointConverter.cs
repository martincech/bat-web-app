﻿using System;
using DataContext.DTOs.Enums;
using Veit.Bat.Common.Units;
using Veit.Bat.Common.Units.Conversion;
using CurvePoint = DataContext.DTOs.CurvePoint;

namespace DataContext.Convert
{
   public partial class Converter
   {
      public CurvePoint Convert(CurveType type, CurvePoint po)
      {
         if (po == null) return null;
         var point = new CurvePoint
         {
            Value = po.Value,
            Day = po.Day,
            Max = po.Max,
            Min = po.Min
         };

         if (!IsTemperatureUnitSame && type == CurveType.Temperature)
         {  // temperature
            point.Min = ConvertTemperature.Convert(point.Min, TemperatureFrom, TemperatureTo);
            point.Max = ConvertTemperature.Convert(point.Max, TemperatureFrom, TemperatureTo);
         }
         if (!IsWeightUnitSame &&
             (type == CurveType.BirdFemale || type == CurveType.BirdMale ||
              type == CurveType.BirdUndefined))
         {  // weight
            point.Value = ConvertWeight.Convert(point.Value, WeightFrom, WeightTo);
            if (point.Value.HasValue)
            {
               if (WeightTo == WeightUnits.G) point.Value = Math.Round(point.Value.Value, 2);
               if (WeightTo != WeightUnits.G) point.Value = Math.Round(point.Value.Value, 5);
            }
         }
         return point;
      }
   }
}
