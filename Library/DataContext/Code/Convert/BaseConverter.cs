﻿using DataContext.Mapping;
using Veit.Bat.Common.Units;

namespace DataContext.Convert
{
   public partial class Converter
   {
      public WeightUnits WeightFrom { get; set; }
      public WeightUnits WeightTo { get; set; }
      protected bool IsWeightUnitSame {
         get { return WeightFrom == WeightTo; }
      }

      public TemperatureUnits TemperatureFrom { get; set; }
      public TemperatureUnits TemperatureTo { get; set; }

      protected bool IsTemperatureUnitSame {
         get { return TemperatureFrom == TemperatureTo; } 
      }

      public void Weight(string from, string to)
      {
         WeightFrom = MapUnit.Weight(from);
         WeightTo = MapUnit.Weight(to);
      }

      public void Temperature(string from, string to)
      {
         TemperatureFrom = MapUnit.Temperature(from);
         TemperatureTo = MapUnit.Temperature(to);
      }
   }
}
