﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataContext.DTOs;
using DataContext.DTOs.Enums;
using Veit.Bat.Common.Units;
using Veit.Bat.Common.Units.Conversion;

namespace DataContext.Convert
{
   public partial class Converter
   {
      public IEnumerable<GroupData> Convert(IEnumerable<GroupData> stats, List<DataModel.Event> events = null, List<DataModel.House> houses = null)
      {
         if (stats == null)
         {
            return null;
         }
         var result = stats.Select(Convert).ToList();

         if (events == null || houses == null) return result;

         foreach (var stat in result)
         {
            UpdateWeightSexMode(stat, events, houses);
         }
         return result;
      }


      private void UpdateWeightSexMode(GroupData stat, List<DataModel.Event> events, List<DataModel.House> houses)
      {
         var undefinedWeight = String.Format("{0}_{1}", SensorType.WEIGHT.ToLowerName(), SexMode.UNDEFINED.ToLowerName());
         if (stat == null || !stat.Sensors.Contains(undefinedWeight)) return;

         DataModel.Event ev = null;
         if (stat.Group == "device") ev = events.Where(f => f.Devices.Any(a => a.Uid == stat.GroupId)).FirstOrDefault(w => w.From < DateTime.Now && w.To > DateTime.Now);
         if (stat.Group == "house")
         {
            var house = houses.FirstOrDefault(f => f.Id.ToString() == stat.GroupId);
            if (house == null) return;
            ev = house.Devices.SelectMany(s => s.Events).FirstOrDefault(w => w.From < DateTime.Now && w.To > DateTime.Now);
         }
         if (stat.Group == "event") ev = events.FirstOrDefault(f => f.Id.ToString() == stat.GroupId);
         if (ev == null) return;
         if (ev.SexMode == DataModel.SexMode.Mixed || ev.SexMode == DataModel.SexMode.Undefined) return;
         stat.Sensors = stat.Sensors.Select(f => f.Replace(DataModel.SexMode.Undefined.ToString().ToLower(), ev.SexMode.ToString().ToLower())).ToList();
      }

      public GroupData Convert(GroupData stat)
      {
         if (stat == null) return null;
         var loTypes = stat.Types.Select(t => t.ToLower()).ToList();
         var loSensors = stat.Sensors.Select(s => s.ToLower()).ToList();
         var averageIndex = loTypes.IndexOf(AggregationType.AVERAGE.ToLowerName());
         var targetIndex = loTypes.IndexOf(AggregationType.TARGET.ToLowerName());
         var minIndex = loTypes.IndexOf(AggregationType.MIN.ToLowerName());
         var maxIndex = loTypes.IndexOf(AggregationType.MAX.ToLowerName());
         var gainIndex = loTypes.IndexOf(AggregationType.GAIN.ToLowerName());
         if (!IsWeightUnitSame && loSensors.Any(a => a.IsWeightSensorString()))
         {
            foreach (var sensor in loSensors.Where(w => w.IsWeightSensorString()))
            {
               var index = loSensors.IndexOf(sensor);

               for (var i = 0; i < stat.Values[index].Count; i++)
               {
                  if (stat.Values[index][i].Count < 1) continue;
                  if (averageIndex != -1)
                  {
                     stat.Values[index][i][averageIndex] =
                        ConvertWeight.Convert(System.Convert.ToDouble(stat.Values[index][i][averageIndex]),
                           WeightUnits.G, WeightTo);
                  }
                  if (minIndex != -1)
                  {
                     stat.Values[index][i][minIndex] = ConvertWeight.Convert(System.Convert.ToDouble(stat.Values[index][i][minIndex]),
                        WeightUnits.G, WeightTo);
                  }
                  if (maxIndex != -1)
                  {
                     stat.Values[index][i][maxIndex] = ConvertWeight.Convert(System.Convert.ToDouble(stat.Values[index][i][maxIndex]),
                        WeightUnits.G, WeightTo);
                  }
                  if (gainIndex != -1)
                  {
                     stat.Values[index][i][gainIndex] = ConvertWeight.Convert(System.Convert.ToDouble(stat.Values[index][i][gainIndex]),
                        WeightUnits.G, WeightTo);
                  }
                  if (targetIndex != -1)
                  {
                     stat.Values[index][i][targetIndex] = ConvertWeight.Convert(System.Convert.ToDouble(stat.Values[index][i][targetIndex]),
                        WeightUnits.G, WeightTo);
                  }
               }
            }
         }

         if (!IsTemperatureUnitSame && loSensors.Contains(SensorType.TEMPERATURE.ToLowerName()))
         {
            var index = loSensors.IndexOf(SensorType.TEMPERATURE.ToLowerName());
            for (var i = 0; i < stat.Values[index].Count; i++)
            {
               if (averageIndex != -1)
               {
                  stat.Values[index][i][averageIndex] =
                     ConvertTemperature.Convert(System.Convert.ToDouble(stat.Values[index][i][averageIndex]),
                        TemperatureUnits.Celsius, TemperatureTo);
               }
               if (minIndex != -1)
               {
                  stat.Values[index][i][minIndex] = ConvertTemperature.Convert(System.Convert.ToDouble(stat.Values[index][i][minIndex]),
                     TemperatureUnits.Celsius, TemperatureTo);
               }
               if (maxIndex != -1)
               {
                  stat.Values[index][i][maxIndex] = ConvertTemperature.Convert(System.Convert.ToDouble(stat.Values[index][i][maxIndex]),
                     TemperatureUnits.Celsius, TemperatureTo);
               }
               if (gainIndex != -1)
               {
                  stat.Values[index][i][gainIndex] = ConvertTemperature.Convert(System.Convert.ToDouble(stat.Values[index][i][gainIndex]),
                     TemperatureUnits.Celsius, TemperatureTo);
               }
            }
         }
         return stat;
      }

      public IEnumerable<Stat> Convert(IEnumerable<Stat> stats)
      {
         if (stats == null)
         {
            return null;
         }
         return stats.Select(Convert).ToList();
      }

      public Stat Convert(Stat stat)
      {
         if (stat == null) return stat;
         if (!IsTemperatureUnitSame && stat.Type == SensorType.TEMPERATURE)
         {
            stat.Average = ConvertTemperature.Convert(stat.Average, TemperatureFrom, TemperatureTo);
            stat.Min = ConvertTemperature.Convert(stat.Min, TemperatureFrom, TemperatureTo);
            stat.Max = ConvertTemperature.Convert(stat.Max, TemperatureFrom, TemperatureTo);
         }
         return stat;
      }
   }
}
