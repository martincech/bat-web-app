﻿using System.Collections.Generic;
using System.Linq;
using DataContext.DTOs;
using Veit.Bat.Common.Units.Conversion;

namespace DataContext.Convert
{
   public partial class Converter
   {
      public IEnumerable<WeightStat> Convert(IEnumerable<WeightStat> stats)
      {
         if (stats == null)
         {
            return null;
         }
         return stats.Select(Convert).ToList();
      }

      public WeightStat Convert(WeightStat stat)
      {
         if (stat == null) return stat;
         if (!IsWeightUnitSame)
         {
            stat.Target = ConvertWeight.Convert(stat.Target, WeightFrom, WeightTo);
            stat.Gain = ConvertWeight.Convert(stat.Gain, WeightFrom, WeightTo);
            stat.Average = ConvertWeight.Convert(stat.Average, WeightFrom, WeightTo);
            stat.Sigma = ConvertWeight.Convert(stat.Sigma, WeightFrom, WeightTo);
         }
         return stat;
      }
   }
}
