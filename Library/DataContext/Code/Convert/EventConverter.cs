﻿using System.Collections.Generic;
using System.Linq;
using DataContext.DTOs;
using Veit.Bat.Common.Units.Conversion;

namespace DataContext.Convert
{
   public partial class Converter 
   {
      public IEnumerable<Event> Convert(IEnumerable<Event> events)
      {
         return events.Select(Convert).ToList();
      }

      public Event Convert(Event eve)
      {
         if (eve == null) return null;
         if (!IsWeightUnitSame)
         {
            eve.InitialWeight = ConvertWeight.Convert(eve.InitialWeight, WeightFrom, WeightTo);
            eve.InitialWeightFemale = ConvertWeight.Convert(eve.InitialWeightFemale, WeightFrom, WeightTo);
         }
         return eve;
      }
   }
}
