﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataContext.Context;
using DataContext.DTOs;
using DataContext.DTOs.Enums;
using DataContext.Generator;
using Xunit;

namespace DataContext.Tests.Generator
{
   
   public class DataGeneratorTests
   {
      private WeightStat stat;
      private ICollection<WeightSample> samples;
     
      public DataGeneratorTests()
      {
         samples = DataGenerator.CreateWeightSamples(new CurvePoint
         {
            Value = 1320,
            Day = 105,
            Max = 30,
            Min = 30
         }, 801, SexMode.MALE);      
         stat = (WeightStat) BatContext.StatisticsCalculator.CalcStatistics(samples, AggregationInterval.Day).First();
      }

      // TODO this test tests calculation of raw samples from statistics - this functionality is not used
      //[Fact]
      //public void CreateWeightSamplesFromStats()
      //{
      //   var data = DataGenerator.CreateWeightSamples(stat);
      //   var calcStats = (WeightStat) BatContext.StatisticsCalculator.CalcStatistics(data, AggregationInterval.Day).First();
      //   Assert.Equal(stat.Count, calcStats.Count);        
      //   Assert.Equal(stat.Day, calcStats.Day);
      //   Assert.Equal(stat.Gain, calcStats.Gain);
      //   Assert.Equal(stat.Sex, calcStats.Sex);
      //   Assert.Equal(stat.Average, calcStats.Average);
      //   Assert.Equal(stat.Sigma, calcStats.Sigma);
      //   Assert.Equal(stat.Cv, calcStats.Cv);
      //   Assert.Equal(stat.Uniformity, calcStats.Uniformity);

      //}

      [Fact]
      public void Bat2StdDevCalculationIsSame()
      {
         var xSuma = samples.Sum(s => s.Value);
         var x2Suma = samples.Sum(s => Math.Pow(s.Value, 2));
         var t = Math.Pow(xSuma, 2) / samples.Count;
         t = x2Suma - t;
         var stdDev = Math.Sqrt(t / (samples.Count - 1) );

         Assert.Equal(Math.Truncate(stat.Sigma.Value), Math.Truncate(stdDev));
      }

     
   }
}
