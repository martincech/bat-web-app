﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataContext.DeviceGroup;
using DataModel;
using KafkaBatLibrary;
using KafkaBatLibrary.RW;
using Xunit;
using Moq;

namespace DataContext.Tests.DeviceGroup
{
    public class GroupProducerTests
    {
        private string Database = "test";
        private GroupProducer gp;
        private List<KafkaBatLibrary.DeviceGroup> deviceGroups;
        private Mock<IKafkaSender> mockTopicSender;
        private GroupProducerTestsData mockData;

        public GroupProducerTests()
        {
            mockData = new GroupProducerTestsData();
            mockTopicSender = new Mock<IKafkaSender>();
            mockTopicSender
              .Setup(s => s.Send(It.IsAny<List<KafkaBatLibrary.DeviceGroup>>()))
              .Callback<List<KafkaBatLibrary.DeviceGroup>>(d => deviceGroups = d)
              .Returns(() => true);

            gp = new GroupProducer(Database, mockTopicSender.Object, mockData.BatModelContainer);
        }

        [Fact]
        public void UpdateByDevice_WhenNoHouseNoEvent()
        {
            Assert.True(gp.UpdateGroup(mockData.Device1.Id));
            ValidateGroup(mockData.Device1);
        }

        [Fact]
        public void UpdateByDevice_WhenHasHouseNoEvent()
        {
            Assign(mockData.Device1, null, mockData.House1);

            Assert.True(gp.UpdateGroup(mockData.Device1.Id));
            ValidateGroup(mockData.Device1);
        }

        [Fact]
        public void UpdateByDevice_WhenNoHouseHasEvent()
        {
            Assign(mockData.Device1, mockData.ActualEvent, null);

            Assert.True(gp.UpdateGroup(mockData.Device1.Id));
            ValidateGroup(
               mockData.Device1,
               CreateGroups(new[] { mockData.Device1 }, null, new[] { mockData.ActualEvent }));
        }

        [Fact]
        public void UpdateByDevice_WhenHasHouseHasEvent()
        {
            Assign(mockData.Device1, mockData.ActualEvent, mockData.House1);

            Assert.True(gp.UpdateGroup(mockData.Device1.Id));
            ValidateGroup(
               mockData.Device1,
               CreateGroups(new[] { mockData.Device1 }, new[] { mockData.House1 }, new[] { mockData.ActualEvent }));
        }

        [Fact]
        public void UpdateByDevice_WhenHasFinishedEvent()
        {
            Assign(mockData.Device1, mockData.FinishedEvent, mockData.House1);

            Assert.True(gp.UpdateGroup(mockData.Device1.Id));
            ValidateGroup(mockData.Device1);
        }

        [Fact]
        public void UpdateByDevice_WhenHasPlannedEvent()
        {
            Assign(mockData.Device1, mockData.PlannedEvent, mockData.House1);

            Assert.True(gp.UpdateGroup(mockData.Device1.Id));
            ValidateGroup(
               mockData.Device1,
               CreateGroups(new[] { mockData.Device1 }, new[] { mockData.House1 }, new[] { mockData.PlannedEvent }));
        }

        [Fact]
        public void UpdateByDevice_WhenHasFinishedAndActualAndPlannedEvent()
        {
            Assign(mockData.Device1, mockData.FinishedEvent, mockData.House1);
            Assign(mockData.Device1, mockData.ActualEvent, null);
            Assign(mockData.Device1, mockData.PlannedEvent, null);

            Assert.True(gp.UpdateGroup(mockData.Device1.Id));
            ValidateGroup(
               mockData.Device1,
               CreateGroups(
                  new[] { mockData.Device1, mockData.Device1 },
                  new[] { mockData.House1, mockData.House1 },
                  new[] { mockData.ActualEvent, mockData.PlannedEvent }));
        }

        [Fact]
        public void UpdateMultiple_WhenHasHouseHasEvent()
        {
            Assign(mockData.Device1, mockData.ActualEvent, mockData.House1);
            Assign(mockData.Device2, mockData.ActualEvent, mockData.House1);

            Assert.True(gp.UpdateGroup(new[] { mockData.Device1.Id, mockData.Device2.Id }.ToList()));

            mockTopicSender.Verify(s => s.Send(It.IsAny<List<KafkaBatLibrary.DeviceGroup>>()), Times.Once);
            Assert.NotNull(deviceGroups);
            Assert.Equal(2, deviceGroups.Count);

        }

        private void ValidateGroup(Device device, string[] groups = null)
        {
            Assert.NotNull(device);
            mockTopicSender.Verify(s => s.Send(It.IsAny<List<KafkaBatLibrary.DeviceGroup>>()), Times.Once);
            Assert.NotNull(deviceGroups);
            Assert.Single(deviceGroups);
            ValidateGrupData(device.Uid, deviceGroups.FirstOrDefault(), groups);
        }

        private void ValidateGrupData(string uid, KafkaBatLibrary.DeviceGroup deviceGroup, string[] groups)
        {
            if (groups == null) groups = new string[] { };
            Assert.NotNull(deviceGroup);
            Assert.Equal(groups.Length, deviceGroup.Groups.Count);
            Assert.Equal(uid, deviceGroup.Uid);
            Assert.Equal(Database, deviceGroup.Database);
            Assert.Empty(groups.Except(deviceGroup.Groups.Select(s => s.Group).ToList()));
        }

        private static string[] CreateGroups(Device[] devices, House[] houses, Event[] events)
        {
            var groups = new List<string>();
            if (devices != null) groups.AddRange(devices.Select(s => string.Format("device_{0}", s.Uid)));
            if (houses != null) groups.AddRange(houses.Select(s => string.Format("house_{0}", s.Id)));
            if (events != null) groups.AddRange(events.Select(s => string.Format("event_{0}", s.Id)));
            return groups.ToArray();
        }

        private static void Assign(Device device, Event ev, House house)
        {
            if (device == null) return;
            if (house != null)
            {
                device.House = house;
                if (house.Devices == null) house.Devices = new List<Device>();
                house.Devices.Add(device);
            }

            if (ev != null)
            {
                var ed = new EventDevice { Event = ev, Device = device };
                if (device.EventDevices == null) device.EventDevices = new List<EventDevice>();
                device.EventDevices.Add(ed);
                if (ev.EventDevices == null) ev.EventDevices = new List<EventDevice>();
                ev.EventDevices.Add(ed);
            }
        }
    }
}
