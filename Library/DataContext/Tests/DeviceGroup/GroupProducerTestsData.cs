﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataModel;
using Microsoft.EntityFrameworkCore;
using Moq;

namespace DataContext.Tests.DeviceGroup
{
    internal class GroupProducerTestsData
    {
        private readonly Mock<DbSet<Device>> mockDeviceSet = new Mock<DbSet<Device>>();
        private readonly Mock<DbSet<Event>> mockEventSet = new Mock<DbSet<Event>>();
        private readonly Mock<DbSet<House>> mockHouseSet = new Mock<DbSet<House>>();


        public BatModelContainer BatModelContainer { get; private set; }

        public Event FinishedEvent = new Event
        {
            Name = "Finished event",
            Id = 1,
            From = DateTime.Now.AddDays(-100),
            To = DateTime.Now.AddDays(-45)
        };

        public Event ActualEvent = new Event
        {
            Name = "Actual event",
            Id = 2,
            From = DateTime.Now.AddDays(-35),
            To = DateTime.Now.AddDays(75)
        };

        public Event PlannedEvent = new Event
        {
            Name = "Planned event",
            Id = 3,
            From = DateTime.Now.AddDays(76),
            To = DateTime.Now.AddDays(200)
        };

        public Device Device1 = new Device { Id = 1, Uid = "D1-T-T1" };
        public Device Device2 = new Device { Id = 2, Uid = "D2-T-T1" };
        public Device Device3 = new Device { Id = 3, Uid = "D3-T-T2" };

        public House House1 = new House { Id = 1 };
        public House House2 = new House { Id = 2 };

        internal GroupProducerTestsData()
        {

            var options = new DbContextOptionsBuilder<BatModelContainer>().UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;
            BatModelContainer = new BatModelContainer(options);

            BatModelContainer.Devices.Add(Device1);
            BatModelContainer.Devices.Add(Device2);
            BatModelContainer.Devices.Add(Device3);

            BatModelContainer.Houses.Add(House1);
            BatModelContainer.Houses.Add(House2);

            BatModelContainer.Events.Add(FinishedEvent);
            BatModelContainer.Events.Add(ActualEvent);
            BatModelContainer.Events.Add(PlannedEvent);


            BatModelContainer.SaveChanges();
        }
    }
}
