﻿using System;
using DataContext.Mapping;
using DataModel;
using Xunit;

namespace DataContext.Tests.UnitTests.Mapping
{
   
   public class MapEventStateTests
   {
      #region Private fields

      private EventState modelState;
      private DTOs.Enums.EventState contextState;

      private DateTime from;
      private DateTime to;

      #endregion

      #region DTO => Model tests

      [Fact]
      public void ModelPaused_IntervalBefore_DtoClosed()
      {
         SetTimeIntervalPast();
         modelState = EventState.Pause;
         var state = modelState.Map(from, to);
         Assert.Equal(DTOs.Enums.EventState.Closed, state);
      }

      [Fact]
      public void ModelPaused_IntervalInside_DtoPaused()
      {
         SetTimeIntervalActive();
         modelState = EventState.Pause;
         var state = modelState.Map(from, to);
         Assert.Equal(DTOs.Enums.EventState.Paused, state);
      }

      [Fact]
      public void ModelPaused_IntervalAfter_DtoStopped()
      {
         SetTimeIntervalFuture();
         modelState = EventState.Pause;
         var state = modelState.Map(from, to);
         Assert.Equal(DTOs.Enums.EventState.Stopped, state);
      }

      [Fact]
      public void ModelUnpaused_IntervalBefore_DtoClosed()
      {
         SetTimeIntervalPast();
         modelState = EventState.Unpause;
         var state = modelState.Map(from, to);
         Assert.Equal(DTOs.Enums.EventState.Closed, state);
      }

      [Fact]
      public void ModelUnpaused_IntervalInside_DtoActive()
      {
         SetTimeIntervalActive();
         modelState = EventState.Unpause;
         var state = modelState.Map(from, to);
         Assert.Equal(DTOs.Enums.EventState.Active, state);
      }

      [Fact]
      public void ModelUnpaused_IntervalAfter_DtoStopped()
      {
         SetTimeIntervalFuture();
         modelState = EventState.Unpause;
         var state = modelState.Map(from, to);
         Assert.Equal(DTOs.Enums.EventState.Stopped, state);
      }

      #endregion

      #region Model => DTO tests

      [Fact]
      public void DtoActive_ModelUnpause()
      {
         contextState = DTOs.Enums.EventState.Active;
         Assert.Equal(EventState.Unpause, contextState.Map());
      }

      [Fact]
      public void DtoPaused_ModelPause()
      {
         contextState = DTOs.Enums.EventState.Paused;
         Assert.Equal(EventState.Pause, contextState.Map());
      }

      [Fact]
      public void DtoStopped_ModelUnpause()
      {
         contextState = DTOs.Enums.EventState.Stopped;
         Assert.Equal(EventState.Unpause, contextState.Map());
      }

      [Fact]
      public void DtoClosed_ModelUnpause()
      {
         contextState = DTOs.Enums.EventState.Closed;
         Assert.Equal(EventState.Unpause, contextState.Map());
      }

      #endregion

      #region Private helpers

      private void SetTimeIntervalPast()
      {
         ChangeTimeIntervalInHours(-5, -2);
      }

      private void SetTimeIntervalActive()
      {
         ChangeTimeIntervalInHours(-5, 5);
      }

      private void SetTimeIntervalFuture()
      {
         ChangeTimeIntervalInHours(5, 10);
      }

      private void ChangeTimeIntervalInHours(int addFromHours, int addToHours)
      {
         var actual = DateTime.Now;
         from = actual.AddHours(addFromHours);
         to = actual.AddHours(addToHours);
      }

      #endregion
   }
}
