﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataContext.Extensions;
using DataModel;
using Xunit;

namespace DataContext.Tests.Mapping
{
   
   public class MapCurvePointTests
   {
      private List<CurvePoint> weeklyCurve;
      
      public MapCurvePointTests()
      {
         weeklyCurve = new List<CurvePoint>
         {
            new CurvePoint {Day = 1, Value = 75, Min = 4, Max = 4},
            new CurvePoint {Day = 7, Value = 529, Min = 3.97, Max = 3.97},
            new CurvePoint {Day = 14, Value = 1072, Min = 4.01, Max = 4.01},
            new CurvePoint {Day = 21, Value = 1450, Min = 4, Max = 4},
            new CurvePoint {Day = 28, Value = 1670, Min = 4.01, Max = 4.01},
            new CurvePoint {Day = 35, Value = 1718, Min = 4.02, Max = 3.96},
            new CurvePoint {Day = 42, Value = 1735, Min = 3.98, Max = 3.98},
            new CurvePoint {Day = 49, Value = 1751, Min = 4, Max = 4},
            new CurvePoint {Day = 56, Value = 1760, Min = 3.98, Max = 3.98},
            new CurvePoint {Day = 63, Value = 1769, Min = 4.01, Max = 4.01},
            new CurvePoint {Day = 70, Value = 1778, Min = 4.05, Max = 3.99},
            new CurvePoint {Day = 77, Value = 1786, Min = 3.98, Max = 4.03},
            new CurvePoint {Day = 84, Value = 1795, Min = 4.01, Max = 4.01},
         };
      }

      [Fact]
      public void GetApprox_EachDayMapped()
      {
         var mappedCurve = weeklyCurve.GetApprox();
         var fistDay = weeklyCurve.First().Day;
         var lastDay = weeklyCurve.Last().Day;
         for (var day = fistDay; day <= lastDay; day++)
         {
            Assert.Contains(mappedCurve, p => p.Day == day);   
         }
      }
      [Fact]
      public void GetApprox_ValueMappedCorrectly()
      {
         CheckAproxCurveValue(p => p.Value);
      }

      [Fact]
      public void GetApprox_MinMappedCorrectly()
      {
         CheckAproxCurveValue(p => p.Min);
      }
      [Fact]
      public void GetApprox_MaxMappedCorrectly()
      {
         CheckAproxCurveValue(p => p.Max);
      }

      private void CheckAproxCurveValue(Func<CurvePoint, double?> valueGetter)
      {
         var mappedCurve = weeklyCurve.GetApprox();
         for (var i = 1; i < weeklyCurve.Count; i++)
         {
            var prevPoint = weeklyCurve[i - 1];
            var point = weeklyCurve[i];
            var valueDiff = valueGetter?.Invoke(point) - valueGetter?.Invoke(prevPoint);
            var dayDiff = point.Day - prevPoint.Day;
            var increase = valueDiff / dayDiff;
            var firstDay = prevPoint.Day;
            for (var day = firstDay; day <= point.Day; day++)
            {
               var mappedPoint = mappedCurve.First(p => p.Day == day);
               var expectedValue = valueGetter?.Invoke(prevPoint) + (mappedPoint.Day - firstDay) * increase;
               Assert.Equal(valueGetter?.Invoke(mappedPoint), expectedValue);
            }
         }
      }
   }
}
