﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System;
using KafkaBatLibrary.Model;
using System.Text;

namespace KafkaBatLibrary
{
   public interface IStorage<T>
   {
      void Save(T data);
      T Load();
   }

   public class FileStorage : IStorage<IEnumerable<KafkaMessage>>
   {
      private readonly string file;

      public FileStorage(string file)
      {
         if (string.IsNullOrEmpty(file)) throw new ArgumentException("file attribute cannot be empty");
         this.file = file;
         if (!Directory.Exists(Path.GetDirectoryName(file)))
            Directory.CreateDirectory(Path.GetDirectoryName(file));
      }

      public IEnumerable<KafkaMessage> Load()
      {
         if (!File.Exists(file)) return new List<KafkaMessage>();
         var text = File.ReadAllText(file, Encoding.UTF8);
         return JsonConvert.DeserializeObject<List<KafkaMessage>>(text);
      }

      public void Save(IEnumerable<KafkaMessage> data)
      {
         var json = data != null ? JsonConvert.SerializeObject(data) : "";
         File.WriteAllText(file, json, Encoding.UTF8);
      }
   }
}
