﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace KafkaBatLibrary.RW
{
   public interface ICachedQueue<T> : IDisposable
   {
      List<T> Cached { get; }
      void Add(T item);
      IEnumerable<T> GetConsumingEnumerable(CancellationToken token);
      void Refresh(T item);
      void Remove(T item);
   }

   public class CachedQueue<T> : ICachedQueue<T>
   {
      private readonly IStorage<IEnumerable<T>> storage;
      private BlockingCollection<T> blockingCollection;
      private List<T> cache;
      private object myLock = new object();
      private System.Timers.Timer timer;     

      public List<T> Cached { get { return cache.ToList(); } }

      public CachedQueue(IStorage<IEnumerable<T>> storage, int cachingPeriod = 5000)
      {
         blockingCollection = new BlockingCollection<T>();
         cache = new List<T>();
         this.storage = storage;
         Load();
         timer = new System.Timers.Timer
         {
            AutoReset = true,
            Interval = cachingPeriod
         };
         timer.Elapsed += Timer_Elapsed;
         timer.Start();
      }

      public CachedQueue(IStorage<IEnumerable<T>> storage, System.Timers.Timer timer)
      {
         blockingCollection = new BlockingCollection<T>();
         cache = new List<T>();
         this.storage = storage;
         Load();
         timer.Elapsed += Timer_Elapsed;
      }

      private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
      {
         Save();
      }

      private void Load()
      {
         if (storage == null) return;
         var items = storage.Load() ?? new List<T>();
         foreach (var item in items)
         {
            Add(item);
         }
      }

      private void Save()
      {
         if (storage == null) return;
         lock (myLock)
         {
            storage.Save(cache.ToList());
         }
      }

      public void Add(T item)
      {
         lock (myLock)
         {
            cache.Add(item);
         }
         blockingCollection.Add(item);
      }

      public void Refresh(T item)
      {
         lock (myLock)
         {
            if (!cache.Contains(item)) return;
         }
         blockingCollection.Add(item);
      }

      public void Remove(T item)
      {
         lock (myLock)
         {
            cache.Remove(item);
         }
      }

      public IEnumerable<T> GetConsumingEnumerable(CancellationToken token)
      {       
         return blockingCollection.GetConsumingEnumerable(token);
      }


      public void Dispose()
      {         
         timer.Stop();        
         Save();
         timer.Dispose();        
      }
   }
}