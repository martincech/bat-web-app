﻿using NLog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Confluent.Kafka;
using KafkaBatLibrary.RW;
using KafkaBatLibrary.Model;
using System.Threading;
using Veit.Bat.Common.Sample.Co2;

namespace KafkaBatLibrary
{
   public class KafkaSender : IKafkaSender, IDisposable
   {
      private Task consumer;
      private readonly IProducer producer;
      private readonly ICachedQueue<KafkaMessage> cache;
      private readonly CancellationTokenSource cancellationTokenSource;

      private static readonly Logger LOG = LogManager.GetCurrentClassLogger();

      public bool Connected { get; private set; }

      public KafkaSender(string brokerList, string cacheFile)
         : this(
              new Producer(new Dictionary<string, object> {
                 { "bootstrap.servers", brokerList },
                 { "socket.keepalive.enable", true }
              }),
              new CachedQueue<KafkaMessage>(new FileStorage(cacheFile)),
              new CancellationTokenSource())
      {
      }

      public KafkaSender(IProducer producer, ICachedQueue<KafkaMessage> cache, CancellationTokenSource tokenSource)
      {
         cancellationTokenSource = tokenSource;
         this.producer = producer;
         producer.OnError += Producer_OnError;
         producer.OnLog += Producer_OnLog;
         this.cache = cache;
         StartConsumer();
      }

      private static void Producer_OnLog(object sender, LogMessage e)
      {
         LOG.Debug(e);
      }

      private void Producer_OnError(object sender, Error e)
      {
         if (e.Code != ErrorCode.Local_AllBrokersDown)
         {
            LOG.Warn(e);
         }
         else
         {
            LOG.Error(e);
            Connected = false;
            AddToCache(Topic.Co2.ToString(), new Co2Sample { SensorUid = "PING" });
         }
      }

      private static byte[] Serialize<T>(T item)
         => JsonSerializer.FakeUtcAsBytes(item);

      public bool Send(Co2Message message)
         => SendRoutine(message);

      public bool Send(TemperatureMessage message)
         => SendRoutine(message);

      public bool Send(HumidityMessage message)
         => SendRoutine(message);

      public bool Send(BirdWeightMessage message)
         => SendRoutine(message);

      public bool Send(RawWeightMessage message)
         => SendRoutine(message);

      public bool Send(DeviceMessage message)
         => SendRoutine(message);

      public bool Send(List<DeviceGroup> messages)
         => Send(Topic.Groups.ToString(), messages);

      public bool Send(List<DailyWeight> messages)
         => Send(Topic.DailyWeight.ToString(), messages);

      public bool Send<T, U>(T message) where T : TopicMessage<U>
        => SendRoutine(message);

      private bool SendRoutine<T>(TopicMessage<T> message)
      {
         if (message == null) return false;
         return Send(message.Topic.ToString(), message);
      }

      private bool Send<T>(string topic, List<T> messages)
      {
         if (!Connected) return false;
         foreach (var message in messages)
         {
            AddToCache(topic, message);
         }
         return true;
      }

      private bool Send<T>(string topic, T message)
      {
         if (!Connected) return false;
         AddToCache(topic, message);
         return true;
      }

      private void AddToCache<T>(string topic, T message)
      {
         var serializedMessage = Serialize(message);
         cache.Add(new KafkaMessage(topic, serializedMessage));

         LOG.Debug($"Kafka cache add: {message.ToString()}");
      }


      private void StartConsumer()
      {
         consumer = Task.Factory.StartNew(Consume, cancellationTokenSource.Token);
         Connected = true;
      }

      private void Consume()
      {
         foreach (var message in cache.GetConsumingEnumerable(cancellationTokenSource.Token))
         {
            try
            {
               if (cancellationTokenSource.IsCancellationRequested) break;
               producer.ProduceAsync(message.Topic, null, message.Message)
                  .ContinueWith((task) =>
                  {
                     var hasError = task.Result.Error.HasError;
                     LOG.Debug($"KafkaSender cache consume: has error: {hasError}");

                     if (hasError)
                     {
                        Connected = false;
                        cache.Refresh(message);
                     }
                     else
                     {
                        Connected = true;
                        cache.Remove(message);
                     }
                  });
            }
            catch (Exception e)
            {
               Connected = false;
               cache.Refresh(message);
               LOG.Error(e);
            }
         }
      }

      public void Dispose()
      {
         Dispose(true);
         GC.SuppressFinalize(this);
      }

      protected virtual void Dispose(bool disposing)
      {
         if (disposing)
         {
            cancellationTokenSource.Cancel();
            try
            {
               consumer.Wait();
            }
            catch (Exception e)
            {
               LOG.Error(e, "Cancelation of KafkaSender task.");
            }

            cancellationTokenSource.Dispose();
            consumer.Dispose();

            producer.Dispose();
            cache.Dispose();
         }
      }
   }
}
