﻿using Veit.Bat.Common;

namespace KafkaBatLibrary.Model
{
   public class CurvePoint
   {
      public int Day { get; set; }
      public double Value { get; set; }
      public Percent Min { get; set; }
      public Percent Max { get; set; }
   }
}
