﻿namespace KafkaBatLibrary.Model
{
   public enum WeighMode
   {
      Curve,
      AutomaticSlow,
      AutomaticFast
   }
}
