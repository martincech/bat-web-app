﻿using System;
using Veit.Bat.Common;

namespace KafkaBatLibrary.Model
{
   public enum SexMode : byte
   {
      Undefined,
      Male,
      Female,
      Mixed
   }

   public static class MapSexMode
   {
      public static Sex Map(this SexMode mode)
      {
         switch (mode)
         {
            case SexMode.Male:
               return Sex.Male;
            case SexMode.Female:
               return Sex.Female;
            case SexMode.Undefined:
               return Sex.Undefined;
            default:
               throw new ArgumentOutOfRangeException();
         }
      }

      public static SexMode MapFrom(this Sex sex)
      {
         switch (sex)
         {
            case Sex.Male:
               return SexMode.Male;
            case Sex.Female:
               return SexMode.Female;
            case Sex.Undefined:
               return SexMode.Undefined;
            default:
               throw new ArgumentOutOfRangeException();
         }
      }
   }
}
