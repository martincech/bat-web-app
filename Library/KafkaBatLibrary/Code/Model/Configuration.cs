﻿using System;

namespace KafkaBatLibrary.Model
{
   public class Configuration
   {
      public WeighMode WeighMode { get; set; }
      public SexMode SexMode { get; set; }
      public Curve Curve { get; set; }
      public Curve CurveFemale { get; set; }
      public double InitialWeight { get; set; }
      public double InitialWeightFemale { get; set; }
      public double DetectionRange { get; set; }
      public double DetectionRangeFemale { get; set; }

      #region Overrides of Object

      private const double TOLERANCE = 0.0001;

      public override bool Equals(object obj)
      {
         if (ReferenceEquals(null, obj)) return false;
         if (ReferenceEquals(this, obj)) return true;
         return obj.GetType() == GetType() && Equals((Configuration)obj);
      }

      private bool Equals(Configuration obj)
      {
         if (WeighMode != obj.WeighMode || SexMode != obj.SexMode) return false;
         if (WeighMode != WeighMode.Curve)
         {
            if (Math.Abs(InitialWeight - obj.InitialWeight) > TOLERANCE ||
                Math.Abs(DetectionRange - obj.DetectionRange) > TOLERANCE)
            {
               return false;
            }
            if (SexMode == SexMode.Mixed &&
                (Math.Abs(InitialWeightFemale - obj.InitialWeightFemale) > TOLERANCE ||
                 Math.Abs(DetectionRangeFemale - obj.DetectionRangeFemale) > TOLERANCE))
            {
               return false;
            }
          
         }
         if (WeighMode != WeighMode.Curve) return true;
         if (Curve == null && obj.Curve == null) return true;
         if (Curve != null && !Curve.Equals(obj.Curve)) return false;
         return SexMode != SexMode.Mixed || CurveFemale.Equals(obj.CurveFemale);
      }

      public override int GetHashCode()
      {
         var hash = 13;
         hash = (hash * 7) + WeighMode.GetHashCode();
         hash = (hash * 7) + Curve.GetHashCode();
         hash = (hash * 7) + CurveFemale.GetHashCode();
         hash = (hash * 7) + InitialWeight.GetHashCode();
         hash = (hash * 7) + InitialWeightFemale.GetHashCode();
         hash = (hash * 7) + DetectionRange.GetHashCode();
         hash = (hash * 7) + DetectionRangeFemale.GetHashCode();
         return hash;
      }

      #endregion
   }
}
