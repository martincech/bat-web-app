﻿namespace KafkaBatLibrary.Model
{
   public class KafkaMessage
   {
      public KafkaMessage() { }
      public KafkaMessage(string topic, byte[] message)
      {
         Topic = topic;
         Message = message;
      }

      public string Topic { get; set; }
      public byte[] Message { get; set; }
   }
}
