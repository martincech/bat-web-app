﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace KafkaBatLibrary.Model
{
   public class Curve
   {
      public Curve()
      {
         CurvePoints = new List<CurvePoint>();
      }

      public ICollection<CurvePoint> CurvePoints { get; set; }

      /// <summary>
      /// Get point for selected day.
      /// </summary>
      /// <param name="day">Day number</param>
      /// <returns>target weight for selected day</returns>
      public CurvePoint GetPoint(int day)
      {
         if (CurvePoints.Count == 0 || day < 0)
         {
            return null;
         }

         var list = CurvePoints.OrderBy(i => i.Day).ToList();
         var nearestPointIndex = -1;      //index of the nearest points (smaller than x)
         for (var i = 0; i < list.Count(); i++)
         {
            var point = list.ElementAt(i);
            if (point.Day < day)
            {
               nearestPointIndex = i;
            }
            else if (point.Day == day)
            {
               return point;
            }
            else
            {
               break;
            }
         }

         // condition for interpolation: x0 < x < x1
         if (nearestPointIndex < 0)
         {
            var firstValue = list.First();
            var value = Interpolate(0, 0, firstValue.Day, firstValue.Value, day);
            return new CurvePoint
            {
               Day = day,
               Value = value,
               Min = firstValue.Min,
               Max = firstValue.Max
            };
         }
         if (nearestPointIndex + 1 >= list.Count)
         {
            return list.Last();
         }

         //point doesn't exist in list, we must interpolate it
         var interpolatedValue = Interpolate(list[nearestPointIndex].Day, list[nearestPointIndex].Value,
            list[nearestPointIndex + 1].Day, list[nearestPointIndex + 1].Value, day);
         return  new CurvePoint
         {
            Day = day,
            Value = interpolatedValue,
            Min = list[nearestPointIndex].Min,
            Max = list[nearestPointIndex].Max
         };
      }

      #region Overrides of Object

      private const double TOLERANCE = 0.0001;

      public override bool Equals(object obj)
      {
         if (ReferenceEquals(null, obj)) return false;
         if (ReferenceEquals(this, obj)) return true;
         if (obj.GetType() != GetType()) return false;
         return Equals((Curve)obj);
      }

      private bool Equals(Curve obj)
      {
         if (CurvePoints.Count != obj.CurvePoints.Count) return false;

         return !CurvePoints.Where((t, i) => CurvePoints.ElementAt(i).Day != obj.CurvePoints.ElementAt(i).Day ||
               Math.Abs(CurvePoints.ElementAt(i).Value - obj.CurvePoints.ElementAt(i).Value) > TOLERANCE || 
               CurvePoints.ElementAt(i).Min != obj.CurvePoints.ElementAt(i).Min || 
               CurvePoints.ElementAt(i).Max != obj.CurvePoints.ElementAt(i).Max).Any();
      }

      public override int GetHashCode()
      {
         var hash = 13;
         hash = (hash * 7) + CurvePoints.GetHashCode();
         return hash;
      }

      #endregion

      #region Private helpers

      /// <summary>
      /// Linear interpolation
      /// </summary>
      private double Interpolate(double x0, double y0, double x1, double y1, double x)
      {
         var y = y0 + ((y1 - y0) / (x1 - x0)) * (x - x0);
         return y;
      }

      #endregion
   }
}
