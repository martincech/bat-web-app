﻿using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace KafkaBatLibrary.Model
{
   public class JsonSerializer
   {
      public static string FakeUtc<T>(T item)
         => JsonConvert.SerializeObject(item, new IsoDateTimeConverter { DateTimeFormat = "yyyy-MM-ddTHH:mm:ss.fffZ" },
               new StringEnumConverter { CamelCaseText = true }); //enum converts to lower case string

      public static byte[] GetBytes(string str)
         => Encoding.UTF8.GetBytes(str);

      public static byte[] FakeUtcAsBytes<T>(T item)
         => GetBytes(FakeUtc(item));
   }
}
