﻿using System;
using Veit.Bat.Common.Sample.Co2;
using Veit.Bat.Common.Sample.Humidity;
using Veit.Bat.Common.Sample.Temperature;
using Veit.Bat.Common.Sample.Weight;

namespace KafkaBatLibrary
{
   public static class SampleToTopicMapperExtension
   {
      public static Topic MapSample(this Type sample)
      {
         if (sample == typeof (Co2Sample))
         {
            return Topic.Co2;
         }
         if (sample == typeof(TemperatureSample))
         {
            return Topic.Temperature;
         }
         if (sample == typeof(HumiditySample))
         {
            return Topic.Humidity;
         }
         if (sample == typeof(BirdWeight))
         {
            return Topic.BirdWeight;
         }
         if (sample == typeof(WeightSample))
         {
            return Topic.RawWeight;
         }

         throw new ArgumentOutOfRangeException("Type is not corresponding to any Topic");
      }
   }
}
