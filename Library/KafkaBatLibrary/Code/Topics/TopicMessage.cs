using System.Runtime.Serialization;

namespace KafkaBatLibrary
{
   [DataContract]
   public abstract class TopicMessage<T> : Message<T>, ITopicRelated
   {
      public abstract Topic Topic { get; }

      #region Overrides of Message<T>

      public override string ToString()
      {
         return string.Format("{0} {1}", Topic, base.ToString());
      }

      #endregion
   }
}