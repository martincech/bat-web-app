using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace KafkaBatLibrary
{
   [DataContract]
   public class DeviceGroup
   {
      public DeviceGroup()
      {
         Groups = new List<StatGroup>();
      }

      [DataMember]
      public string Uid { get; set; }
      [DataMember]
      public string Database { get; set; }
      [DataMember]
      public List<StatGroup> Groups { get; set; }
   }

   public class StatGroup
   {  
      [DataMember]
      public string Group { get; set; }
      [DataMember]
      public DateTime From { get; set; }
      [DataMember]
      public DateTime To { get; set; }
   }
}