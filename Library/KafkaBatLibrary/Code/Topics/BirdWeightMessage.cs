using Veit.Bat.Common.Sample.Weight;

namespace KafkaBatLibrary
{
   public class BirdWeightMessage : TopicMessage<BirdWeight>
   {
      public override Topic Topic
      {
         get { return Topic.BirdWeight; }
      }
   }
}