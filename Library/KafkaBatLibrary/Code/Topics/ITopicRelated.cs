namespace KafkaBatLibrary
{
   public interface ITopicRelated
   {
      Topic Topic { get; }
   }
}