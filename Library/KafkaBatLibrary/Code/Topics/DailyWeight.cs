﻿using System;
using System.Runtime.Serialization;

namespace KafkaBatLibrary
{
   [DataContract]
   public class DailyWeight
   {
      /// <summary>
      /// Sensor unique id
      /// </summary>
      [DataMember]
      public string Uid;

      /// <summary>
      /// TimeStamp
      /// </summary>
      [DataMember]
      public DateTime TimeStamp;

      /// <summary>
      /// Number of birds
      /// </summary>
      [DataMember]
      public int Count;

      /// <summary>
      /// Weighing day
      /// </summary>
      [DataMember]
      public int Day;

      /// <summary>
      /// Average weight
      /// </summary>
      [DataMember]
      public double Average;     

      /// <summary>
      /// Daily gain
      /// </summary>
      [DataMember]
      public double Gain;

      /// <summary>
      /// Standard deviation
      /// </summary>
      [DataMember]
      public double Sigma;

      /// <summary>
      /// Coeficient of variation
      /// </summary>
      [DataMember]
      public double Cv;

      /// <summary>
      /// Uniformity
      /// </summary>
      [DataMember]
      public double Uniformity;

      /// <summary>
      /// Sex
      /// </summary>
      [DataMember]
      public string Sex;
   }
}
