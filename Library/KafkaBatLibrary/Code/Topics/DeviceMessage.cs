﻿using Veit.Bat.Common.DeviceStatusInfo;

namespace KafkaBatLibrary
{
   public class DeviceMessage : TopicMessage<DeviceConnectivityInfo>
   {
      public override Topic Topic
      {
         get { return Topic.Device; }
      }
   }
}
