using Veit.Bat.Common.Sample.Weight;

namespace KafkaBatLibrary
{
   public class RawWeightMessage : TopicMessage<WeightSample>
   {
      public override Topic Topic
      {
         get { return Topic.RawWeight; }
      }
   }
}