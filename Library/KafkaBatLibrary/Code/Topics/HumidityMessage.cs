using Veit.Bat.Common.Sample.Humidity;

namespace KafkaBatLibrary
{
   public class HumidityMessage : TopicMessage<HumiditySample>
   {
      public override Topic Topic
      {
         get { return Topic.Humidity; }
      }
   }
}