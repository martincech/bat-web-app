﻿using System.Runtime.Serialization;

namespace KafkaBatLibrary
{
   [DataContract]
   public class KafkaBatMessage
   {
      [DataMember(Name = "TerminalSn", Order = 1)]
      public string TerminalId { get; set; }
   }
   [DataContract]
   public class Message<T> : KafkaBatMessage
   {
      [DataMember(Name = "Data", Order = 3)]
      public T Value { get; set; }
    
      public override string ToString()
      {
         return string.Format("T:{0} {1}", TerminalId, Value);
      }
   }
}
