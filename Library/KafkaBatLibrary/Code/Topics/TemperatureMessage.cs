using Veit.Bat.Common.Sample.Temperature;

namespace KafkaBatLibrary
{
   public class TemperatureMessage : TopicMessage<TemperatureSample>
   {
      public override Topic Topic
      {
         get { return Topic.Temperature; }
      }
   }
}