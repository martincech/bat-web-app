using Veit.Bat.Common.Sample.Co2;

namespace KafkaBatLibrary
{
   public class Co2Message : TopicMessage<Co2Sample>
   {
      public override Topic Topic
      {
         get {return Topic.Co2;}
      }
   }
}