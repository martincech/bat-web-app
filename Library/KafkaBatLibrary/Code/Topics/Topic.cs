﻿namespace KafkaBatLibrary
{
   public enum Topic
   {
      Co2,
      Temperature,
      Humidity,
      BirdWeight,
      RawWeight,
      Device,
      Groups,
      DailyWeight
   }
}
