﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Confluent.Kafka;

namespace KafkaBatLibrary
{
   public interface IProducer : IDisposable
   {
      event EventHandler<LogMessage> OnLog;
      event EventHandler<Error> OnError;
      Task<Message> ProduceAsync(string topic, byte[] key, byte[] val);
   }

   public class Producer : Confluent.Kafka.Producer, IProducer
   {
      public Producer(IEnumerable<KeyValuePair<string, object>> config) : base(config)
      {
      }
   }
}
