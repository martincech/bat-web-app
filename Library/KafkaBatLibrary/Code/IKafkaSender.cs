﻿using System.Collections.Generic;

namespace KafkaBatLibrary
{
   public interface IKafkaSender
   {
      bool Send(Co2Message message);
      bool Send(TemperatureMessage message);
      bool Send(HumidityMessage message);
      bool Send(BirdWeightMessage message);
      bool Send(RawWeightMessage message);
      bool Send(DeviceMessage message);
      bool Send(List<DeviceGroup> messages);
      bool Send(List<DailyWeight> messages);
      bool Send<T, U>(T message) where T : TopicMessage<U>;
   }
}