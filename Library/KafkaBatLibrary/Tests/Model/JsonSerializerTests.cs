﻿using System;
using KafkaBatLibrary.Model;
using Xunit;

namespace KafkaBatLibrary.Tests.Model
{
   
   public class JsonSerializerTests
   {
      [Fact]
      public void FakeUtctTimeStamp_TimeZoneUnspecified()
      {
         const int year = 2018;
         const int month = 1;
         const int day = 11;
         const int hour = 10;
         const int minute = 32;
         const int second = 0;
         const int millisecond = 99;

         //unspecified time zone
         var timeStampStr = AlignNumber(day) + "." + AlignNumber(month) + "." + year + " " + AlignNumber(hour) + ":" +
                            AlignNumber(minute) + ":" + AlignNumber(second) + "." + millisecond.ToString("000");
         
         DateTime timeStamp;
         DateTime.TryParse(timeStampStr, out timeStamp);

         CheckUtcFormat(timeStamp);
      }

      [Fact]
      public void FakeUtctTimeStamp_TimeZoneLocal()
      {
         var timeStamp = DateTime.Now;
         Assert.Equal(DateTimeKind.Local, timeStamp.Kind);

         CheckUtcFormat(timeStamp);
      }

      #region Private helpers

      private void CheckUtcFormat(DateTime timeStamp)
      {
        

         var actual = JsonSerializer.FakeUtc(timeStamp);
         var expected = "\"" +
                        GetUtcFormat(timeStamp.Year, timeStamp.Month, timeStamp.Day, timeStamp.Hour, timeStamp.Minute,
                           timeStamp.Second, timeStamp.Millisecond) + "\"";

         Assert.Equal(expected, actual);
      }

      //return "yyyy-MM-ddThh:mm:ss.fffZ"
      private string GetUtcFormat(int year, int month, int day, int hour, int minute, int second, int millisecond)
      {
         return year + "-" + AlignNumber(month) + "-" + AlignNumber(day) + "T" + AlignNumber(hour) + ":" +
                AlignNumber(minute) + ":" + AlignNumber(second) + "."+ millisecond.ToString("000")+ "Z";
      }

      private static string AlignNumber(int number)
      {
         return number.ToString("00");
      }

      #endregion

   }
}
