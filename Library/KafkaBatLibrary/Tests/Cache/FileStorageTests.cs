﻿using KafkaBatLibrary.Model;
using Xunit;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System;

namespace KafkaBatLibrary.Tests.Cache
{
   
   public class FileStorageTests : IDisposable
   {
      private const string FILE = "fileStorageTest.txt";
      private const string FOLDER = "fileStorageTestFolder";

      private string FilePath => Path.Combine(FOLDER, FILE);

      private FileStorage fileStorage;

      string messagesString = "[{\"Topic\":\"Topic1\",\"Message\":\"AQIDBAU=\"},{\"Topic\":\"Topic2\",\"Message\":\"BQQDAgE=\"}]";
      private List<KafkaMessage> messages = new List<KafkaMessage>
      {
            new KafkaMessage("Topic1", new byte []{ 1,2,3,4,5 }),
            new KafkaMessage("Topic2", new byte []{ 5,4,3,2,1 })
      };

     
      public void Dispose()
      {
         if (File.Exists(FilePath)) File.Delete(FilePath);
         if (Directory.Exists(FOLDER)) Directory.Delete(FOLDER);
      }

      [Fact]
      public void Save_CreateFolderIfNotExist()
      {
         Assert.False(Directory.Exists(FOLDER));
         fileStorage = new FileStorage(FilePath);
         fileStorage.Save(messages);
         Assert.True(Directory.Exists(FOLDER));
      }

      [Fact]
      public void Save_SavedToFile()
      {
         Assert.False(File.Exists(FilePath));
         fileStorage = new FileStorage(FilePath);
         fileStorage.Save(messages);
         Assert.True(File.Exists(FilePath));
         var file = File.ReadAllText(FilePath, Encoding.UTF8);       
         Assert.Equal(messagesString, file);
      }

      [Fact]
      public void Load_LoadedFromFile()
      {
         fileStorage = new FileStorage(FilePath);
         File.WriteAllText(FilePath, messagesString, Encoding.UTF8);
         Assert.True(File.Exists(FilePath));
         var loadedMessages = fileStorage.Load().ToList();
         Assert.Equal(2, loadedMessages.Count());
         messages.ForEach(m =>
         {
            var message = loadedMessages.FirstOrDefault(f => f.Topic == m.Topic);
            Assert.NotNull(message);
            Assert.True(m.Message.SequenceEqual(message.Message));
         });
      }
   }
}
