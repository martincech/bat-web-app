﻿using KafkaBatLibrary.RW;
using Xunit;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace KafkaBatLibrary.Tests.Cache
{
   
   public class CachedQueueTests
   {
      private CachedQueue<string> cache;
      private Mock<IStorage<IEnumerable<string>>> storageMock;
      private string[] testStrings = new[] { "Text1", "Text2", "Text3", "Text4" };

     
      public CachedQueueTests()
      {
         storageMock = new Mock<IStorage<IEnumerable<string>>>();
         cache = new CachedQueue<string>(storageMock.Object);
      }


      [Fact]
      public void Add_ConsumeItems()
      {
     
         var task = Task.Factory.StartNew(() =>
          {
             var i = 0;
             foreach (var item in cache.GetConsumingEnumerable(CancellationToken.None))
             {

                Assert.Equal(testStrings[i++], item);
                if (i == testStrings.Length - 1) break;
             }
          });
          testStrings.ToList().ForEach(f => cache.Add(f));
         Assert.True(task.Wait(500));
      }

      [Fact]
      public void Add_RemoveConsumedFromCache()
      {         
         var task = Task.Factory.StartNew(() =>
         {
            var i = 0;
            foreach (var item in cache.GetConsumingEnumerable(CancellationToken.None))
            {
               Assert.Equal(testStrings[i++], item);
               Assert.Contains(item, cache.Cached);
               cache.Remove(item);
               Assert.DoesNotContain(item, cache.Cached);
               if (i == testStrings.Length) break;
            }
         });
         testStrings.ToList().ForEach(f => cache.Add(f));
         Assert.True(task.Wait(500));
         Assert.False(cache.Cached.Any());
      }

      [Fact]
      public void Add_RefreshConsumedFromCache()
      {
         var text = "Text1";
         var i = 0;
         var task = Task.Factory.StartNew(() =>
         {
            foreach (var item in cache.GetConsumingEnumerable(CancellationToken.None))
            {
               Assert.Equal(text, item);
               Assert.True(cache.Cached.Count == 1);
               Assert.Contains(item, cache.Cached);
               cache.Refresh(item);
               i++;
               if (i == 5) break;
            }
         });
         cache.Add(text);
         Assert.True(task.Wait(500));
         Assert.Equal(5, i);
      }     
   }
}
