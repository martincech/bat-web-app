﻿using Amazon.CognitoIdentityProvider.Model;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using UsersDataModel;

namespace CognitoUsers
{
   internal static class UserIdentityExtension
   {
      public const string ID = "sub";
      public const string NAME = "name";
      public const string EMAIL = "email";
      public const string EMAIL_VERIFIED = "email_verified";
      public const string PHONE = "phone_number";
      public const string NOTE = "custom:note";

      private static readonly Logger LOG = LogManager.GetCurrentClassLogger();

      public static KeyValuePair<UserType, User>? Map(this UserIdentity source)
      {
         if (source == null) return null;
         var user = source.MapUser();
         var userType = source.MapUserType();
         return new KeyValuePair<UserType, User>(userType, user);
      }


      public static User MapUser(this UserIdentity source)
      {
         if (source == null) return null;
         return new User
         {
            Id = source.Id,
            UserName = source.UserName,
            Company = source.Company,
            CompanyId = source.CompanyId,
            LastLoginDate = source.LastLoginDate,
            UserDevices = source.UserDevices,
            UserRoles = source.UserRoles,
            Settings = source.Settings
         };        
      }

      public static UserType MapUserType(this UserIdentity source)
      {
         if (source == null) return null;
         return new UserType
         {
            Enabled = source.Enabled,
            Username = source.UserName,
            UserStatus = source.UserStatus,
            Attributes = new List<AttributeType>
            {
               new AttributeType { Name = ID, Value = source.Id.ToString() },
               new AttributeType { Name = NAME, Value = source.Name },
               new AttributeType { Name = EMAIL, Value = source.Email },
               new AttributeType { Name = NOTE, Value = source.Note },
               new AttributeType { Name = PHONE, Value = source.Phone }
            }
         };
      }

      public static UserIdentity Map(this UserType source, User user) => user.Map(source);
      public static UserIdentity Map(this User source, UserType userType)
      {
         if (source == null && userType == null) return null;
         var user = new UserIdentity();

         if (source != null)
         {
            user.Id = source.Id;
            user.UserRoles = source.UserRoles;
            user.LastLoginDate = source.LastLoginDate;
            user.Settings = source.Settings;
            user.UserDevices = source.UserDevices;
            user.Company = source.Company;
            user.CompanyId = source.CompanyId;
         }

         if (userType != null)
         {
            user.Id = user.Id == Guid.Empty ? userType.Attributes.MapAttr<Guid>(ID) : user.Id;
            user.UserName = userType.Username;
            user.UserStatus = userType.UserStatus;
            user.Enabled = userType.Enabled;
            user.Note = userType.Attributes.MapAttr(NOTE);
            user.Phone = userType.Attributes.MapAttr(PHONE);
            user.Email = userType.Attributes.MapAttr(EMAIL);
            user.Name = userType.Attributes.MapAttr(NAME);
         }

         return user;
      }

      public static string MapAttr(this List<AttributeType> attributtes, string attributeName)
         => attributtes.MapAttr<string>(attributeName);

      public static T MapAttr<T>(this List<AttributeType> attributtes, string attributeName)
      {
         if (attributtes != null)
         {
            try
            {
               var value = attributtes.FirstOrDefault(f => f.Name == attributeName);
               if (value != null) return ConvertValue<T>(value.Value);
            }
            catch (Exception e)
            {
               LOG.Debug(e);
            }
         }
         return default(T);
      }

      public static T ConvertValue<T>(string value)
      {
         return (T)TypeDescriptor.GetConverter(typeof(T)).ConvertFromInvariantString(value);
      }
   }
}
