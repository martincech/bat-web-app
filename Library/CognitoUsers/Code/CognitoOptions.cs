﻿namespace CognitoUsers
{
   public class CognitoOptions
   {
      public string UserPoolId { get; set; }
      public string Region { get; set; }
      public string IAMId { get; set; }
      public string IAMSecret { get; set; }
   }
}
