﻿using UsersDataModel;

namespace CognitoUsers
{
   public class UserIdentity : User
   {
      public string Name { get; set; }

      public bool Enabled { get; set; }

      public string UserStatus { get; set; }

      public string Email { get; set; }

      public string Note { get; set; }

      public string Phone { get; set; }
   }
}
