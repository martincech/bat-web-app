﻿using Amazon;
using Amazon.CognitoIdentityProvider;
using Amazon.CognitoIdentityProvider.Model;
using Microsoft.EntityFrameworkCore;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using UsersDataModel;
using UsersDataModel.DTO;
using UsersDataModel.Infrastructure;

namespace CognitoUsers
{
   public class CognitoUserManager : UserStore
   {
      private static readonly Logger LOG = LogManager.GetCurrentClassLogger();
      private readonly UserModelContainer db;
      private readonly IAmazonCognitoIdentityProvider client;
      private readonly string userPoolId;

      public CognitoUserManager(UserModelContainer db, CognitoOptions options)
      : this(db, options.IAMId, options.IAMSecret, options.Region, options.UserPoolId)
      {
      }

      public CognitoUserManager(UserModelContainer db, string awsAccessKeyId, string awsSecretAccessKey, string endpoint, string userPoolId)
      : this(db, new AmazonCognitoIdentityProviderClient(awsAccessKeyId, awsSecretAccessKey, RegionEndpoint.GetBySystemName(endpoint)), userPoolId)
      {
      }

      public CognitoUserManager(UserModelContainer db, IAmazonCognitoIdentityProvider client, string userPoolId)
         : base(db)
      {
         this.client = client;
         this.userPoolId = userPoolId;
         this.db = db;
      }

      public DbSet<User> Users => db.Users;

      public async Task<List<UserIdentity>> GetUsersAsync(List<User> userList)
      {

         if (userList == null) return new List<UserIdentity>();
         var response = await client.ListUsersAsync(new ListUsersRequest { UserPoolId = userPoolId });
         var ids = userList.Select(s => s.Id);
         var users = response.Users.ToDictionary(k => Guid.Parse(k.Attributes.First(s => s.Name == "sub").Value), v => v);

         var companyUsers = new List<UserIdentity>();
         foreach (var user in userList)
         {
            if (!users.ContainsKey(user.Id)) continue;
            companyUsers.Add(users[user.Id].Map(user));
         }

         return companyUsers;
      }

      public async Task<Dictionary<string, string>> GetAllCognitoUsers()
      {
         var response = await client.ListUsersAsync(new ListUsersRequest { UserPoolId = userPoolId });
         return response.Users.ToDictionary(
            k => k.Attributes.FirstOrDefault(s => s.Name == "sub")?.Value,
            v => v.Attributes.FirstOrDefault(s => s.Name == "email")?.Value);
      }

      public async Task<UserType> GetCognitoUserAsync(Guid userId)
      {
         try
         {
            var user = await client.ListUsersAsync(new ListUsersRequest { UserPoolId = userPoolId, Filter = $"sub=\"{userId}\"", Limit = 1 });
            return user.Users.FirstOrDefault();
         }
         catch (Exception e)
         {
            LOG.Error(e);
         }
         return null;
      }


      public async Task<UserIdentity> GetUserAsync(Guid userId)
      {
         var cognito = await GetCognitoUserAsync(userId);
         if (cognito == null) return null;
         var id = cognito.Attributes.MapAttr<Guid>("sub");
         var user = await FindByIdAsync(id.ToString(), CancellationToken.None);
         return user.Map(cognito);
      }

      public async Task<UserType> GetCognitoAsync(string username)
      {
         try
         {
            var result = await client.AdminGetUserAsync(new AdminGetUserRequest { Username = username, UserPoolId = userPoolId });
            return result.HttpStatusCode != System.Net.HttpStatusCode.OK ? null : new UserType
            {
               Attributes = result.UserAttributes,
               Enabled = result.Enabled,
               Username = result.Username,
               UserStatus = result.UserStatus
            };
         }
         catch (UserNotFoundException)
         {
            return null;
         }
         catch (Exception e)
         {
            LOG.Error(e);
         }
         return null;
      }

      public async Task<UserIdentity> CreateUserAsync(string username, Guid? companyId = null, RoleE role = RoleE.Worker)
      {
         try
         {
            if (string.IsNullOrEmpty(username)) throw new InvalidParameterException($"{nameof(username)} cannot be empty");
            if (!companyId.HasValue) throw new InvalidParameterException($"{nameof(companyId)} cannot be null");
            var cognito = await CreateCognitoAsync(username);
            var sub = cognito?.Attributes.FirstOrDefault(f => f.Name == "sub");
            if (sub == null) return null;

            var user = await CreateLocalUserAsync(Guid.Parse(sub.Value), companyId.Value, role);
            if (user == null)
            {
               LOG.Debug($"Rollback cognito user: {sub.Value} | {username}");
               await DeleteCognitoAsync(sub.Value);
            }
            else
            {
               return user.Map(cognito);
            }
         }
         catch (Exception e)
         {
            LOG.Error(e);
         }
         return null;
      }


      public async Task<User> CreateLocalUserAsync(Guid userId, Guid companyId, RoleE role = RoleE.Worker)
      {
         try
         {

            var user = new User
            {
               Id = userId,
               CompanyId = companyId,
               UserName = userId.ToString()
            };

            var result = await CreateAsync(user, CancellationToken.None);
            if (result.Succeeded)
            {
               await AddToRoleAsync(user, role.ToString(), CancellationToken.None);
               return user;
            }
            else
            {
               return null;
            }
         }
         catch (Exception e)
         {
            LOG.Error(e);
         }
         return null;
      }

      public async Task<UserIdentity> UpdateUserAsync(UserDto source)
      {
         var user = await FindByIdAsync(source.Id.ToString(), CancellationToken.None);
         if (user == null) return null;
         try
         {
            if (!await IsInRoleAsync(user, source.Role, CancellationToken.None))
            {
               if (user.Roles.Any())
               {
                  foreach (var role in user.Roles.Select(s => s.Name).ToArray())
                  {
                     await RemoveFromRoleAsync(user, role, CancellationToken.None);
                  }
               }
               await AddToRoleAsync(user, source.Role, CancellationToken.None);
            }

            var oldDeviceList = user.Devices.Select(s => s.DeviceId).ToList();

            var toAdd = source.Devices.Except(oldDeviceList).ToList();
            var toRemove = oldDeviceList.Except(source.Devices).ToList();

            if (toRemove.Any())
            {
               foreach (var removeDevice in toRemove)
               {
                  var device = user.UserDevices.FirstOrDefault(f => f.Device.DeviceId == removeDevice);
                  if (device != null) user.UserDevices.Remove(device);
               }
            }

            if (toAdd.Any())
            {
               foreach (var addDevice in toAdd)
               {
                  var device = db.Devices.FirstOrDefault(d => d.DeviceId == addDevice) ?? new Device { DeviceId = addDevice };
                  user.UserDevices.Add(new UserDevice { Device = device, User = user });
               }
            }

            var r = await UpdateAsync(user, CancellationToken.None);

            var attrs = new Dictionary<string, string>
            {
               { "name",  source.Name ?? source.Email },
               { "phone_number",  source.Phone ?? "" },
               { "custom:note",  source.Comment ?? "" }
            };

            var result = await UpdateCognitoUserAttributesAsync(source.Email, attrs);
         }
         catch (Exception e)
         {
            LOG.Error(e);
            return null;
         }
         return user.Map(await GetCognitoAsync(source.Email));
      }

      public async Task<bool> UpdateCognitoUserAttributesAsync(string userName, Dictionary<string, string> attributes)
      {
         try
         {
            var p = new AdminUpdateUserAttributesRequest
            {
               UserPoolId = userPoolId,
               Username = userName,
               UserAttributes = attributes.Select(s => new AttributeType { Name = s.Key, Value = s.Value ?? "" }).ToList()
            };
            var result = await client.AdminUpdateUserAttributesAsync(p);
            return result.HttpStatusCode == System.Net.HttpStatusCode.OK;
         }
         catch (Exception e)
         {
            LOG.Error(e);
            return false;
         }
      }

      public async Task<UserType> CreateCognitoAsync(string username)
      {
         try
         {
            var result = await client.AdminCreateUserAsync(new AdminCreateUserRequest
            {
               UserPoolId = userPoolId,
               Username = username,
               MessageAction = MessageActionType.SUPPRESS,
               UserAttributes = new List<AttributeType>
               {
                  new AttributeType{ Name = UserIdentityExtension.NAME, Value = username },
                  new AttributeType{ Name = UserIdentityExtension.EMAIL, Value = username },
                  new AttributeType{ Name = UserIdentityExtension.EMAIL_VERIFIED, Value = "true" }
               }
            });
            return result.HttpStatusCode == System.Net.HttpStatusCode.OK ? result.User : null;
         }
         catch (Exception e)
         {
            LOG.Error(e);
         }
         return null;
      }


      public async Task<bool> DeleteUserAsync(Guid userId, string userName)
      {
         var success = false;
         try
         {
            await DeleteCognitoAsync(userName);
            var user = await FindByIdAsync(userId.ToString(), CancellationToken.None);
            if (user != null)
            {

               var result = await DeleteAsync(user, CancellationToken.None);
               success = result.Succeeded;
            }
            else
            {
               success = true;
            }
         }
         catch (Exception e)
         {
            LOG.Error(e);
         }
         return success;
      }

      private async Task<bool> DeleteCognitoAsync(string username)
      {
         var success = false;
         try
         {
            var result = await client.AdminDeleteUserAsync(new AdminDeleteUserRequest
            {
               UserPoolId = userPoolId,
               Username = username
            });
            success = result.HttpStatusCode == System.Net.HttpStatusCode.OK;
         }
         catch (UserNotFoundException)
         {
            return true;
         }
         catch (Exception e)
         {
            LOG.Error(e);
         }
         return success;
      }


      public async Task<bool> ResetUserPasswordAsync(string userName)
      {
         try
         {
            var user = await GetCognitoAsync(userName);
            if (user == null) return false;
            var identity = user.Map(new User());
            var result = await client.AdminCreateUserAsync(new AdminCreateUserRequest { Username = identity.Email, UserPoolId = userPoolId, MessageAction = MessageActionType.RESEND });
            return result.HttpStatusCode == System.Net.HttpStatusCode.OK;

         }
         catch (Exception e)
         {
            LOG.Error(e);
         }
         return false;
      }

      public async Task<bool> DeleteCompanyAndItsUsers(Guid companyId)
      {
         var company = db.Companies.FirstOrDefault(f => f.Id == companyId);
         if (company == null) return false;
         LOG.Debug($"Delete company : {company.Name} | ID: {company.Id} | DB: {company.Database} ");
         foreach (var user in company.Users.ToList())
         {
            if (!user.Roles.Any(u => u.Name == RoleE.Veit.ToString()))
            {
               await DeleteUserAsync(user.Id, user.Id.ToString());
            }
         }

         try
         {
            company.Users.Clear();
            db.Terminals.RemoveRange(company.Terminals);
            company.Terminals.Clear();
            db.Companies.Remove(company);
            await db.SaveChangesAsync();
         }
         catch (Exception e)
         {
            LOG.Error(e, $"Failed to delete company with ID : {companyId}");
            return false;
         }

         return true;
      }

      public static UserDto Map(UserIdentity source)
      {
         if (source == null) return null;

         return new UserDto
         {
            Id = source.Id,
            Email = source.Email,
            Comment = source.Note,
            CompanyId = source.CompanyId,
            Devices = source.Devices?.Select(s => s.DeviceId).ToList(),
            Name = source.Name,
            Phone = source.Phone,
            Role = source.Roles?.FirstOrDefault()?.Name,
            Enabled = source.Enabled,
            UserStatus = source.UserStatus
         };
      }
   }
}
