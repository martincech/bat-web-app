﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;

namespace TokenService
{
   public static class JwtSettingsExtensions
   {
      private static readonly Logger LOG = LogManager.GetCurrentClassLogger();
      public static JwtSecurityToken Validate(this Dictionary<string, JwtSettings> jwks, string token)
      {
         if (string.IsNullOrEmpty(token)) return null;
         JwtSecurityToken validatedToken = null;
         try
         {
            if (jwks == null || !jwks.Any()) throw new ArgumentException("Json web keys collection cannot be empty.");

            var tokenHandler = new JwtSecurityTokenHandler();
            if (tokenHandler.CanReadToken(token))
            {
               var tokenObject = new JwtSecurityToken(token);
               JwtSettings jwk = null;
               if (jwks.TryGetValue(tokenObject.Header.Kid, out jwk))
               {
                  var principal = new JwtSecurityTokenHandler()
                     .ValidateToken(token, jwk.TokenValidationParameters, out var rawValidatedToken);
                  validatedToken = (JwtSecurityToken)rawValidatedToken;
               }
            }
         }
         catch (ArgumentException ex)
         {
            LOG.Error(ex);
         }
         catch (Exception ex)
         {
            LOG.Debug($"Invalid token : { ex?.Message}");
         }
         return validatedToken;
      }

      public static List<Claim> GetValidClaims(this Dictionary<string, JwtSettings> jwks, string token)
         => Validate(jwks, token)?.Claims.ToList();

      public static Dictionary<string, JwtSettings> GetJsonWebKeys(string issuer, string path = "/.well-known/jwks.json")
      {
         LOG.Debug($"Get json web keys from : {issuer + path}");
         var keys = new Dictionary<string, JwtSettings>();
         using (WebClient wc = new WebClient())
         {
            try
            {
               var json = wc.DownloadString(issuer + path);
               var jwks = JsonConvert.DeserializeObject<JWKS>(json);
               keys = jwks.Keys.ToDictionary(j => j.KeyId, v => new JwtSettings { Expo = v.Exponent, Issuer = issuer, Key = v.Modulus });
            }
            catch (Exception e)
            {
               LOG.Error(e, "Failed to load json web keys.");
            }
         }
         return keys;
      }
   }

   internal class JWKS
   {
      [JsonProperty(PropertyName = "keys")]
      public List<JWK> Keys { get; set; }
   }

   internal class JWK
   {
      [JsonProperty(PropertyName = "alg")]
      public string Algorithm { get; set; }

      [JsonProperty(PropertyName = "e")]
      public string Exponent { get; set; }

      [JsonProperty(PropertyName = "kid")]
      public string KeyId { get; set; }

      [JsonProperty(PropertyName = "kty")]
      public string KeyType { get; set; }

      [JsonProperty(PropertyName = "n")]
      public string Modulus { get; set; }

      [JsonProperty(PropertyName = "use")]
      public string KeyUse { get; set; }
   }
}

