﻿using System;
using System.IO;
using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Transfer;

namespace S3Bucket
{
   public class S3
   {
      private const string BUCKET_NAME = 
#if !DEBUG
         "diagdata.veit.cz";
#else
         "diagdata-stg.veit.cz";
#endif

      private const string UP_ACCESS_KEY = "AKIAJLWLNJRYZMOSMFGQ";
      private const string UP_SECRET_KEY = "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";

      public void Put(string filePath, string name)
      {
         var creds = new BasicAWSCredentials(UP_ACCESS_KEY, UP_SECRET_KEY);
         using (var fileTransferUtility = new TransferUtility(new AmazonS3Client(creds, RegionEndpoint.EUCentral1)))
         {
            fileTransferUtility.Upload(filePath, BUCKET_NAME, FormatKeyName(filePath, name));
         }
      }


      private string FormatKeyName(string file, string name)
      {
         var extension = Path.GetExtension(file);
         var timeStamp = DateTime.Now.ToString("O");  //to ISO 8601
         return string.Format("upload/{0}_{1}{2}", name, timeStamp, extension);
      }
   }
}
