﻿using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace S3Bucket
{
   public class S3File
   {
      protected static readonly Logger LOG = LogManager.GetCurrentClassLogger();

      private IAmazonS3 client;

      public S3File(string accessKey, string secretKey, string region)
      {
         client = new AmazonS3Client(new BasicAWSCredentials(accessKey, secretKey), RegionEndpoint.GetBySystemName(region));
      }

      public async Task<KeyValuePair<string, string>> ReadAsync(string bucketName, string keyName, string lastEtag = null)
      {
         try
         {
            var request = new GetObjectRequest
            {
               BucketName = bucketName,
               Key = keyName,
               EtagToNotMatch = lastEtag
            };
            using (GetObjectResponse response = await client.GetObjectAsync(request))
            using (Stream responseStream = response.ResponseStream)
            using (StreamReader reader = new StreamReader(responseStream))
            {
               if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
               {
                  return new KeyValuePair<string, string>(response.ETag, await reader.ReadToEndAsync());
               }
            }
         }
         catch (AmazonS3Exception e)
         {
            if (e.StatusCode != System.Net.HttpStatusCode.NotModified)
            {
               LOG.Error(e);
            }
         }
         catch (Exception e)
         {
            LOG.Error(e);
         }
         return new KeyValuePair<string, string>(lastEtag, null);
      }

      public async Task<string> WriteAsync(string bucketName, string keyName, string content)
      {
         LOG.Debug($"Update content of {bucketName}/{keyName}.");
         try
         {
            var response = await client.PutObjectAsync(new PutObjectRequest
            {
               BucketName = bucketName,
               Key = keyName,
               ContentBody = content
            });
            return response.HttpStatusCode == System.Net.HttpStatusCode.OK ? response.ETag : null;
         }
         catch (Exception e)
         {
            LOG.Error(e);
         }
         return null;
      }
   }
}
