﻿using Newtonsoft.Json;

namespace Heartbeat
{
   public class UserInfo
   {
      public OSInfo OS { get; set; }
      public AppInfo App { get; set; }
      public override string ToString()
      {
         return JsonConvert.SerializeObject(this);
      }
   }

   public class OSInfo : BaseInfo
   {
      public string Name { get; set; }
      public string Edition { get; set; }
   }

   public class AppInfo : BaseInfo
   {
      public string Id { get; set; }
   }

   public abstract class BaseInfo
   {
      public string Version { get; set; }
      public string Localization { get; set; }
   }
}