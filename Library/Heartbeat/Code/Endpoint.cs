﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;

namespace Heartbeat
{
   public class Endpoint
   {
      public Endpoint()
      {
         Attributes = new Dictionary<string, string>();
         Version = 1;
         MinVersion = 1;
      }
      [JsonConverter(typeof(StringEnumConverter))]
      public EndpointType Type { get; set; }
      public double Version { get; set; }
      public double MinVersion { get; set; }
      public string Address { get; set; }
      public Dictionary<string, string> Attributes { get; set; }
   }

   public enum EndpointType
   {
      Bat,
      Sms,
      Identity,
      Mq,
      WebApp
   }
}