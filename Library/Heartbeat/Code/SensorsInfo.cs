﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Veit.Bat.Common;

namespace Heartbeat
{
   public class SensorsInfo
   {
      public int Frequency { get; set; }
      public int Samples { get; set; }
      [JsonConverter(typeof(StringEnumConverter))]
      public DataType Type { get; set; }
      [JsonConverter(typeof(StringEnumConverter))]
      public ValueType[] Value { get; set; }
   }

   public enum ValueType
   {
      Last,
      Avg,
      Min,
      Max
   }
}