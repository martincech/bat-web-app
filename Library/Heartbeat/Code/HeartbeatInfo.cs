﻿using System.Collections.Generic;

namespace Heartbeat
{
   public class HeartbeatInfo
   {
      public UpdateInfo UpdateInfo { get; set; }
      public Endpoint[] Endpoints { get; set; }
      public SensorsInfo[] SensorsInfo { get; set; }
   }
}