using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace UsersDataModel.Infrastructure
{
   public class RoleStore : IRoleStore<Role>
   {
      private readonly UserModelContainer _db;

      public RoleStore(UserModelContainer db)
      {
         _db = db;
      }

      #region RoleStore

      public Task<IdentityResult> CreateAsync(Role role, CancellationToken cancellationToken)
      {
         try
         {
            if (role == null)
            {
               throw new ArgumentException(nameof(role));
            }

            _db.Roles.AddAsync(role, cancellationToken);
            _db.SaveChangesAsync(cancellationToken);

            return Task.FromResult(IdentityResult.Success);
         }
         catch (Exception ex)
         {
            return Task.FromResult(IdentityResult.Failed(new IdentityError
            {
               Code = "ERR201",
               Description = ex.Message
            }));
         }
      }

      public Task<IdentityResult> DeleteAsync(Role role, CancellationToken cancellationToken)
      {
         try
         {
            if (role == null)
            {
               throw new ArgumentException(nameof(role));
            }

            _db.Roles.Remove(role);
            _db.SaveChangesAsync(cancellationToken);

            return Task.FromResult(IdentityResult.Success);
         }
         catch (Exception ex)
         {
            return Task.FromResult(IdentityResult.Failed(new IdentityError
            {
               Code = "ERR204",
               Description = ex.Message
            }));
         }
      }

      public void Dispose()
      {
         if (_db != null)
         {
            _db.Dispose();
         }
      }

      public Task<Role> FindByIdAsync(string roleId, CancellationToken cancellationToken)
      {
         if (String.IsNullOrWhiteSpace(roleId))
         {
            throw new ArgumentException(nameof(roleId));
         }

         return _db.Roles.FirstOrDefaultAsync(r => r.Id.ToString() == roleId, cancellationToken);
      }

      public Task<Role> FindByNameAsync(string normalizedRoleName, CancellationToken cancellationToken)
      {
         if (String.IsNullOrWhiteSpace(normalizedRoleName))
         {
            throw new ArgumentException(nameof(normalizedRoleName));
         }

         return _db.Roles.FirstOrDefaultAsync(r => r.Name == normalizedRoleName, cancellationToken);
      }

      public Task<string> GetNormalizedRoleNameAsync(Role role, CancellationToken cancellationToken)
      {
         if (role == null)
         {
            throw new ArgumentException(nameof(role));
         }

         return Task.FromResult(role.Name);
      }

      public Task<string> GetRoleIdAsync(Role role, CancellationToken cancellationToken)
      {
         if (role == null)
         {
            throw new ArgumentException(nameof(role));
         }

         return Task.FromResult(role.Id.ToString());
      }

      public Task<string> GetRoleNameAsync(Role role, CancellationToken cancellationToken)
      {
         if (role == null)
         {
            throw new ArgumentException(nameof(role));
         }

         return Task.FromResult(role.Name);
      }

      public Task SetNormalizedRoleNameAsync(Role role, string normalizedName, CancellationToken cancellationToken)
      {
         if (role == null)
         {
            throw new ArgumentException(nameof(role));
         }

         return Task.FromResult(role.Name);
      }

      public Task SetRoleNameAsync(Role role, string roleName, CancellationToken cancellationToken)
      {
         if (role == null)
         {
            throw new ArgumentException(nameof(role));
         }

         role.Name = roleName;

         _db.Roles.Update(role);
         _db.SaveChangesAsync(cancellationToken);

         return Task.FromResult<object>(null);
      }

      public Task<IdentityResult> UpdateAsync(Role role, CancellationToken cancellationToken)
      {
         try
         {
            if (role == null)
            {
               throw new ArgumentException(nameof(role));
            }

            _db.Roles.Update(role);
            _db.SaveChangesAsync(cancellationToken);

            return Task.FromResult(IdentityResult.Success);
         }
         catch (Exception ex)
         {
            return Task.FromResult(IdentityResult.Failed(new IdentityError
            {
               Code = "ERR203",
               Description = ex.Message
            }));
         }
      }

      #endregion
   }
}