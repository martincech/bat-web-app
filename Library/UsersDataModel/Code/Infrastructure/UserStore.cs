﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace UsersDataModel.Infrastructure
{

   public class UserStore : IUserStore<User>, IUserRoleStore<User>
   {
      protected readonly UserModelContainer _db;

      public UserStore(UserModelContainer db)
      {
         _db = db;
      }

      #region IUserStore

      public async Task<IdentityResult> CreateAsync(User user, CancellationToken cancellationToken)
      {
         try
         {
            if (user == null)
            {
               throw new ArgumentException(nameof(user));
            }

            _db.Users.Add(user);
            await _db.SaveChangesAsync();

            return IdentityResult.Success;
         }
         catch (Exception ex)
         {
            return IdentityResult.Failed(new IdentityError
            {
               Code = "ERR101",
               Description = ex.Message
            });
         }
      }

      public async Task<IdentityResult> DeleteAsync(User user, CancellationToken cancellationToken)
      {
         try
         {
            if (user == null)
            {
               throw new ArgumentException(nameof(user));
            }

            _db.Users.Remove(user);
            await _db.SaveChangesAsync(cancellationToken);

            return IdentityResult.Success;
         }
         catch (Exception ex)
         {
            return IdentityResult.Failed(new IdentityError
            {
               Code = "ERR104",
               Description = ex.Message
            });
         }
      }

      public void Dispose()
      {
         if (_db != null)
         {
            _db.Dispose();
         }
      }

      public Task<User> FindByIdAsync(string userId, CancellationToken cancellationToken)
      {
         if (String.IsNullOrWhiteSpace(userId))
         {
            throw new ArgumentException(nameof(userId));
         }

         return _db.Users.FirstOrDefaultAsync(
             predicate: u => u.Id.ToString() == userId,
             cancellationToken: cancellationToken);
      }

      public Task<User> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
      {
         if (String.IsNullOrWhiteSpace(normalizedUserName))
         {
            throw new ArgumentException(nameof(normalizedUserName));
         }

         return _db.Users.FirstOrDefaultAsync(
          predicate: u => u.UserName == normalizedUserName,
          cancellationToken: cancellationToken);
      }

      public Task<string> GetNormalizedUserNameAsync(User user, CancellationToken cancellationToken)
      {
         if (user == null)
         {
            throw new ArgumentException(nameof(user));
         }

         return Task.FromResult(user.UserName);
      }

      public Task<string> GetUserIdAsync(User user, CancellationToken cancellationToken)
      {
         if (user == null)
         {
            throw new ArgumentException(nameof(user));
         }

         return Task.FromResult(user.Id.ToString());
      }

      public Task<string> GetUserNameAsync(User user, CancellationToken cancellationToken)
      {
         if (user == null)
         {
            throw new ArgumentException(nameof(user));
         }

         return Task.FromResult(user.UserName);
      }

      public async Task SetNormalizedUserNameAsync(User user, string normalizedName, CancellationToken cancellationToken)
      {
         if (user == null)
         {
            throw new ArgumentException(nameof(user));
         }

         if (String.IsNullOrWhiteSpace(normalizedName))
         {
            throw new ArgumentException(nameof(normalizedName));
         }

         user.UserName = normalizedName;

         _db.Users.Update(user);
         await _db.SaveChangesAsync(cancellationToken);
      }

      public async Task SetUserNameAsync(User user, string userName, CancellationToken cancellationToken)
      {
         if (user == null)
         {
            throw new ArgumentException(nameof(user));
         }

         if (String.IsNullOrWhiteSpace(userName))
         {
            throw new ArgumentException(nameof(userName));
         }

         user.UserName = userName;
         _db.Users.Update(user);
         await _db.SaveChangesAsync(cancellationToken);
      }

      public async Task<IdentityResult> UpdateAsync(User user, CancellationToken cancellationToken)
      {
         if (user == null)
         {
            throw new ArgumentException(nameof(user));
         }
         try
         {
            _db.Users.Update(user);
            await _db.SaveChangesAsync(cancellationToken);

            return IdentityResult.Success;
         }
         catch (Exception ex)
         {
            return IdentityResult.Failed(new IdentityError
            {
               Code = "ERR103",
               Description = ex.Message
            });
         }
      }

      #endregion

      #region 

      public Task<IList<string>> GetRolesAsync(User user, CancellationToken cancellationToken)
      {
         if (user == null)
         {
            throw new ArgumentNullException(nameof(user));
         }

         if (user.Id == Guid.Empty)
         {
            throw new ArgumentNullException(nameof(user.Id));
         }
         return Task.FromResult(user.Roles?.Select(s => s.Name).ToList() ?? new List<string>() as IList<string>);
      }

      public Task<IList<User>> GetUsersInRoleAsync(string roleName, CancellationToken cancellationToken)
      {
         if (string.IsNullOrEmpty(roleName))
         {
            throw new ArgumentNullException(nameof(roleName));
         }

         var role = _db.Roles.FirstOrDefault(f => f.Name == roleName);

         if (role == null)
         {
            throw new NullReferenceException(nameof(role));
         }
         return Task.FromResult(role.UserRoles.Select(s => s.User).ToList() as IList<User>);
      }

      public Task<bool> IsInRoleAsync(User user, string roleName, CancellationToken cancellationToken)
      {
         if (user == null)
         {
            throw new ArgumentNullException(nameof(user));
         }

         if (string.IsNullOrEmpty(roleName))
         {
            throw new ArgumentNullException(nameof(roleName));
         }

         return Task.FromResult(user.Roles.Any(a => a.Name == roleName));
      }

      public async Task AddToRoleAsync(User user, string roleName, CancellationToken cancellationToken)
      {
         if (user == null)
         {
            throw new ArgumentNullException(nameof(user));
         }

         if (string.IsNullOrEmpty(roleName))
         {
            throw new ArgumentNullException(nameof(roleName));
         }

         var role = _db.Roles.FirstOrDefault(f => f.Name == roleName);
         if (role == null)
         {
            throw new NullReferenceException(nameof(role));
         }

         if (!user.UserRoles.Any(a => a.RoleId == role.Id))
         {
            user.UserRoles.Add(new UserRole { RoleId = role.Id, UserId = user.Id });
            _db.Users.Update(user);
            await _db.SaveChangesAsync(cancellationToken);
         }
      }

      public async Task RemoveFromRoleAsync(User user, string roleName, CancellationToken cancellationToken)
      {
         if (user == null)
         {
            throw new ArgumentNullException(nameof(user));
         }

         if (string.IsNullOrEmpty(roleName))
         {
            throw new ArgumentNullException(nameof(roleName));
         }

         var role = _db.Roles.FirstOrDefault(f => f.Name == roleName);
         if (role == null)
         {
            throw new NullReferenceException(nameof(role));
         }

         var userRole = user.UserRoles.FirstOrDefault(a => a.RoleId == role.Id);

         if (userRole != null)
         {
            user.UserRoles.Remove(userRole);
            _db.Users.Update(user);
            await _db.SaveChangesAsync(cancellationToken);
         }   
      }

      #endregion

   }
}
