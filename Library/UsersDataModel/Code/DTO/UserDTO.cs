﻿using System;
using System.Collections.Generic;

namespace UsersDataModel.DTO
{
   public class UserDto
   {
      public UserDto()
      {
         Devices = new List<string>();
      }
      public Guid? Id { get; set; }
      public Guid? CompanyId { get; set; }
      public string Name { get; set; }
      public string Email { get; set; }
      public string Phone { get; set; }
      public string Comment { get; set; }
      public List<string> Devices { get; set; }
      public string Role { get; set; }
      public bool Invited { get; set; }

      public string UserStatus { get; set; }
      public bool Enabled { get; set; }

   }
}