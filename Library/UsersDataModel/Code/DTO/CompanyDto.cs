﻿using System;
using System.Collections.Generic;

namespace UsersDataModel.DTO
{
   public class CompanyDto
   {
      public CompanyDto()
      {
         Users = new List<string>();
      }
      public Guid Id { get; set; }
      public string Name { get; set; }
      public string Database { get; set; }

      public string PersonName { get; set; }
      public string PersonEmail { get; set; }
      public string PersonPhone { get; set; }
      public int? Vat { get; set; }
      public string Address { get; set; }
      public string Postal { get; set; }
      public string City { get; set; }
      public string Country { get; set; }
      public int? EventLength { get; set; }

      public bool DisableDemoData { get; set; }

      public List<string> Users { get; set; }
      public List<string> Terminals { get; set; }
   }
}