﻿namespace UsersDataModel.DTO
{
   public class TerminalDto
   {
      public TerminalDto()
      {
      }

      public int Id { get; set; }
      public string CompanyId { get; set; }
      public string Uid { get; set; }
   }
}