﻿namespace UsersDataModel.DTO
{
   public class UserNameDto
   {
      public string Id { get; set; }     
      public string CompanyId { get; set; }
      public string Role { get; set; }
      public string Name { get; set; }

      public string Sub { get; set; }
      public string Email { get; set; }
   }
}