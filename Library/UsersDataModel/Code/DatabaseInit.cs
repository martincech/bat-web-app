﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using UsersDataModel.DTO;

namespace UsersDataModel
{
   public static class DatabaseInit
   {
      private const string COMPANY = "VEIT";
      public const string COMAPNY_ID = "24192f95-5ed3-4ae4-b7f1-69bc62486801";

      /// <summary>
      ///    Initializes a new instance of the <see cref="T:System.Object" /> class.
      /// </summary>
      static DatabaseInit()
      {
         databaseExist = true;

         USER_DICTIONARY = new Dictionary<string, RoleE>
         {
            {"veit@owner.cz", RoleE.Veit},
            {"owner@owner.cz", RoleE.Owner},
            {"admin@owner.cz", RoleE.Admin},
            {"worker@owner.cz", RoleE.Worker}
         };
      }

      private static readonly Dictionary<string, RoleE> USER_DICTIONARY;
      private const string COMPANY_DATABASE = "VeitBat3Data";
      private const string PASS = "AJwhyhmqfVK65mX1EY2Ja1E95J+0FpnWZQ/kUmHYcETF89CsdQGKMjrclenQO2fbOA=="; //Admin123.
      private const string SEC_STAMP = "38f019d0-1f18-444e-9b35-fff6cac8b71c";

      public static string Initialize(this UserModelContainer db)
      {
         db.Database.Migrate();

         foreach (var role in Enum.GetNames(typeof(RoleE)))
         {
            if (!db.Roles.Any(a => a.Name == role)) db.Roles.Add(new Role { Name = role });
         }
         db.SaveChanges();

         var company = db.Companies.FirstOrDefault(f => f.Name == COMPANY) ??
                       db.Companies.Add(new Company
                       {
                          Name = COMPANY,
                          Id = Guid.Parse(COMAPNY_ID),
                          Database = COMPANY_DATABASE
                       }).Entity;         

         db.SaveChanges();
         return COMPANY_DATABASE;
      }      

      private static bool databaseExist;     

      public static bool DatabaseExist()
      {
         return databaseExist;
      }
   }
}
