namespace UsersDataModel
{
    using System.ComponentModel.DataAnnotations;

    public partial class Newsletter
    {
        public int Id { get; set; }

        [Required]
        public string Email { get; set; }
    }
}
