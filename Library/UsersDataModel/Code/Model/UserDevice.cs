﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace UsersDataModel
{
   public class UserDevice
   {
      [Column("Users_Id")]
      public Guid UserId { get; set; }
      public virtual User User { get; set; }

      [Column("Devices_Id")]
      public int DeviceId { get; set; }
      public virtual Device Device { get; set; }
   }
}
