using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace UsersDataModel
{

   public partial class UserModelContainer : DbContext, IDesignTimeDbContextFactory<UserModelContainer>
   {
      public UserModelContainer()
      {
      }

      public UserModelContainer(DbContextOptions<UserModelContainer> options) : base(options)
      {
      }

      public override int SaveChanges()
      {
         return base.SaveChanges();
      }

      public virtual DbSet<Company> Companies { get; set; }
      public virtual DbSet<Device> Devices { get; set; }
      public virtual DbSet<Newsletter> Newsletters { get; set; }
      public virtual DbSet<Role> Roles { get; set; }
      public virtual DbSet<Terminal> Terminals { get; set; }
      public virtual DbSet<User> Users { get; set; }

      protected override void OnModelCreating(ModelBuilder modelBuilder)
      {

         modelBuilder.Entity<UserDevice>()
            .HasKey(bc => new { bc.UserId, bc.DeviceId });
         modelBuilder.Entity<UserRole>()
          .HasKey(bc => new { bc.UserId, bc.RoleId });         
      }

      public UserModelContainer CreateDbContext(string[] args)
      {
         var builder = new DbContextOptionsBuilder<UserModelContainer>();
         builder.UseSqlServer("data source=localhost;initial catalog=Bat3AppUsers;integrated security=False;user id=sa;password=Passw0rd!;multipleactiveresultsets=True;application name=Web");
         return new UserModelContainer(builder.Options);
      }
   }
}
