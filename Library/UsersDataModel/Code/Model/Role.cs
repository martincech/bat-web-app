using Microsoft.AspNetCore.Identity;

namespace UsersDataModel
{
   using System;
   using System.Collections.Generic;
   using System.ComponentModel.DataAnnotations;

   public partial class Role
   {
      public Role()
      {
         Id = Guid.NewGuid();        
      }

      public Guid Id { get; set; }

      [Required]
      [StringLength(256)]
      public string Name { get; set; }

      public virtual ICollection<UserRole> UserRoles { get; } = new List<UserRole>();      
   }
}
