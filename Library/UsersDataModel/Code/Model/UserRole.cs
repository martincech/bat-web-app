﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace UsersDataModel
{
    public class UserRole
    {
      [Column("Users_Id")]
      public Guid UserId { get; set; }
      public virtual User User { get; set; }

      [Column("Roles_Id")]
      public Guid RoleId { get; set; }
      public virtual Role Role { get; set; }
   }
}
