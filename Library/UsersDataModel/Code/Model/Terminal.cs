using System.ComponentModel.DataAnnotations.Schema;
namespace UsersDataModel
{
   using System;
   using System.ComponentModel.DataAnnotations;

   public partial class Terminal
   {
      public Terminal()
      {
      }

      public int Id { get; set; }

      [Required]
      [MaxLength(255)]
      public string Uid { get; set; }

      public Guid? CompanyId { get; set; }

      public virtual Company Company { get; set; }
   }
}
