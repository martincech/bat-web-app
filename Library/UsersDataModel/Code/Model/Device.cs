namespace UsersDataModel
{
   using System.Collections.Generic;
   using System.ComponentModel.DataAnnotations;
   using System.ComponentModel.DataAnnotations.Schema;
   using System.Linq;

   public partial class Device
   {
      public int Id { get; set; }

      [Required]
      public string DeviceId { get; set; }


      public virtual ICollection<UserDevice> UserDevices { get; } = new List<UserDevice>();

      [NotMapped]
      public IEnumerable<User> Users => UserDevices.Select(s => s.User);
   }
}
