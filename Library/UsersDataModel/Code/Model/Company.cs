namespace UsersDataModel
{
   using System;
   using System.Collections.Generic;
   using System.ComponentModel.DataAnnotations;

   public partial class Company
   {    
      public Company()
      {
         Id = Guid.NewGuid();
         Users = new HashSet<User>();
         Terminals = new HashSet<Terminal>();
      }

      public Guid Id { get; set; }

      [Required]
      public string Name { get; set; }

      public string Database { get; set; }

      public string PersonName { get; set; }

      public string PersonEmail { get; set; }

      public string PersonPhone { get; set; }

      public int? Vat { get; set; }

      public string Address { get; set; }

      public string Postal { get; set; }

      public string City { get; set; }


      public string Country { get; set; }

      public int? EventLength { get; set; }

      public bool DisableDemoData { get; set; }
     
      public virtual ICollection<User> Users { get; set; }
     
      public virtual ICollection<Terminal> Terminals { get; set; }
   }
}
