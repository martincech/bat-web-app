namespace UsersDataModel
{
   using System;
   using System.Collections.Generic;
   using System.ComponentModel.DataAnnotations;
   using System.ComponentModel.DataAnnotations.Schema;
   using System.Linq;

   public partial class User
   {
      public User()
      {
         Id = Guid.NewGuid();
      }

      public Guid Id { get; set; }

      [Required]
      [StringLength(256)]
      public string UserName { get; set; }

      public DateTime? LastLoginDate { get; set; }

      public Guid? CompanyId { get; set; }

      public string Settings { get; set; }

      public virtual Company Company { get; set; }

      public virtual ICollection<UserDevice> UserDevices { get; set; } = new List<UserDevice>();

      public virtual ICollection<UserRole> UserRoles { get; set; } = new List<UserRole>();

      [NotMapped]
      public IEnumerable<Device> Devices => UserDevices.Select(s => s.Device);

      [NotMapped]
      public IEnumerable<Role> Roles => UserRoles.Select(s => s.Role);
   }
}
