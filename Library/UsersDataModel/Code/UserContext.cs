﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using NLog;
using UsersDataModel.DTO;
using UsersDataModel.Mapping;

namespace UsersDataModel
{
   public class UserContext : IDisposable
   {
      public Guid UserId { get; private set; }

      private readonly UserModelContainer container;
      public IPrincipal Principal { get; private set; }

      private static readonly Logger LOG = LogManager.GetCurrentClassLogger();

      public UserContext(UserModelContainer container, IPrincipal principal)
      {
         this.container = container;
         Principal = principal;
         UserId = GetUserId();
      }

      private Guid GetUserId()
      {
         if (Principal == null) return Guid.Empty;
         var guid = (Principal.Identity as ClaimsIdentity)?.FindFirst(i => i.Type == ClaimTypes.NameIdentifier)?.Value;
         return string.IsNullOrEmpty(guid) ? Guid.Empty : Guid.Parse(guid);
      }

      public UserModelContainer Container => container;

      public bool IsInRole(RoleE role) => Principal.IsInRole(role.ToString());

      public User User
      {
         get { return container.Users.FirstOrDefault(u => u.Id == UserId); }
      }
      #region Master Data access

      public IQueryable<Company> Companies
      {
         get
         {
            if (IsInRole(RoleE.Veit))
            {
               return container.Companies.AsQueryable();
            }
            return container.Companies.Where(c => c.Users.Any(u => u.Id == UserId));
         }
      }

      public IQueryable<User> Users
      {
         get
         {
            if (IsInRole(RoleE.Veit))
            {
               return container.Users.AsQueryable();
            }

            var user = container.Users.Find(UserId);
            if (user.Company == null) return container.Users.Where(u => u.Id == user.Id);

            if (IsInRole(RoleE.Owner))
            {
               return user.Company.Users.Where(
                  w => w.Roles.Select(s => s.Name).Any(
                     a => a.Contains(RoleE.Admin.ToString()) || a.Contains(RoleE.Worker.ToString()))
                     ).AsQueryable();
            }

            if (IsInRole(RoleE.Admin))
            {
               return user.Company.Users.Where(
                  w => w.Roles.Select(s => s.Name).Any(
                     a => a.Contains(RoleE.Worker.ToString()))
                     ).AsQueryable();
            }

            return container.Users.Where(u => u.Id == user.Id);
         }
      }

      public void SaveChanges()
      {
         container.SaveChanges();
      }

      public async Task SaveChangesAsync()
      {
         await container.SaveChangesAsync();
      }

      #endregion

      #region COMPANY

      public IEnumerable<CompanyDto> GetCompanies()
      {
         return container.Companies.Map();
      }

      public CompanyDto CreateCompany(CompanyDto newCompany)
      {

         var company = new Company
         {
            Id = Guid.NewGuid(),
            Name = newCompany.Name,
            Database = newCompany.Database
         };
         container.Companies.Add(company);
         container.SaveChanges();
         var companyDto = company.Map();
         LOG.Debug("Created company : {0}", Json(companyDto));
         return companyDto;
      }

      public CompanyDto UpdateCompany(CompanyDto company)
      {
         var cp = container.Companies.FirstOrDefault(f => f.Id == company.Id);
         if (cp == null) return null;
         cp.Database = company.Database;
         cp.Name = company.Name;
         cp.PersonName = company.PersonName;
         cp.PersonEmail = company.PersonEmail;
         cp.PersonPhone = company.PersonPhone;
         cp.Vat = company.Vat;
         cp.Address = company.Address;
         cp.Postal = company.Postal;
         cp.City = company.City;
         cp.Country = company.Country;
         if (company.Terminals == null) company.Terminals = new List<string>();
         foreach (var terminal in container.Terminals.ToList())
         {
            if (company.Terminals.Contains(terminal.Uid))
            {
               terminal.CompanyId = company.Id;
            }
            else if (terminal.CompanyId == company.Id)
            {
               terminal.CompanyId = null;
            }
         }

         foreach (var user in container.Users.ToList())
         {
            if (company.Users.Contains(user.Id.ToString()))
            {
               user.CompanyId = company.Id;
            }
            else if (user.CompanyId == company.Id)
            {
               user.CompanyId = null;
            }
         }
         SaveChanges();
         return cp.Map();
      }

      public async Task<bool> DisableDemoData(bool disableDemoData)
      {
         var cp = User.Company;
         cp.DisableDemoData = disableDemoData;
         await SaveChangesAsync();
         return cp.DisableDemoData;
      }

      public async Task<bool> DisableDemoData(Guid companyId, bool disable)
      {
         var comapny = container.Companies.FirstOrDefault(f => f.Id == companyId);
         if (comapny == null) return false;
         comapny.DisableDemoData = disable;
         await SaveChangesAsync();
         return true;
      }

      #endregion

      #region USER

      public IEnumerable<User> GetUsers(bool excludeLoggedUser = false)
      {
         if (excludeLoggedUser) return Users.Where(w => w.Id != UserId);
         return Users;
      }

      public IEnumerable<UserNameDto> GetUsersName(Dictionary<string, string> cognitoUsers)
      {
         var users = Users.ToList().Select(s => new UserNameDto
         {
            Id = s.Id.ToString(),
            Role = s.Roles.FirstOrDefault()?.Name,
            Name = s.UserName,
            CompanyId = s.CompanyId?.ToString(),
         }).ToList();

         foreach (var cUser in cognitoUsers)
         {
            var user = users.FirstOrDefault(f => f.Id == cUser.Key);
            if (user == null)
            {
               user = new UserNameDto();
               users.Add(user);
            }
            user.Sub = cUser.Key;
            user.Email = cUser.Value;
         }
         return users;
      }

      public UserDto CreateUser(UserDto user)
      {
         if (user == null) return null;
         var newUser = new User
         {
            UserName = user.Email,

            CompanyId = container.Users.First(f => f.Id == UserId).CompanyId
         };
         newUser.UserRoles.Add(new UserRole { RoleId = container.Roles.First(f => f.Name == RoleE.Worker.ToString()).Id, UserId = user.Id ?? Guid.Empty });

         container.Users.Add(newUser);
         SaveChanges();
         var userDto = newUser.Map();
         LOG.Debug("Created user : {0}", Json(userDto));
         return userDto;
      }

      public bool AddDeviceToWorker(string deviceUid)
      {
         var user = container.Users.FirstOrDefault(f => f.Id == UserId);
         if (user == null || string.IsNullOrEmpty(deviceUid)) return false;
         if (user.Roles.All(a => a.Name != RoleE.Worker.ToString())) return false;
         if (user.Devices.Any(a => a.DeviceId == deviceUid)) return true;
         user.UserDevices.Add(new UserDevice { UserId = user.Id, DeviceId = DeviceCreateIfNotExists(deviceUid).Id });
         SaveChanges();
         LOG.Info("Added access to device : {0} for user : {1}.", deviceUid, user.UserName);
         return true;
      }
      public bool DeleteDeviceByUid(string uid)
      {
         var device = container.Devices.FirstOrDefault(d => d.DeviceId == uid);
         if (device != null)
         {
            device.UserDevices.Clear();
            SaveChanges();
            container.Devices.Remove(device);
            SaveChanges();
         }
         return true;
      }
      public List<Guid> GetVisibleUsers()
      {
         return GetVisibleUsers(UserId);
      }

      public string GetUserCompanyId(string userId)
      {
         var user = Users.FirstOrDefault(f => f.Id.ToString() == userId);
         if (user == null || user.CompanyId == null) return null;
         return user.CompanyId.ToString();
      }

      public CompanyDto GetCurrentUserCompany()
      {
         return User.Company.Map();
      }

      private List<Guid> GetVisibleUsers(Guid userId)
      {
         var result = new List<Guid>();

         var user = container.Users.Find(userId);

         if (user == null)
         {
            return result;
         }

         result.Add(user.Id);
         result.AddRange(container.Users.Where(u => u.CompanyId == user.CompanyId).Select(s => s.Id));
         return result;
      }

      public bool SetEventLength(int? eventLegnth)
      {
         var company = User.Company;
         if (company == null) return false;
         company.EventLength = eventLegnth;
         SaveChanges();
         return true;
      }

      public void UpdateUserSettings(Dictionary<string, string> userSettings)
      {
         if (userSettings == null) return;
         var user = User;
         user.Settings = JsonConvert.SerializeObject(userSettings);
         SaveChanges();
      }

      private Device DeviceCreateIfNotExists(string deviceId)
      {
         var device = container.Devices.FirstOrDefault(f => f.DeviceId == deviceId);
         return device ?? container.Devices.Add(new Device() { DeviceId = deviceId }).Entity;
      }

      public List<string> GetCurrentUserDevices()
      {
         if (!IsInRole(RoleE.Worker)) return null;
         return container.Users.FirstOrDefault(f => f.Id == UserId)?.Devices?.Select(s => s.DeviceId).ToList();
      }

      #endregion

      #region NEWSLETTER

      public void RegisterToNewsletter(string email)
      {
         if (!container.Newsletters.Select(s => s.Email).Contains(email))
            container.Newsletters.Add(new Newsletter() { Email = email });
         SaveChanges();
      }

      #endregion

      #region Implementation of IDisposable

      /// <summary>
      /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
      /// </summary>
      public void Dispose()
      {
         container.Dispose();
      }

      #endregion

      #region Terminal

      public IEnumerable<TerminalDto> GetTerminals()
      {
         return container.Terminals.Map();
      }

      public bool IsTerminalAvailable(string terminalUid)
      {
         var terminal = container.Terminals.FirstOrDefault(a => a.Uid == terminalUid);
         if (terminal == null || terminal.CompanyId == null) return true;
         return terminal.CompanyId == User.CompanyId;
      }

      public async Task<TerminalDto> GetOrCreateTerminal(string terminalUid, Guid? companyId)
      {
         if (string.IsNullOrEmpty(terminalUid)) return null;
         var terminal = container.Terminals.FirstOrDefault(t => t.Uid == terminalUid);
         if (terminal == null) return await CreateTerminal(terminalUid, companyId);
         if (terminal.CompanyId == null) return await UpdateTerminal(terminalUid, companyId);
         return terminal.CompanyId != User.CompanyId ? null : terminal.Map();
      }

      public async Task<TerminalDto> CreateOrUpdateTerminal(string uid, Guid? companyId)
      {
         if (!container.Terminals.Any(f => f.Uid == uid)) return await CreateTerminal(uid, companyId);
         return await UpdateTerminal(uid, companyId);
      }

      private async Task<TerminalDto> CreateTerminal(string uid, Guid? companyId)
      {
         var terminal = container.Terminals.FirstOrDefault(f => f.Uid == uid);
         if (terminal != null) return terminal.CompanyId == companyId ? terminal.Map() : null;
         terminal = new Terminal { Uid = uid, CompanyId = companyId };
         container.Terminals.Add(terminal);
         await SaveChangesAsync();
         var terminalDto = terminal.Map();
         LOG.Debug("Created terminal in users db: {0}", Json(terminalDto));
         return terminalDto;
      }

      public async Task<TerminalDto> FreeTerminal(string uid)
      {
         return await UpdateTerminal(uid, null);
      }
      public async Task<TerminalDto> UpdateTerminal(string uid, Guid? companyId)
      {
         var terminal = container.Terminals.FirstOrDefault(f => f.Uid == uid);
         if (terminal == null) return null;
         terminal.CompanyId = companyId;
         await SaveChangesAsync();
         return terminal.Map();
      }

      #endregion

      public async Task<bool> SetUserCompany(Guid userId, Guid companyId)
      {
         var user = container.Users.FirstOrDefault(f => f.Id == userId);
         var company = container.Companies.FirstOrDefault(f => f.Id == companyId);
         if (user == null || company == null) return false;
         user.CompanyId = companyId;
         await SaveChangesAsync();
         return true;
      }

      public async Task<bool> SetUserRole(Guid userId, RoleE roleType)
      {
         var user = container.Users.FirstOrDefault(f => f.Id == userId);
         var role = container.Roles.FirstOrDefault(f => f.Name == roleType.ToString());
         if (user == null || role == null) return false;
         user.UserRoles.Clear();
         user.UserRoles.Add(new UserRole { RoleId = role.Id, UserId = user.Id });
         await SaveChangesAsync();
         return true;
      }

      private string Json(object o)
      {
         if (o == null) return "";
         try
         {
            return JsonConvert.SerializeObject(o);
         }
         catch (Exception)
         {
            LOG.Warn("Failed to serialize object to log.");
            return "";
         }
      }
   }
}
