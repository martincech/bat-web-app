﻿using System.Collections.Generic;
using System.Linq;
using UsersDataModel.DTO;

namespace UsersDataModel.Mapping
{
   public static class MapCompany
   {
      public static CompanyDto Map(this Company item)
      {
         return new CompanyDto
         {
            Id = item.Id,
            Name = item.Name,
            Database = item.Database,
            PersonName = item.PersonName,
            PersonEmail = item.PersonEmail,
            PersonPhone = item.PersonPhone,
            Vat = item.Vat,
            Address = item.Address,
            Postal = item.Postal,
            City = item.City,
            Country = item.Country,
            DisableDemoData = item.DisableDemoData,
            EventLength = item.EventLength,
            Users = item.Users != null ? item.Users.Select(s=>s.Id.ToString()).ToList() : new List<string>(),
            Terminals = item.Terminals != null ? item.Terminals.Select(s => s.Uid).ToList() : new List<string>(),
         };
      }

      public static IEnumerable<CompanyDto> Map(this IEnumerable<Company> items)
      {
         return items == null ? new List<CompanyDto>() : items.ToList().Select(Map);
      }

      public static Company Map(this CompanyDto item)
      {
         return new Company
         {
            Id = item.Id,
            Name = item.Name,
            Database = item.Database,
            PersonName = item.PersonName,
            PersonEmail = item.PersonEmail,
            PersonPhone = item.PersonPhone,
            Vat = item.Vat,
            Address = item.Address,
            Postal = item.Postal,
            City = item.City,
            Country = item.Country,
            DisableDemoData = item.DisableDemoData,
            EventLength = item.EventLength
         };
      }

      public static IEnumerable<Company> Map(this IEnumerable<CompanyDto> items)
      {
         return items == null ? new List<Company>() : items.ToList().Select(Map);
      }
   }
}