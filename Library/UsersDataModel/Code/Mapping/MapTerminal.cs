﻿using System.Collections.Generic;
using System.Linq;
using UsersDataModel.DTO;

namespace UsersDataModel.Mapping
{
   public static class MapTerminal
   {
      public static TerminalDto Map(this Terminal item)
      {
         return new TerminalDto
         {
            Id = item.Id,
            Uid = item.Uid,
            CompanyId = item.CompanyId.HasValue ? item.CompanyId.ToString() : null           
         };
      }

      public static IEnumerable<TerminalDto> Map(this IEnumerable<Terminal> items)
      {
         return items == null ? new List<TerminalDto>() : items.ToList().Select(Map);
      }

      public static Terminal Map(this TerminalDto item)
      {
         return new Terminal { Id = item.Id, Uid = item.Uid };
      }

      public static IEnumerable<Terminal> Map(this IEnumerable<TerminalDto> items)
      {
         return items == null ? new List<Terminal>() : items.ToList().Select(Map);
      }
   }
}