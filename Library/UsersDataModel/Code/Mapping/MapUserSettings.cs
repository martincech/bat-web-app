﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace UsersDataModel.Mapping
{
   public static class MapUserSettings
   {
      public static List<string> CookieKeys = new List<string>
      {
         "dateFormat",
         "decimals",
         "lang",     
         "numberFormat",
         "visited",
         "weightUnit",
         "temperatureUnit"
      };

      public static Dictionary<string, string> MapSettings(this User user)
      {
         if (user == null || string.IsNullOrEmpty(user.Settings)) return new Dictionary<string, string>();
         return JsonConvert.DeserializeObject<Dictionary<string, string>>(user.Settings);
      }

      public static Dictionary<string, string> MapSetttings(this IRequestCookieCollection cookieCollection)
      {
         return cookieCollection.Keys.Distinct()
            .Where(cookie => CookieKeys.Contains(cookie) && cookieCollection[cookie] != null)
            // ReSharper disable once PossibleNullReferenceException
            .ToDictionary(cookie => cookie, cookie => cookieCollection[cookie]);
      }
   }
}
