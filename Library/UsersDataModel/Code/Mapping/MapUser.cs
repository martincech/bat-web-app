﻿using System;
using System.Collections.Generic;
using System.Linq;
using UsersDataModel.DTO;

namespace UsersDataModel.Mapping
{
   public static class MapUser
   {
      public static User Map(this UserDto item)
      {
         return new User
         {
            Id = item.Id ?? Guid.Empty,
            CompanyId = item.CompanyId,
            UserName = item.Email       
         };
      }

      public static IEnumerable<User> Map(this IEnumerable<UserDto> items)
      {
         return items == null ? new List<User>() : items.ToList().Select(Map);
      }

      public static UserDto Map(this User item)
      {
         var user = new UserDto
         {
            Id = item.Id,
            CompanyId = item.CompanyId,       
            Devices = item.Devices == null ? new List<string>() : item.Devices.Select(s => s.DeviceId).ToList(),
            Role = item.Roles.Count() != 0 ? item.Roles.First().Name : RoleE.Worker.ToString()
         };
         return user;
      }

      public static IEnumerable<UserDto> Map(this IEnumerable<User> items)
      {
         return items == null ? new List<UserDto>() : items.ToList().Select(Map);
      }
   }
}